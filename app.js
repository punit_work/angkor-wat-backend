require('dotenv').config()

const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const path = require('path');
const userRoutes = require('./routes/end-users/user.js');
const systemUserRoutes = require('./routes/system-users/system-user.js');
const serviceRoutes = require('./routes/services/services.js');
const shopRoutes = require('./routes/shop/shop.js');
const productsRoutes = require('./routes/products/products.js');
const cartRoutes = require('./routes/cart/cart.js');
const wishlistRoutes = require('./routes/wishlist/wishlist.js');
const hotelRoutes = require('./routes/hotel/hotel.js');
const promocodeRoutes = require('./routes/promocode/promocode.js');
const billingRoutes = require('./routes/billing-details/billing-details.js');
const shippingRoutes = require('./routes/shipping/shipping.js');
const orderSummaryRoutes = require('./routes/order-summary/order-summary.js');
const categoryRoutes = require('./routes/category/category.js');
const provinceRoutes = require('./routes/provinces/province.js');
const templeRoutes = require('./routes/temple/temple.js');
const tagRoutes = require('./routes/tag/tag.js');
const locationRoutes = require('./routes/location/location.js');
const articleRoutes = require('./routes/article/article.js');
const paymentRoutes = require('./routes/payment/payment.js');
const mediaLibraryRoutes = require('./routes/media-library/media-library.js');
const bugReportRoutes = require('./routes/bug-report/bug-report.js');
const feedbackRoutes = require('./routes/feedback/feedback.js');
const logger = require('morgan');
const cors = require('cors');
const fs = require("fs");
const Response = require('./global/response.js')
const ResponseCode = require('./global/code.js')
const ResponseMessage = require('./global/message.js')
const firebase = require('firebase/app')
const firebaseAuth = require('firebase/auth');
const swaggerDoc = require('./global/swagger.json');
const swaggerUI = require('swagger-ui-express');
const http = require("http");
const https = require("https");

const app = express();
const uri = process.env.MONGO_URI;

// Config options to pass in mongoose.connect() method
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    user: process.env.DB_USER,
    pass: process.env.DB_PASSWORD
};


// Firebase config options
const firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID
};

// Initialize firebase   
firebase.initializeApp(firebaseConfig);

// Connection to MongoDB
function connectDatabase() {

    // MongoDB connection
    mongoose.connect(uri, options)
        .then(() => console.log("Database connected:", uri))
        .catch(error => console.log(error));
}

const DisableTryItOutPlugin = function() {
    return {
      statePlugins: {
        spec: {
          wrapSelectors: {
            allowTryItOutFor: () => () => true
          }
        }
      }
    }
  }

const swaggerOptions = {
swaggerOptions: {
    plugins: [
            DisableTryItOutPlugin
    ]
    }
};

app.use(cors());
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
    cookie:{
      sameSite:'none',
      secure:true
    }
}));



app.use('/v1/swagger/documentation', swaggerUI.serve, swaggerUI.setup(swaggerDoc,swaggerOptions));
app.use('/v1/uploads/documents', express.static(path.join(__dirname, 'uploads', 'documents')));
app.use('/v1/uploads/images/avatar', express.static(path.join(__dirname, 'uploads', 'images', 'avatar')));
app.use('/v1/uploads/shop-image', express.static(path.join(__dirname, 'uploads', 'shop-image')));
app.use('/v1/uploads/service-image', express.static(path.join(__dirname, 'uploads', 'service-image')));
app.use('/v1/uploads/article-images', express.static(path.join(__dirname, 'uploads', 'article-images')));
app.use('/v1/uploads/product-images', express.static(path.join(__dirname, 'uploads', 'product-images')));
app.use('/v1/uploads/category-images', express.static(path.join(__dirname, 'uploads', 'category-images')));
app.use('/v1/uploads/media-library', express.static(path.join(__dirname, 'uploads','media-library')));
app.use('/v1/user', userRoutes);
app.use('/v1/system-user', systemUserRoutes);
app.use('/v1/services', serviceRoutes);
app.use('/v1/shop', shopRoutes);
app.use('/v1/products', productsRoutes);
app.use('/v1/cart', cartRoutes);
app.use('/v1/wishlist', wishlistRoutes);
app.use('/v1/category', categoryRoutes);
app.use('/v1/hotel', hotelRoutes);
app.use('/v1/billing-details', billingRoutes);
app.use('/v1/promocode', promocodeRoutes);
app.use('/v1/shipping', shippingRoutes);
app.use('/v1/order-summary', orderSummaryRoutes);
app.use('/v1/province', provinceRoutes);
app.use('/v1/temple', templeRoutes);
app.use('/v1/tag', tagRoutes);
app.use('/v1/locations', locationRoutes);
app.use('/v1/article', articleRoutes);
app.use('/v1/payment', paymentRoutes);
app.use('/v1/media-library',mediaLibraryRoutes);
app.use('/v1/bug-report',bugReportRoutes);
app.use('/v1/feedback',feedbackRoutes);


// Middleware which will execute if it doesn't matches any of the specified routes
app.use((req, res, next) => {

    return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_ROUTE_NOT_FOUND);
})

// Middleware which will execute if error is thrown from new Error() object
app.use((error, req, res) => {
    // Remove file if exist
    if (req.file) {
        fs.unlink(req.file.path, (err) => {
            err && console.log(err);
        })
    }
    return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_ROUTE_NOT_FOUND);
})


try {

    const credentials = {
        key: fs.readFileSync(process.env.SSL_PRIVATE_KEY, 'utf8'),
        cert: fs.readFileSync(process.env.SSL_CERTIFICATE, 'utf8'),
        ca: fs.readFileSync(process.env.SSL_CHAIN, 'utf8')
    };

    runSecureServer(credentials);

} catch (e) {
    runUnsecureServer();
}

// Function for running server without SSL (Not secure)
function runUnsecureServer() {

    const httpServer = http.createServer(app);

    httpServer.listen(5006, () => {
        console.log('HTTP Server running on port 5006');
        connectDatabase();
    });

    httpServer.timeout = 1800000
}

// Function for running server with SSL (Secure)
function runSecureServer(credentials) {

    const httpsServer = https.createServer(credentials, app);

    httpsServer.listen(5006, () => {
        console.log('HTTPS Server running on port ' + 5006);
        connectDatabase();
    });

    httpsServer.timeout = 1800000
}