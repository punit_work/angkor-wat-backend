const fs = require('fs');

// Function to check whether file is attached req.file object
// If yes, remove file from server
function doFileExist(file){
    if (file) {

        // Removing file if exist
        fs.unlink(file.path, (err) => {
            if (err) console.log(err);
            else console.log('File Deleted');
        })
    }
}

// check if multiple files exists in the req
// if yes, remove them from the server
async function removeMultipleFiles(files) {
    
    if(files.length) {

        for(let file of files) {

            //Removing all the files if they exists
            fs.unlink(file.path, (err) => {
                if(err) console.log('Error while unlinking files : ' + err )
                else console.log('File Deleted.')
            })

        }
    }
}

function removeFileIfExists(path) {
    if(path) {
        // Removing file if exist
        fs.unlink(path, (err) => {
            if (err) console.log(err);
            else console.log('Previous File Deleted');
        })
    }
}
module.exports = {doFileExist , removeFileIfExists , removeMultipleFiles};
