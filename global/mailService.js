require('dotenv').config()
const nodemailer = require("nodemailer");


// async..await is not allowed in global scope, must use a wrapper
async function sendFeedbackMail(data) {

  let {subject,description,user_email,fname,lname} = data;

  if(!data){

    subject = 'Demo subject';
    user_email = 'nikunj.p@xcitech.in';
    description = 'This is demo description';

  }

  try{
        // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: process.env.MAIL_USER, 
      pass: process.env.MAIL_USER_PASSWORD
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: user_email, // sender address
    to: process.env.MAIL_USER, // list of receivers
    subject: subject, // Subject line
    html: `<p>${description}</p>`, // html body
  });

  console.log(info);

  if(info.messageId){
      return true;
  }
  else{
      return false;
  }

  }
  catch(err){
      console.log(err);
      return false;
  }
   


}

module.exports = {sendFeedbackMail};
