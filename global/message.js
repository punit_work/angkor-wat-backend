module.exports = {
  // General error messages :
  ERROR_UNKNOWN: "Unknown error has occurred. Please try again later",
  ERROR_UNAUTHORIZED: "Unauthorized access",
  ERROR_FORBIDDEN: "Email not yet verified",
  ERROR_BANNED: "This user is banned from the system. Please contact our support administrator",
  ERROR_DATABASE: "Unable to write to database",
  ERROR_NOT_FOUND: "No result found",
  ERROR_RESTRICTED: "Access restricted",
  ERROR_PASSWORD_LENGTH: "Password should be atleast 8 characters long.",
  ERROR_FIELD_EMPTY: "Field cannot be empty.",
  ERROR_ROUTE_NOT_FOUND: "Specified route does not exist.",
  ERROR_LOGIN_TYPE: "Please provide a valid login type",

  // Specific error messages for:

  // Validations
  INVALID_EMAIL: "Please enter a valid Email Id.",
  INVALID_CONTACT_LENGTH: "Contact number cannot be less than 10 digits.",
  INVALID_MOBILE_NUMBER: "Please enter a valid mobile number.",
  INVALID_GENDER: "Gender should be male, female or others",
  INVALID_INPUT_LENGTH: "Length must be of 8 characters.",
  INVALID_CREDENTIALS: "Invalid credentials.",
  INVALID_TYPE_PARAMETER: "Paramter Gender should be either male or female.",
  INVALID_CATEGORY: "Category or relavant sub category is invalid or not found",
  INVALID_PROMOCODE_DETAILS: "Please provide valid promocode details",
  INVALID_TAX_DETAILS: "Please provide valid tax details",
  INVALID_USER_TYPE: "This user type is not permitted",
  INVALID_PARAMATER: "Invalid Parameter",
  INVALID_MIN_PRICE: "Minimum price should contain numbers only.",
  INVALID_MAX_PRICE: "Maximum price should contain numbers only.",
  INVALID_OWNER: "You are not valid owner for this action",
  INVALID_FILE_TYPE: "Only .jpg, .png and .jpef files are valid.",
  INVALID_RATINGS: "Invalid ratings",
  INVALID_FILTER: "Please select valid filter",

  // User login and register
  NO_USER_FOUND: "No user found",
  USER_ALREADY_EXIST: "User already exists.",
  USER_EXIST: "User exists with this number.",
  USER_LOGGED_IN_SUCCESSFULLY: "User logged in successfully.",
  USER_REGISTERED_SUCCESSFULLY: "User registered successfully.",
  USER_REGISTRATION_FAILED: "User registration failed.",
  USER_UPDATION_FAILED: "User information could not be updated.",
  USER_UPDATED_SUCCESSFULLY: "User data has been updated successfully.",
  UPDATED_USER_NOT_FOUND: "Updated user not found.",
  SYSTEM_PROVIDER_ADDED_SUCCESSFULLY: "System provider added successfully.",
  SYSTEM_PROVIDER_UPDATED_SUCCESSFULLY: "System provider updated successfully.",
  SYSTEM_PROVIDER_FETCHED_SUCCESSFULLY: "System provider fetched successfully.",

  // User logout and session
  USER_LOGGED_OUT_SUCCESSFULLY: "User logged out successfully",
  NO_SESSION_FOUND: "No session found",
  SESSION_NOT_CREATED: "Session could not be created",
  TOKEN_NOT_GENERATED: "Token coudld not be generated",

  // Documents
  DOCUMENT_MISSING: "ID Proof is missing",
  IMAGE_MISSING: "Image file missing",

  // Stores
  RETRIVED_SHOP_DATA_SUCCESSFULLY: "Retrived shop data successfully",
  NO_SHOPS_AVAILABLE: "No shops were found",
  SHOP_CREATED_SUCCESSFULLY: "Shop has been created successfully",
  SHOPS_MARKED_AS_FEATURED: "Shops marked as featured successfully",
  SHOPS_UNMARKED_FROM_FEATURED: "Shops removed from featured successfully",
  SHOP_UPDATED_SUCCESSFULLY: "Shop details has been updated successfully",

  // Products
  PRODUCT_CREATED_SUCCESSFULLY: "Product has been added successfully",
  PRODUCT_UPDATED_SUCCESSFULLY: "Product updated successfully",
  PRODUCT_DELETED_SUCCESSFULLY: "Product deleted successfully",
  PRODUCTS_FETCHED_SUCCESSFULLY: "Products fetched successfully",
  NO_PRODUCT_FOUND: "Product not found",
  NO_PRODUCT_AVAILABLE: "No product were found",
  RETRIVED_PRODUCT_DATA_SUCCESSFULLY: "Retrived products data successfully",
  PRODUCT_UPDATED_SUCCESSFULLY: "Product updated successfully",

  //Cart
  CART_PRODUCT_ADDED_SUCCESSFULLY: "Product has been added successfully in cart",
  CART_PRODUCT_REMOVED_SUCCESSFULLY: "Product has been removed successfully from cart",
  CART_PRODUCT_UPDATED_SUCCESSFULLY: "Product quantity has been updated successfully",
  CART_DATA_RETRIVED_SUCCESSFULLY: "Cart data retrived successfully!!",
  EMPTY_CART_SUCCESSFULLY: "Cart has been empty",
  CART_QUANTITY_UPDATED: "Cart quantity updated",
  CART_PRODUCT_NOT_FOUND: "Cart product not found",
  CART_IS_EMPTY: "Your cart is empty",
  CANNOT_ADD_CART_PRODUCT: "This product can not to be add in cart",
  CANNOT_UPDATE_CART_PRODUCT: "Can not add more quantity",
  PRODUCT_OUT_OF_STOCK: "Product out of stock",
  PRODUCT_ALREADY_IN_CART: "Product already in cart",

  //Cart - EXPERIENCE
  CART_SERVICE_ADDED_SUCCESSFULLY: "Service has been added successfully in cart",
  CART_SERVICE_REMOVED_SUCCESSFULLY: "Service has been removed successfully from cart",
  CART_SERVICE_UPDATED_SUCCESSFULLY: "Service quantity has been updated successfully",
  CART_DATA_RETRIVED_SUCCESSFULLY: "Cart data retrived successfully!!",
  EMPTY_CART_SUCCESSFULLY: "Cart has been empty",
  CART_QUANTITY_UPDATED: "Cart quantity updated",
  CART_SERVICE_NOT_FOUND: "Cart Service not found",
  CART_IS_EMPTY: "Your cart is empty",
  CANNOT_ADD_CART_SERVICE: "This Service can not to be add in cart",
  CANNOT_UPDATE_CART_SERVICE: "Can not add more quantity",
  SERVICE_OUT_OF_STOCK: "Service out of stock",
  SERVICE_ALREADY_IN_CART: "This package is already in your cart",

  //Wishlist
  WISHLIST_PRODUCT_ADDED_SUCCESSFULLY: "Product added successfully to wishlist",
  WISHLIST_PRODUCT_REMOVED_SUCCESSFULLY: "Product removed from wishlist successfully",
  EMPTY_WISHLIST: "All products from wishlist removed successfully",
  PRODUCT_ALREADY_IN_WISHLIST: "This product already in your wishlist",
  WISHLIST_PRODUCT_NOT_FOUND: "There is no product in your wishlist",
  WISHLIST_PRODUCT_RETRIVED: "Wishlist products retrived successfully",

  WISHLIST_SERVICE_ADDED_SUCCESSFULLY: "Service added successfully to wishlist",
  WISHLIST_SERVICE_REMOVED_SUCCESSFULLY: "Service removed from wishlist successfully",
  EMPTY_WISHLIST_SERVICE: "All service from wishlist removed successfully",
  SERVICE_ALREADY_IN_WISHLIST: "This service already in your wishlist",
  WISHLIST_SERVICE_NOT_FOUND: "There is no service in your wishlist",
  WISHLIST_SERVICE_RETRIVED: "Wishlist services retrived successfully",


  //Hotels
  HOTEL_RETRIVED_SUCCESSFULLY: "All hotels retrived successfully",
  HOTEL_ADDED_SUCCESSFULLY: "Hotel added successfully",
  HOTEL_EXIST: "This hotel is already exist",

  //Shipping
  SHIPPING_DETAILS_ADDED_SUCCESSFULLY: "Shipping details added successfully",
  SHIPPING_DATA_RETRIVED_SUCCESSFULLY: "Shipping data retrived successfully",

  //Summary
  SUMMARY_RETRIVED: "Retrived order summary successfully",
  NO_SUMMARY_DATA: "No summary data found",
  SUMMARY_ADDED: "Summary details has been added successfully",

  // Services
  ACCESS_RETRICTED: "Access restricted",
  SERVICE_ADDED: "New service added successfully",
  INVALID_LAT_LNG: "Invalid Latitude or Longitude",
  INVALID_DATE: "Invalid Date",
  NO_SERVICE_FOUND: "No service found",
  NO_LOCATION_FOUND: "No location found",
  SERVICES_FETCHED_SUCCESSFULLY: "Services fetched successfully",
  CANNOT_USE_START_DATE_AS_PAST_DATE: "Start date cannot be a past date",
  CANNOT_USE_END_DATE_AS_PAST_DATE: "End date cannot be a past date",
  CANNOT_USER_END_DATE_BEFORE_START_DATE: "End date cannot be before Start date",
  SERVICE_UPDATED_SUCCESSFULLY: "Service updated successfully",
  SERVICE_DELETED_SUCCESSFULLY: "Service deleted successfully",
  FILE_NOT_FOUND: "Please upload image(s).",
  SERVICES_UNMARKED_FROM_FEATURES: "Services(s) unmarked from featured",
  SERVICES_MARKED_AS_FEATURES: "Services(s) marked as featured",

  // Category
  PARAMETER_CATEGORY_FOR_IS_INVALID: "Parameter [category_for] should only be for shop, product or tour package",
  CATEGORY_ADDED: "Category added successfully",
  CATEGORY_UPDATED: "Category updated successfully",
  CATEGORY_REMOVED: "Category removed successfully",
  NO_CATEGORY_FOUND: "No Category Found",
  CATEGORY_FETCHED: "Category fetched successfully",
  SUB_CATEGORY_REMOVED: "Sub category removed successfully",
  NOTHING_TO_UPDATE: "Uh oh, nothing to update",
  CATEGORY_ORDER_UPDATED:'Category order has be updated successfully',
  CATEGORY_USED_IN_ARTICLE:'Could not delete category as it is used in an article',

  // Province
  PROVINCE_ADDED: "Province added successfully",
  NO_PROVINCE_FOUND: "No Province found",
  PROVINCE_UPDATED: "Province updated successfully",
  PROVINCE_REMOVED: "Province deleted successfully",
  PROVINCE_FETCHED: "Province fetched successfully",

  // District
  DISTRICT_ALREADY_EXISTS:'District already exists',
  DISTRICT_ADDED:'District added successfully',

  COMMUNE_ADDED :'Commune added successfully',
  COMMUNE_UPDATED:'Commune updated successfully',
  COMMUNE_DELETED:'Commune deleted successfully',
  NO_DISTRICT_FOUND:'No District found',
  COMMUNE_ALREADY_EXISTS:'Commune Already exists',

  // Temple
  TEMPLE_ADDED: "Temple added successfully",
  NO_TEMPLE_FOUND: "No Temple found",
  TEMPLE_UPDATED: "Temple updated successfully",
  TEMPLE_REMOVED: "Temple deleted successfully",
  TEMPLE_FETCHED: "Temple fetched successfully",

  // Tag
  TAG_ADDED: "Tag added successfully",
  NO_TAG_FOUND: "No Tag found",
  TAG_UPDATED: "Tag updated successfully",
  TAG_REMOVED: "Tag deleted successfully",
  TAG_FETCHED: "Tag fetched successfully",

  // Location
  LOCATION_FETCHED: "Locations fetched successfully",

  // Ratings
  RATING_ALREADY_ADDED: "Your rating has already been added",
  RATING_ADDED: "Rating added successfully",
  RATING_UPDATED: "Rating updated successfully",
  RATINGS_FETCHED_SUCCESSFULLY: "Ratings fetched successfully",
  NO_RATINGS_FOUND: "No ratings found",

  // Reviews
  REVIEW_ADDED_SUCCESSFULLY: "Review added successfully",
  NO_REVIEW_FOUND: "No review found",
  REVIEW_UPDATED_SUCCESSFULLY: "Review updated successfully",
  REVIEW_DELETED_SUCCESSFULLY: "Review deleted successfully",
  NO_REVIEWS_FOUND: "No reviews found for this product",
  REVIEWS_FETCHED_SUCCESSFULLY: "Reviews fetched successfully",
  REVIEW_LIKED: "Review liked successfully",
  REVIEW_UNLIKED: "Review unliked successfully",


  // Article
  ARTICLE_ADDED_SUCCESSFULLY: "Article added successfully",
  ARTICLE_UPDATED_SUCCESSFULLY: "Article updated successfully",
  ARTICLE_DELETED_SUCCESSFULLY: "Article deleted successfully",
  ARTICLE_UPDATED_SUCCESSFULLY: "Article updated successfully",
  ARTICLE_FETCHED_SUCCESSFULLY: "Article fetched successfully",
  NO_ARTICLE_FOUND: "No article found",
  ARTICLE_UPDATE_FAILED: "Update failed",
  ARTICLE_LIKED: "Article liked successfully",
  ARTICLE_UNLIKED: "Article unliked successfully",
  ARTICLE_SAVED: "Article saved successfully",
  ARTICLE_UNSAVED: "Article unsaved successfully",
  ARTICLE_UNMARKED_FROM_FEATURES: "Article(s) unmarked from featured",
  ARTICLE_MARKED_AS_FEATURES: "Article(s) marked as featured",
  ARTICLE_REMOVED_FROM_FAVOURITE: "Article(s) removed from bookmark"

};
