// Success response to return
function success(res, code, message, data) {
    res.status(code).json({code: code, message: message, data: data});
}

// Error response to return
function error(res, code, message) {
    res.status(code).json({code: code, message: message});
}

module.exports = {
    success,
    error
};
