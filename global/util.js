exports.addTranslatedData = async ({
  data,
  lang,
  requiredFields,
  translationObject
}) => {
  if (
    !data ||
    typeof data !== "object" ||
    Array.isArray(data) ||
    (typeof data === "object" && Object.keys(data).length === 0)
  ) {
    throw new Error(`${lang} language is a required field`)
  }

  if (requiredFields && requiredFields.length) {
    requiredFields.forEach((field) => {
      if (data[field]) {
        translationObject[lang][field] = data[field];
      } else {
        throw new Error(`[${field}] for ${lang} language is a required field`)
      }
    });
  } else {
    throw new Error(`${lang} required fileds cannot be empty`)
  }
};