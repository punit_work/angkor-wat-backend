## Hirerachy Explaination and what fields will be available for which user type.


#  USER TYPES     |      role       | admin_type     |   discover_type   |  commerce_type
#                 |                 |                |                   |
#  Super Admin    |  super_admin    |     -          |         -         |       -
#  Business Owner |  business_owner |     -          |         -         |       -
#                 |                 |                |                   |
#  Discover Admin |     admin       |  discover      |         -         |       -
#  Author         |  discover_user  |     -          |       author      |       -
#  Editor         |  discover_user  |     -          |       editor      |       -
#                 |                 |                |                   |
#  Commerce Admin |     admin       |  commerce      |         -         |       -
#  Shop Owner     |  commerce_user  |     -          |         -         |      shop
#  Service Owner  |  commerce_user  |     -          |         -         |     service