const jwt = require('jsonwebtoken');
const Response = require('../global/response');
const ResponseCode = require('../global/code');
const ResponseMessage = require('../global/message');
const SystemUserController = require('../routes/system-users/system-user-controller');

// This function will be used as middleware in all the APIs that require authentication
module.exports = async (req, res, next) => {

    // Check if token is active
    const header = req.get('Authorization');

    let token;



    // Sending error response if no Authorization header found
    if (!header) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);

    else token = header.split(' ')[1];

    // Sending error response if Authorization header is not in proper format
    if (!token) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);

    let decodedToken;

    try {

        // Verifying jwt token using secret-key
        decodedToken = jwt.verify(token, process.env.JWT_SECRET_KEY);

        const {user_type,status} = decodedToken;

        if(user_type !== "end_user" && status !== "active"){

            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, "You are currently blocked")

        }
        

    } catch (err) {
        // Sending error response if token is not verified
        return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }

    // Sending error response if token is expired
    if (!decodedToken) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);

    // Checking user session is active or not
    // If active => proceed further, else => unauthorized
    const userSession = await SystemUserController.isUserSessionActive(decodedToken);

    // Sending error response if session is not active
    if (!userSession) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);

    // Attaching user_id, session_id and user_type from decoded token to request object
    req.user_id = decodedToken.user_id;
    req.session_id = decodedToken.session_id;
    req.user_type = decodedToken.user_type;
    next();
}