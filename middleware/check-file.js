const fs = require('fs');

//Function to check whether file is attached in multipart form data or not
// If yes, remove file from server
async function doFileExist(file) {
    if (file) {

        //Removing file if file exist
        fs.unlink(file.path, (err) => {
            if (err) console.log(err);
            else console.log('File Deleted');
        })
    }
}

async function doFilesExist(files) {

    for (let file of files) {

        if (file) {

            //Removing file if file exist
            fs.unlink(file.path, (err) => {
                if (err) console.log(err);
                else console.log('File Deleted');
            })
        }
    }
}

async function removeFileIfExists(path) {
    if (path) {
        // Removing file if exist
        fs.unlink(path, (err) => {
            if (err) console.log(err);
            else console.log('Previous File Deleted');
        })
    }
}

async function removeFilesIfExist(paths) {

    for (let path of paths) {

        if (path) {
            // Removing file if exist
            fs.unlink(path, (err) => {
                if (err) console.log(err);
                else console.log('Previous File Deleted');
            })
        }
    }
}

module.exports = { doFileExist, doFilesExist, removeFileIfExists, removeFilesIfExist };