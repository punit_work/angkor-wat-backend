const multer = require('multer');

//Allowed mime type
const MIME_TYPE = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
  'video/mp4': '.mp4'
};

//Giving specific filename and destination to store attached file from request body 
const fileStorage = multer.diskStorage({
    
    destination : (req , file , callback) => {

        //Absolute path for storing file : 'uploads/store-images'
        callback(null , 'uploads/article-images')
    },
    
    filename : (req , file ,callback) => {

        let uid = Math.floor(100000000 + Math.random() * 800000000);
        callback(null, uid + '-' + file.originalname.replace(/ /g, "-"))
    }
})

// Image filter
const filter = (req, file, callback) => {

    //Checking if file has valid mime type or not
    const isFileTypeValid = !!MIME_TYPE[file.mimetype];
    let error = isFileTypeValid ? null : new Error('Only .jpg, .png, .jpeg , .mp4 files are valid.');
    callback(error, isFileTypeValid);
    
}

const uploadArticleImage = multer({storage : fileStorage , fileFilter : filter});

module.exports = uploadArticleImage;