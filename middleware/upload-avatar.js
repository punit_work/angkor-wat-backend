const multer = require('multer');

// For storing files in images folder
const fileStorage = multer.diskStorage({
    
    destination : (req , file , cb) => {
        cb(null , 'uploads/images/avatar')
    },
    
    filename : (req , file ,cb) => {
        var uid = Math.floor(100000000 + Math.random() * 800000000);
        cb(null, uid + '-' + file.originalname.replace(/ /g, "-"))
    }
})

// Image filter
const filter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

const uploadAvatar = multer({storage : fileStorage , fileFilter : filter});

module.exports = uploadAvatar;