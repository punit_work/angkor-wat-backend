const multer = require('multer');

//Allowed mime type
const MIME_TYPE = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg'
};

//Giving specific filename and destination to store attached file from request body 
const fileStorage = multer.diskStorage({
    
    destination : (req , file , callback) => {

        //Absolute path for storing file : 'uploads/category-images'
        callback(null , 'uploads/category-images')
    },
    
    filename : (req , file ,callback) => {

        let uid = Math.floor(100000000 + Math.random() * 800000000);
        callback(null, uid + '-' + file.originalname.replace(/ /g, "-"))
    }
})

// Image filter
const filter = (req, file, callback) => {

    //Checking if file has valid mime type or not
    const isFileTypeValid = !!MIME_TYPE[file.mimetype];
    let error = isFileTypeValid ? null : new Error('Only .jpg, .png, .jpeg files are valid.');
    callback(error, isFileTypeValid);
    
}

const uploadCategoryImage = multer({storage : fileStorage , fileFilter : filter});

module.exports = uploadCategoryImage;