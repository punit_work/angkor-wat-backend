const multer = require('multer');

//Allowed mime type
const MIME_TYPE = {
    'application/pdf': 'pdf'
};

//Giving specific filename and destination to store attached file from request body 
const fileStorage = multer.diskStorage({
    
    destination : (req , file , callback) => {

        //Absolute path for storing file : 'uploads/documents'
        callback(null , 'uploads/documents')
    },
    
    filename : (req , file ,callback) => {

        let uid = Math.floor(100000000 + Math.random() * 800000000);
        callback(null, uid + '-' + file.originalname.replace(/ /g, "-"));
    }
})

// Image filter
const filter = (req, file, callback) => {

    //Checking if file has valid mime type or not
    const isFileTypeValid = !!MIME_TYPE[file.mimetype];
    let error = isFileTypeValid ? null : new Error('Only .pdf file is valid.');
    callback(error, isFileTypeValid);
    
}

const uploadDocument = multer({storage : fileStorage , fileFilter : filter});

module.exports = uploadDocument;
