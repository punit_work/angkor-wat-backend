const multer = require("multer");
const { v4 } = require("uuid");

//Allowed mime type
const MIME_TYPE = {
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/jpg": "jpg",
  "video/mp4": ".mp4",
};

//Giving specific filename and destination to store attached file from request body
const fileStorage = multer.diskStorage({
  destination: (req, file, callback) => {
    //Absolute path for storing file : 'uploads/store-images'
    callback(null, "uploads/media-library");
  },

  filename: (req, file, callback) => {
    let uid = Math.floor(100000000 + Math.random() * 800000000);
    callback(
      null,
      file.originalname && file.originalname === "blob"
        ? `${v4()}-blob.${file.mimetype.split("/")[1]}`
        : `${v4()}-${file.originalname.replace(/ /g, "-")}`
    );
  },
});

// Image filter
const filter = (req, file, callback) => {
  console.log(file);
  //Checking if file has valid mime type or not
  const isFileTypeValid = !!MIME_TYPE[file.mimetype];
  let error = isFileTypeValid
    ? null
    : new Error("Only .jpg, .png, .jpeg , .mp4 files are valid.");
  callback(error, isFileTypeValid);
};

const uploadMedia = multer({ storage: fileStorage, fileFilter: filter });

module.exports = uploadMedia;
