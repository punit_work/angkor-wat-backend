const mongoose = require("mongoose");

const ArticleTranslationSchema = new mongoose.Schema({
  title:{
    type:String,
    required:true
  },
  description:{
    type:String,
    required:true
  },
  article_content:{
    type:String,
    required:true
  },
  tags:[
    {type:String}
  ]
},{_id:false});

const article = new mongoose.Schema(
  {
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "System_User"
    },
    // title: {
    //   type: "String"
    // },
    category: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category"
    }],
    status: {
      type: String,
      enum: ['active', 'draft']
    },
    sub_category: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category'
    }],
    // description: {
    //   type: "String"
    // },
    province: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Province"
    }],
    // article_content: {
    //   type: "String",
    // },
    // article_description: [{
    //   language: { type: String },
    //   content: { type: String }
    // }],
    media_images:[{
      type: mongoose.Schema.Types.ObjectId,
      ref: "mediaLibrary"
    }],
    media_video:{
      type: mongoose.Schema.Types.ObjectId,
      ref: "mediaLibrary"
    },
    is_updating: {
      type: Boolean,
      default: false
    },
    update_info: [
      {
        /// For the author and editor
        editor_id: { type: mongoose.Schema.Types.ObjectId, ref: "System_User" },
        updated_at: { type: Date },
        user_type: { type: String, enum: ['editor', 'author', 'admin'] }
      }
    ],
    article_video: {
      type: String
    },
    share_count: {
      type: Number,
      default: 0
    },
    liked_by: [
      { type: mongoose.Schema.Types.ObjectId, ref: "User" }
    ],
    saved_by: [
      { type: mongoose.Schema.Types.ObjectId, ref: "User" }
    ],
    is_featured: {
      type: Boolean,
      default: false
    },
    temple_id:[{
      type: mongoose.Schema.Types.ObjectId, ref: "temple"
    }],
    // tags:[
    //   {type:String}
    // ]
    translations:{
      en:ArticleTranslationSchema,
      kh:ArticleTranslationSchema
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Article", article);
