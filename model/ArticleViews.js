const mongoose = require("mongoose");

const articleViews = new mongoose.Schema(
    {
        article_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Article",
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "System_User",
        },

    },
    { timestamps: true }
);

module.exports = mongoose.model("Article_Views", articleViews);