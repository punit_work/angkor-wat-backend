const mongoose = require('mongoose');

const billingDetails = new mongoose.Schema({
   user_id:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
   }, 
   full_name: {
        type: String
    },
   province:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
   },
   district:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Province'
   },
   commune:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Province'
   },
   postal_code:{
    type:Number
   },
   address_line_1: {
    type: String
   },
   address_line_2:{
    type: String
   },
   email:{
       type:String
   },
   phone_number:{
       type:String
   }
} , {timestamps : true});

module.exports = mongoose.model('BillingDetails' , billingDetails);