const mongoose = require('mongoose');

const BugReport = new mongoose.Schema({
   module: {
        type: String,
        enum:['Discover', 'Shop', 'Experience','Organization' ]
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    title:{
        type: String,
    },
   description:{
        type: String,
   },
   priority:{
    type: String,
    enum:['High','Low','Medium']
   },
   status:{
    type: String,
    enum:['Active','Closed'],
    default:'Active'
   },
   bug_id:{
    type:String
   }
} , {timestamps : true});

module.exports = mongoose.model('BugReport' , BugReport);