const mongoose = require('mongoose');

const cart = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
   product: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
   },
   variant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product'
   },
   quantity: {
          type: Number
   },
   shop:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shop'
   },
   business_owner:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'SystemUsers'
   },
   attribute:{
        promotion:{
             code:{
                  type:String,
             },
             value:{
                  type:Number,
                  default:0
             },
             discount:{
                  type: String
             }
        }
   }
} , {timestamps : true});

module.exports = mongoose.model('Cart' , cart);