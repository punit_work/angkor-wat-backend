const mongoose = require("mongoose");

const LangBasedCategorySchema = new mongoose.Schema(
  {
    category_name: String,
  },
  { _id: false }
);

const LangBasedSubCategorySchema = new mongoose.Schema(
  {
    name: String,
  },
  { _id: false }
);

// Category Schema
const category = new mongoose.Schema(
  {
    category_for: {
      type: String,
      enum: ["Product", "Shop", "Tour Package", "Service", "Article"],
    },

    // category_name: {
    //   type: String,
    // },

    category_image: {
      type: String,
    },
    translations: {
      en: LangBasedCategorySchema,
      kh: LangBasedCategorySchema,
    },
    sub_category: [
      {
        // name: {
        //   type: String,
        // },
        sub_category_order: {
          type: Number,
          default: 0,
        },
        translations: {
          en: LangBasedSubCategorySchema,
          kh: LangBasedSubCategorySchema,
        },
        sub_sub_category: [
          {
            name: {
              type: String,
            },
          },
        ],
      },
    ],
    category_order: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Category", category);
