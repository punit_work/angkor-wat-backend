const mongoose = require("mongoose");

const experienceCart = new mongoose.Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    service: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Service"
    },
    service_package: [{ 
        package_id: {  
            type: mongoose.Schema.Types.ObjectId, 
            ref: "Service"
        },
        quantity : {type: 'Number'}
    }],
    service_slot:{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Service"

    },
    shop:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Shop'
    },
    business_owner:{
          type: mongoose.Schema.Types.ObjectId,
          ref: 'SystemUsers'
    },
    attribute:{
          promotion:{
              code:{
                    type:String,
              },
              value:{
                    type:Number,
                    default:0
              },
              discount:{
                    type: String
              }
          }
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("ExperienceCart", experienceCart);
