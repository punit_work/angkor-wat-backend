const mongoose = require('mongoose');

const experienceSummary = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    service_details: [
        {
            service_id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Service'
            },
            service_provider_id :{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'System_User'

            },
            price: {
                type: Number
            },
            for_person: {
                type: Number
            },
            service_name: {
                type: String
            },
            service_img: [{
                type: String
            }]
        }
    ],
    sub_total: {
        type: Number
    },
    tax: {
        type: Number
    },
    promotion_applied: {
        type: Number
    },
    order_total: {
        type: Number
    },
    shipping_details: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ExperienceShipping'
    },
    payment_method: {
        type: mongoose.Schema.Types.ObjectId
    },
    is_payment_done: {
        type: Boolean,
        default: false
    },
}, { timestamps: true });

module.exports = mongoose.model('Experience_Summary', experienceSummary);