const mongoose = require('mongoose');

const Feedback = new mongoose.Schema({
   module: {
        type: String,
        enum:['Discover', 'Shop', 'Experience','Organization' ]
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
   description:{
        type: String,
   },
   rating:{
    type: Number,
    enum:[1,2,3,4,5]
   }
} , {timestamps : true});

module.exports = mongoose.model('Feedback' , Feedback);