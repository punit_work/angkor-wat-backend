const mongoose = require('mongoose');

const hotel = new mongoose.Schema({
   hotel_name: {
        type: String
    },
    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    province:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
   },
   district:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Province'
   },
   commune:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Province'
   },
   postal_code:{
    type:Number
   },
   address: {
          type: String
   }
} , {timestamps : true});

module.exports = mongoose.model('Hotel' , hotel);