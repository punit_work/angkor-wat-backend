const mongoose = require("mongoose");

const mediaLibrary = new mongoose.Schema(
  {
    path: {
      type: String
    },
    added_by:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'System_User'
    },
    module:{
        type:String,
        enum: ['Discover', 'Shop', 'Experience','Organization'],
        default:"Discover"
    },
    type:{
        type:String,
        enum: ['Image','Video']
    }
    
  },
  { timestamps: true }
);

module.exports = mongoose.model("mediaLibrary", mediaLibrary);
