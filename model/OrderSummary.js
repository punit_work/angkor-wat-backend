const mongoose = require('mongoose');

const orderSummary = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    order_id:{
        type: String
    },
    cart_details:[
        { 

            business_owner:{
                _id:{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'SystemUsers'
                },
                fname:{
                    type:String
                },
                lname:{
                    type:String
                }
            },
            products:[{

                product_id:{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'Product'
                },
                product_name:{
                    type:String
                },
                variants:{
                    _id:{
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Product'
                    },
                    unit:{ type:String},
                    price:{ type:Number},
                    cost_price:{type:Number}
                },
                attribute:{
                    promotion:{
                         code:{
                              type:String,
                         },
                         value:{
                              type:Number,
                              default:0
                         },
                         discount:{
                              type: String
                         }
                    }
               },
                quantity:{
                    type:Number
                },
                shop:{
                    _id:{
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Shop'
                    },
                    shop_name:{
                        type:String
                    }
                },
            }
            ],
            shop_total:{ 
                type: Number
            }
        }
    ],
    tax:{
        type:String
    },
    promocode:{
        code_name:{
            type:String
        },
        ammount:{
            type:String
        }
    },
    order_total:{
        type:Number
    },
    shipping_details: {
        delivery_type :{
           type:String
        },
        hotel_details:{
            hotel_name:{
                type:String
            },
            hotel_address:{
                type:String
            },
            province:{
                type:String
            },
            district:{
                type:String
            },
            commune:{
                type:String
            },
            postal_code:{
                type:Number
            }
        },
        billing_details:{
            full_name: {
                type: String
            },
           email: {
                type: String
            },
           address_line_1: {
                  type: String
                },
           address_line_2:{
               type:String
                },
           postal_code:{
               type:Number
                },
           province:{
               type:String
                },
           district:{
               type:String
                },
           commune:{
               type:String
                },
           phone_number:{
               type:String
                }
        },
        self_pickup:[
            {
                products:[{
                    product_name:{type:String},
                    quantity:{type:Number}
                }],
                shop:{
                    address:{type:String},
                    province:{type:String},
                    commune:{ type: String},
                    district:{type: String},
                    postal_code:{ type: Number},
                    is_pickup_point:{
                        type: Boolean,
                        default: true
                    }
                },
                business_owner:{
                    address:{type:String},
                    province:{type:String},
                    commune:{ type: String},
                    district:{type: String},
                    postal_code:{ type: Number}
                }

            }
        ]

    },
    payment_method : {
        type: mongoose.Schema.Types.ObjectId
    },
    is_payment_done :{
        type :Boolean,
        default :false
    },
    order_date :{
        type : Date
    },
    dispatched_date : {
        type : Date
    },
    delivery_status : {
        type : String,
        enum : ['Not yet shipped','Dispatched','Delivered','cancelled'],
        default : "Not yet shipped"
    },
    is_active:{
        type :Boolean,
        default :false
    }
} , {timestamps : true});

module.exports = mongoose.model('Order_Summary' , orderSummary);