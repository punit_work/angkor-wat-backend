const mongoose = require('mongoose');

// Payment Methods Schema
const paymentMethods = new mongoose.Schema({
    
    payment_method_name: {
        type: String
    },

    is_deleted: {
        type: Boolean,
        default: false
    }

} , {timestamps : true});

module.exports = mongoose.model('Payment_Method', paymentMethods);