const mongoose = require('mongoose');

const LangBasedShopSchema = new mongoose.Schema(
  {
    product_name: String,
    product_description:String,
  },
  { _id: false }
);


// Product Schema
const product = new mongoose.Schema({
    
    //vendor id
    shop_id : {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'Shop'
    },

  //   product_name : {
  //       type: String
  //   },

  //   product_description: [{
  //     language: { type: String },
  //     content: { type: String }
  // }],

  translations:{
    en: LangBasedShopSchema,
    kh: LangBasedShopSchema
  },

    product_images : [{
        type: String
    }],

    variants: [{

        color: { type: String },
        images: [{ type: String }],
        price: { type: Number },
        dimensions: { type: String },
        unit: { type: String },
        unit_quantity: { type: Number },
        available_quantity: { type: Number }
        
    }],

    category : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },

    sub_category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    
    sub_sub_category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },

    tags : [{
        type: String
    }],
    
    is_featured: {
        type: Boolean,
        default: false
    },
    show_reviews : {
        type: Boolean,
        default: true
     }

} , {timestamps : true});

module.exports = mongoose.model('Product', product);