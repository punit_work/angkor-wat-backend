const mongoose = require('mongoose');

// Rating schema
const promocode = new mongoose.Schema({

    shop_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shop'
    },
    product_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    variant_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'

    },
    service_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Service'
    },

    code:{
        type :String,
        unique :true
    },

    is_percent:{
        type:Boolean,
        default : true
    },
    amount:{
        type:Number
    },
    on_total:{
        type:Number
    },
    expire_date:{
        type:Date
    },
    is_active:{
        type:Boolean,
        default :true
    }


}, {timestamps: true});

module.exports = mongoose.model('Promocode', promocode);