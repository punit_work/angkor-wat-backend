const mongoose = require("mongoose");

const LangBasedProvince = new mongoose.Schema(
  {
    province_name: {
      type:String,
      required:true
    }
  },
  {_id: false}
)

const LangBasedDistrict = new mongoose.Schema(
  {
    district_name: {
      type:String,
      required:true
    }
  },
  {_id: false}
)

const LangBasedCommune = new mongoose.Schema(
  {
    commune_name: {
      type:String,
      required:true
    }
  },
  {_id: false}
)

// Province schema
const province = new mongoose.Schema(
  {
    translations:{
      en:LangBasedProvince,
      kh:LangBasedProvince
    },
    province_order: {
      type: Number,
      default:0
    },
    district: [
      {
        translations:{
          en:LangBasedDistrict,
          kh:LangBasedDistrict
        },
        district_order: {
          type: Number,
          defult:0
        },
        commune: [
          {
            commune_order: {
              type: Number,
              default:0
            },
            translations:{
              en:LangBasedCommune,
              kh:LangBasedCommune
            },
            postal_code: { type: Number },
          },
        ],
      },
    ],
    location: {
      type: {
        type: String,
        enum: ["Point"],
        required: true,
      },

      coordinates: {
        type: [Number],
        required: true,
      },
    },

    country: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Province", province);
