const mongoose = require('mongoose');

// Rating schema
const ratings = new mongoose.Schema({

    shop_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shop'
    },

    product_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },

    service_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Service'
    },

    no_of_ratings: {type: Number},

    average_rating: {type: Number},

    rated_by: [{

        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },

        rating: {type: Number},
        ratingOn:{type : Date, default: Date.now }
        
    }]

}, {timestamps: true});

module.exports = mongoose.model('Rating', ratings);