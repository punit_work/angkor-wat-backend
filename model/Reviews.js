const mongoose = require('mongoose');

// Review schema
const reviews = new mongoose.Schema({

    product_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },

    service_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Service'
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    
    comment: {
        type: String
    },

    liked_by: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
    
}, {timestamps: true});

module.exports = mongoose.model('Review', reviews);