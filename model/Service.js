const mongoose = require("mongoose");

// Service Schema
const service = new mongoose.Schema(
  {
    // vendor_id
    shop_id : {
      type : mongoose.Schema.Types.ObjectId,
      ref: 'Shop'
    },

    service_images: [
      {
        type: String
      }
    ],

    service_title: {
      type: String,
    },

    service_details: [
      {
        language: { type: String },
        highlights: { type: String },
        description: { type: String },
        duration: { type: String },
        departure_details:{type: String},
        return_details:{type: String},
        inclusions:[{
            type: String,
          }],
        exclusions:[{
        type: String,
        }],
        additional_information:[{
            type: String,
        }]
      }
    ],
    
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category"
    },

    sub_category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category"
    },
    sub_sub_category:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Category"
    },

    start_date: {
      type: Date
    },
    end_date: {
      type: Date
    },

    available_slot :[{
        day:{
            type: String,
            enum: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
        },
        slot:{
         start_time:{
            type: String
         },
         end_time:{
            type: String
         }
        }
    }],

    province: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Province"
    },

    package:[
        {
            for: { type: String },
            cost: { type: Number }
        }
    ],

    city: {
      type: String
    },
    address: {
      type: String
    },
    location: {
      type: {
        type: String,
        enum: ["Point"],
        required: true
      },
      coordinates: {
        type: [Number],
        required: true
      },
    },

    pincode: {
      type: String
    },

    tags: [
      {
        type: String
      },
    ],

    payment_methods: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Payment_Methods"
      }
    ],

    is_featured: {
      type: Boolean,
      default: false
    },

    max_persons_allowed: {
      type: Number
    },

    is_available: {
      type: Boolean
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Service", service);
