const mongoose = require('mongoose');

// Session Schema
const sessionSchema = new mongoose.Schema({

    user_type: {
        type: String,
        enum: ['super_admin','admin', 'business_owner', 'end_user', 'service_provider', 'vendor','discover','commerce','author','editor','shop','service'],
        default: 'end_user'
    },

    system_user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'System_User'
    },

    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    is_active: {
        type: Boolean,
        default: true
    },

    ip_address: {
        type: String
    },

    user_agent: {
        type: String
    },

    session_id: {
        type: String
    }

}, { timestamps: true });

module.exports = mongoose.model('Session', sessionSchema);
