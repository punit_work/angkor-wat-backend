const mongoose = require('mongoose');

const shipping = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    delivery_type: {
        type: String,
        enum: ['hotel', 'self-collection','user-address']
    },
    hotel_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Hotel'
    },
    billing_details:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'BillingDetails'
    }

} , {timestamps : true});

module.exports = mongoose.model('Shipping' , shipping);