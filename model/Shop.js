const mongoose = require('mongoose');

const LangBasedShopSchema = new mongoose.Schema(
  {
    shop_name: String,
    shop_description:String,
    address: String,
  },
  { _id: false }
);

// Vendor Schema
const shop = new mongoose.Schema({

    business_owner_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'System_User'
    },
    owner_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'System_User'
    },

    vendor_type: {
        type: String,
        enum: ['service_provider','product_seller']
    },
    // fname: {
    //     type: String
    // },

    // lname: {
    //     type: String
    // },

    // password: {
    //     type: String
    // },

    // email: {
    //     type: String
    // },

    phone_number: {
        type: String
    },
    
    website: {
        type: String
    },

    province: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
    },
    district:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
    },
    commune:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
    },
    postal_code:{
        type:Number
    },
    city: {
        type: String
    },
    // address: {
    //     type: String
    // },
    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },

    pincode: {
        type: String
    },
    
    shop_images: [{
        type: String
    }],

    shop_logo: {type : String},

    translations:{
      en: LangBasedShopSchema,
      kh: LangBasedShopSchema
    },

    // shop_name: {
    //     type: String
    // },

    // shop_description: [{
    //     language: { type: String },
    //     content: { type: String }
    // }],

    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },

    tags: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tag'
    }],

    payment_methods: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payment_Methods'
    }],

    is_featured: {
        type: Boolean,
        default: false
    },

    is_pickup_point:{
        type: Boolean,
        default: true
    }

}, { timestamps: true });

module.exports = mongoose.model('Shop', shop);