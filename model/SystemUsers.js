const mongoose = require('mongoose');

// System User Schema i.e commerce
const systemUser = new mongoose.Schema({

    fname: {
        type: String,
        required: true
    },

    lname: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true
    },

    phone_number: {
        type: String
    },

    country: {
        type: String
    },
    province: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
    },
    district: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
    },
    commune: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
    },
    postal_code: {
        type: Number
    },
    address: {
        type: String
    },
    age: {
        type: Number
    },
    role: {
        type: String,
        enum: ['super_admin','admin','business_owner','discover_user','commerce_user']
    },
    parent_admin_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'System_User'
    },
    created_by: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'System_User'
    },
    admin_type: {
        type: String,
        enum: ['discover', 'commerce']
    },
    discover_type: {
        type: String,
        enum: ['author', 'editor']
    },
    commerce_type: {
        type: String,
        enum: ['shop', 'service']
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },

    id_proof_name: {
        type: String
    },

    id_proof: {
        type: String
    },

    status:{
        type: String,
        enum: ['active','block'],
        default: 'active'
    },

}, { timestamps: true });

module.exports = mongoose.model('System_User', systemUser);