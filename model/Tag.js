const mongoose = require('mongoose');

const LangBasedTempleSchema = new mongoose.Schema({
  tag:{
    type:String,
    required:true
  }
},  { _id: false })

// System User Schema i.e commerce
const tag = new mongoose.Schema({
    translations:{
      en:LangBasedTempleSchema,
      kh:LangBasedTempleSchema
    }
}, { timestamps: true });

module.exports = mongoose.model('tag', tag);