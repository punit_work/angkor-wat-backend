const mongoose = require('mongoose');

const LangBasedTempleSchema = new mongoose.Schema({
  temple_name:{
    type:String,
    required:true
  }
},  { _id: false })

// System User Schema i.e commerce
const temple = new mongoose.Schema({
    translations:{
      en:LangBasedTempleSchema,
      kh:LangBasedTempleSchema
    },
    temple_order:{
      type: Number,
      default:0
    },
    province_id:{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Province",
      required:true
    }
}, { timestamps: true });

module.exports = mongoose.model('temple', temple);