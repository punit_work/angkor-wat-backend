const mongoose = require('mongoose');

// Tour Package Schema
const tourPackage = new mongoose.Schema({
    
    business_owner_id : {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'Service_Provider'
    },

    package_name : {
        type: String
    },

    package_images : [{
        type: String
    }],

    package_description : {
        type: String
    },

    category : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },

    start_date : {
        type : Date
    },

    end_date: {
        type: Date
    },

    available_slots : {
        type: Number
    },

    no_of_seats : {
        type: Boolean
    },

    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },

    price: {
        type: Number
    },

    special_offers: {
        type: String
    },

    tags : [{
        type: String
    }]

} , {timestamps : true});

module.exports = mongoose.model('Tour_Package', tourPackage);