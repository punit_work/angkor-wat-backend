const mongoose = require('mongoose');

// Users Schema
const user = new mongoose.Schema({

    fname : {
        type : String
    },

    lname : {
        type: String
    },
    
    avatar : {
        type: String
    },

    email : {
        type: String
    },

    phone_number : {
        type : String
    },

    last_active : {
        type: Date,
        default: Date.now()
    },

    country : {
        type: String
    },

    country_code:{
        type: String
    },

    age : {
        type: Number
    },

    gender : {
       type:String,
       enum: ['male', 'female']
    },

    google_id: {
        type: String
    },

    facebook_id: { 
        type: String
    },

    apple_id:{
        type: String
    }

} , {timestamps : true});

module.exports = mongoose.model('User' , user);