const mongoose = require('mongoose');

const wishlist = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
   product: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
   },
   product_variant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product'
   },
   service: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Service'
    }
} , {timestamps : true});

module.exports = mongoose.model('Wishlist' , wishlist);