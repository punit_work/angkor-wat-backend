// Models

const article = require("../../model/Article");
const articleView = require("../../model/ArticleViews");
const category = require("../../model/Category");
const temple = require("../../model/Temple");
const mongoose = require("mongoose");
const province = require("../../model/Province");
const fs = require("fs");
const ArticleHelper = require("./article-helper");
const moment = require("moment");
const SystemUsers = require("../../model/SystemUsers");

const generalFilters = [
  {
    $lookup: {
      from: "categories",
      let: { category: "$category" },
      pipeline: [
        {
          $match: {
            $expr: { $in: ["$_id", "$$category"] },
          },
        },
        { $project: { _id: 1, translations: 1 } },
      ],
      as: "category",
    },
  },
  {
    $lookup: {
      from: "provinces",
      let: {
        province_id: "$province",
      },

      as: "province",
      pipeline: [
        {
          $match: {
            $expr: {
              $in: ["$_id", "$$province_id"],
            },
          },
        },
        {
          $project: {
            _id: 1,
            translations: 1,
          },
        },
      ],
    },
  },
  {
    $lookup: {
      from: "medialibraries",
      localField: "media_images",
      foreignField: "_id",
      as: "media_images",
    },
  },
  {
    $lookup: {
      from: "medialibraries",
      localField: "media_video",
      foreignField: "_id",
      as: "media_video",
    },
  },
  {
    $unwind: {
      path: "$media_video",
      preserveNullAndEmptyArrays: true,
    },
  },
];

// Route to add article
exports.addArticle = async (req) => {
  // console.log(req.body);
  let result = {};

  const { user_id, user_type } = req;

  if (["admin", "author"].includes(user_type)) {
    const articleData = { ...req.body };
    articleData.author = user_id;

    try {
      //extract image file path from images and save image

      if (req.files.article_images) {
        articleData.article_images = req.files.article_images.map(({ path }) =>
          path.replace(/\\/g, "/")
        );
      }

      if (req.files.article_video) {
        articleData.article_video = req.files.article_video[0].path.replace(
          /\\/g,
          "/"
        );
      }

      if (articleData.media_images) {
        articleData.media_images = JSON.parse(articleData.media_images);
      }

      if (articleData.category) {
        articleData.category = Array.isArray(articleData.category)
          ? articleData.category
          : JSON.parse(articleData.category);
      }
      if (articleData.sub_category) {
        articleData.sub_category = Array.isArray(articleData.sub_category)
          ? articleData.sub_category
          : JSON.parse(articleData.sub_category);
      }
      if (articleData.province) {
        articleData.province = Array.isArray(articleData.province)
          ? articleData.province
          : JSON.parse(articleData.province);
      }
      if (articleData.temple_id) {
        articleData.temple_id = Array.isArray(articleData.temple_id)
          ? articleData.temple_id
          : JSON.parse(articleData.temple_id);
      }

      const parsedEn = JSON.parse(articleData.en);
      const parsedKh = JSON.parse(articleData.kh);

      articleData.translations = {
        en: {
          title: "",
          description: "",
          article_content: "",
          tags: [],
        },
        kh: {
          title: "",
          description: "",
          article_content: "",
          tags: [],
        },
      };

      const langBasedArticleData = (parsedData, lang) => {
        if (parsedData.title && parsedData.title !== "") {
          articleData.translations[lang].title = parsedData.title;
        }

        if (parsedData.description && parsedData.description !== "") {
          articleData.translations[lang].description = parsedData.description;
        }
        if (parsedData.article_content && parsedData.article_content !== "") {
          articleData.translations[lang].article_content =
            parsedData.article_content;
        }

        if (parsedData.tags) {
          let tagsArray = [];
          if (Array.isArray(parsedData.tags)) {
            tagsArray = parsedData.tags;
          } else {
            tagsArray = JSON.parse(parsedData.tags);
          }
          const hasSeenTag = {};
          const filteredTags = tagsArray.reduce((acc, tag) => {
            if (!hasSeenTag[tag]) {
              hasSeenTag[tag] = true;
              return acc.concat(tag);
            } else {
              return acc;
            }
          }, []);
          articleData.translations[lang].tags = filteredTags;
        }
      };

      langBasedArticleData(parsedEn, "en");
      langBasedArticleData(parsedKh, "kh");

      result.article = await new article(articleData).save();
      return result;
    } catch (error) {
      console.log(error);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

//Route to get all article for end-user / admins
exports.getAllArticle = async (req, page, limit) => {
  let result = {};
  const { user_type, user_id } = req;

  // if user is end user fetch all articles
  if (
    [
      "end_user",
      "admin",
      "author",
      "editor",
      "super_admin",
      "discover",
    ].includes(user_type)
  ) {
    try {
      let articleData = [];

      let query = {
        end_user: {
          status: "active",
        },
        admin: {
          status: "active",
        },
        super_admin: {
          status: "active",
        },
        editor: {
          status: "active",
        },
        author: {
          author: user_id,
        },
        discover: {
          status: "active",
        },
      };

      const modifiedFilters = generalFilters.slice(1,-1);
      commonFilters = [
        ...modifiedFilters,
        {
          $lookup: {
            from: "categories",
            let: { category: "$category" },
            pipeline: [
              {
                $match: {
                  $expr: { $in: ["$_id", "$$category"] },
                },
              },
            ],
            as: "category",
          },
        },
        {
          $lookup: {
            from: "article_views",
            let: {
              article_id: "$_id",
            },

            as: "article_views",
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ["$article_id", "$$article_id"],
                  },
                },
              },

              {
                $project: {
                  user_id: 1,
                  _id: 1,
                },
              },
            ],
          },
        },
        {
          $project: {
            author: 1,
            category: 1,
            province: 1,
            translations: 1,
            sub_category:1,
            status:1,
            article_images: 1,
            media_images: 1,
            media_video: 1,
            liked_by: 1,
            saved_by: 1,
            createdAt: 1,
            updatedAt: 1,
            __v: 1,
            length: {
              $add: [
                { $size: "$article_views" },
                { $multiply: [{ $size: "$saved_by" }, 2] },
              ],
            },
          },
        },
      ];

      commonFilters.push({
        $lookup: {
          from: "system_users",
          let: { author_id: "$author" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$author_id"],
                },
              },
            },
            {
              $project: {
                fname: 1,
                lname: 1,
              },
            },
          ],
          as: "author",
        },
      });

      commonFilters.push({$unwind:"$author"})

      if (page > 0) {
        commonFilters.push({
          $skip: (page - 1) * limit,
        });
      }
      if (limit > 0) {
        commonFilters.push({
          $limit: limit || 10,
        });
      }

      articleData = await article.aggregate(commonFilters);
      // if (page > 0 && limit > 0)
      //   articleData = await article
      //     .find(query[user_type])
      //     .skip((page - 1) * limit)
      //     .limit(limit)
      //     .populate("category", "translations category_id")
      //     .populate("author", "fname lname")
      //     .populate("media_images")
      //     .populate("media_video")
      //     .sort({ createdAt: -1 });
      // else
      //   articleData = await article
      //     .find(query[user_type])
      //     .populate("category", "category_name category_id")
      //     .populate("author", "fname lname")
      //     .populate("media_images")
      //     .populate("media_video")
      //     .sort({ createdAt: -1 });

      if (articleData.length) {
        if (page > 0 && limit > 0) {
          const articleCounts = await article.countDocuments({});
          const response = ArticleHelper.paginateData(
            page,
            limit,
            articleCounts
          );

          response.total_articles_count = articleCounts;
          response.article = articleData;
          response.current_page_articles_count = response.article.length;

          if (response.article.length === 0) {
            result.articlesNotFound = true;
            return result;
          }

          delete response.startIndex;
          delete response.endIndex;

          result.article = { ...response };
          return result;
        }
        result.article = articleData;
        return result;
      } else {
        result.articlesNotFound = true;
        return result;
      }
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

//Route to get all article for end-user / admins
exports.getArticleByID = async (req) => {
  let result = {};

  // if user is end user fetch all articles
  if (req.user_type === "end_user") {
    try {
      const { article_id } = req.body;

      const articleData = await article
        .findOne({ _id: article_id })
        .populate("category", "_id translations")
        .populate("province", "province")
        .populate("author", "fname lname")
        .populate("media_images")
        .populate("media_video");

      if (articleData) {
        const resultData = {};
        resultData.articleData = articleData;
        resultData.hasUserLikedArticle = articleData.liked_by.includes(
          req.user_id
        );
        resultData.hasUserSavedArticle = articleData.saved_by.includes(
          req.user_id
        );
        result.article = resultData;
      } else {
        result.articlesNotFound = true;
      }
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }

    if (result.article) {
      /// For adding views.
      try {
        const articleViewObj = await articleView.find({
          user_id: req.user_id,
          article_id: req.body.article_id,
        });
        if (articleViewObj.length === 0) {
          const obj = {
            article_id: req.body.article_id,
            user_id: req.user_id,
          };

          await articleView(obj).save();
        }
      } catch (e) {
        console.log(e);
      }
    }

    return result;
  }
  // //if user is admin i.e author fetch all articles belongs to admin / author
  // if(req.user_type === "admin"){

  //     try{

  //         const articleData = await article.find({author: req.user_id});

  //         if(articleData.length){
  //            result.article = articleData;
  //            return result;
  //         }
  //         else{
  //             result.articlesNotFound = true;
  //             return result;
  //         }

  //     }
  //     catch(err){
  //         console.log(err);
  //         result.databaseError = true;
  //         return result;
  //     }

  // }
};

//Route to Delete articles
exports.deleteArticle = async (req) => {
  const { user_type, user_id } = req;

  let result = {};

  // if user is end user fetch all articles
  if (["author", "editor", "super_admin", "discover"].includes(user_type)) {
    const articleIds = req.body.articles.map((data) => {
      return mongoose.Types.ObjectId(data);
    });

    const query = {
      admin: { _id: { $in: articleIds } },
      super_admin: { _id: { $in: articleIds } },
      editor: { _id: { $in: articleIds } },
      author: { author: user_id, _id: { $in: articleIds } },
      discover: { _id: { $in: articleIds } },
    };

    try {
      if (query[user_type]) {
        result.remove = await article.deleteMany(query[user_type]);

        result.remove.deletedCount > 0
          ? (result.success = true)
          : (result.failuer = true);

        return result;
      } else {
        result.databaseError = true;
        return result;
      }
    } catch (error) {
      console.log(error);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

/// Route to set status of update to false.
exports.setUpdateFalse = async (req) => {
  let result = {};

  if (req.user_type == "editor" || req.user_type == "author") {
    try {
      const articleObj = await article.findById(req.query.article_id);
      articleObj.is_updating = false;
      await articleObj.save();
      result.success = true;
      return result;
    } catch (e) {
      console.log(e);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

/// Route to check status of update in articles.

exports.checkUpdate = async (req) => {
  let result = {};

  if (req.user_type == "editor" || req.user_type == "author") {
    try {
      const articleObj = await article.findById(req.query.article_id);
      if (articleObj) {
        if (articleObj.is_updating) {
          result.success = true;
          result.isUpdating = false;
        } else {
          articleObj.is_updating = true;
          await articleObj.save();
          result.success = true;
          result.isUpdating = true;
        }
      } else {
        result.articleNotFound = true;
      }
      return result;
    } catch (e) {
      console.log(e);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

//Route to update article

exports.updateArticle = async (req) => {
  let result = {};

  if (["editor", "author", "admin"].includes(req.user_type)) {
    try {
      const obj = await article.findById(req.body.article_id);
      var isUpdated = false;
      if (obj != null) {
        if (req.files.article_images) {
          obj.article_images = req.files.article_images.map(({ path }) =>
            path.replace(/\\/g, "/")
          );
          isUpdated = true;
        }

        if (req.files.article_video) {
          obj.article_video = req.files.article_video[0].path.replace(
            /\\/g,
            "/"
          );
          console.log("uploaded");
          isUpdated = true;
        }

        if (req.body.media_images) {
          obj.media_images = JSON.parse(req.body.media_images);
          isUpdated = true;
        }
        if (req.body.media_video) {
          obj.media_video = req.body.media_video;
          isUpdated = true;
        } else if (req.body.remove_article_video) {
          console.log(obj.article_video);
          if (obj.article_video != null) {
            console.log("Inside remove");
            obj.article_video = undefined;
            isUpdated = true;
          }
        }

        if (req.body.category) {
          const userCategory = JSON.parse(req.body.category);
          const cat = await Promise.allSettled(
            userCategory.map(async (cat) => await category.findById(cat))
          );
          if (cat) {
            obj.category = cat.reduce(
              (acc, data) =>
                data.value && data.value._id ? acc.concat(data.value._id) : acc,
              []
            );
            isUpdated = true;
          }
        }

        if (req.body.sub_category) {
          const userSubCategory = JSON.parse(req.body.sub_category);
          const cat = await Promise.allSettled(
            userSubCategory.map(
              async (subCat) =>
                await category.findOne({
                  sub_category: {
                    $elemMatch: { _id: mongoose.Types.ObjectId(subCat) },
                  },
                })
            )
          );
          if (cat) {
            obj.sub_category = cat.reduce(
              (acc, data, ind) =>
                data.value && data.value._id
                  ? acc.concat(mongoose.Types.ObjectId(userSubCategory[ind]))
                  : acc,
              []
            );
            isUpdated = true;
          }
        }

        if (req.body.province) {
          const userProvince = JSON.parse(req.body.province);
          const prov = await Promise.allSettled(
            userProvince.map(async (prov) => await province.findById(prov))
          );
          if (prov) {
            obj.province = prov.reduce(
              (acc, data, ind) =>
                data.value && data.value._id
                  ? acc.concat(mongoose.Types.ObjectId(userProvince[ind]))
                  : acc,
              []
            );
            isUpdated = true;
          }
        }

        if (req.body.status != null) {
          obj.status = req.body.status;
          isUpdated = true;
        }

        if (req.body.temple_id) {
          const userTemple = JSON.parse(req.body.temple_id);
          const temp = await Promise.allSettled(
            userTemple.map(async (temp) => await temple.findById(temp))
          );
          if (temp) {
            obj.temple_id = temp.reduce(
              (acc, data) =>
                data.value && data.value._id ? acc.concat(data.value._id) : acc,
              []
            );
            isUpdated = true;
          }
        }

        const parsedEn = JSON.parse(req.body.en);
        const parsedKh = JSON.parse(req.body.kh);

        const langBasedArticleData = (parsedData, lang) => {
          if (parsedData.title && parsedData.title !== "") {
            obj.translations[lang].title = parsedData.title;
            isUpdated = true;
          }
          if (parsedData.description && parsedData.description !== "") {
            obj.translations[lang].description = parsedData.description;
            isUpdated = true;
          }
          if (parsedData.article_content && parsedData.article_content !== "") {
            obj.translations[lang].article_content = parsedData.article_content;
            isUpdated = true;
          }

          if (parsedData.tags) {
            let tagsArray = [];
            if (Array.isArray(parsedData.tags)) {
              tagsArray = parsedData.tags;
            } else {
              tagsArray = JSON.parse(parsedData.tags);
            }
            const hasSeenTag = {};
            const filteredTags = tagsArray.reduce((acc, tag) => {
              if (!hasSeenTag[tag]) {
                hasSeenTag[tag] = true;
                return acc.concat(tag);
              } else {
                return acc;
              }
            }, []);
            obj.translations[lang].tags = filteredTags;
          }
        };

        langBasedArticleData(parsedEn, "en");
        langBasedArticleData(parsedKh, "kh");

        if (isUpdated) {
          /// For storing time stamps of the editors.
          const date = new Date();
          obj.update_info.push({
            editor_id: req.user_id,
            updated_at: date,
            user_type: req.user_type,
          });
          obj.is_updating = false;
          const updateStatus = await obj.save();
          if (
            req.body.remove_article_images &&
            req.body.remove_article_images.length > 0
          ) {
            obj.article_images = obj.article_images.filter((img) => {
              if (req.body.remove_article_images.includes(img))
                fs.unlink(img, (err) => console.log(err));
              else return img;
            });
          }
          if (req.body.remove_article_video) {
            fs.unlink(req.body.remove_article_video, (err) => console.log(err));
          }

          // console.log(updateStatus);
          if (updateStatus) {
            result.articleUpdated = true;
            return result;
          } else {
            result.articleNotUpdates = true;
            return result;
          }
        } else {
          result.noArticleFoundWithId = true;
          return result;
        }
      } else {
        result.noArticleFoundWithId = true;
        return result;
      }
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

//Route to like article
exports.likeArticle = async (req) => {
  let result = {};

  const { article_id } = req.body;

  if (req.user_type === "end_user") {
    try {
      //find article requested by user
      const articleData = await article.findOne({ _id: article_id });

      if (articleData) {
        const index = articleData.liked_by.indexOf(req.user_id);
        if (index >= 0) {
          articleData.liked_by.splice(index, 1);
          result.unliked = true;
        } else {
          articleData.liked_by.push(req.user_id);
          result.liked = true;
        }

        result.article = await article.findOneAndUpdate(
          { _id: article_id },
          { ...articleData },
          {
            new: true,
          }
        );

        result.likeCount = result.article.liked_by.length;

        return result;
      }
    } catch (error) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

//Route to save article
exports.saveArticle = async (req) => {
  let result = {};

  const { article_id } = req.body;

  if (req.user_type === "end_user") {
    try {
      //find article requested by user
      const articleData = await article.findOne({ _id: article_id });

      if (articleData) {
        const index = articleData.saved_by.indexOf(req.user_id);
        if (index >= 0) {
          articleData.saved_by.splice(index, 1);
          result.saved = false;
        } else {
          articleData.saved_by.push(req.user_id);
          result.saved = true;
        }

        result.article = await article.findOneAndUpdate(
          { _id: article_id },
          { ...articleData },
          {
            new: true,
          }
        );

        return result;
      }
    } catch (error) {
      result.databaseError = true;
      return result;
    }
  }
};

exports.removeFavouriteArticles = async (req) => {
  let results = {};

  if (req.user_type == "end_user") {
    try {
      for (let id of req.body.article_ids) {
        const obj = await article.findById(id);

        if (obj) {
          obj.saved_by.pull(req.user_id);
          obj.save();
        }
      }
      results.articlesRemovedFromFavourite = true;
      return results;
    } catch (e) {
      results.databaseError = true;
      return results;
    }
  } else {
    results.accessRestricted = true;
    return results;
  }
};

//Route to get user's favorite articles
//Route to get all article for end-user / admins
exports.getFavouriteArticle = async (req, page, limit) => {
  let result = {};

  // if user is end user fetch all articles
  if (req.user_type === "end_user") {
    try {
      if (page > 0 && limit > 0)
        articleData = await article
          .find({ saved_by: { $in: [req.user_id] } })
          .skip((page - 1) * limit)
          .limit(limit)
          .populate("category", "category_name category_id")
          .populate("author", "fname lname")
          .populate("media_images")
          .populate("media_video")
          .sort({ createdAt: -1 });
      else
        articleData = await article
          .find({ saved_by: { $in: [req.user_id] } })
          .populate("category", "category_name category_id")
          .populate("author", "fname lname")
          .populate("media_images")
          .populate("media_video")
          .sort({ createdAt: -1 });

      let articleCounts = await article.countDocuments({
        saved_by: { $in: [req.user_id] },
      });

      if (articleData.length) {
        if (page > 0 && limit > 0) {
          const response = ArticleHelper.paginateData(
            page,
            limit,
            articleCounts
          );

          response.total_articles_count = articleCounts;
          response.article = articleData;
          response.current_page_articles_count = response.article.length;

          if (response.article.length === 0) {
            result.articlesNotFound = true;
            return result;
          }

          delete response.startIndex;
          delete response.endIndex;

          result.article = { ...response };
          return result;
        }

        result.article = articleData;
        return result;
      } else {
        result.articlesNotFound = true;
        return result;
      }
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }
  }
};

exports.sortArticles = async (req, params, page, limit) => {
  let results = {},
    articleCounts = 0;

  const selectedLanguage = params.lang
    ? ["en", "kh"].includes(params.lang.toLowerCase())
      ? params.lang.toLowerCase()
      : "en"
    : "en";

  if (req.user_type === "end_user") {
    try {
      results.articles = [];

      commonFilters = [
        {
          $lookup: {
            from: "article_views",
            let: {
              article_id: "$_id",
            },

            as: "article_views",
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ["$article_id", "$$article_id"],
                  },
                },
              },

              {
                $project: {
                  user_id: 1,
                  _id: 1,
                },
              },
            ],
          },
        },
        ...generalFilters,
        {
          $project: {
            author: 1,
            category: 1,
            province: 1,
            translations: 1,
            article_images: 1,
            media_images: 1,
            media_video: 1,
            liked_by: 1,
            saved_by: 1,
            createdAt: 1,
            updatedAt: 1,
            __v: 1,
            length: {
              $add: [
                { $size: "$article_views" },
                { $multiply: [{ $size: "$saved_by" }, 2] },
              ],
            },
          },
        },
      ];

      if (params.category_id) {
        commonFilters.unshift({
          $match: {
            category: {
              $in: [mongoose.Types.ObjectId(params.category_id)],
            },
          },
        });
      }

      const sortBy = {
        popularity: {
          $sort: {
            saved_by: 1,
          },
        },
        date: {
          $sort: {
            createdAt: -1,
          },
        },
        alphabetical: {
          $sort: {
            [`translations.${selectedLanguage}.title`]: 1,
          },
        },
        popularity_alphabetical: {
          $sort: {
            saved_by: 1,
            [`translations.${selectedLanguage}.title`]: 1,
          },
        },
        popularity_date: {
          $sort: {
            saved_by: 1,
            createdAt: -1,
          },
        },
      };

      if (
        params.sort_by !== "popularity" &&
        (!params.category_id || params.category_id === "")
      ) {
        // throw error
      }

      const finalQuery = [...commonFilters, sortBy[params.sort_by]];

      if (page > 0) {
        finalQuery.push({ $skip: (page - 1) * limit });
      }

      if (limit > 0) {
        finalQuery.push({ $limit: limit || 10 });
      }

      results.articles = await article.aggregate(finalQuery);

      articleCounts = await article.countDocuments(
        params.category_id
          ? {
              category: params.category_id,
            }
          : {}
      );

      if (results.articles.length) {
        if (page > 0 && limit > 0) {
          const response = ArticleHelper.paginateData(
            page,
            limit,
            articleCounts
          );

          response.total_articles_count = articleCounts;
          response.articles = results.articles;
          response.current_page_articles_count = response.articles.length;

          if (response.articles.length === 0) {
            result.articlesNotFound = true;
            return result;
          }

          delete response.startIndex;
          delete response.endIndex;

          results = { ...response };
          return results;
        }

        return results;
      } else {
        results.articlesNotFound = true;
        return results;
      }
    } catch (err) {
      console.log(err);
      results.databaseError = true;
      return results;
    }
  } else {
    results.unAuthorised = true;
    return results;
  }
};

exports.filterArticles = async (req, page, limit) => {
  let results = {},
    articleCounts = 0;

  const selectedLanguage = req.body.lang
    ? ["en", "kh"].includes(req.body.lang.toLowerCase())
      ? req.body.lang.toLowerCase()
      : "en"
    : "en";

  const commonFilters = [
    {
      $lookup: {
        from: "article_views",
        let: {
          article_id: "$_id",
        },

        as: "article_views",
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$article_id", "$$article_id"],
              },
            },
          },

          {
            $project: {
              user_id: 1,
              _id: 1,
            },
          },
        ],
      },
    },
    ...generalFilters,
    {
      $project: {
        author: 1,
        category: 1,
        province: 1,
        translations: 1,
        article_images: 1,
        media_images: 1,
        media_video: 1,
        liked_by: 1,
        saved_by: 1,
        createdAt: 1,
        updatedAt: 1,
        __v: 1,
        length: {
          $add: [
            { $size: "$article_views" },
            { $multiply: [{ $size: "$saved_by" }, 2] },
          ],
        },
      },
    },
    {
      $sort: {
        createdAt: -1,
      },
    },
  ];

  if (page && page > 0) {
    commonFilters.push({ $skip: (page - 1) * limit });
  }

  if (limit && limit > 0) {
    commonFilters.push({ $limit: limit });
  }

  if (req.user_type === "end_user") {
    try {
      results.articles = [];

      const filterOptions = {
        "category": {
          $match: {
            category: {
              $in: [mongoose.Types.ObjectId(req.body.category_id)],
            },
          },
        },
        "province": {
          $match: {
            province: {
              $in: [mongoose.Types.ObjectId(req.body.province_id)],
            },
          },
        },
        "category-and-sub-category": {
          $match: {
            $and: [
              {
                category: {
                  $in: [mongoose.Types.ObjectId(req.body.category_id)],
                },
              },
              {
                sub_category: {
                  $in: [mongoose.Types.ObjectId(req.body.sub_category_id)],
                },
              },
            ],
          },
        },
        "category-and-province": {
          $match: {
            $and: [
              {
                category: {
                  $in: [mongoose.Types.ObjectId(req.body.category_id)],
                },
              },
              {
                province: {
                  $in: [mongoose.Types.ObjectId(req.body.province_id)],
                },
              },
            ],
          },
        },
        "title": {
          $or: [
            { tags: { $in: [req.body.title] } },
            {
              [`translations.${selectedLanguage}.title`]: {
                $regex: req.body.title,
                $options: "i",
              },
            },
          ],
        },
      };

      if (req.body.filter_by === "title") {
        // COMMON FILTER THAT WILL ALWAYS GET APPLIED IF TYPE IS TITLE
        let filters;

        // THIS WILL ONLY BE APPLIED IF WE HAVE CATEGORY_ID AND TYPE IS TITLE
        if (req.body.category_id) {
          filters = {
            $and: [
              {
                category: {
                  $in: [mongoose.Types.ObjectId(req.body.category_id)],
                },
              },
              { ...filterOptions[req.body.filter_by] },
            ],
          };
        } else {
          filters = { ...filterOptions[req.body.filter_by] };
        }
        results.articles = await article.aggregate([
          {
            $match: filters,
          },
          ...commonFilters,
        ]);

        articleCounts = await article.countDocuments(
          filterOptions[req.body.filter_by]
        );
      } else {
        results.articles = await article.aggregate([
          filterOptions[req.body.filter_by],
          ...commonFilters,
        ]);

        articleCounts = await article.countDocuments(
          filterOptions[req.body.filter_by]["$match"]
        );
      }
      // else if (req.body.filter_by == "popularity_province") {
      //   if (page > 0 && limit > 0) {
      //     skipPage = { $skip: (page - 1) * limit };
      //     limitPage = { $limit: limit };
      //     sortingFilter = [
      //       { $sort: { createdAt: -1 } },
      //       skipPage,
      //       limitPage,
      //     ];
      //   } else {
      //     sortingFilter = [
      //       ...commonFilters,
      //       { $sort: { createdAt: -1 } },
      //     ];
      //   }
      //   results.articles = await article.aggregate(sortingFilter);

      //   articleCounts = (
      //     await article.aggregate([
      //       {
      //         $project: {
      //           author: 1,
      //           title: 1,
      //           category: 1,
      //           description: 1,
      //           province: 1,
      //           article_content: 1,
      //           article_images: 1,
      //           liked_by: 1,
      //           saved_by: 1,
      //           createdAt: 1,
      //           updatedAt: 1,
      //           __v: 1,
      //           length: {
      //             $size: "$saved_by",
      //           },
      //         },
      //       },
      //       { $sort: { createdAt: -1 } },
      //     ])
      //   ).length;
      // }

      if (results.articles.length) {
        if (page > 0 && limit > 0) {
          const response = ArticleHelper.paginateData(
            page,
            limit,
            articleCounts
          );

          response.total_articles_count = articleCounts;
          response.articles = results.articles;
          response.current_page_articles_count = response.articles.length;

          if (response.articles.length === 0) {
            result.articlesNotFound = true;
            return result;
          }

          delete response.startIndex;
          delete response.endIndex;

          return response;
        }

        return results;
      } else {
        results.articlesNotFound = true;
        return results;
      }
    } catch (err) {
      console.log(err);
      results.databaseError = true;
      return results;
    }
  } else {
    results.unAuthorised = true;
    return result;
  }
};

exports.markArticleAsFeatured = async (req) => {
  let result = {};

  const { user_type, user_id } = req;
  const articleIds = req.body.article_ids;

  try {
    if (["super_admin", "discover"].includes(user_type)) {
      // check if more than 10 articles are featured or not

      const featuredArticlesCount = await article.countDocuments({
        is_featured: true,
      });

      if (featuredArticlesCount >= 10) {
        result.limitReached = true;
        return result;
      }

      const currentLength = featuredArticlesCount + articleIds.length;

      if (currentLength > 10) {
        result.limitReached = true;
        result.limit = 11 - featuredArticlesCount;
        return result;
      }

      for (let id of req.body.article_ids) {
        try {
          const obj = await article.findById(id);

          if (obj) {
            obj.is_featured = true;
            await obj.save();
          }
        } catch (err) {
          console.log(err);
          continue;
        }
      }

      result.articlesMarkedAsFeatured = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
};

exports.unmarkArticleFromFeatured = async (req) => {
  const { user_type, user_id } = req;
  const articleIds = req.body.article_ids;
  let result = {};

  try {
    if (["super_admin", "discover"].includes(user_type)) {
      for (let id of req.body.article_ids) {
        try {
          const obj = await article.findById(id);

          if (obj) {
            obj.is_featured = false;
            await obj.save();
          }
        } catch (err) {
          continue;
        }
      }

      result.articlesUnMarkedFromFeatured = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
};

exports.getFeaturedArticles = async (user_type, page, limit) => {
  let result = {};

  try {
    if (["super_admin", "discover", "end_user"].includes(user_type)) {
      if (page > 0 && limit > 0)
        obj = await article
          .find({ is_featured: true })
          .skip((page - 1) * limit)
          .limit(limit)
          .populate("category")
          .populate("author", "fname lname")
          .populate("media_images")
          .populate("media_video")
          .sort({ createdAt: -1 });
      else
        obj = await article
          .find({ is_featured: true })
          .populate("category")
          .populate("author", "fname lname")
          .populate("media_images")
          .populate("media_video")
          .sort({ createdAt: -1 });

      if (obj.length) {
        if (page > 0 && limit > 0) {
          const articleCounts = await article.countDocuments({
            is_featured: true,
          });
          const response = ArticleHelper.paginateData(
            page,
            limit,
            articleCounts
          );

          response.total_articles_count = articleCounts;
          response.featuredArticles = obj;
          response.current_page_articles_count =
            response.featuredArticles.length;

          if (response.featuredArticles.length === 0) {
            result.noArticleFound = true;
            return result;
          }

          delete response.startIndex;
          delete response.endIndex;

          result.featuredArticles = { ...response };
          return result;
        }

        result.featuredArticles = obj;
        return result;
      }

      result.noArticleFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
};

exports.updateAuthor = async (old_author, new_author) => {
  const result = {};

  // find all articles belong to old user and update

  try {
    // count how many articles user has.
    const articleCount = await article
      .countDocuments({ author: old_author })
      .limit(2);

    let updateStatus = false;

    if (articleCount > 1) {
      updateStatus = await article.updateMany(
        { author: old_author },
        { author: new_author }
      );
    } else if (articleCount === 1) {
      updateStatus = await article.updateOne(
        { author: old_author },
        { author: new_author }
      );
    } else {
      updateStatus = false;
    }

    return updateStatus;
  } catch (err) {
    console.log(err);
  }
};

/// DASHBOARD DATA

/// -------------------------

/// SUPER ADMIN
exports.getSuperAdminDashboardData = async (req) => {
  let result = {};
  let data = {};

  const commonFilters = [
    {
      $lookup: {
        from: "system_users",
        as: "author",
        localField: "author",
        foreignField: "_id",
      },
    },
    {
      $unwind: "$author",
    },
    {
      $lookup: {
        from: "medialibraries",
        localField: "media_images",
        foreignField: "_id",
        as: "media_images",
      },
    },
    {
      $lookup: {
        from: "medialibraries",
        localField: "media_video",
        foreignField: "_id",
        as: "media_video",
      },
    },
    {
      $unwind: {
        path: "$media_video",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "categories",
        let: {
          category_id: "$category",
        },

        as: "category",
        pipeline: [
          {
            $match: {
              $expr: {
                $in: ["$_id", "$$category_id"],
              },
            },
          },

          {
            $project: {
              translations: 1,
              _id: 1,
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "provinces",
        let: {
          province_id: "$province",
        },

        as: "province",
        pipeline: [
          {
            $match: {
              $expr: {
                $in: ["$_id", "$$province_id"],
              },
            },
          },
          {
            $project: {
              _id: 1,
              translations: 1,
            },
          },
        ],
      },
    },
  ];

  try {
    if (req.user_type != "super_admin") {
      result.unAuthorised = true;
      return result;
    }

    /// All discover admin count
    data.discover_admin_count = await SystemUsers.countDocuments({
      admin_type: "discover",
    });

    /// All author count
    data.author_count = await SystemUsers.countDocuments({
      discover_type: "author",
    });

    /// All editor count
    data.editor_count = await SystemUsers.countDocuments({
      discover_type: "editor",
    });

    /// Most popular
    let filter = [
      ...commonFilters,
      {
        $project: {
          "author.fname": 1,
          "author.lname": 1,
          "category": 1,
          "province": 1,
          "translations": 1,
          "article_images": 1,
          "media_images": 1,
          "media_video": 1,
          "liked_by": 1,
          "saved_by": 1,
          "createdAt": 1,
          "updatedAt": 1,
          "__v": 1,
          "length": {
            $size: "$saved_by",
          },
        },
      },
      { $sort: { length: -1, createdAt: -1 } },
      {
        $limit: 10,
      },
    ];

    data.most_popular = await article.aggregate(filter);

    /// Featured

    data.featured = await article
      .find({ is_featured: true })
      .populate("author", "fname lname")
      .populate("media_images")
      .populate("media_video");

    /// Articles by category
    filter = [
      ...commonFilters,
      {
        $group: {
          _id: "$category.translations.en.category_name",
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          _id: 0,
          category: "$_id",
          count: 1,
        },
      },
    ];

    data.articles_by_cat = await article.aggregate(filter);

    /// Last seven days articles
    filter = {};
    filter["createdAt"] = {
      $lte: moment().toDate(),
      $gt: moment().subtract(7, "days"),
    };
    data.last_seven = await article.aggregate([
      {
        $match: {
          createdAt: {
            $gt: new Date(moment().subtract(7, "days")),
          },
        },
      },
      ...commonFilters,
    ]);

    const mostViewedCommonFilters = [
      {
        $lookup: {
          from: "system_users",
          as: "article.author",
          localField: "article.author",
          foreignField: "_id",
        },
      },
      {
        $unwind: "$article.author",
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "article.media_images",
          foreignField: "_id",
          as: "article.media_images",
        },
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "article.media_video",
          foreignField: "_id",
          as: "article.media_video",
        },
      },
      {
        $unwind: {
          path: "$article.media_video",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "categories",
          let: {
            category_id: "$article.category",
          },

          as: "article.category",
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$category_id"],
                },
              },
            },

            {
              $project: {
                translations: 1,
                _id: 1,
              },
            },
          ],
        },
      },
      {
        $project: {
          "article.article_images": 1,
          "article.media_images": 1,
          "article.media_video": 1,
          "article.is_updating": 1,
          "article.share_count": 1,
          "article.saved_by": 1,
          "article.is_featured": 1,
          "article.title": 1,
          "article.description": 1,
          "article.category": 1,
          "article.sub_category": 1,
          "article.status": 1,
          "article.createdAt": 1,
          "article.updatedAt": 1,
          "article.author.fname": 1,
          "article.author.lname": 1,

          "count": 1,
        },
      },
    ];
    /// Top 10 most viewed
    filter = [
      {
        $group: {
          _id: "$article_id",
          count: { $sum: 1 },
        },
      },
      {
        $sort: {
          count: -1,
        },
      },
      {
        $lookup: {
          from: "articles",
          as: "article",
          localField: "_id",
          foreignField: "_id",
        },
      },
      {
        $unwind: "$article",
      },
      ...mostViewedCommonFilters,
      {
        $limit: 10,
      },
    ];

    data.most_viewed = await articleView.aggregate(filter);

    /// Most viewed in last seven days
    filter = [
      {
        $match: {
          createdAt: {
            $gte: new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000),
          },
        },
      },
      {
        $group: {
          _id: "$article_id",
          count: { $sum: 1 },
        },
      },
      {
        $sort: {
          count: -1,
        },
      },
      {
        $lookup: {
          from: "articles",
          let: {
            article_id: "$_id",
          },

          as: "article",
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$article_id"],
                },
              },
            },
          ],
        },
      },
      {
        $unwind: "$article",
      },
      ...mostViewedCommonFilters,
      {
        $limit: 10,
      },
    ];

    data.most_viewed_seven = await articleView.aggregate(filter);

    result.success = true;
    result.message = "Super admin data fetched successfully.";
    result.dashboardData = data;
    return result;
  } catch (e) {
    console.log(e);
    result.databaseError = true;
    result.message = "Something went wrong in fetching admin data";
    return result;
  }
};

/// DISCOVER ADMIN
exports.getDiscoverAdminDashboardData = async (req) => {
  let result = {};
  let data = {};
  console.log(req.user_type);
  if (req.user_type != "discover") {
    result.unAuthorised = true;
    return result;
  }

  try {
    /// All author count
    data.author_count = await SystemUsers.countDocuments({
      discover_type: "author",
    });

    /// All editor count
    data.editor_count = await SystemUsers.countDocuments({
      discover_type: "editor",
    });

    /// Most popular
    let filter = [
      {
        $project: {
          author: 1,
          title: 1,
          category: 1,
          description: 1,
          province: 1,
          article_content: 1,
          article_images: 1,
          media_images: 1,
          media_video: 1,
          liked_by: 1,
          saved_by: 1,
          createdAt: 1,
          updatedAt: 1,
          __v: 1,
          length: {
            $size: "$saved_by",
          },
        },
      },
      { $sort: { length: -1, createdAt: -1 } },
      {
        $lookup: {
          from: "system_users",
          as: "author",
          localField: "author",
          foreignField: "_id",
        },
      },
      {
        $unwind: "$author",
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "media_images",
          foreignField: "_id",
          as: "media_images",
        },
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "media_video",
          foreignField: "_id",
          as: "media_video",
        },
      },
      {
        $unwind: {
          path: "$media_video",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "categories",
          let: {
            category_id: "$category",
          },

          as: "category",
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$category_id"],
                },
              },
            },

            {
              $project: {
                translations: 1,
                _id: 1,
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: "provinces",
          let: {
            province_id: "$province",
          },

          as: "province",
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$province_id"],
                },
              },
            },
            {
              $project: {
                _id: 1,
                translations: 1,
              },
            },
          ],
        },
      },
      {
        $project: {
          "author.fname": 1,
          "author.lname": 1,
          "translations": 1,
          "category": 1,
          "province": 1,
          "article_images": 1,
          "media_images": 1,
          "media_video": 1,
          "liked_by": 1,
          "saved_by": 1,
          "createdAt": 1,
          "updatedAt": 1,
          "__v": 1,
        },
      },
      {
        $limit: 10,
      },
    ];

    data.most_popular = await article.aggregate(filter);

    /// Featured

    data.featured = await article
      .find({ is_featured: true })
      .populate("author", "fname lname")
      .populate("media_images")
      .populate("media_video");

    /// Articles by category

    filter = [
      {
        $lookup: {
          from: "categories",
          let: {
            category_id: "$category",
          },

          as: "category",
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$category_id"],
                },
              },
            },

            {
              $project: {
                translations: 1,
                _id: 1,
              },
            },
          ],
        },
      },
      { $unwind: "$category" },
      {
        $group: {
          _id: "$category.translations.en.category_name",
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          _id: 0,
          category: "$_id",
          count: 1,
        },
      },
    ];

    data.articles_by_cat = await article.aggregate(filter);

    /// Last seven days articles
    filter = {};
    filter["createdAt"] = {
      $lte: moment().toDate(),
      $gt: moment().subtract(7, "days"),
    };
    data.last_seven = await article
      .find(filter)
      .populate("author", "fname lname")
      .populate("media_images")
      .populate("media_video");

    /// Top 10 most viewed

    filter = [
      {
        $group: {
          _id: "$article_id",
          count: { $sum: 1 },
        },
      },
      {
        $sort: {
          count: -1,
        },
      },
      {
        $lookup: {
          from: "articles",
          as: "article",
          localField: "_id",
          foreignField: "_id",
        },
      },
      {
        $unwind: "$article",
      },
      {
        $lookup: {
          from: "system_users",
          as: "article.author",
          localField: "article.author",
          foreignField: "_id",
        },
      },
      {
        $unwind: "$article.author",
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "article.media_images",
          foreignField: "_id",
          as: "article.media_images",
        },
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "article.media_video",
          foreignField: "_id",
          as: "article.media_video",
        },
      },
      {
        $unwind: {
          path: "$article.media_video",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          "article.article_images": 1,
          "article.media_images": 1,
          "article.media_video": 1,
          "article.is_updating": 1,
          "article.share_count": 1,
          "article.saved_by": 1,
          "article.is_featured": 1,
          "article.title": 1,
          "article.description": 1,
          "article.category": 1,
          "article.sub_category": 1,
          "article.status": 1,
          "article.createdAt": 1,
          "article.updatedAt": 1,
          "article.author.fname": 1,
          "article.author.lname": 1,

          "count": 1,
        },
      },
      {
        $limit: 10,
      },
    ];

    data.most_viewed = await articleView.aggregate(filter);

    /// Most viewed in last seven days

    filter = [
      {
        $match: {
          createdAt: {
            $gte: new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000),
          },
        },
      },
      {
        $group: {
          _id: "$article_id",
          count: { $sum: 1 },
        },
      },
      {
        $sort: {
          count: -1,
        },
      },
      {
        $lookup: {
          from: "articles",
          let: {
            article_id: "$_id",
          },

          as: "article",
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$article_id"],
                },
              },
            },
          ],
        },
      },
      {
        $unwind: "$article",
      },
      {
        $lookup: {
          from: "system_users",
          as: "article.author",
          localField: "article.author",
          foreignField: "_id",
        },
      },
      {
        $unwind: "$article.author",
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "article.media_images",
          foreignField: "_id",
          as: "article.media_images",
        },
      },
      {
        $lookup: {
          from: "medialibraries",
          localField: "article.media_video",
          foreignField: "_id",
          as: "article.media_video",
        },
      },
      {
        $unwind: {
          path: "$article.media_video",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          "article.article_images": 1,
          "article.media_video": 1,
          "article.media_images": 1,
          "article.is_updating": 1,
          "article.share_count": 1,
          "article.saved_by": 1,
          "article.is_featured": 1,
          "article.title": 1,
          "article.description": 1,
          "article.category": 1,
          "article.sub_category": 1,
          "article.status": 1,
          "article.createdAt": 1,
          "article.updatedAt": 1,
          "article.author.fname": 1,
          "article.author.lname": 1,

          "count": 1,
        },
      },

      {
        $limit: 10,
      },
    ];

    data.most_viewed_seven = await articleView.aggregate(filter);

    result.success = true;
    result.message = "Discover admin data fetched successfully.";
    result.dashboardData = data;
    return result;
  } catch (e) {
    console.log(e);
    result.databaseError = true;
    result.message = "Something went wrong in fetching admin data";
    return result;
  }
};

/// AUTHOR
exports.getDiscoverAuthorDashboardData = async (req) => {
  let result = {};
  const { user_type, user_id } = req;

  if (user_type === "author") {
    try {
      let dashboardData = {};

      // 1. Author's total article
      dashboardData.authorsTotalArticles = await article.countDocuments({
        author: user_id,
      });

      // 2. Total article in db
      dashboardData.totalArticles = await article.countDocuments({});

      // 3. Last 7 days
      dashboardData.last7DaysArticles = await article.aggregate([
        {
          $match: {
            author: mongoose.Types.ObjectId(user_id),
            createdAt: {
              $gte: new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000),
            },
          },
        },
        {
          $sort: {
            createdAt: -1,
          },
        },
        {
          $lookup: {
            from: "system_users",
            as: "author",
            localField: "author",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$author",
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_images",
            foreignField: "_id",
            as: "media_images",
          },
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_video",
            foreignField: "_id",
            as: "media_video",
          },
        },
        {
          $unwind: {
            path: "$media_video",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            "author.fname": 1,
            "author.lname": 1,
            "article_images": 1,
            "media_images": 1,
            "media_video": 1,
            "is_featured": 1,
            "title": 1,
            "description": 1,
            "status": 1,
            "createdAt": 1,
            "updatedAt": 1,
          },
        },
      ]);

      // 4. Recent published article
      dashboardData.recentPublishedArticles = await article.aggregate([
        {
          $match: {
            author: mongoose.Types.ObjectId(user_id),
            status: "active",
          },
        },
        {
          $sort: {
            createdAt: -1,
          },
        },
        {
          $lookup: {
            from: "system_users",
            as: "author",
            localField: "author",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$author",
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_images",
            foreignField: "_id",
            as: "media_images",
          },
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_video",
            foreignField: "_id",
            as: "media_video",
          },
        },
        {
          $unwind: {
            path: "$media_video",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            "author.fname": 1,
            "author.lname": 1,
            "article_images": 1,
            "media_images": 1,
            "media_video": 1,
            "is_featured": 1,
            "title": 1,
            "description": 1,
            "status": 1,
            "createdAt": 1,
            "updatedAt": 1,
          },
        },
      ]);

      if (dashboardData) {
        result.dashboardData = dashboardData;
        result.success = true;
        result.message = "Dashboard data for author";
      } else {
        result.dataNotFound = true;
        result.message = "Dashboard data for author not found";
      }

      return result;
    } catch (error) {
      console.log(error);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

/// EDITOR
exports.getDiscoverEditorDashboardData = async (req) => {
  let result = {};
  const { user_type, user_id } = req;

  if (user_type === "editor") {
    try {
      let dashboardData = {};

      // 1. Total article in db
      dashboardData.totalArticles = await article.countDocuments({});

      // 2. Most popular articles in db
      dashboardData.mostPopularArticles = await article.aggregate([
        {
          $addFields: {
            saveCount: { $size: "$saved_by" },
          },
        },
        {
          $sort: {
            saveCount: -1,
          },
        },
        {
          $lookup: {
            from: "system_users",
            as: "author",
            localField: "author",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$author",
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_images",
            foreignField: "_id",
            as: "media_images",
          },
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_video",
            foreignField: "_id",
            as: "media_video",
          },
        },
        {
          $unwind: {
            path: "$media_video",
            preserveNullAndEmptyArrays: true,
          },
        },

        {
          $project: {
            "article_images": 1,
            "media_images": 1,
            "media_video": 1,
            "is_featured": 1,
            "author.fname": 1,
            "author.lname": 1,
            "title": 1,
            "description": 1,
            "status": 1,
            "saveCount": 1,
            "createdAt": 1,
            "updatedAt": 1,
          },
        },
      ]);

      // 3. Featured articles
      dashboardData.featuredArticles = await article.aggregate([
        {
          $match: { is_featured: true },
        },
        {
          $sort: {
            createdAt: -1,
          },
        },
        {
          $lookup: {
            from: "system_users",
            as: "author",
            localField: "author",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$author",
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_images",
            foreignField: "_id",
            as: "media_images",
          },
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_video",
            foreignField: "_id",
            as: "media_video",
          },
        },
        {
          $unwind: {
            path: "$media_video",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            "article_images": 1,
            "media_images": 1,
            "media_video": 1,
            "author.fname": 1,
            "author.lname": 1,
            "is_featured": 1,
            "title": 1,
            "description": 1,
            "status": 1,
            "createdAt": 1,
            "updatedAt": 1,
          },
        },
        {
          $limit: 10,
        },
      ]);

      // 4. Total edited
      dashboardData.totalEdited = await article.aggregate([
        {
          $match: {
            "update_info.editor_id": mongoose.Types.ObjectId(user_id),
          },
        },
        {
          $lookup: {
            from: "system_users",
            as: "author",
            localField: "author",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$author",
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_images",
            foreignField: "_id",
            as: "media_images",
          },
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_video",
            foreignField: "_id",
            as: "media_video",
          },
        },
        {
          $unwind: {
            path: "$media_video",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            "author.fname": 1,
            "author.lname": 1,
            "article_images": 1,
            "media_images": 1,
            "media_video": 1,
            "is_featured": 1,
            "title": 1,
            "description": 1,
            "update_info": 1,
            "status": 1,
            "createdAt": 1,
            "updatedAt": 1,
          },
        },
      ]);

      // 5. Last 7 days
      dashboardData.last7DaysArticles = await article.aggregate([
        {
          $match: {
            status: "active",
          },
        },
        {
          $sort: {
            createdAt: -1,
          },
        },
        {
          $lookup: {
            from: "system_users",
            as: "author",
            localField: "author",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$author",
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_images",
            foreignField: "_id",
            as: "media_images",
          },
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_video",
            foreignField: "_id",
            as: "media_video",
          },
        },
        {
          $unwind: {
            path: "$media_video",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            "author.fname": 1,
            "author.lname": 1,
            "article_images": 1,
            "media_images": 1,
            "media_video": 1,
            "is_featured": 1,
            "title": 1,
            "description": 1,
            "status": 1,
            "createdAt": 1,
            "updatedAt": 1,
          },
        },
      ]);

      // 6. Recent published article
      dashboardData.recentPublishedArticles = await article.aggregate([
        {
          $match: {
            status: "active",
          },
        },
        {
          $sort: {
            createdAt: -1,
          },
        },
        {
          $lookup: {
            from: "system_users",
            as: "author",
            localField: "author",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$author",
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_images",
            foreignField: "_id",
            as: "media_images",
          },
        },
        {
          $lookup: {
            from: "medialibraries",
            localField: "media_video",
            foreignField: "_id",
            as: "media_video",
          },
        },
        {
          $unwind: {
            path: "$media_video",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            "author.fname": 1,
            "author.lname": 1,
            "article_images": 1,
            "media_images": 1,
            "media_video": 1,
            "is_featured": 1,
            "title": 1,
            "description": 1,
            "status": 1,
            "createdAt": 1,
            "updatedAt": 1,
          },
        },
      ]);

      if (dashboardData) {
        result.dashboardData = dashboardData;
        result.success = true;
        result.message = "Dashboard data for editor";
      } else {
        result.dataNotFound = true;
        result.message = "Dashboard data for editor not found";
      }

      return result;
    } catch (error) {
      console.log(error);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

/// --------------------------

/// API FOR FILTER AND SORT COMBINED
exports.getFilteredArticles = async (req, page, limit) => {
  const { user_type, user_id } = req;
  let result = {};
  if (user_type === "end_user") {
    const {
      filter_by,
      sort_by,
      category_id,
      sub_category_id,
      province_id,
      temple_id,
      page,
      limit,
      page_type,
      lang,
    } = req.body;
    // console.log("filter - by", filter_by);
    let basicFilter = [];

    let articleList = [];

    const selectedLanguage = lang
      ? ["en", "kh"].includes(lang.toLowerCase())
        ? lang.toLowerCase()
        : "en"
      : "en";


      const basicFilterOptions = {
        "category": {
          $match: {
            category: { $in: [mongoose.Types.ObjectId(category_id)] },
          },
        },
        "province": {
          $match: {
            province: { $in: [mongoose.Types.ObjectId(province_id)] },
          },
        },
        "temple": {
          $match: {
            temple_id: { $in: [mongoose.Types.ObjectId(temple_id)] },
          },
        },
        "title":{
          $match:{
            $or: [
              { tags: { $in: [req.body.title] } },
              {
                [`translations.${selectedLanguage}.title`]: {
                  $regex: req.body.title,
                  $options: "i",
                },
              },
            ],
          }
        },
        "sub_category":{
          $match:{
            sub_category: {
              $in: [mongoose.Types.ObjectId(sub_category_id)],
            },
          }
        }
      }

    const filterOptions = {
      ...basicFilterOptions,
      "category-and-sub-category": {
        $match: {
          $and: [ basicFilterOptions['category']['$match'],basicFilterOptions['sub_category']['$match']],
        },
      },
      "category-and-province": {
        $match: {
          $and: [ basicFilterOptions['category']['$match'],basicFilterOptions['province']['$match']],
        },
      },
      "category-and-temple": {
        $match: {
          $and: [ basicFilterOptions['category']['$match'],basicFilterOptions['temple']['$match'] ],
        },
      },
      "category-sub_category-province": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['sub_category']['$match'],basicFilterOptions['province']['$match']],
        },
      },
      "category-sub_category-temple": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['sub_category']['$match'],basicFilterOptions['temple']['$match']],
        },
      },
      "category-title": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['title']['$match']],
        },
      },
      "category-title-sub_category": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['title']['$match'],basicFilterOptions['sub_category']['$match']],
        },
      },
      "category-title-province": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['title']['$match'],basicFilterOptions['province']['$match']],
        },
      },
      "category-title-temple": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['title']['$match'],basicFilterOptions['temple']['$match']],
        },
      },
      "category-title-sub_category-province": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['title']['$match'],basicFilterOptions['sub_category']['$match'],basicFilterOptions['province']['$match']],
        },
      },
      "category-title-sub_category-temple": {
        $match: {
          $and: [basicFilterOptions['category']['$match'],basicFilterOptions['title']['$match'],basicFilterOptions['sub_category']['$match'],basicFilterOptions['temple']['$match']],
        },
      },
      "title-sub_category-temple": {
        $match: {
          $and: [basicFilterOptions['title']['$match'],basicFilterOptions['sub_category']['$match'],basicFilterOptions['temple']['$match']],
        },
      },
      "title-sub_category-province": {
        $match: {
          $and: [basicFilterOptions['title']['$match'],basicFilterOptions['sub_category']['$match'],basicFilterOptions['province']['$match']],
        },
      },
      "title-sub_category": {
        $match: {
          $and: [basicFilterOptions['title']['$match'],basicFilterOptions['sub_category']['$match']],
        },
      },
      "title-temple": {
        $match: {
          $and: [basicFilterOptions['title']['$match'],basicFilterOptions['temple']['$match']],
        },
      },
      "title-province": {
        $match: {
          $and: [basicFilterOptions['title']['$match'],basicFilterOptions['province']['$match']],
        },
      },
    };

    const sortOptions = {
      date: {
        $sort: { createdAt: -1 },
      },
      alphabetical: {
        $sort: {
          [`translations.${selectedLanguage}.title`]: 1,
        },
      },
      popularity: {
        $sort: {
          saved_by: 1,
        },
      },
      popularity_alphabetical: {
        $sort: {
          [`translations.${selectedLanguage}.title`]: 1,
          length: -1,
        },
      },
      popularity_date: {
        $sort: {
          saved_by: 1,
        },
        $sort: {
          createdAt: -1,
        },
      },
    };

    // const mostPopularFilterOptions = {
    //   category: {
    //     $match: {
    //       category: { $in: [mongoose.Types.ObjectId(category_id)] },
    //     },
    //   },
    //   popularity_province: {
    //     $match: { province: { $in: [mongoose.Types.ObjectId(province_id)] } },
    //   },
    //   "province": {
    //     $match: {
    //       province: { $in: [mongoose.Types.ObjectId(province_id)] },
    //     },
    //   },
    //   "temple": {
    //     $match:{
    //       temple_id: { $in: [mongoose.Types.ObjectId(temple_id)] },
    //     }
    //   },
    // };

    const commonOptions = [
      {
        $lookup: {
          from: "article_views",
          let: {
            article_id: "$_id",
          },

          as: "article_views",
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$article_id", "$$article_id"],
                },
              },
            },

            {
              $project: {
                user_id: 1,
                _id: 1,
              },
            },
          ],
        },
      },
      ...generalFilters,
      {
        $addFields: {
          length: {
            $add: [
              { $size: "$article_views" },
              { $multiply: [{ $size: "$saved_by" }, 2] },
            ],
          },
        },
      },
      {
        $project: {
          article_images: 1,
          media_images: 1,
          media_video: 1,
          is_featured: 1,
          category: 1,
          sub_category: 1,
          province: 1,
          translations: 1,
          author: 1,
          createdAt: 1,
          updatedAt: 1,
          temple_id: 1,
        },
      },
    ];

    if (filter_by && sort_by) {
      if (
        filter_by === "popularity_province" ||
        ["popularity_date", "popularity_alphabetical"].includes(sort_by)
      ) {
        basicFilter = [
          filterOptions[filter_by],
          ...commonOptions,
          sortOptions[sort_by],
        ];
      } else {
        basicFilter = [
          filterOptions[filter_by],
          ...commonOptions,
          sortOptions[sort_by],
        ];
      }
    }

    try {
      if (page > 0 && limit > 0) {
        skipPage = { $skip: (page - 1) * limit };
        limitPage = { $limit: limit };
        let paginatedFilter = [];
        paginatedFilter = basicFilter;
        paginatedFilter.push(skipPage);
        paginatedFilter.push(limitPage);
        articleList = await article.aggregate(paginatedFilter);
        articleCounts = await article.countDocuments(
          filterOptions[filter_by]["$match"]
        );
        const response = ArticleHelper.paginateData(page, limit, articleCounts);

        response.total_articles_count = articleCounts;
        response.articles = articleList;
        response.current_page_articles_count = response.articles.length;

        if (response.articles.length === 0) {
          result.articlesNotFound = true;
          return result;
        }

        delete response.startIndex;
        delete response.endIndex;

        result = { ...response };
        return result;
      } else {
        articleList = await article.aggregate(basicFilter);

        if (articleList.length) {
          result.articles = articleList;
        } else {
          result.articlesNotFound = true;
        }
      }

      return result;
    } catch (error) {
      console.log("Error at sortBy : ", error);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unauthorizedAccess = true;
    return result;
  }
};
