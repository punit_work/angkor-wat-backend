// Function for adding pagination to given data
function paginateData(page, limit, data) {

    const paginationResult = {};

    if (page > 0 && limit > 0) {

        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;

        paginationResult.startIndex = startIndex;
        paginationResult.endIndex = endIndex;

        if (startIndex > 0) {

            paginationResult.previousPage = {
                page: parseInt(page) - 1,
                limit: parseInt(limit)
            }
        }

        if (endIndex < data) {

            paginationResult.nextPage = {
                page: parseInt(page) + 1,
                limit: parseInt(limit)
            }
        }

        return paginationResult;
    }
}

function getClientIp(req) {
    with (req)
    return (headers['x-forwarded-for'] || '').split(',')[0]
        || connection.remoteAddress
}

module.exports = {
    paginateData,
    getClientIp
}