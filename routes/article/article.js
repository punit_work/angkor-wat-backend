const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const {
  removeFilesIfExist,
  doFileExist,
  doFilesExist,
} = require("../../middleware/check-file.js");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");
const fs = require("fs");

//helper and controller
const ArticleController = require("./article-controller");
const ArticleHelper = require("./article-helper");

const uploadArticleImage = require("../../middleware/upload-article-image");

//middleware
const authCheck = require("../../middleware/check-auth");

//Add article
router.post(
  "/add-article",
  uploadArticleImage.fields([
    { name: "article_images" },
    { name: "article_video" },
  ]),
  authCheck,
  [
    // body("title")
    //   .notEmpty()
    //   .withMessage("Title for article is required"),
    // body("description")
    //   .notEmpty()
    //   .withMessage("Description for article is required"),
    body("category").notEmpty().withMessage("Category for article is required"),
    body("sub_category")
      .notEmpty()
      .withMessage("Sub Category for article is required"),
    // body("article_content")
    //   .optional()
    //   .trim()
    //   .isLength({ min: 1 })
    //   .withMessage("Article Content is required"),
    // body("article_description")
    //   .trim()
    //   .isLength({ min: 1 })
    //   .withMessage("Article Description is required in atleast one language"),
    body("province").notEmpty().withMessage("Province is requrired"),
    body("media_images")
      .optional()
      .notEmpty()
      .withMessage("Please provide valid media"),
    body("media_video")
      .optional()
      .isMongoId()
      .withMessage("Please provide valid id for media_video"),
    body("temple_id")
      .optional()
      .notEmpty()
      .withMessage("Please provide valid id for temple"),
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    // if (req.body.article_description) {

    //   const article_description = JSON.parse(req.body.article_description);

    //   if (article_description.length === 0) {

    //     if (req.files.article_video) {
    //       doFilesExist(req.files.article_video);
    //     }

    //     if (req.files.article_images) {
    //       doFilesExist(req.files.article_images);
    //     }

    //     return Response.error(
    //       res,
    //       ResponseCode.UNPROCESSABLE_ENTITY,
    //       "Please provide article description in atleast one language"
    //     );

    //   }

    // }

    if (req.body['en']) {
      const parsedData = JSON.parse(req.body['en']);

      const removeFiles = () => {
        if (req.files.article_video) {
          doFilesExist(req.files.article_video);
        }

        if (req.files.article_images) {
          doFilesExist(req.files.article_images);
        }
      };

      if (
        !parsedData.description ||
        (parsedData.description && parsedData.description.trim().length === 0)
      ) {
        removeFiles();

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          `Please provide article description in [${'en'}] language`
        );
      }

      if (
        !parsedData.title ||
        (parsedData.title && parsedData.title.trim().length === 0)
      ) {
        removeFiles();

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          `Please provide article title in [${'en'}] language`
        );
      }

      if (
        !parsedData.article_content ||
        (parsedData.article_content &&
          parsedData.article_content.trim().length === 0)
      ) {
        removeFiles();
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          `Please provide article_content in [${'en'}] language`
        );
      }
    }
    if (req.body['kh']) {
      const parsedData = JSON.parse(req.body['kh']);

      const removeFiles = () => {
        if (req.files.article_video) {
          doFilesExist(req.files.article_video);
        }

        if (req.files.article_images) {
          doFilesExist(req.files.article_images);
        }
      };

      if (
        !parsedData.description ||
        (parsedData.description && parsedData.description.trim().length === 0)
      ) {
        removeFiles();

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          `Please provide article description in [${'kh'}] language`
        );
      }

      if (
        !parsedData.title ||
        (parsedData.title && parsedData.title.trim().length === 0)
      ) {
        removeFiles();

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          `Please provide article title in [${'kh'}] language`
        );
      }

      if (
        !parsedData.article_content ||
        (parsedData.article_content &&
          parsedData.article_content.trim().length === 0)
      ) {
        removeFiles();
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          `Please provide article_content in [${'kh'}] language`
        );
      }
    }

    if (req.body.media_images) {
      const data = JSON.parse(req.body.media_images);
      if (!Array.isArray(data)) {
        if (req.files.article_video) {
          doFilesExist(req.files.article_video);
        }

        if (req.files.article_images) {
          doFilesExist(req.files.article_images);
        }

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide media field as array"
        );
      } else if (data.length === 0) {
        if (req.files.article_video) {
          doFilesExist(req.files.article_video);
        }

        if (req.files.article_images) {
          doFilesExist(req.files.article_images);
        }

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide atleaset 1 media [ image / video ]"
        );
      }
    }

    const response = await ArticleController.addArticle(req);

    if (response.unAuthorised) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    }
    if (response.databaseError) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.article)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_ADDED_SUCCESSFULLY,
        response.article
      );
  }
);

//Get all articles
router.get("/get-all-articles", authCheck, async (req, res) => {
  const { page, limit } = req.query;

  // Sending error resposne if page && limit not valid
  if ((page && !limit) || (!page && limit) || page < 0 || limit < 0)
    return Response.error(
      res,
      ResponseCode.NOT_FOUND,
      ResponseMessage.ERROR_NOT_FOUND
    );

  const response = await ArticleController.getAllArticle(
    req,
    parseInt(page),
    parseInt(limit)
  );

  if (response.unAuthorised) {
    return Response.error(
      res,
      ResponseCode.UNAUTHORIZED_ACCESS,
      ResponseMessage.ERROR_UNAUTHORIZED
    );
  } else if (response.articlesNotFound) {
    return Response.error(
      res,
      ResponseCode.NOT_FOUND,
      ResponseMessage.NO_ARTICLE_FOUND
    );
  } else if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.article)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
      response.article
    );
});

//Get single article by id
router.post("/get-article-by-id", authCheck, async (req, res) => {
  const response = await ArticleController.getArticleByID(req);

  if (response.articlesNotFound) {
    return Response.error(
      res,
      ResponseCode.NOT_FOUND,
      ResponseMessage.NO_ARTICLE_FOUND
    );
  }
  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.article)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
      response.article
    );
});

//Delete articles by given array of ids
router.post(
  "/delete-articles",
  authCheck,
  [
    body("articles")
      .isArray({ min: 1 })
      .withMessage("Parameter [articles] is missing or invalid"),
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ArticleController.deleteArticle(req);

    if (response.unAuthorised)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.success)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_DELETED_SUCCESSFULLY
      );
    if (response.failuer)
      return Response.success(
        res,
        ResponseCode.DATABASE_ERROR,
        "Article is not deleted " + ResponseMessage.ERROR_UNKNOWN
      );
    else
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.ERROR_UNKNOWN
      );
  }
);

//Like articles
router.post(
  "/like-article",
  authCheck,
  [body("article_id").notEmpty().withMessage("Article id is required")],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ArticleController.likeArticle(req);

    if (response.unAuthorised)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    if (response.liked)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_LIKED,
        response.likeCount
      );
    if (response.unliked)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_UNLIKED,
        response.likeCount
      );
    else
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.ERROR_UNKNOWN
      );
  }
);

router.post(
  "/upload-remove-content-image",

  uploadArticleImage.single("content_image"),

  [body("is_remove").optional().isArray().withMessage("is_remove is invalid")],

  authCheck,

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    if (req.body.is_remove) {
      for (let path of req.body.is_remove) {
        fs.unlink(path, (err) => console.log(err));
      }

      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Image deleted successfully"
      );
    } else {
      console.log(req.file);

      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Image uploaded successfully",
        req.file.path.replace(/\\/g, "/")
      );
    }
  }
);

/// Make is_updating flag down while updating and to get the status of update.
router.get(
  "/get_update_status",
  authCheck,

  [
    query("article_id")
      .notEmpty()
      .isMongoId()
      .withMessage("Parameter [article_id] is invalid or missing"),
  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ArticleController.checkUpdate(req);

    if (response.success)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Update Status Fetched Successfully",
        response.isUpdating
      );
    if (response.unAuthorised)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    if (response.noArticleFound)
      return Response.error(res, ResponseCode.NOT_FOUND, "Article Not Found");
    else
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.ERROR_UNKNOWN
      );
  }
);

/// Make is_updating flag down.
router.get(
  "/set_update_false",
  authCheck,

  [
    query("article_id")
      .notEmpty()
      .isMongoId()
      .withMessage("Parameter [article_id] is invalid or missing"),
  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ArticleController.setUpdateFalse(req);

    if (response.success)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Update Status Updated Successfully"
      );
    if (response.unAuthorised)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    if (response.noArticleFound)
      return Response.error(res, ResponseCode.NOT_FOUND, "Article Not Found");
    else
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.ERROR_UNKNOWN
      );
  }
);

// Update Article
router.post(
  "/update-article",

  uploadArticleImage.fields([
    { name: "article_images" },
    { name: "article_video" },
  ]),

  authCheck,
  [
    body("article_id")
      .notEmpty()
      .isMongoId()
      .withMessage("Parameter [article_id] is invalid or missing."),
    body("category")
      .optional()
      .notEmpty()
      .withMessage("Parameter [category] is invalid."),
    body("sub_category")
      .optional()
      .notEmpty()
      .withMessage("Parameter [sub_category] is invalid."),
    body("province")
      .optional()
      .notEmpty()
      .withMessage("Parameter [province] is invalid."),
    body("en").isString().withMessage("Parameter [en] language is invalid."),
    body("kh").isString().withMessage("Parameter [kh] language is invalid."),
    body("status")
      .optional()
      .isString()
      .withMessage("Parameter [status] is invalid."),
    // body("article_description")
    //   .optional()
    //   .trim()
    //   .isLength({ min: 1 })
    //   .withMessage("Article Description is required in atleast one language"),
    body("media_images")
      .optional()
      .notEmpty()
      .withMessage("Please provide valid media images"),
    body("media_video")
      .optional()
      .isMongoId()
      .withMessage("Please provide valid id for media_video"),
    body("temple_id")
      .optional()
      .notEmpty()
      .withMessage("Please provide valid id for temple"),
  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

      if (req.body['en']) {
        const parsedData = JSON.parse(req.body['en']);

        const removeFiles = () => {
          if (req.files.article_video) {
            doFilesExist(req.files.article_video);
          }

          if (req.files.article_images) {
            doFilesExist(req.files.article_images);
          }
        };

        if (
          !parsedData.description ||
          (parsedData.description && parsedData.description.trim().length === 0)
        ) {
          removeFiles();

          return Response.error(
            res,
            ResponseCode.UNPROCESSABLE_ENTITY,
            `Please provide article description in [${'en'}] language`
          );
        }

        if (
          !parsedData.title ||
          (parsedData.title && parsedData.title.trim().length === 0)
        ) {
          removeFiles();

          return Response.error(
            res,
            ResponseCode.UNPROCESSABLE_ENTITY,
            `Please provide article title in [${'en'}] language`
          );
        }

        if (
          !parsedData.article_content ||
          (parsedData.article_content &&
            parsedData.article_content.trim().length === 0)
        ) {
          removeFiles();
          return Response.error(
            res,
            ResponseCode.UNPROCESSABLE_ENTITY,
            `Please provide article_content in [${'en'}] language`
          );
        }
      }
      if (req.body['kh']) {
        const parsedData = JSON.parse(req.body['kh']);

        const removeFiles = () => {
          if (req.files.article_video) {
            doFilesExist(req.files.article_video);
          }

          if (req.files.article_images) {
            doFilesExist(req.files.article_images);
          }
        };

        if (
          !parsedData.description ||
          (parsedData.description && parsedData.description.trim().length === 0)
        ) {
          removeFiles();

          return Response.error(
            res,
            ResponseCode.UNPROCESSABLE_ENTITY,
            `Please provide article description in [${'kh'}] language`
          );
        }

        if (
          !parsedData.title ||
          (parsedData.title && parsedData.title.trim().length === 0)
        ) {
          removeFiles();

          return Response.error(
            res,
            ResponseCode.UNPROCESSABLE_ENTITY,
            `Please provide article title in [${'kh'}] language`
          );
        }

        if (
          !parsedData.article_content ||
          (parsedData.article_content &&
            parsedData.article_content.trim().length === 0)
        ) {
          removeFiles();
          return Response.error(
            res,
            ResponseCode.UNPROCESSABLE_ENTITY,
            `Please provide article_content in [${'kh'}] language`
          );
        }
      }



    if (req.body.status) {
      if (req.body.status != "active" && req.body.status != "draft") {
        if (req.files.article_video) {
          doFilesExist(req.files.article_video);
        }

        if (req.files.article_images) {
          doFilesExist(req.files.article_images);
        }

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide status 'draft' or 'active' only."
        );
      }
    }

    if (req.body.media_images) {
      const data = JSON.parse(req.body.media_images);
      if (!Array.isArray(data)) {
        if (req.files.article_video) {
          doFilesExist(req.files.article_video);
        }

        if (req.files.article_images) {
          doFilesExist(req.files.article_images);
        }

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide media field as array"
        );
      } else if (data.length === 0) {
        if (req.files.article_video) {
          doFilesExist(req.files.article_video);
        }

        if (req.files.article_images) {
          doFilesExist(req.files.article_images);
        }

        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide atleaset 1 media [ image / video ]"
        );
      }
    }

    const response = await ArticleController.updateArticle(req);

    if (response.unAuthorised) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    }
    if (response.databaseError) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }

      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    }
    if (response.noArticleFoundWithId) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }

      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.ARTICLE_UPDATE_FAILED
      );
    }
    if (response.articleNotUpdates) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }

      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.ARTICLE_UPDATE_FAILED
      );
    }
    if (response.articleInUpdate) {
      if (req.files.article_video) {
        doFilesExist(req.files.article_video);
      }

      if (req.files.article_images) {
        doFilesExist(req.files.article_images);
      }

      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        "Article is being edited by another editor."
      );
    }
    if (response.articleUpdated)
      return Response.success(
        res,
        ResponseCode.CREATED,
        ResponseMessage.ARTICLE_UPDATED_SUCCESSFULLY
      );
  }
);

//Save articles
router.post(
  "/save-article",
  authCheck,
  [body("article_id").notEmpty().withMessage("Article id is required")],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ArticleController.saveArticle(req);

    if (response.unAuthorised)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    if (response.saved)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_SAVED,
        response
      );
    if (!response.saved)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_UNSAVED,
        response
      );
    else
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.ERROR_UNKNOWN
      );
  }
);

//Get favourite articles by user
router.get("/get-favourite-articles", authCheck, async (req, res) => {
  const { page, limit } = req.query;

  // Sending error resposne if page && limit not valid
  if ((page && !limit) || (!page && limit) || page < 0 || limit < 0)
    return Response.error(
      res,
      ResponseCode.NOT_FOUND,
      ResponseMessage.ERROR_NOT_FOUND
    );

  const response = await ArticleController.getFavouriteArticle(
    req,
    parseInt(page),
    parseInt(limit)
  );

  if (response.articlesNotFound) {
    return Response.error(
      res,
      ResponseCode.NOT_FOUND,
      ResponseMessage.NO_ARTICLE_FOUND
    );
  }
  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.article)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
      response.article
    );
});

router.post(
  "/remove-articles-from-favourite",

  authCheck,

  [
    body("article_ids")
      .isArray({ min: 1 })
      .withMessage("Parameter [article_ids] is missing or invalid"),
  ],

  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ArticleController.removeFavouriteArticles(req);

    // Sending error response if database error
    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_UNKNOWN
      );
    // Sending error response if no admin found
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than admin"
      );
    // Sending success response if shops unmarked from featured
    else if (response.articlesRemovedFromFavourite)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_REMOVED_FROM_FAVOURITE
      );
  }
);

router.get("/sort-articles", authCheck, async (req, res) => {
  const { page, limit } = req.query;

  // Sending error resposne if page && limit not valid
  if ((page && !limit) || (!page && limit) || page < 0 || limit < 0)
    return Response.error(
      res,
      ResponseCode.NOT_FOUND,
      ResponseMessage.ERROR_NOT_FOUND
    );

  const response = await ArticleController.sortArticles(
    req,
    req.query,
    parseInt(page),
    parseInt(limit)
  );
  if (response.articlesNotFound) {
    return Response.error(
      res,
      ResponseCode.NOT_FOUND,
      ResponseMessage.NO_ARTICLE_FOUND
    );
  } else if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.unAuthorised) {
    return Response.error(
      res,
      ResponseCode.UNAUTHORIZED_ACCESS,
      ResponseMessage.UNAUTHORIZED_ACCESS
    );
  } else if (response)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
      response
    );
});

router.post(
  "/filter-articles",
  authCheck,
  [
    body("filter_by")
      .notEmpty()
      .isString()
      .withMessage("Enter proper filter_by type."),
    body("category_id")
      .optional()
      .isMongoId()
      .withMessage("Enter proper category_id."),
    body("sub_category_id")
      .optional()
      .isMongoId()
      .withMessage("Enter proper sub_category_id."),
    body("province_id")
      .optional()
      .isMongoId()
      .withMessage("Enter proper province id."),
    body("title").optional().isString().withMessage("title is required"),
  ],
  async (req, res) => {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const { page, limit } = req.body;

    // Sending error resposne if page && limit not valid
    if ((page && !limit) || (!page && limit) || page < 0 || limit < 0)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.ERROR_NOT_FOUND
      );

    const filter = req.body.filter_by;

    // nested validation for filter

    if (filter === "category") {
      if (!req.body.category_id)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide a category id"
        );
    }
    if (filter === "province") {
      if (!req.body.province_id)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide a province id"
        );
    }
    if (filter === "category-and-sub-category") {
      if (!(req.body.category_id && req.body.sub_category_id))
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide a category id and sub category id"
        );
    }
    if (filter === "category-and-province") {
      if (!(req.body.category_id && req.body.province_id))
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide a category id and province id"
        );
    }
    if (filter === "title") {
      if (!req.body.title)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide title"
        );
    }

    const response = await ArticleController.filterArticles(
      req,
      parseInt(page),
      parseInt(limit)
    );
    if (response.articlesNotFound) {
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_ARTICLE_FOUND
      );
    } else if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.unAuthorised) {
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.UNAUTHORIZED_ACCESS
      );
    } else if (response)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
        response
      );
  }
);

router.get(
  "/get-featured-articles",

  authCheck,

  async (req, res) => {
    const { page, limit } = req.query;

    // Sending error resposne if page && limit not valid
    if ((page && !limit) || (!page && limit) || page < 0 || limit < 0)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.ERROR_NOT_FOUND
      );

    const response = await ArticleController.getFeaturedArticles(
      req.user_type,
      parseInt(page),
      parseInt(limit)
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noArticleFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_ARTICLE_FOUND
      );
    else if (response.featuredArticles)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
        response.featuredArticles
      );
  }
);

//To perform batch operations
router.get("/update-batch-article", async (req, res) => {
  const response = await ArticleController.updateAllArticle();
});
/// Get dashboard API for admin dashboard analytics.

router.get(
  "/get-admin-dashboard-analytics",

  authCheck,

  [query("type").notEmpty().isString().withMessage("Enter proper type.")],

  async (req, res) => {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ArticleController.getAdminDashboardAnalytics(req);

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noArticleFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_ARTICLE_FOUND
      );
    else if (response.noFilterFound)
      return Response.error(res, ResponseCode.NOT_FOUND, "Enter proper type.");
    else if (response.infoFound)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
        response.info
      );
  }
);

/// Filters and sorting API.
router.post(
  "/get-filtered-sorted-articles",
  authCheck,
  [
    body("filter_by")
      .optional()
      .trim()
      .isLength({ min: 1 })
      .withMessage("Please give valid filter option"),
    body("sort_by")
      .optional()
      .trim()
      .isLength({ min: 1 })
      .withMessage("Please give valid sort option"),
    body("category_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter category is invalid"),
    body("sub_category_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter sub_category is invalid"),
    body("title").optional().not().isEmpty().withMessage("Title is required"),
    body("province_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter province is invalid"),
    body("temple_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter province is invalid"),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    // check if user has sent category field to filter
    if (!errors.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        errors.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    if (!(req.body.filter_by || req.body.sort_by))
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide sort or filter options"
      );

    // FILTER

    if (req.body.filter_by) {
      if (
        ![
          "category",
          "province",
          "temple",
          "category-and-sub-category",
          "category-and-province",
          "category-and-temple",
          "category-sub_category-province",
          "category-sub_category-temple",
          "category-title",
          "category-title-sub_category",
          "category-title-province",
          "category-title-temple",
          "category-title-sub_category-province",
          "category-title-sub_category-temple",
          "title-sub_category-temple",
          "title-sub_category-province",
          "title-sub_category",
          "title-temple",
          "title-province",
          "title"
        ].includes(req.body.filter_by)
      )
        return Response.error(
          res,
          ResponseCode.BAD_REQUEST,
          "Please provide valid filter options"
        );

      const filter_by = req.body.filter_by;

      if (filter_by === "category-and-province") {
        if (!req.body.province_id)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide province_id"
          );
      }

      if (filter_by === "category-and-temple") {
        if (!req.body.temple_id)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide temple_id"
          );
      }

      if (filter_by === "category-and-sub-category") {
        if (!req.body.sub_category_id)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide category-and-sub-category id"
          );
      }

      if (filter_by === "category-sub_category-province") {
        const { province_id, category_id, sub_category_id } = req.body;

        if (!(province_id && category_id && sub_category_id)) {
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide province_id , category_id , sub_category_id"
          );
        }
      }
    }

    // SORT

    if (req.body.sort_by) {
      if (
        ![
          "popularity",
          "date",
          "alphabetical",
          "popularity_alphabetical",
          "popularity_date",
        ].includes(req.body.sort_by)
      )
        return Response.error(
          res,
          ResponseCode.BAD_REQUEST,
          "Please provide valid sort options from [ popularity, date, alphabetical, popularity_alphabetical,popularity_date]"
        );
    }

    const { page, limit } = req.body;

    // Sending error resposne if page && limit not valid
    if ((page && !limit) || (!page && limit) || page < 0 || limit < 0)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.ERROR_NOT_FOUND
      );

    // get all the articles according to the filter & sort applied
    const response = await ArticleController.getFilteredArticles(
      req,
      page,
      limit
    );

    // check if invalid user is trying to access
    if (response.invalidUser)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.INVALID_USER_TYPE
      );
    // check if category is found or not
    else if (response.categoryError)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.NO_CATEGORY_FOUND
      );
    // check if database Error
    else if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    // check if any articles are found using applied category
    else if (response.articlesNotFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_ARTICLE_FOUND
      );
    // check if category is found or not
    else if (response.inValidFilterSelection)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide valid filter selection"
      );
    // send articles in response
    else if (response)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.ARTICLE_FETCHED_SUCCESSFULLY,
        response
      );
  }
);

module.exports = router;

