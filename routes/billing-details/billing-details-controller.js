//Models
const billingDetails = require("../../model/BillingDetails");
const mongoose = require('mongoose');

//Add Billing Details
exports.addBillingDetails = async req => {
  let result = {};
  const billingData = { ...req.body };
  const {user_type,user_id} = req

  billingData.user_id = user_id;

  if(user_type === 'end_user'){

    try {

      result.billingDetails = await new billingDetails(billingData).save();
      return result;
  
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }

  }
  else{
    result.unAuthorised = true;
    return result;
  }

  
};

// Get Billing Details 
exports.getBillingDetails = async req => {
  let result = {};
  const {user_type,user_id} = req

  
    try {
      

      result.billingDetails = await billingDetails.aggregate([
        {
          "$match":{user_id:mongoose.Types.ObjectId(req.user_id)}
        },
        {
          "$lookup": {
            "from": "provinces",
            "as": "province",
            "localField": "province",
            "foreignField": "_id"
          }
      },
      {
        "$unwind":{
          "path":"$province",
          "preserveNullAndEmptyArrays":true
        }
      },
      {
        "$addFields":{
          "district": {"$filter": {
            "input": '$province.district',
           "as": 'var',
            "cond": {"$eq": ['$$var._id', '$district']}
        }},
        }

      },
      {
        "$unwind":{
          "path":"$district",
          "preserveNullAndEmptyArrays":true
        }
      },
      {
        "$addFields":{
          "commune": {"$filter": {
            "input": '$district.commune',
           "as": 'var',
            "cond": {"$eq": ['$$var._id', '$commune']}
        }},
        }

      },
      {
        "$unwind":{
          "path":"$commune",
          "preserveNullAndEmptyArrays":true
        }
      },
      {
        "$project":{
          "full_name":1,
          "email":1,
          "phone_number":1,
          "user_id":1,
          "createdAt":1,
          "updatedAt":1,
          "province._id":1,
          "province.province_name":"$province.province",
          "province.country":1,
          "district._id":1,
          "district.district_name":1,
          "commune":1,
          "postal_code":1,
          "address_line_1":1,
          "address_line_2":1
        }
      }
      ])

      if (!result.billingDetails.length) {
        result.noBillingDetails = true ; 
        return result ; 
      }

      return result;

    } catch (err) {
      result.databaseError = true;
      return result;
    }
};

//Update Billing Details
exports.updateBillingDetails = async req => {
  let result = {};
  const billingData = { ...req.body };
  const {user_type,user_id} = req;
  const {id} = req.params;

  if(user_type === 'end_user'){

    try {

      // find billing details exist or not

      const myquery = {user_id:user_id,_id:id};
      const newvalues = { $set: billingData };

      const existingBillingDetails = await billingDetails.findOne(myquery);



      if(existingBillingDetails){

        const updated = await billingDetails.updateOne(myquery,newvalues);

        if(updated.nModified > 0){

          result.updated = true;
          return result;

        }
        else{
          result.nothingUpdated = true;
          return result;
        }

      }
      else{

        result.billingDetailsNotFound = true;
        return result;

      }

     
  
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }

  }
  else{
    result.unAuthorised = true;
    return result;
  }

  
};

//Remove Billing Details