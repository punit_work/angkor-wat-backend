const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const BillingController = require("./billing-details-controller");
const BillingHelper = require("./billing-helper");

const authCheck = require("../../middleware/check-auth");

// Path - /v1/hotel/
// All Routes for hotel

router.post(
  "/add-billing-details",
  authCheck,
  [
    body("full_name")
      .notEmpty()
      .withMessage("Full name is required"),
    body("email")
    .notEmpty()
    .withMessage("Email is required"),
    body("phone_number")
      .notEmpty()
      .withMessage("Phone number is required"),
    body("address_line_1")
      .optional()
      .notEmpty()
      .withMessage("Address line 1 is required"),
    body("address_line_2")
    .optional()
    .notEmpty()
    .withMessage("Address line 2 is required"),
    body("province")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid province id"),
    body("district")
      .optional() 
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid district id"),
    body("commune")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid commune id"),
    body("postal_code")
      .optional()
      .notEmpty()
      .isNumeric()
      .withMessage("Please provide valid postal code")
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    const response = await BillingController.addBillingDetails(req);

    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.billingDetails)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Billing Details Added successfully",
        response.billingDetails
      );
  }
);



//Get hotels
//v1/hotel/get-hotel
router.get("/get-billing-details", authCheck ,  async (req, res) => {

  const error = validationResult(req);
  if (!error.isEmpty()) {
    return Response.error(
      res,
      ResponseCode.UNPROCESSABLE_ENTITY,
      error.array().map(error => ({
        field: error.param,
        errorMessage: error.msg
      }))
    );
  }

  const response = await BillingController.getBillingDetails(req);

  if(response.noBillingDetails){
      return Response.error(
          res,
          ResponseCode.NOT_FOUND,
          "Billing details not found"
      );
  }

  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.billingDetails)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      "Billing Details retrived successfully",
      response.billingDetails
    );
});

router.post(
  "/update-billing-details/:id",
  authCheck,
  [
    body("full_name")
      .optional()
      .notEmpty()
      .withMessage("Full name is required"),
    body("email")
      .optional()
      .notEmpty()
      .withMessage("Email is required"),
    body("phone_number")
      .optional()
      .notEmpty()
      .withMessage("Phone number is required"),
    body("address_line_1")
      .optional()
      .notEmpty()
      .withMessage("Address line 1 is required"),
    body("address_line_2")
      .optional()
      .notEmpty()
      .withMessage("Address line 2 is required"),
    body("province")
      .optional()
      .isMongoId()
      .withMessage("Please provide valid province id"),
    body("district")
      .optional() 
      .isMongoId()
      .withMessage("Please provide valid district id"),
    body("commune")
      .optional()
      .isMongoId()
      .withMessage("Please provide valid commune id"),
    body("postal_code")
      .optional()
      .isNumeric()
      .withMessage("Please provide valid postal code")
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    if(req.body.constructor === Object && Object.keys(req.body).length === 0) {
      return Response.error( res,ResponseCode.UNPROCESSABLE_ENTITY, "You need to provide atleast 1 entity to process update" );
    }

    const response = await BillingController.updateBillingDetails(req);

    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } 
    if(response.billingDetailsNotFound){
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        "Billing Details Not Found"
      );
    }
    if(response.nothingUpdated){
      return Response.success(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
       "Nothing to update !"
      );

    }
    else if (response.updated)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
       "Billing Details Updated successfully"
      );
  }
);

module.exports = router;
