//Models
const mongoose = require('mongoose'); 
const bugReport = require("../../model/BugReport");
const BugReportHelper = require("./bug-report-helper");

//Add Bug
exports.addBug = async req => {

  let result = {};

  const { user_id, user_type} = req;
  const {module,title,description} = req.body;

  if(['super_admin', 'discover', 'author','editor','commerce','business_owner','end_user'].includes(user_type)){

    try {

        // Create bug id
        const bug_id = BugReportHelper.generateBugId(user_id);
        // Bind all details
        const data = {
          module,
          title,
          description,
          bug_id,
          user:user_id
        }

        const generateBug = await bugReport.create(data);

        if(generateBug){
          result.success = true;
          result.message = `Bug Successfully added with Bug ID : ${bug_id}`;
          result.data = generateBug
        }
        else{
          result.error = true;
          result.message = "Error at posting bug";
        }

        return result;
        

    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }

  }
  else{
    result.unAuthorised = true;
    return result;

  }
 
};


//Get bug
exports.getBugs = async req =>{

  let result = {};

  const { user_id, user_type} = req;
  const {by_status,by_id,by_priority} = req.query;
  let page,limit;

  if(['super_admin','discover','commerce'].includes(user_type)){

    if(req.query.page && req.query.limit){

      page = parseInt(req.query.page);
      limit = parseInt(req.query.limit);
  
    }
  
    try{
  
      const bugs = await bugReport.find({}).populate('user','fname lname');
  
      if(bugs && bugs.length > 0){
  
        result.success = true;
        result.data = bugs;
  
  
  
      }
      else{
        result.dataNotFound = true;
      }
  
      return result;
  
  
    }
    catch(err){
  
      console.log(err);
      result.databaseError = true;
      return result;
  
  
    }
  


  }
  else{
    result.unAuthorised = true;
    return result;
  }



}


// //Delete media
// exports.deleteMedia = async req =>{

//   let result = {};
//   const {user_id,user_type} = req;

//   const {media_ids} = req.body;

 
//   try{

//     const mappedMediaIds = media_ids.map((id) => {
//       return mongoose.Types.ObjectId(id)
//     })
  

//     const mediaData = await mediaLibrary.find({ _id: { $in: mappedMediaIds } });

//     // Delete from local
//     if(mediaData && mediaData.lenght > 0){

//       for(data of mediaData){

//         fs.unlink(data.path, err => console.log(err));
  
//       }
//     }

//     // Delete from db
//     const deleteStatus = await mediaLibrary.deleteMany( { _id: { $in: mappedMediaIds } });

//     deleteStatus.deletedCount > 0 ? result.success = true : result.error = true;
  
//     if(result.success){
//       result.message = `Deleted ${deleteStatus.deletedCount} media`;
//     } 
//     if(result.error){
//       result.message = `Noting deleted !`;
//     }
  
//     return result;

//   }
//   catch(err){

//     console.log(err);
//     result.databaseError = true;
//     return result;


//   }


// }