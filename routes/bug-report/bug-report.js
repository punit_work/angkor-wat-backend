const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const BugReportController = require("./bug-report-controller");
const BugReportHelper = require("./bug-report-helper");

const authCheck = require("../../middleware/check-auth");




// Path - /v1/media-library/
// All Routes for media library
router.post(
  "/add-bug",
  authCheck,
  [
    body("module")
    .notEmpty()
    .withMessage("Please select valid module from ['Discover', 'Shop', 'Experience','Organization'] "),
    body("title")
    .notEmpty()
    .withMessage("Please give valid title"),
    body("description")
    .notEmpty()
    .withMessage("Please give valid description")

  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    if(!['Discover', 'Shop', 'Experience','Organization'].includes(req.body.module)){

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide valid module name from [ 'Discover', 'Shop', 'Experience','Organization' ]"
      )

    }


    const response = await BugReportController.addBug(req);

    if(response.unAuthorised){

      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      )

    }

    if (response.error) {

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
       response.message
      );
    }

    if (response.databaseError) {

      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.success)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        response.message,
        response.data
      );
  }
);

//Get Media
//v1/media-library/get-media
router.get("/get-bugs", 

authCheck,

[
  query('by_status').optional().isString().withMessage("by_status status is required"),
  query('by_id').isString().withMessage("by_id is required"),
  query('by_priority').optional().isString().withMessage("by_priority is required"),
  query('page').optional().isFloat({ min: 1}),
  query('limit').optional().isFloat({ min: 1})
],

async (req, res) => {

  const response = await BugReportController.getBugs(req);

  if(response.unAuthorised){
    return Response.error(
      res,
      ResponseCode.UNAUTHORIZED_ACCESS,
      ResponseMessage.ERROR_UNAUTHORIZED
  );

  }

  if(response.dataNotFound){
      return Response.error(
          res,
          ResponseCode.NOT_FOUND,
          ResponseMessage.ERROR_NOT_FOUND
      );
  }

  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.success)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      "Data retrived successfully !!",
      response.data
    );
});

// // Delete files

// router.post("/delete-media", 

//   authCheck,
  
//   [

//     body('media_ids').isArray({min: 1})

//   ],

//   async (req, res) => {

//     const error = validationResult(req);
//     if (!error.isEmpty()) {
//       return Response.error(
//         res,
//         ResponseCode.UNPROCESSABLE_ENTITY,
//         error.array().map(error => ({
//           field: error.param,
//           errorMessage: error.msg
//         }))
//       );
//     }

//     const response = await MediaLibraryController.deleteMedia(req);

//     if(response.unAuthorised){

//       return Response.error(
//         res,
//         ResponseCode.UNAUTHORIZED_ACCESS,
//         ResponseMessage.ERROR_UNAUTHORIZED
//       )

//     }

//     if (response.error) {

//       return Response.error(
//         res,
//         ResponseCode.UNPROCESSABLE_ENTITY,
//        response.message
//       );

//     }

//     if (response.databaseError) {

//       return Response.error(
//         res,
//         ResponseCode.DATABASE_ERROR,
//         ResponseMessage.ERROR_DATABASE
//       );
//     } 
//     else if (response.success){

//       return Response.success(
//         res,
//         ResponseCode.SUCCESS,
//         response.message,
//         response.data
//       );
//     }
    
  

// })



module.exports = router;
