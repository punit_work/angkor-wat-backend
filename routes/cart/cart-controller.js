//Models
const product = require('../../model/Product');
const systemUsers = require('../../model/SystemUsers');
const promocode = require('../../model/Promocode');
const cart = require('../../model/Cart');
const { find } = require('../../model/Cart');
const mongoose = require('mongoose');
const service = require('../../model/Service');
const experienceCart = require('../../model/ExperienceCart');


// action model data => add - Cart - Product

// FOR SHOP

//Add products to cart !if product already exist increment it's quantity
exports.addCartProduct = async (req) => {

    let result = {}

    if (req.user_type === "end_user") {

        const cartData = { ...req.body, user_id: req.user_id }

        try {

            

            //Check if quantity is > 0  or not    
            const productData = await product.findOne({ 
                _id: cartData.product_id, 
                variants : { $elemMatch: { _id :cartData.variant_id, available_quantity:{$gt:cartData.quantity}} }
            },{
                variants : { $elemMatch: { _id :cartData.variant_id, available_quantity:{$gt:cartData.quantity}} }
            }
            ).populate('shop_id',{'_id':1,'business_owner_id':1});
          
           
            if (!productData) {
                result.productOutOfStock = true;
                return result;
            }

            cartData.shop = productData.shop_id._id;
            cartData.business_owner = productData.shop_id.business_owner_id; 

            //Check if product is already in cart or not
            const cartProduct = await cart.findOne({
                 user_id: cartData.user_id, 
                 product: cartData.product_id , 
                 variant: cartData.variant_id});

            if (cartProduct) {

                //if product in cart than check for quantity
                //Check if required quantity is available or not    

                // const isProductInStock = await product.findOne({ _id: cartData.product_id, available_quantity: { $gt: cartProduct.quantity } })

                // if (!isProductInStock) {
                //     result.productOutOfStock = true;
                //     return result;
                // }

                // if sufficient quantity is available
                // decrement from product data - will be moved to order place time
                // productData.available_quantity = productData.available_quantity - 1;
                // await productData.save();

                //increment in cart

                cartProduct.quantity = cartData.quantity;
                await cartProduct.save();
                result.cart = cartProduct;
                return result;
            }
            // new product to cart assign quantity 1 

            cartData.quantity = cartData.quantity;
            cartData.product = cartData.product_id;
            cartData.variant = cartData.variant_id;
            result.cart = await new cart(cartData).save();

            // decrement from product data - will be moved to order place time
            // productData.available_quantity = productData.available_quantity - 1;
            // await productData.save();

            return result;
        }
        catch (err) {
            console.log(err);
            result.databaseError = true;
            return result;
        }

    }
    else {

    }

}

//Get products from cart
exports.getAllCartProducts = async (req) => {

    let result = {};

    if (req.user_type === "end_user") {

        try {

            const cartData = {};

            

            const cartItems = await cart.aggregate([
                {
                    "$match":{
                        user_id:mongoose.Types.ObjectId(req.user_id)
                    }
                },
                {
                    "$lookup": {
                      "from": "products",
                      "as": "product",
                      "localField": "product",
                      "foreignField": "_id"
                    }
                },
                {"$unwind":"$product"},
                {
                    "$lookup":{
                      "from":"shops",
                      "as":"shop",
                      "localField":"product.shop_id",
                      "foreignField":"_id"
                    }
                  },
                  {
                    "$unwind":"$shop"
                  },
                  {
                    "$lookup":{
                        "from":"system_users",
                        "as":"business_owner",
                        "localField":"shop.business_owner_id",
                        "foreignField":"_id"
                      }

                },
                {
                    "$unwind":"$business_owner"
                },
                {
                    "$project":{
                        "product.variants": {"$filter": {
                            "input": '$product.variants',
                           "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$variant']}
                        }},
                        "product._id":1,
                        "product.product_images":1,
                        "product.quantity":"$quantity",
                        "product.product_name":1,
                        "product.category":1,
                        "product.sub_category":1,
                        "product.createdAt":1,
                        "product.updatedAt":1,
                        "product.cart_item_id":"$_id",
                        "product.attribute":"$attribute",
                        "shop.shop_name":1,
                        "shop.province":1,
                        "shop.business_owner_id":1,
                        "business_owner.fname":1,
                        "business_owner.lname":1,
                        "business_owner._id":1,
                        "shop._id":1
                    }
                },
                {
                    "$unwind":"$product.variants"
                },
                {
                    "$addFields":{
                        "product.shop":"$shop",
                        "product.variants.cost_price":{
                             "$subtract":["$product.variants.price","$product.attribute.promotion.value"]
                        }
                    }
                },
                {
                    "$group":{
                        "_id":"$business_owner._id",
                        "cart_item_id":{"$first":"$_id"},
                        "business_owner":{"$first":"$business_owner"},
                        "products":{"$push":"$product"}
                    }
                },
                {
                    "$addFields":
                        { "productsPrice":
                            {
                              "$map":
                                 {
                                   input: "$products",
                                   as: "product",
                                   in: {"$multiply": [ "$$product.quantity", "$$product.variants.cost_price" ]}
                                 }
                            }
                        }
                },
                {
                    "$addFields":{
                      "shop_total":{"$sum":"$productsPrice"}
                    }
                },
                {
                   
                    "$project":{
                     "products":1,
                     "shop_total":1,
                     "business_owner":1,
                    }
                
                 }
                
            ])


            // const cartItems = await cart.find({ user_id: req.user_id },{
                
            //         variants : { $elemMatch: { _id :cartData.variant_id, unit_quantity:{$gt:0}} }
                
            // }).populate("product");

            if (cartItems.length) {

                cartData.cartItems = cartItems;

                //function to find subtotal
                const totalCost = cartItems.reduce((total, data) => {
                  return  total + data.shop_total;
                }, 0);

                cartData.subTotal = totalCost;

      

                result.cart = cartData;
                return result;
            }
            else {
                result.cartIsEmpty = true;
                return result;
            }

        }

        catch (err) {
            console.log(err);
            result.databaseError = true;
            return result;

        }

    }
    else {
        result.unAuthorised = true;
        return result;
    }

}

//Remove single product from cart
exports.removeCartProduct = async (req) => {


    let result = {};

    if (req.user_type === "end_user") {
        try {

            const cartProduct = await cart.findOne({
                user_id: req.user_id, 
                 product: req.params.product_id , 
                 variant: req.params.variant_id
            });

            if (cartProduct) {

                result.cart = await cartProduct.remove();
                return result;

            }
            else {
                result.productNotFound = true;
                return result;
            }

        }
        catch (err) {
            result.databaseError = true;
            return result;
        }
    }
    else {
        result.unAuthorised = true;
        return result;

    }

}

//Empty cart
exports.emptyCart = async (req) => {

    let result = {};
    if (req.user_type === "end_user") {

        //check if cart data is available or not

        const cartData = await cart.findOne({ user_id: req.user_id });
        if (cartData) {

            const removeCartData = await cart.deleteMany({ user_id: req.user_id });

            if (removeCartData) {
                result.cartEmptySuccessful = true;
                return result;
            }
            else {
                result.databaseError = true;
                return result;
            }

        }
        else {
            result.cartIsEmpty = true;
            return result;
        }


    }
    else {

        result.unAuthorised = true;
        return result;

    }

}

// Update cart
exports.updateCart = async (req) => {

    let result = {};
    const {user_id,user_type} = req;

    const {action} = req.body;


    if(action === 'apply-promocode'){

        const {cart_item_id,product_id,variant_id,code} = req.body;

        let promocodeData ;

        // check if promocode is valid or not

       try {

        const ValidPromocode = await promocode.findOne({product_id: product_id,variant_id: variant_id, code: code, is_active:true});

       
        if(!ValidPromocode){

            result.invalidPromocode = true;
            return result;

        }
        if(!ValidPromocode.is_active){
            result.promocodeExpired = true;
            return result;
        }

                // check for valid user is requesting for apply promocode

                try{

                    const cartData = await cart.findOne({ user_id: user_id,_id: cart_item_id})
        
                    if(!cartData){
        
                        result.cartDataNotFound = true;
                        return result;
                    }

                    // check if promotion is already applied or not

                    if(cartData.attribute.promotion.code){

                        result.promotionAlreadyApplied = true;
                        return result;

                    }
        
                    // if we found valid cart data and valid requested user 
        
                    const productDetails = await product.aggregate([
                        {
                            "$match":{
                                _id:mongoose.Types.ObjectId(product_id)
                            }
                        },
                        {
                            "$project":{
                                "variants": {"$filter": {
                                    "input": '$variants',
                                   "as": 'var',
                                    "cond": {"$eq": ['$$var._id',mongoose.Types.ObjectId(variant_id) ]}
                                }}
                            }
                        },
                        {
                            "$unwind":"$variants"
                        }
        
                    ])
        
                    if(productDetails.length > 0 ){
        
                        const variantPrice = productDetails[0].variants.price;

                        const promotion = {}

                        promotion.code = code;

                       

                        if(ValidPromocode.is_percent){

                            promotion.value = ( variantPrice / 100 ) * ValidPromocode.amount  
                            promotion.discount = ValidPromocode.amount + " %"

                        }
                        else{

                            promotion.value = variantPrice - ValidPromocode.amount  
                            promotion.discount = ValidPromocode.amount + " $"

                        }

                        cartData.attribute.promotion = promotion;
                        
                        result.promotionApplied = await cartData.save();
                        return result;

        
                    }
                    else{
        
                        result.productDetailsNotFound = true;
                        return result;
        
        
                    }
        
        
                }
                catch(error){
        
                    console.log("Error at finding valid cart data",error);
        
                    result.databaseError = true;
                    return result;
        
        
                }



       } catch (error) {

        console.log("Error at finding valid promocode",error);

        result.databaseError = true;
        return result;
           
       }

       






    }

    if(action === 'remove-promocode'){

        const {cart_item_id} = req.body;

        try{

            const cartData = await cart.findOne({ user_id: user_id,_id: cart_item_id})

            if(!cartData){

                result.cartDataNotFound = true;
                return result;
            }

            const promotion = {
                value:0
            }

                cartData.attribute.promotion = promotion;
                
                result.promotionRemoved = await cartData.save();
                return result;


    


        }
        catch(error){

            console.log("Error at finding valid cart data",error);

            result.databaseError = true;
            return result;


        }

    }



}

// GET CART ITEMS COUNTS
exports.getProductCount = async(req) =>{
    let result = {};

    if (req.user_type === "end_user") {
        try {

            const cartProducts = await cart.countDocuments({user_id: req.user_id});

            if(cartProducts > 0){
                result.cartProducts = cartProducts;
                return result;
            }
            else{
                result.cartIsEmpty = true;
                return result;
            }

          
        }
        catch (err) {
            result.databaseError = true;
            return result;
        }
    }
    else {
        result.unAuthorised = true;
        return result;

    }


}


// FOR EXPERIENCE

//Add service to cart !if service already exist increment it's quantity
exports.addCartService = async (req) => {

    let result = {}

    if (req.user_type === "end_user") {

        const cartData = { ...req.body, user_id: req.user_id }

        try {

            // count total quantity [includes all variants] and total price
            const totalQuantity = cartData.service_package.reduce((total,{quantity})=> total+parseInt(quantity),0)

            // check available quantity
            const serviceData = await service.findOne({ _id: cartData.service_id, max_persons_allowed: { $gte: totalQuantity } }).populate('shop_id',{'_id':1,'business_owner_id':1});

            if (!serviceData) {
                result.serviceOutOfStock = true;
                return result;
            }

            cartData.shop = serviceData.shop_id._id;
            cartData.business_owner = serviceData.shop_id.business_owner_id; 

            //Check if product is already in cart or not
            const cartService = await experienceCart.findOne({ user_id: cartData.user_id, service: cartData.service_id });

            if (cartService) {

                //if product in cart than check for quantity
                //Check if required quantity is available or not    
                //const isServiceInStock = await service.findOne({ _id: cartData.service_id, max_persons_allowed: { $gt: cartService.quantity } })

                // if (!isServiceInStock) {
                //     result.serviceOutOfStock = true;
                //     return result;
                // }

                //Update to new service package
                cartService.service_package = cartData.service_package;
                await cartService.save();
                result.experienceCart = cartService;
                return result;
            }
            // new product to cart assign quantity 1 

            cartData.service = cartData.service_id;
            result.experienceCart = await new experienceCart(cartData).save();

            return result;
        }
        catch (err) {
            console.log(err);
            result.databaseError = true;
            return result;
        }

    }
    else {

    }

}

//Get products from cart
exports.getAllCartServices = async (req) => {



    let result = {};

    if (req.user_type === "end_user") {

        try {

            const cartData = {};

            // const cartItems = await experienceCart.find({ user_id: req.user_id }).populate("service");

            const basicFilter = [
                {
                    "$match":{
                        user_id:mongoose.Types.ObjectId(req.user_id)
                    }
                },
                {
                    "$lookup": {
                        "from": "services",
                        "as": "service",
                        "localField": "service",
                        "foreignField": "_id"
                    }
                },
                {
                    "$unwind":"$service"
                },
                {
                    "$unwind":"$service_package"
                },
                {
                    "$addFields":{
                        "selected_package":{"$filter": {
                            "input": '$service.package',
                           "as": 'package',
                            "cond": {"$eq": ['$$package._id','$service_package.package_id']}} 
                        }
                    }

                },
                {
                    "$unwind":"$selected_package"
                },
                {
                    "$addFields":{
                        "service_package.for":"$selected_package.for",
                        "service_package.cost":"$selected_package.cost"
                    }
                },
                {
                    "$lookup": {
                        "from": "shops",
                        "as": "service.shop",
                        "localField": "shop",
                        "foreignField": "_id"
                    }
                },
                {
                    "$unwind":"$service.shop"
                },
                {
                    "$project":{
                        "_id":1,
                        "service_package":1,
                        "user_id":1,
                        "service._id":1,
                        "service.service_image":{"$first":"$service.service_images"},
                        "service.service_title":1,
                        "business_owner":1,
                        "service.shop.shop_name":1,
                        "service.shop.province":1,
                        "service.cart_item_id":"$_id"
                    }
                },
                {
                    "$group":{
                        "_id":"$_id",
                        "service_package":{"$push":"$service_package"},
                        "user_id":{"$first":"$user_id"},
                        "service":{"$first":"$service"},
                        "business_owner":{"$first":"$business_owner"}
                    }
                },
                {
                    "$addFields":{
                        "service.service_package":"$service_package",
                        "service.serviceTotal":
                            {
                              "$reduce":
                                 {
                                   input: "$service_package",
                                   initialValue: 0,
                                   in: {"$add":["$$value",{"$multiply": [ "$$this.quantity", "$$this.cost" ]}]}
                                 }
                            }
                    }
                },
                {
                    "$group":{
                        "_id":"$business_owner",
                        "service":{"$push":"$service"}
                    }
                },
                {
                    "$addFields":{
                        "business_owner_total":{$sum:"$service.serviceTotal"}
                    }
                }
            ]

            const cartItems = await experienceCart.aggregate(basicFilter);

            if (cartItems.length) {

                cartData.cartItems = cartItems;

                   //function to find subtotal
                   const totalCost = cartItems.reduce((total, data) => {
                    return  total + data.business_owner_total;
                  }, 0);
  
                  cartData.subTotal = totalCost;

                result.experienceCart = cartData;
                return result;
            }
            else {
                result.cartIsEmpty = true;
                return result;
            }

        }
        catch (err) {
            console.log(err);
            result.databaseError = true;
            return result;

        }

    }
    else {
        result.unAuthorised = true;
        return result;
    }

}

//Remove single service from cart
exports.removeCartService = async (req) => {


    let result = {};

    if (req.user_type === "end_user") {
        try {

            const cartService = await experienceCart.findOne({
                user_id: req.user_id,
                service: req.params.service_id
            });

            if (cartService) {

                result.experienceCart = await cartService.remove();
                return result;

            }
            else {
                result.productNotFound = true;
                return result;
            }

        }
        catch (err) {
            result.databaseError = true;
            return result;
        }
    }
    else {
        result.unAuthorised = true;
        return result;

    }

}

//Empty Service cart
exports.emptyServiceCart = async (req) => {

    let result = {};
    if (req.user_type === "end_user") {

        //check if cart data is available or not

        const cartData = await experienceCart.findOne({ user_id: req.user_id });
        if (cartData) {

            const removeCartData = await experienceCart.deleteMany({ user_id: req.user_id });

            if (removeCartData) {
                result.cartEmptySuccessful = true;
                return result;
            }
            else {
                result.databaseError = true;
                return result;
            }

        }
        else {
            result.cartIsEmpty = true;
            return result;
        }


    }
    else {

        result.unAuthorised = true;
        return result;

    }

}

// Update cart quantity used for decrement cart quantity
exports.decreaseCartServiceQuantity = async (req) => {

    let result = {};

    if (req.user_type === "end_user") {
        try {

            const cartService = await experienceCart.findOne({
                user_id: req.user_id,
                service: req.params.service_id
            });

            if (cartService) {

                if (cartService.quantity === 1) {
                    result.cartQuantityUpdated = await cartService.remove();
                    return result;

                }

                cartService.quantity = cartService.quantity - 1;
                await cartService.save();

                result.cartQuantityUpdated = cartService;
                return result;

            }
            else {
                result.serviceNotFound = true;
                return result;
            }

        }
        catch (err) {
            result.databaseError = true;
            return result;
        }
    }
    else {
        result.unAuthorised = true;
        return result;

    }

}
