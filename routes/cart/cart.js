const express = require("express");
const router = express.Router();
const { body, validationResult, query ,param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const CartController = require('./cart-controller');
const CartHelper = require('./cart-helper');

//middleware
const authCheck = require('../../middleware/check-auth');

// Path - /v1/cart/
// All Routes for cart

//Add products to cart or increment product quantity if product already exist
router.post(
    "/add-cart-product",
    authCheck,
    [
        body('product_id').notEmpty().withMessage('Product id is required'),
        body('variant_id').notEmpty().withMessage('Variant id is required'),
        body('quantity').notEmpty().withMessage('Quantity is required')
        
    ],
    async (req, res) => {
        const error = validationResult(req);
        
        
        if (!error.isEmpty()) {
          return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
              field: error.param,
              errorMessage: error.msg
          })));
      }
    
    
        const response = await CartController.addCartProduct(req);

        if(response.productOutOfStock){
            return Response.error(res,ResponseCode.BAD_REQUEST,ResponseMessage.PRODUCT_OUT_OF_STOCK);
        }
        if(response.productAlreadyInCart){
            return Response.error(res,ResponseCode.BAD_REQUEST,ResponseMessage.PRODUCT_ALREADY_IN_CART);
        }
        if (response.databaseError) {
          return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
        } else if( response.cart ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_PRODUCT_ADDED_SUCCESSFULLY , response.cart )
      }
);

//Get products from cart
router.get("/get-product-from-cart",
    authCheck,
    async(req,res)=>{

        const response = await CartController.getAllCartProducts(req);

        if(response.unAuthorised){

            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if(response.cartIsEmpty ){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_IS_EMPTY);

        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.cart ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_DATA_RETRIVED_SUCCESSFULLY , response.cart )

    }

)

//Empty cart 
router.delete("/empty-cart",
    authCheck,
    async(req,res)=>{

        const response = await CartController.emptyCart(req);

        if(response.unAuthorised){

            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if(response.cartIsEmpty ){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_IS_EMPTY);

        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.cartEmptySuccessful ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.EMPTY_CART_SUCCESSFULLY)

    }
    
)

//Remove single product from cart
router.delete("/remove-cart-product/:product_id/:variant_id",

    authCheck,
    [
        param('product_id').notEmpty().withMessage('Parameter [product_id] is missing or invalid'),
        param('variant_id').notEmpty().withMessage('Variant id is required')  
    ]
    ,

    async(req,res)=>{

    // Checking if req.body has any error
   const error = validationResult(req);

   if (!error.isEmpty()) {

       return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
           field: error.param,
           errorMessage: error.msg
       })));
   }

        const response = await CartController.removeCartProduct(req);
        
        if(response.unAuthorised){
            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if(response.productNotFound){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_PRODUCT_NOT_FOUND);
        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.cart ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_PRODUCT_REMOVED_SUCCESSFULLY)

    }
    
)


//decrease cart quantity remove product if quantity gets 0
router.post("/decrease-cart-quantity/:product_id",

    authCheck,
    [
        param('product_id').isMongoId().withMessage('Parameter [product_id] is missing or invalid')  
    ]
    ,

    async(req,res)=>{

        // Checking if req.body has any error
   const error = validationResult(req);

   if (!error.isEmpty()) {

       return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
           field: error.param,
           errorMessage: error.msg
       })));
   }

        const response = await CartController.decreaseCartQuantity(req);
        
        if(response.unAuthorised){
            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if(response.productNotFound){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_PRODUCT_NOT_FOUND);
        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.cartQuantityUpdated ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_QUANTITY_UPDATED)

    }
    
)


//Get Cart Items Count
router.get("/get-product-count-from-cart",
    authCheck,
    async(req,res)=>{

        const response = await CartController.getProductCount(req);

        if(response.unAuthorised){

            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if(response.cartIsEmpty ){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_IS_EMPTY);

        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.cartProducts ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_DATA_RETRIVED_SUCCESSFULLY , response.cartProducts )

    }

)

// Update cart and
router.post("/update-cart", 
    authCheck , 
    [
        body('action').notEmpty().withMessage("Please provide valid action to update cart"),
        body('cart_item_id').isMongoId().withMessage('Please provide valid cart item id'),
        body('code').notEmpty().withMessage("Please give valid promocode"),
        body('product_id').optional().isMongoId().withMessage('Please provide valid product id'),
        body('variant_id').optional().isMongoId().withMessage('Please provide valid product id')
    ],
    async (req, res)=>{

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if(!['apply-promocode','remove-promocode'].includes(req.body.action)){

            return Response.error(res,ResponseCode.UNPROCESSABLE_ENTITY,"Please provide valid action from ['apply-promocode','remove-promocode'] ");
            
        }

        if(['apply-promocode','remove-promocode'].includes(req.body.action)){

            if(!(req.body.product_id && req.body.variant_id)){
                return Response.error(res,ResponseCode.UNPROCESSABLE_ENTITY,"Please provide product id and variant id ");
            }

        }

        const response = await CartController.updateCart(req);
        
        if(response.unAuthorised){
            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if(response.invalidPromocode){
            return Response.error(res,ResponseCode.NOT_FOUND,"Promocode not found !");
        }
        if(response.promocodeExpired){
            return Response.error(res,ResponseCode.NOT_FOUND,"This Promocode is Expired !");
        }
        
        if(response.cartDataNotFound){
            return Response.error(res,ResponseCode.NOT_FOUND,"Cart data not found !");
        }

        if(response.promotionAlreadyApplied){
            return Response.error(res,ResponseCode.NOT_FOUND,"Promocode is already applied on this product");
        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          }
           else if( response.promotionApplied ) return Response.success( res , ResponseCode.SUCCESS , "Promocode applied successfully !!!")
           else if( response.promotionRemoved) return Response.success(res , ResponseCode.SUCCESS , "Promocode removed successfully !!!")



})


// EXPERIENCE -------------------------------------------------------

//Add services to cart or increment service quantity if service already exist
router.post("/add-cart-service",
    authCheck,
    [
        body('service_id').notEmpty().withMessage('Service is required'),
        body('service_package').isArray().notEmpty().withMessage('Please Provide service package information')
    ],
    async (req, res) => {
        const error = validationResult(req);
        
        
        if (!error.isEmpty()) {
          return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
              field: error.param,
              errorMessage: error.msg
          })));
      }
    
        const response = await CartController.addCartService(req);

        if(response.serviceOutOfStock){
            return Response.error(res,ResponseCode.BAD_REQUEST,ResponseMessage.SERVICE_OUT_OF_STOCK);
        }
        if(response.serviceAlreadyInCart){
            return Response.error(res,ResponseCode.BAD_REQUEST,ResponseMessage.SERVICE_ALREADY_IN_CART);
        }
        if (response.databaseError) {
          return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
        } else if( response.experienceCart ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_SERVICE_ADDED_SUCCESSFULLY , response.experienceCart )
      }
);

//Get services from cart
router.get("/get-service-from-cart",
    authCheck,
    async(req,res)=>{

        const response = await CartController.getAllCartServices(req);

        if(response.unAuthorised){

            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if(response.cartIsEmpty ){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_IS_EMPTY);

        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.experienceCart ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_DATA_RETRIVED_SUCCESSFULLY , response.experienceCart )

    }

)


//Empty service-cart
router.delete("/empty-service-cart",
    authCheck,
    async(req,res)=>{

        const response = await CartController.emptyServiceCart(req);

        if(response.unAuthorised){

            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if(response.cartIsEmpty ){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_IS_EMPTY);

        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.cartEmptySuccessful ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.EMPTY_CART_SUCCESSFULLY)

    }
    
)

//Remove single service from cart
router.delete("/remove-cart-service/:service_id",

    authCheck,
    [
        param('service_id').isMongoId().withMessage('Parameter [service_id] is missing or invalid')  
    ]
    ,

    async(req,res)=>{

    // Checking if req.body has any error
   const error = validationResult(req);

   if (!error.isEmpty()) {

       return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
           field: error.param,
           errorMessage: error.msg
       })));
   }

        const response = await CartController.removeCartService(req);
        
        if(response.unAuthorised){
            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if(response.productNotFound){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_SERVICE_NOT_FOUND);
        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.experienceCart ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_SERVICE_REMOVED_SUCCESSFULLY)

    }
    
)


//decrease cart quantity remove product if quantity gets 0
router.post("/decrease-cart-service-quantity/:service_id",

    authCheck,
    [
        param('service_id').isMongoId().withMessage('Parameter [service_id] is missing or invalid')  
    ]
    ,

    async(req,res)=>{

        // Checking if req.body has any error
   const error = validationResult(req);

   if (!error.isEmpty()) {

       return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
           field: error.param,
           errorMessage: error.msg
       })));
   }

        const response = await CartController.decreaseCartServiceQuantity(req);
        
        if(response.unAuthorised){
            return Response.error(res,ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if(response.serviceNotFound){
            return Response.error(res,ResponseCode.NOT_FOUND,ResponseMessage.CART_SERVICE_NOT_FOUND);
        }
        if (response.databaseError) {
            return Response.error(res,ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);
          } else if( response.cartQuantityUpdated ) return Response.success( res , ResponseCode.SUCCESS , ResponseMessage.CART_QUANTITY_UPDATED)

    }
    
)



module.exports = router;

