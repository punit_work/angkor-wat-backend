const Article = require("../../model/Article");
const Category = require("../../model/Category");
const mongoose = require('mongoose')

// Function for adding new category
async function addNewCategory(
  user_type,
  { category_for, category_name, sub_category, category_order, en, kh },
  file
) {
  let result = {};
  try {
    if (["discover", "business_owner", "super_admin"].includes(user_type)) {
      const IsCategoryExist = await Category.findOne({
        category_for: category_for,
        'translations.en.category_name': JSON.parse(en).category_name,
      });

      if (IsCategoryExist) {
        result.categoryAlreadyExists = true;
        return result;
      }

      const resultObject = {};
      if(sub_category){
        const sub_cat = JSON.parse(sub_category);
        const validSubCat  = sub_cat.reduce((acc,ele) => ((ele.en && ele.en.name && ele.en.name !== '') && (ele.kh && ele.kh.name && ele.kh.name !== '')) ? acc.concat({
          sub_category_order:ele.sub_category_order ? ele.sub_category_order : 0,
          translations:{
            en:{
              name:ele.en ? ele.en.name : ''
            },
            kh:{
              name:ele.kh ? ele.kh.name : ''
            }
          }
        }): acc,[])

        resultObject.sub_category = validSubCat;
      } 
      resultObject.category_image = file.path.replace(/\\/g, "/");
      resultObject.category_for = category_for;
      // resultObject.category_name = category_name;
      resultObject.category_order = category_order ? category_order : 0;
      resultObject.translations = {
        en:{
          category_name:JSON.parse(en).category_name
        },
        kh:{
          category_name:JSON.parse(kh).category_name
        }
      }
      await new Category(resultObject).save();

      result.categoryAdded = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for updating existing category
async function updateCategory(
  user_type,
  {
    category_id,
    category_for,
    en,
    kh,
    update_sub_category,
    add_sub_category,
    remove_sub_category,
    update_sub_sub_category,
    add_sub_sub_category,
    remove_sub_sub_category,
    category_order,
  },
  file
) {
  let result = {};
  const validateValue = (data,lang,fieldName) => {
    if(data.translations && data.translations[lang] && data.translations[lang][fieldName]){
      return data.translations[lang][fieldName]
    }
  }
  
  try {
    if (["discover", "business_owner", "super_admin"].includes(user_type)) {
      const category = await Category.findById(category_id);

      if (category) {
        category.category_for = category_for
          ? category_for
          : category.category_for;

          const parsedEn = en && en !== '' ? JSON.parse(en) : null;
          const parsedKh = kh && kh !== '' ? JSON.parse(kh) : null;

          const updateCatName = async (data,lang) => {
            if(data && data.category_name && data.category_name !== ''){
              if(validateValue(category,lang,'category_name') !== data.category_name){
                const isUsedName = await Category.findOne({[`translations.${lang}.category_name`]:data.category_name});
                if(!isUsedName){
                  category.translations[lang].category_name = data.category_name
                }
              }
            }
          }
          await updateCatName(parsedEn,'en')
          await updateCatName(parsedKh,'kh')

        // category.category_name = category_name
        //   ? category_name
        //   : category.category_name;
        category.category_order = category_order
          ? category_order
          : category.category_order;
        console.log(category);

        if (update_sub_category && JSON.parse(update_sub_category).length) {
          for (let data of JSON.parse(update_sub_category)) {
            const sub_category_index = category.sub_category.findIndex(
              (cat) => cat._id.toString() == data._id
            );

            if (category.sub_category && sub_category_index !== -1) {

              const updateSubCatName = (lang) => {
                if(!category.sub_category.find(ele => (ele._id.toString() !== data._id && ele.translations[lang].name === data.translations[lang].name))){
                  category.sub_category[sub_category_index].translations[lang].name = validateValue(data,lang,'name');
                }
              }

              updateSubCatName('en');
              updateSubCatName('kh');
              if (data.sub_category_order) {
                category.sub_category[sub_category_index].sub_category_order =
                  data.sub_category_order;
              }
            }
          }
        }

        if (remove_sub_category && JSON.parse(remove_sub_category).length) {
          for (let data of JSON.parse(remove_sub_category)) {
            const sub_category_index = category.sub_category.findIndex(
              (cat) => cat._id.toString() === data._id
            );

            if (category.sub_category && sub_category_index !== -1) {
              category.sub_category.splice([sub_category_index], 1);
            }
          }
        }

        if (add_sub_category && JSON.parse(add_sub_category).length) {

          for (let data of JSON.parse(add_sub_category)) {
            const newSubCat = {translations:{en:{name:''},kh:{name:''}}};

            const addSubCatName = (lang) => {
              if(!category.sub_category.find(ele => (ele._id.toString() !== data._id && validateValue(ele,lang,'name') === data[lang].name))){
                newSubCat.translations[lang].name = data[lang] && data[lang].name;
              }
            }
            addSubCatName('en');
            addSubCatName('kh');
            if (data.sub_category_order) {
              newSubCat.sub_category_order = data.sub_category_order;
            }
            category.sub_category.push(newSubCat);
          }
        }

        if (update_sub_sub_category) {
          const sub_sub_category_data = JSON.parse(update_sub_sub_category);
          const sub_category_index = category.sub_category.findIndex(
            (cat) => cat._id.toString() == sub_sub_category_data.sub_category_id
          );

          if (sub_category_index !== -1) {
            for (let data of sub_sub_category_data.sub_sub_category) {
              const sub_sub_category_index = category.sub_category[
                sub_category_index
              ].sub_sub_category.findIndex(
                (cat) => cat._id.toString() == data._id
              );

              if (sub_sub_category_index !== -1) {
                category.sub_category[sub_category_index].sub_sub_category[
                  sub_sub_category_index
                ].name = data.name;
              }
            }
          }
        }

        if (remove_sub_sub_category) {
          const sub_sub_category_data = JSON.parse(remove_sub_sub_category);
          const sub_category_index = category.sub_category.findIndex(
            (cat) => cat._id.toString() == sub_sub_category_data.sub_category_id
          );

          if (sub_category_index !== -1) {
            for (let data of sub_sub_category_data.sub_sub_category) {
              const sub_sub_category_index = category.sub_category[
                sub_category_index
              ].sub_sub_category.findIndex((cat) => cat._id.toString() == data);

              if (sub_sub_category_index !== -1) {
                category.sub_category[
                  sub_category_index
                ].sub_sub_category.splice([sub_sub_category_index], 1);
              }
            }
          }
        }

        if (add_sub_sub_category) {
          const sub_sub_category_data = JSON.parse(add_sub_sub_category);
          const sub_category_index = category.sub_category.findIndex(
            (cat) => cat._id.toString() == sub_sub_category_data.sub_category_id
          );

          if (sub_category_index !== -1) {
            for (let data of sub_sub_category_data.sub_sub_category) {
              category.sub_category[sub_category_index].sub_sub_category.push({
                name: data,
              });
            }
          }
        }

        category.category_image = file
          ? file.path.replace(/\\/g, "/")
          : category.category_image;
        await category.save();

        result.categoryUpdated = true;
        return result;
      }

      result.noCategoryFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for removing category
async function removeCategory(user_type, category_id) {
  let result = {};

  try {
    if (["discover", "business_owner", "super_admin"].includes(user_type)) {
      const category = await Category.findById(category_id);

      if (category) {
        const articleList = await Article.findOne({ category: {"$in":[mongoose.Types.ObjectId(category._id)]} });
        if (!articleList) {
          await category.remove();
          result.categoryRemoved = true;
          return result;
        }
        result.usedInArticle = true;
        return result;
      }

      result.noCategoryFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for fetching all categories
async function getAllCategories() {
  let result = {};

  try {
    const category = await Category.find().sort({ category_order: 1 });

    if (category.length) {
      result.category = category;
      return result;
    }

    result.noCategoryFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for removing sub-category of a category
async function removeSubCategory(user_type, sub_category_id) {
  let result = {};

  try {
    if (["discover", "business_owner", "super_admin"].includes(user_type)) {
      const category = await Category.findOne({
        sub_category: {
          $elemMatch: {
            _id: sub_category_id,
          },
        },
      });

      if (category) {
        const sub_category_index = category.sub_category.findIndex(
          (cat) => cat._id.toString() == sub_category_id
        );

        category.sub_category.splice([sub_category_index], 1);
        await category.save();

        result.subCategoryRemoved = true;
        return result;
      }

      result.noCategoryFound = true;
      return result;
    }
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for get-category-by id
async function getCategoryByType(category_type) {
  let result = {};

  try {
    const category = await Category.find({
      category_for: category_type,
    }).sort({ category_order: 1 });

    if (category.length) {
      result.category = category;
      return result;
    }

    result.noCategoryFound = true;
    return result;
  } catch (err) {
    console.log(err);

    result.databaseError = true;
    return result;
  }
}

// CHANGE THE category_order/ SEQUENCE OF THE CATEGORIES
async function changeCategoryOrder(user_type, { category_id, category_order }) {
  let result = {};

  try {
    const category = await Category.findByIdAndUpdate(
      category_id,
      { $set: { category_order } },
      { new: true }
    );
    if (category) {
      result.orderUpdated = true;
      return result;
    }

    result.noCategoryFound = true;
    return result;
  } catch (err) {
    console.log(err);

    result.databaseError = true;
    return result;
  }
}

module.exports = {
  addNewCategory,
  updateCategory,
  removeCategory,
  getAllCategories,
  getCategoryByType,
  removeSubCategory,
  changeCategoryOrder,
};

