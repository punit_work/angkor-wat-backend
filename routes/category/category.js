const express = require('express');
const router = express.Router();
const Response = require('../../global/response');
const ResponseCode = require('../../global/code');
const ResponseMessage = require('../../global/message');
const authCheck = require('../../middleware/check-auth');
const CategoryController = require('./category-controller');
const {body ,param, validationResult, query} = require('express-validator');

// Path - /v1/category/get-categories
// Route for fetching all categories
router.get('/get-categories',

    // Middleware to check if user is authenticated
    authCheck,
    
    async (req, res) => {
        
        const response = await CategoryController.getAllCategories();
        
        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        
        // Sending error response if no category found
        else if (response.noCategoryFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_CATEGORY_FOUND);
        
        // Sending success response if category fetched
        else if (response.category) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.CATEGORY_FETCHED, response.category);
    }
)

// Path - /v1/category/get-category-by-type/:category_type
// Route for fetching categories by type
router.get('/get-category-by-type/:category_type',

    // Middleware to check if user is authenticated
    authCheck,
    
    [ param('category_type').trim().isLength({min:1}).withMessage('Parameter [category_type] is missing or invalid') ]
    ,
    
    async (req, res) => {
        
        const error = validationResult(req);
        
        if (!error.isEmpty()) {
          return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
              field: error.param,
              errorMessage: error.msg
          })));
        }
        
        const response = await CategoryController.getCategoryByType( req.params.category_type );
        
        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        
        // Sending error response if no category found
        else if (response.noCategoryFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_CATEGORY_FOUND);
        
        // Sending success response if category fetched
        else if (response.category) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.CATEGORY_FETCHED, response.category);
    }
)

module.exports = router;