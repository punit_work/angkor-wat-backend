//package import
const Session = require('../../model/Session.js');
const UserHelper = require('./user-helper.js')
const fs = require('fs');
const { doFileExist , removeFileIfExists } = require('../../global/fileCheck');

// Schema
const User = require('../../model/Users.js');


// Update User
exports.updateUserInfo = async ( req ) => {

    // Check if user already exists
    let user , result = {};
    const {user_id, user_type} = req;
    try {
        user = await User.findOne({_id:req.user_id});
    } catch(err) {
        // remove file if req contains
        doFileExist(req.file)
    
        result.databaseError = true;
        return result;
    }
    
    // if user has not been created
    if(!user){
        // remove file if req contains
        doFileExist(req.file)

        result.invalidCredentials = true;
        return result;
    }
    
    // update user if all the params are correct
    try {

        const {fname,lname,country,age,gender} = req.body;
        const avatar = req.file ? req.file.path : false;

        const data = {};

        if(fname){

            data.fname = fname;

        }
        if(lname){

            data.lname = lname;

        }
        if(country){

            data.country = country;

        }
        if(age){

            data.age  = age;

        }
        if(gender){

            data.gender = gender;

        }
        if(avatar){

            data.avatar = req.file.path;

        }

        if(data){

            const updateStatus = await User.updateOne({_id:user_id}, data );

            if(updateStatus.nModified > 0){

                //If update is done and if avtar has been updated, remove previous avatar;
                if(avatar) {
                    removeFileIfExists(user.avatar);
                }

                result.userUpdated = true;
                result.user_details = await User.findOne({_id:user_id});
                return result;


            }
            else{

                 // remove file if req contains
                doFileExist(req.file)

                result.updateUserFailed = true;
                return result;


            }


        }
        else{

            // remove file if req contains
            doFileExist(req.file)

            result.nothingToUpdate = true;
            return result;

        }
        

    
    }catch(err) {
        // remove file if req contains
        //console.log('err : ' + err)
        doFileExist(req.file)
        
        result.updateDatabaseError = true;
        return result;
    }
  
}

// function to logout user
exports.logoutUser = async ( user_id , session_id ) => {

    let userSession , result = {};

    try{
        // find user session
        userSession = await Session.findOne({user_id , session_id})
    } 
    catch( err ) {
        result.databaseError = true;
        return result;
    } 

    if(userSession) {
        
        // end user session
        userSession.is_active = false;
        await userSession.save();
        result.userLoggedOut = true;
        return result;
    } 
    else {

        // no user session found
        result.noSessionFound = true;
        return result;
    }
}

// User login using google
exports.loginUsingGoogleId = async ( credentials , req ) => {

    let user , result = {};

    try {
        // find if user exists with the provided uid
        user = await User.findOne({google_id:credentials.uid})
    }catch( err ) {
        result.databaseError = true;
        return result;
    }

    // if user does not exists
    if(!user) {

        // creating new user
        let new_user , data = {};

        if(!credentials.uid) {
            result.uidError = true;
            result.errorMessage = 'User Id cannot be null'
            return result;
        }
        // add uid provided by token to data object
        data.google_id = credentials.uid;

        // if displayname is not null, add to data object
        if (credentials.displayName){
            data.fname = credentials.displayName;
        }

        // if photourl is not null, add to data object
        if (credentials.photoUrl) {
            data.avatar = credentials.photoUrl;
        }

        // if email is not null, add to data object
        if (credentials.email) {
            data.email = credentials.email;
        }

        // if phoneNumber is not null, add to data object
        if (credentials.phoneNumber) {
            data.phone_number = credentials.phoneNumber;
        }

        try {
            
            new_user = await createNewGoogleUser( data , req )
        } catch(err) {
            result.unknownError = true;
            return result;
        }

        // check if registration database error
        if ( new_user.registrationDatabaseError ) {
            result.registrationDatabaseError = new_user.registrationDatabaseError;
            return result;
        }

        // check registration failed error
        else if ( new_user.registrationFailed ) {
            result.registrationFailed = new_user.registrationFailed;
            return result;
        }

        // check if token not generated
        else if ( new_user.tokenNotGenerated ) {
            result.tokenNotGenerated = new_user.tokenNotGenerated;
            return result;
        }

        result.user_details = new_user;
        result.invalidCredentials = true;
        return result;
    }

    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    responseObject.fname = user.fname || '';
    responseObject.lname = user.lname || '';
    responseObject.country = user.country || '';
    responseObject.phone_number = user.phone_number || '';
    responseObject.email = user.email || '';
    responseObject.age = user.age || '';
    responseObject.gender = user.gender || '';

    

    const sessionDetails = {
        user_id: user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// creating new google user if not existed before
async function createNewGoogleUser( data , req ) {
    //console.log('data : ' + JSON.stringify(data))
    let new_user , result ={};

    var date = new Date().toISOString();
    data.last_active = date;

    try{
        
        new_user = await new User( data ).save()
    }
    catch(err) {
        //console.log('err ' + err)
        result.registrationDatabaseError = true;
        return result;
    }

    if(!new_user) {
        result.registrationFailed = true;
        return result;
    }

    // generate token and session for user and check if any error exists or not
    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( new_user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    responseObject.fname = new_user.fname || '';
    responseObject.lname = new_user.lname || '';
    responseObject.country = new_user.country || '';
    responseObject.phone_number = new_user.phone_number || '';
    responseObject.email = new_user.email || '';
    responseObject.age = new_user.age || '';
    responseObject.gender = new_user.gender || '';


    const sessionDetails = {
        user_id: new_user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// User Login using Facebook Id
exports.loginUsingFacebookId = async ( credentials , req ) => {

    let user , result = {};

    try {
        // find if user exists with the provided uid
        user = await User.findOne({facebook_id:credentials.uid})
    } 
    catch(err) {
        result.databaseError = true;
        return result;
    }

    // if user does not exists
    if(!user) {

        // creating new user
        let new_user , data = {};

        if(!credentials.uid) {
            result.uidError = true;
            result.errorMessage = 'User Id cannot be null'
            return result;
        }

        // add uid provided by token to data object
        data.facebook_id = credentials.uid

        // if display name is not null, add to data object
        if(credentials.displayName) {
            data.fname = credentials.displayName;
        }

        // if photourl is not null, add it to data object
        if(credentials.photoUrl) {
            data.avatar = credentials.photoUrl;
        }

        // if email is not null, add to data object
        if (credentials.email) {
            data.email = credentials.email;
        }

        // if phoneNumber is not null, add to data object
        if (credentials.phoneNumber) {
            data.phone_number = credentials.phoneNumber;
        }

        try {
            
            new_user = await createNewFacebookUser( data , req )
        } catch(err) {
            result.unknownError = true;
            return result;
        }

        // check if registration database error
        if ( new_user.registrationDatabaseError ) {
            result.registrationDatabaseError = new_user.registrationDatabaseError;
            return result;
        }

        // check registration failed error
        else if ( new_user.registrationFailed ) {
            result.registrationFailed = new_user.registrationFailed;
            return result;
        }

         // check if token not generated
         else if ( new_user.tokenNotGenerated ) {
            result.tokenNotGenerated = new_user.tokenNotGenerated;
            return result;
        }

        result.user_details = new_user;
        result.invalidCredentials = true;
        return result;
    }

    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    responseObject.fname = user.fname || '';
    responseObject.lname = user.lname || '';
    responseObject.country = user.country || '';
    responseObject.phone_number = user.phone_number || '';
    responseObject.email = user.email || '';
    responseObject.age = user.age || '';
    responseObject.gender = user.gender || '';

    const sessionDetails = {
        user_id: user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// create new user using facebook id if not existed before
async function createNewFacebookUser( data , req ) {

    let new_user , result ={};

    var date = new Date().toISOString();
    data.last_active = date;

    try{
        
        new_user = await new User( data ).save()
    }
    catch(err) {
        //console.log('err ' + err)
        result.registrationDatabaseError = true;
        return result;
    }

    if(!new_user) {
        result.registrationFailed = true;
        return result;
    }

    // generate token and session for user and check if any error exists or not
    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( new_user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    
    responseObject.fname = new_user.fname || '';
    responseObject.lname = new_user.lname || '';
    responseObject.country = new_user.country || '';
    responseObject.phone_number = new_user.phone_number || '';
    responseObject.email = new_user.email || '';
    responseObject.age = new_user.age || '';
    responseObject.gender = new_user.gender || '';

    const sessionDetails = {
        user_id: new_user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// User login using phone number
exports.loginUsingPhoneNumber = async ( req ) => {
    let user , result = {};

    const {phone_number,country,country_code} = req.body;

    try {

        user = await User.findOne({phone_number:phone_number})

    } catch(err) {

        result.databaseError = true;
        return result;
    }

    if(!user) {
        //create new user here as user does not exists
        let new_user;

        try {
            new_user = await createNewPhoneUser( req )
        } catch(err) {
            result.unknownError = true;
            return result;
        }

        if(new_user.registrationFailed) {
            result.registrationFailed = true;
            return result;
        } 
        else if (new_user.registrationDatabaseError) {
            result.registrationDatabaseError = true;
            return result;
        }
        else if(new_user.tokenNotGenerated) {
            result.tokenNotGenerated = true;
            return result;
        }
    
        result.user_details = new_user;
    
        result.invalidCredentials = true;
        return result;
    }

    // generate token and session for user and check if any error exists or not
    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    responseObject.fname = user.fname || '';
    responseObject.lname = user.lname || '';
    responseObject.country = user.country || '';
    responseObject.phone_number = user.phone_number || '';
    responseObject.email = user.email || '';
    responseObject.age = user.age || '';
    responseObject.gender = user.gender || '';

    const sessionDetails = {
        user_id: user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// creating a new user using phone if not existed before
async function createNewPhoneUser( req ){
    // register new user if all the params are correct
    let new_user , result ={};

    var date = new Date().toISOString();

    const {phone_number,country,country_code} = req.body;

    const data = {
        phone_number,
        country,
        country_code,
        last_active : date
    }

    try{
        new_user = await new User(data).save()
    }
    catch(err) {
        console.log('err ' + err)
        result.registrationDatabaseError = true;
        return result;
    }

    if(!new_user) {
        result.registrationFailed = true;
        return result;
    }

    // generate token and session for user and check if any error exists or not
    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( new_user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    responseObject.fname = new_user.fname || '';
    responseObject.lname = new_user.lname || '';
    responseObject.country = new_user.country || '';
    responseObject.phone_number = new_user.phone_number || '';
    responseObject.email = new_user.email || '';
    responseObject.age = new_user.age || '';
    responseObject.gender = new_user.gender || '';

    const sessionDetails = {
        user_id: new_user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}


// User login using apple
exports.loginUsingAppleId = async ( credentials , req ) => {

    let user , result = {};

    try {
        // find if user exists with the provided uid
        user = await User.findOne({apple_id:credentials.uid})
    }catch( err ) {
        result.databaseError = true;
        return result;
    }

    // if user does not exists
    if(!user) {

        // creating new user
        let new_user , data = {};

        if(!credentials.uid) {
            result.uidError = true;
            result.errorMessage = 'User Id cannot be null'
            return result;
        }
        // add uid provided by token to data object
        data.apple_id = credentials.uid;

        // if displayname is not null, add to data object
        if (credentials.displayName){
            data.fname = credentials.displayName;
        }

        // if photourl is not null, add to data object
        if (credentials.photoUrl) {
            data.avatar = credentials.photoURL;
        }

        // if email is not null, add to data object
        if (credentials.email) {
            data.email = credentials.email;
        }

        // if phoneNumber is not null, add to data object
        if (credentials.phoneNumber) {
            data.phone_number = credentials.phoneNumber;
        }

        try {
            
            new_user = await createNewAppleUser( data , req )
        } catch(err) {
            result.unknownError = true;
            return result;
        }

        // check if registration database error
        if ( new_user.registrationDatabaseError ) {
            result.registrationDatabaseError = new_user.registrationDatabaseError;
            return result;
        }

        // check registration failed error
        else if ( new_user.registrationFailed ) {
            result.registrationFailed = new_user.registrationFailed;
            return result;
        }

        // check if token not generated
        else if ( new_user.tokenNotGenerated ) {
            result.tokenNotGenerated = new_user.tokenNotGenerated;
            return result;
        }

        result.user_details = new_user;
        result.invalidCredentials = true;
        return result;
    }

    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.fname = user.fname;
    responseObject.lname = user.lname;
    responseObject.token = tokenResponse.token;

    const sessionDetails = {
        user_id: user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// creating new apple user if not existed before
async function createNewAppleUser( data , req ) {
    //console.log('data : ' + JSON.stringify(data))
    let new_user , result ={};

    var date = new Date().toISOString();
    data.last_active = date;

    try{
        
        new_user = await new User( data ).save()
    }
    catch(err) {
        //console.log('err ' + err)
        result.registrationDatabaseError = true;
        return result;
    }

    if(!new_user) {
        result.registrationFailed = true;
        return result;
    }

    // generate token and session for user and check if any error exists or not
    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( new_user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;

    const sessionDetails = {
        user_id: new_user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// User login using phone number
exports.loginUsingEmail = async ( req , email ) => {
    let user , result = {};

    try {

        user = await User.findOne({email:email})

    } catch(err) {

        result.databaseError = true;
        return result;
    }

    if(!user) {
        //create new user here as user does not exists
        let new_user;

        try {
            new_user = await createNewEmailUser( email , req )
        } catch(err) {
            result.unknownError = true;
            return result;
        }

        if(new_user.registrationFailed) {
            result.registrationFailed = true;
            return result;
        } 
        else if (new_user.registrationDatabaseError) {
            result.registrationDatabaseError = true;
            return result;
        }
        else if(new_user.tokenNotGenerated) {
            result.tokenNotGenerated = true;
            return result;
        }
    
        result.user_details = new_user;
    
        result.invalidCredentials = true;
        return result;
    }

    // generate token and session for user and check if any error exists or not
    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    responseObject.fname = user.fname || '';
    responseObject.lname = user.lname || '';
    responseObject.country = user.country || '';
    responseObject.phone_number = user.phone_number || '';
    responseObject.email = user.email || '';
    responseObject.age = user.age || '';
    responseObject.gender = user.gender || '';

    const sessionDetails = {
        user_id: user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

// creating a new user using phone if not existed before
async function createNewEmailUser( email , req ){
    // register new user if all the params are correct
    let new_user , result ={};

    var date = new Date().toISOString();

    try{
        new_user = await new User({
            email : email ,
            last_active : date,
        }).save()
    }
    catch(err) {
        console.log('err ' + err)
        result.registrationDatabaseError = true;
        return result;
    }

    if(!new_user) {
        result.registrationFailed = true;
        return result;
    }

    // generate token and session for user and check if any error exists or not
    const responseObject = {};
    const tokenResponse = await UserHelper.generateToken( new_user , 'end_user' )

    if( tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
    }

    responseObject.token = tokenResponse.token;
    responseObject.fname = new_user.fname || '';
    responseObject.lname = new_user.lname || '';
    responseObject.country = new_user.country || '';
    responseObject.phone_number = new_user.phone_number || '';
    responseObject.email = new_user.email || '';
    responseObject.age = new_user.age || '';
    responseObject.gender = new_user.gender || '';

    const sessionDetails = {
        user_id: new_user._id,
        session_id: tokenResponse.session_id,
        is_active: true,
        ip_address: await UserHelper.getClientIP(req),
        user_agent: req.headers['user-agent']
    }

    await new Session(sessionDetails).save();
    result.user_details = responseObject;
    return result;
}

exports.getUserDetails = async (req) => {

    const { user_type, user_id } = req;

    let result = {};

    if(['end_user'].includes(user_type)){

        try {

            const userData = await User.findOne({_id:user_id}).select('-password');

            if(userData){
                result.success = true;
                result.userDetails = userData;
                return result;
            }
            else{
                result.dataNotFound = true;
                result.message = "User Details not found";
                return result;
            }
                
        }
        catch (err) {
            
            console.log(err);

            result.databaseError = true;
            return result;
        }

    }
    else {
        result.unAuthorised = true;
        return result;
    }


}
