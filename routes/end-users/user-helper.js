const bcrypt = require('bcryptjs')
const jsonwebtoken = require('jsonwebtoken')
const {v4: uuidv4} = require('uuid');
const { body, validationResult } = require('express-validator');

// check if the user entered mobile number is valid or not
exports.isMobileNumberValid = async ( phone_number ) => {
    let result = {};

    const number = await isValidPhone(phone_number);

    if(!number) {
        result.error = true;
        result.errorMessage = 'Please enter a valid phone number.'
    }

    else {
        result.error = false;
    }

    return result;
}


exports.getHashedPassword = async(password) => {
    let hash , result = {};

    hash = await bcrypt.hash( password , 12 )
    .then(result => {
        if(result) return result;
    })
    .catch(err => {
        result.databaseError = true;
        return result;
    })

    if(!hash) return false;

    return hash;
} 

exports.isPasswordMatch = async(user_password , hashed_password) => {
    let passwordMatch , result = {};

    passwordMatch = await bcrypt.compare(user_password , hashed_password).catch(err => {
        result.databaseError = true;
        return result;
    })

    return passwordMatch;
}

// Function for generating jwt token for secure authentication
exports.generateToken = async(user , user_type) => {

    let token, result = {}, session_id;

    try {

        // Generating a unique session_id
        session_id = uuidv4();

        // Signing jwt payload along with secret key and options config
        token = jsonwebtoken.sign({user_type: user_type, user_id: user._id, session_id}, process.env.JWT_SECRET_KEY);
    }

    catch (err) {

        result.tokenNotGenerated = true;
        return result;
    }

    if (token) {
        result.token = token;
        result.session_id = session_id;
        return result;
    }
}

exports.getClientIP = async(requestObject) => {

    with (requestObject)
        return (headers['x-forwarded-for'] || '').split(',')[0] || connection.remoteAddress
}

async function isValidPhone(inputtxt) {
  var phoneno =  /([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{4}/g;
  if((inputtxt.match(phoneno))){
        return true;
    }
    else {    
        return false;
    }
}