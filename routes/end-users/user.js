// importing packages
const bodyParser = require('body-parser');
const express = require('express');
const ResponseMessage = require('../../global/message.js')
const Response = require('../../global/response.js')
const ResponseCode = require('../../global/code.js')
const { body, validationResult } = require('express-validator');
const uploadAvatar = require('../../middleware/upload-avatar.js');
const firebase = require('firebase/app')
const { doFileExist } = require('../../global/fileCheck');

// User Authentication
// const isUserAuth = require('../../auth/check-auth.js');

// User Controller Functions
const UserController = require('./user-controller.js');
const UserHelper = require('./user-helper.js');
const { response } = require('express');
const checkAuth = require('../../middleware/check-auth.js');

const app = express();
const route = express.Router();

app.use(express.json())

const admin = require('firebase-admin')

//var serviceAccount = require('../../google-services-combodia.json');

const serviceAccount = require('../../combodia-serviceaccount.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})


// // All required options are specified by the service account,
// // add service-specific configuration like databaseURL as needed.
// const secondaryAppConfig = {
//     credential: admin.credential.cert(secondaryServiceAccount),
//     // databaseURL: 'https://<DATABASE_NAME>.firebaseio.com'
// };

// const secondary = admin.initializeApp(secondaryAppConfig, 'secondary');

// User login
route.post('/login',
    [
        body('login_type').not().isEmpty().withMessage('Please provide a login_type')
    ]
    , async (req, res, next) => {

        // check if user has entered valid params or not
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return Response.error(res, ResponseCode.BAD_REQUEST, errors.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })))
        }



        // check login type 
        const login_type = req.body.login_type

        if (login_type === 'phone_number') {

            const {country,country_code,phone_number} = req.body;

            if (!phone_number) {
                return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide a phone number.')
            }
            if(!country){

                return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide country')

            }
            if(!country_code){

                return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide country_code')

            }

            // check if request body contains any error
            const response = await UserHelper.isMobileNumberValid(country_code+phone_number);
            
            if (response.error) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.errorMessage);

            //check if valid user is requesting
            let userCredential;

            try {
                userCredential = await admin
                    .auth()
                    .getUserByPhoneNumber(country_code+phone_number)
                    .then((userRecord) => {
                        // See the UserRecord reference doc for the contents of userRecord.
                        return userRecord;
                    })
            }
            catch (error) {
                var errorMessage = error.message;
                return Response.error(res, ResponseCode.BAD_REQUEST, errorMessage)
            }

            // check if firebase returns no user with provided token    
            if (!userCredential){ 
                return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND)
            }

            else if (!response.error) {
                // method to check if user exists with this phone number or not
                const response = await UserController.loginUsingPhoneNumber(req)

                // check if database error while finding user
                if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE,)

                // check if any unknown error occured while saving data
                else if (response.unknownError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN)

                // check if user is registered if not existed before
                else if (response.registrationFailed) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_CREDENTIALS)

                // check if database error while registering new user
                else if (response.registrationDatabaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

                // check user exists or not - sending special code to notify front end 
                else if (response.invalidCredentials) return Response.success(res, ResponseCode.CREATE_NEW_USER, ResponseMessage.NO_USER_FOUND, response.user_details)

                // token not generated error
                else if (response.tokenNotGenerated) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.TOKEN_NOT_GENERATED)

                // success response if user already exists with the provided phone number
                else if (response.user_details) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_IN_SUCCESSFULLY, response.user_details)
            }

        }
        else if (login_type === 'google_id') {

            // check if user has provided google id 
            if (!req.body.google_id) {
                return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide a Google Id token.')
            }

            let userCredential, customToken;

            try {
                userCredential = await admin
                    .auth()
                    .getUser(req.body.google_id)
                    .then((userRecord) => {
                        // See the UserRecord reference doc for the contents of userRecord.
                        return userRecord;
                    })
            }
            catch (error) {
                var errorMessage = error.message;
                return Response.error(res, ResponseCode.BAD_REQUEST, errorMessage)
            }
            // firebase auth for google id
            // let userCredential, customToken;

            // try {

            //     customToken = await admin.auth().createCustomToken(req.body.google_id).catch( err => {
            //         return Response.error( res , ResponseCode.DATABASE_ERROR , 'Firebase ' + ResponseMessage.TOKEN_NOT_GENERATED )
            //     })

            //     userCredential = await firebase.auth().signInWithCustomToken(customToken)
            //     .then((userCredential) => {
            //         // Signed in
            //         //var user = userCredential.user;
            //         return userCredential;

            //     })
            // } catch(error) {

            //     var errorCode = error.code;
            //     var errorMessage = error.message;
            //     console.log('firebase google error : ' + errorMessage)
            //     return Response.error( res , ResponseCode.BAD_REQUEST , errorMessage )
            // }

            // check if firebase returns no user with provided token    
            if (!userCredential) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND)

            // login user using google uid received from token
            const response = await UserController.loginUsingGoogleId(userCredential, req)

            //console.log('response : ' + JSON.stringify(response))
            // check if database error occurred 
            if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if uid error exists or not
            else if (response.uidError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.errorMessage)

            // check if unknown error occured while saving data to the db
            else if (response.unknownError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN)

            // check if registration database error has occured
            else if (response.registrationDatabaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if user is registered if not existed before
            else if (response.registrationFailed) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_CREDENTIALS)

            // check if invalid credentials
            else if (response.invalidCredentials) return Response.success(res, ResponseCode.CREATE_NEW_USER, ResponseMessage.NO_USER_FOUND, response.user_details)

            //check if token has been generated or not  
            else if (response.tokenNotGenerated) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.TOKEN_NOT_GENERATED)

            // success response if user created with provided google_id number
            else if (response.user_details) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_IN_SUCCESSFULLY, response.user_details)

            // end of google_id login type
        }
        else if (login_type === 'facebook_id') {

            console.log("FacebookId" + req.body.facebook_id);

            // check if user has provided facebook id 
            if (!req.body.facebook_id) {
                return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide a Facebook Id token.')
            }

            //firebase auth for facebook id
            let userCredential, customToken;

            try {
                userCredential = await admin
                    .auth()
                    .getUser(req.body.facebook_id)
                    .then((userRecord) => {
                        // See the UserRecord reference doc for the contents of userRecord.
                        return userRecord;
                    })
            }
            catch (error) {
                var errorMessage = error.message;
                return Response.error(res, ResponseCode.BAD_REQUEST, errorMessage)
            }

            // try {

            //     customToken = await admin.auth().createCustomToken(req.body.facebook_id)

            //     userCredentail = await firebase.auth().signInWithCustomToken(customToken)
            //     .then((userCredential) => {
            //         // Signed in
            //         //var user = userCredential.user;
            //         return userCredential;
            //         // ...
            //     })
            // } catch( err ){
            //     var errorMessage = error.message;
            //     console.log('firebase facebook error : ' + error.message)
            //     return Response.error( res , ResponseCode.BAD_REQUEST , errorMessage )
            // }

            // check if firebase returns a user with provided token or not
            if (!userCredential) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND)

            console.log("User_creds" + JSON.stringify(userCredential));

            // login using facebook id received from toke
            const response = await UserController.loginUsingFacebookId(userCredential, req)

            //console.log('response : ' + JSON.stringify(response))
            // check if database error occurred 
            if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if uid error exists or not
            else if (response.uidError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.errorMessage)

            // check if unknown error occured while saving data to the db
            else if (response.unknownError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN)

            // check if registration database error has occured
            else if (response.registrationDatabaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if user is registered if not existed before
            else if (response.registrationFailed) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_CREDENTIALS)

            // check if invalid credentials
            else if (response.invalidCredentials) return Response.success(res, ResponseCode.CREATE_NEW_USER, ResponseMessage.NO_USER_FOUND, response.user_details)

            //check if token has been generated or not  
            else if (response.tokenNotGenerated) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.TOKEN_NOT_GENERATED)

            // success response if user created with provided google_id number
            else if (response.user_details) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_IN_SUCCESSFULLY, response.user_details)
        }
        else if (login_type === 'apple_id') {

            // check if user has provided google id 
            if (!req.body.apple_id) {
                return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide a apple Id token.')
            }

            let userCredential, customToken;

            try {
                userCredential = await admin
                    .auth()
                    .getUser(req.body.apple_id)
                    .then((userRecord) => {
                        // See the UserRecord reference doc for the contents of userRecord.
                        return userRecord;
                    })
            }
            catch (error) {
                var errorMessage = error.message;
                return Response.error(res, ResponseCode.BAD_REQUEST, errorMessage)
            }
            // firebase auth for google id
            // let userCredential, customToken;

            // try {

            //     customToken = await admin.auth().createCustomToken(req.body.google_id).catch( err => {
            //         return Response.error( res , ResponseCode.DATABASE_ERROR , 'Firebase ' + ResponseMessage.TOKEN_NOT_GENERATED )
            //     })

            //     userCredential = await firebase.auth().signInWithCustomToken(customToken)
            //     .then((userCredential) => {
            //         // Signed in
            //         //var user = userCredential.user;
            //         return userCredential;

            //     })
            // } catch(error) {

            //     var errorCode = error.code;
            //     var errorMessage = error.message;
            //     console.log('firebase google error : ' + errorMessage)
            //     return Response.error( res , ResponseCode.BAD_REQUEST , errorMessage )
            // }

            // check if firebase returns no user with provided token    
            if (!userCredential) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND)

            // login user using google uid received from token
            const response = await UserController.loginUsingAppleId(userCredential, req)

            //console.log('response : ' + JSON.stringify(response))
            // check if database error occurred 
            if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if uid error exists or not
            else if (response.uidError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.errorMessage)

            // check if unknown error occured while saving data to the db
            else if (response.unknownError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN)

            // check if registration database error has occured
            else if (response.registrationDatabaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if user is registered if not existed before
            else if (response.registrationFailed) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_CREDENTIALS)

            // check if invalid credentials
            else if (response.invalidCredentials) return Response.success(res, ResponseCode.CREATE_NEW_USER, ResponseMessage.NO_USER_FOUND, response.user_details)

            //check if token has been generated or not  
            else if (response.tokenNotGenerated) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.TOKEN_NOT_GENERATED)

            // success response if user created with provided apple_id number
            else if (response.user_details) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_IN_SUCCESSFULLY, response.user_details)

            // end of apple_id login type
        }
        else if (login_type === 'email') {

            // check if user has provided google id 
            if (!req.body.email) {
                return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide email.')
            }

            let userCredential, customToken;

            try {
                userCredential = await admin
                    .auth()
                    .getUserByEmail(req.body.email)
                    .then((userRecord) => {
                        // See the UserRecord reference doc for the contents of userRecord.
                        return userRecord;
                    })
            }
            catch (error) {
                var errorMessage = error.message;
                return Response.error(res, ResponseCode.BAD_REQUEST, errorMessage)
            }
            // firebase auth for google id
            // let userCredential, customToken;

            // try {

            //     customToken = await admin.auth().createCustomToken(req.body.google_id).catch( err => {
            //         return Response.error( res , ResponseCode.DATABASE_ERROR , 'Firebase ' + ResponseMessage.TOKEN_NOT_GENERATED )
            //     })

            //     userCredential = await firebase.auth().signInWithCustomToken(customToken)
            //     .then((userCredential) => {
            //         // Signed in
            //         //var user = userCredential.user;
            //         return userCredential;

            //     })
            // } catch(error) {

            //     var errorCode = error.code;
            //     var errorMessage = error.message;
            //     console.log('firebase google error : ' + errorMessage)
            //     return Response.error( res , ResponseCode.BAD_REQUEST , errorMessage )
            // }

            // check if firebase returns no user with provided token    
            if (!userCredential) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND)

            // login user using google uid received from token
            const response = await UserController.loginUsingEmail(req, req.body.email)

            //console.log('response : ' + JSON.stringify(response))
            // check if database error occurred 
            if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if uid error exists or not
            else if (response.uidError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.errorMessage)

            // check if unknown error occured while saving data to the db
            else if (response.unknownError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN)

            // check if registration database error has occured
            else if (response.registrationDatabaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

            // check if user is registered if not existed before
            else if (response.registrationFailed) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_CREDENTIALS)

            // check if invalid credentials
            else if (response.invalidCredentials) return Response.success(res, ResponseCode.CREATE_NEW_USER, ResponseMessage.NO_USER_FOUND, response.user_details)

            //check if token has been generated or not  
            else if (response.tokenNotGenerated) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.TOKEN_NOT_GENERATED)

            // success response if user created with provided apple_id number
            else if (response.user_details) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_IN_SUCCESSFULLY, response.user_details)

            // end of apple_id login type
        }
        else {
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.ERROR_LOGIN_TYPE)
        }

    })


// Update user info
route.post('/updateUser'
    ,
    // check if end user is authenticated or not
    checkAuth
    ,
    uploadAvatar.single('avatar')
    ,
    [
        body('fname').optional().not().isEmpty().withMessage('First name ' + ResponseMessage.ERROR_FIELD_EMPTY),
        body('lname').optional().not().isEmpty().withMessage('Last name ' + ResponseMessage.ERROR_FIELD_EMPTY),
        body('email').optional().custom(email => { 
            
            throw new Error('You cannot update your email !'); 
           
        }),
        body('phone_number').optional().custom(phone_number => { 

            throw new Error('You cannot update your phone number !'); 
           
        }),
        body('country').optional().not().isEmpty().withMessage('Country ' + ResponseMessage.ERROR_FIELD_EMPTY),
        body('previous_avatar').optional().not().isEmpty().withMessage('Previous Image ' + ResponseMessage.ERROR_FIELD_EMPTY),
        body('age').optional().custom(age => {  
            if((age < 10 ) || (age > 100)){
                throw new Error('Age must be between 10 and 100'); 
            }
            else{
                return true;
            }
        }),
        body('gender').optional().notEmpty().isIn(['male', 'female', 'others']).withMessage(ResponseMessage.INVALID_GENDER)
    ]
    , async (req, res, next) => {

        //check if user has entered valid credentials
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            // remove files if attached
            doFileExist(req.file)

            return Response.error(res, ResponseCode.BAD_REQUEST, errors.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })))
        }

        const response = await UserController.updateUserInfo(req);

        //console.log('console : ' + JSON.stringify(response))
        // check if it is a database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        // check if user exists or not
        else if (response.invalidCredentials) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.NO_USER_FOUND);

        // check if error occured while updating the user
        else if (response.updateDatabaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

        // check if user not updated
        else if (response.updateUserFailed) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ' User information not updated.')

        // check if user has been updated successfully or not
        else if (response.userUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_UPDATED_SUCCESSFULLY, response.user_details)

    }
)

route.post('/logout'
    ,

    // check if end user is authenticated or not
    checkAuth
    ,
    async (req, res, next) => {

        // logout user if session is active
        const response = await UserController.logoutUser(req.user_id, req.session_id)

        // check if database error has occured
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

        // check if session is invalid or not found
        else if (response.noSessionFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SESSION_FOUND)

        // successfully logout if user had valid session    
        else if (response.userLoggedOut) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_OUT_SUCCESSFULLY)
    }
)


// User data
route.get('/get-user-details',

checkAuth,

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await UserController.getUserDetails(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.UNAUTHORIZED_ACCESS);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.dataNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }
        else if (response.success) return Response.success(res, ResponseCode.SUCCESS, response.message, response.userDetails);
    }
)

module.exports = route;