//Models
const mongoose = require('mongoose'); 
const feedback = require("../../model/Feedback");
const user = require('../../model/Users');
const FeedbackHelper = require("./feedback-helper");
const nodemailer = require("nodemailer");
const {sendFeedbackMail:sendMail} = require("../../global/mailService");

//Give Feedback
exports.addFeedback = async req => {

  let result = {};

  const { user_id, user_type} = req;
  const {module,description} = req.body;

  if(['super_admin', 'discover', 'author','editor','commerce','business_owner','end_user'].includes(user_type)){

    try {

        const data = {
          module,
          description,
          user:user_id
        }

        const addFeedback = await feedback.create(data);

        if(addFeedback){
          result.success = true;
          result.message = `Your Feedback is successfully given`;
          result.data = addFeedback
        }
        else{
          result.error = true;
          result.message = "Error at posting feedback";
        }

        return result;
        

    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }

  }
  else{
    result.unAuthorised = true;
    return result;

  }
 
};

//Get Feedback
exports.getFeedbacks = async req =>{

  let result = {};

  const { user_id, user_type} = req;
  const {by_module} = req.query;
  let page,limit;

  if(['super_admin','discover','commerce'].includes(user_type)){

    if(req.query.page && req.query.limit){

      page = parseInt(req.query.page);
      limit = parseInt(req.query.limit);
  
    }
  
    try{
  
      const feedbacks = await feedback.find({}).populate('user','fname lname');
  
      if(feedbacks && feedbacks.length > 0){
  
        result.success = true;
        result.data = feedbacks;
  
      }
      else{
        result.dataNotFound = true;
      }
  
      return result;
  
  
    }
    catch(err){
  
      console.log(err);
      result.databaseError = true;
      return result;
  
  
    }
  


  }
  else{
    result.unAuthorised = true;
    return result;
  }



}

//Send Feedback mail
exports.sendFeedbackMail = async req =>{

  const {user_id,user_type} = req;
  const {description,subject} = req.body;
  let result = {};

  if(user_type === "end_user"){

   try{
    const userDetails = await user.findOne({_id: user_id},{fname:1,lname:1,email:1});

    if(userDetails){

      const data = {
        fname:userDetails.fname,
        lname:userDetails.lname,
        user_email:userDetails.email,
        description,
        subject
      }


     const status = sendMail(data);

     if(status){

      result.success = true;
      result.message = `${data.fname} your mail has been sent successfully !`
     
     }
     else{

      result.error = true;
      result.message = 'There was an error trying to send your message. Please try again'


     }

     return result;

    }
    else{

      result.error = true;
      result.message = 'Failed at fetching user details'

    }
   }
   catch(error){
     console.log(error);
     result.databaseError = true;
     return result;
   }


  }
  else{
    result.unAuthorised = true;
    return result;
  }

}


// //Delete media
// exports.deleteMedia = async req =>{

//   let result = {};
//   const {user_id,user_type} = req;

//   const {media_ids} = req.body;

 
//   try{

//     const mappedMediaIds = media_ids.map((id) => {
//       return mongoose.Types.ObjectId(id)
//     })
  

//     const mediaData = await mediaLibrary.find({ _id: { $in: mappedMediaIds } });

//     // Delete from local
//     if(mediaData && mediaData.lenght > 0){

//       for(data of mediaData){

//         fs.unlink(data.path, err => console.log(err));
  
//       }
//     }

//     // Delete from db
//     const deleteStatus = await mediaLibrary.deleteMany( { _id: { $in: mappedMediaIds } });

//     deleteStatus.deletedCount > 0 ? result.success = true : result.error = true;
  
//     if(result.success){
//       result.message = `Deleted ${deleteStatus.deletedCount} media`;
//     } 
//     if(result.error){
//       result.message = `Noting deleted !`;
//     }
  
//     return result;

//   }
//   catch(err){

//     console.log(err);
//     result.databaseError = true;
//     return result;


//   }


// }