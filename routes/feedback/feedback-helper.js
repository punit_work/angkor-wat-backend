function generateBugId(user_id){

    const ts = + new Date() 
    const min = parseInt(ts.toString().slice(-5,-1))
    const unique =  Math.floor(Math.random() * (9999 - min)) + min
    const bug_id = user_id.slice(-4) +"-"+ ts.toString().slice(-4) +"-"+ unique;
  
  
    return bug_id;
  }
  
  module.exports = {
    generateBugId
  }