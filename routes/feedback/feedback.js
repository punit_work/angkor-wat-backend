const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//feedback 
const FeedbackController = require("./feedback-controller");
const FeedbackHelper = require("./feedback-helper");

const authCheck = require("../../middleware/check-auth");




// Add feedback
router.post(
  "/add-feedback",
  authCheck,
  [
    body("module")
    .notEmpty()
    .withMessage("Please select valid module from ['Discover', 'Shop', 'Experience','Organization'] "),
    body("description")
    .notEmpty()
    .withMessage("Please give valid description")

  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    if(!['Discover', 'Shop', 'Experience','Organization'].includes(req.body.module)){

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide valid module name from [ 'Discover', 'Shop', 'Experience','Organization' ]"
      )

    }


    const response = await FeedbackController.addFeedback(req);

    if(response.unAuthorised){

      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      )

    }

    if (response.error) {

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
       response.message
      );
    }

    if (response.databaseError) {

      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.success)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        response.message,
        response.data
      );
  }
);

// Get Feedback
router.get("/get-feedbacks", 

authCheck,

[
  query('by_module').optional().isString().withMessage("by_module is required"),
  query('page').optional().isFloat({ min: 1}),
  query('limit').optional().isFloat({ min: 1})
],

async (req, res) => {

  const response = await FeedbackController.getFeedbacks(req);

  if(response.unAuthorised){
    return Response.error(
      res,
      ResponseCode.UNAUTHORIZED_ACCESS,
      ResponseMessage.ERROR_UNAUTHORIZED
  );

  }

  if(response.dataNotFound){
      return Response.error(
          res,
          ResponseCode.NOT_FOUND,
          ResponseMessage.ERROR_NOT_FOUND
      );
  }

  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.success)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      "Data retrived successfully !!",
      response.data
    );
});


// send mail feedback
router.post(
  "/send-mail",
  authCheck,
  [
    body("subject")
    .notEmpty()
    .withMessage("Please give subject"),

    body("description")
    .notEmpty()
    .withMessage("Please give valid description")

  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }



    const response = await FeedbackController.sendFeedbackMail(req);

    if(response.unAuthorised){

      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      )

    }

    if (response.error) {

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
       response.message
      );
    }

    if (response.databaseError) {

      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.success)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        response.message
      );
  }
);



// // Delete files

// router.post("/delete-media", 

//   authCheck,
  
//   [

//     body('media_ids').isArray({min: 1})

//   ],

//   async (req, res) => {

//     const error = validationResult(req);
//     if (!error.isEmpty()) {
//       return Response.error(
//         res,
//         ResponseCode.UNPROCESSABLE_ENTITY,
//         error.array().map(error => ({
//           field: error.param,
//           errorMessage: error.msg
//         }))
//       );
//     }

//     const response = await MediaLibraryController.deleteMedia(req);

//     if(response.unAuthorised){

//       return Response.error(
//         res,
//         ResponseCode.UNAUTHORIZED_ACCESS,
//         ResponseMessage.ERROR_UNAUTHORIZED
//       )

//     }

//     if (response.error) {

//       return Response.error(
//         res,
//         ResponseCode.UNPROCESSABLE_ENTITY,
//        response.message
//       );

//     }

//     if (response.databaseError) {

//       return Response.error(
//         res,
//         ResponseCode.DATABASE_ERROR,
//         ResponseMessage.ERROR_DATABASE
//       );
//     } 
//     else if (response.success){

//       return Response.success(
//         res,
//         ResponseCode.SUCCESS,
//         response.message,
//         response.data
//       );
//     }
    
  

// })



module.exports = router;
