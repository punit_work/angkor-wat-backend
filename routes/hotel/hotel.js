const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const HotelController = require("./hotel-controller");
const HotelHelper = require("./hotel-helper");

// Path - /v1/hotel/
// All Routes for hotel

router.post(
  "/add-hotel",
  [
    body("hotel_name")
      .notEmpty()
      .withMessage("Hotel name is required"),
    body("location")
      //.isObject()
      .notEmpty()
      .withMessage("Location object is required"),
    body("address")
      .notEmpty()
      .withMessage("Address is required"),
    body("province")
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid province id"),
    body("district")
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid district id"),
    body("commune")
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid commune id"),
    body("postal_code")
      .notEmpty()
      .isNumeric()
      .withMessage("Please provide valid postal code")
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    const response = await HotelController.addHotel(req);

    if (response.hotelExist) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.HOTEL_EXIST
      );
    }

    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.hotel)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.HOTEL_ADDED_SUCCESSFULLY,
        response.hotel
      );
  }
);

//Get hotels
//v1/hotel/get-hotel
router.get("/get-hotel", async (req, res) => {
  const response = await HotelController.getAllHotels(req);

  if(response.noHotels){
      return Response.error(
          res,
          ResponseCode.NOT_FOUND,
          ResponseMessage.ERROR_NOT_FOUND
      );
  }

  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.hotels)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      ResponseMessage.HOTEL_RETRIVED_SUCCESSFULLY,
      response.hotels
    );
});

//Update hotel
//v1/hotel/update-hotel
router.post(
  "/update-hotel/:hotel_id",
  [
    body("hotel_name")
      .optional()
      .notEmpty()
      .withMessage("Hotel name is required"),
    body("location")
      //.isObject()
      .optional()
      .notEmpty()
      .withMessage("Location object is required"),
    body("address")
      .optional()
      .notEmpty()
      .withMessage("Address is required"),
    body("province")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid province id"),
    body("district")
      .optional() 
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid district id"),
    body("commune")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid commune id"),
    body("postal_code")
      .optional()
      .notEmpty()
      .isNumeric()
      .withMessage("Please provide valid postal code")
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    const response = await HotelController.updateHotel(req);

    if (response.hotelNotFound) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Hotel not found"
      );
    }

    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    }
    else if (response.nothingUpdated){

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Nothing updated !"
      );
    }
     else if (response.hotel)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Hotel Updated successfully",
        response.hotel
      );
  }
);

module.exports = router;
