const Province = require("../../model/Province");
const Temple = require("../../model/Temple");

// Function for fetching list of province
async function getAllLocations() {
  let result = {};

  try {
    const province = await Province.find({}, { _id: 1, translations: 1, province_order:1 }).sort({province_order:-1});
    const temple = await Temple.find({},{ _id:1, translations: 1,temple_order:1 }).sort({temple_order:-1});

    if (province.length) {
      result.province = province;
    }
    if (temple.length) {
      result.temple = temple;
    }

    if (province.length || temple.length) {
      return result;
    }
    result.noLocationFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

module.exports = {
  getAllLocations,
};
