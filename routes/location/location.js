const express = require('express')
const router = express.Router();
const authCheck = require('../../middleware/check-auth');
const Response = require('../../global/response');
const ResponseMessage = require('../../global/message');
const ResponseCode = require('../../global/code');
const LocationController = require('./location-controller');

// Path - /v1/locations/get-all-locations
// Route for fetching all locations
router.get('/get-all-locations',

    // Middleware to check if user is authenticated
    authCheck,

    async (req, res) => {

        const response = await LocationController.getAllLocations();

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner found
        else if (response.noLocationFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

        // Sending error response if user enters start date as past date
        else if (response.province || response.temple) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.LOCATION_FETCHED, {province:response.province,temple:response.temple});        
    }
)


module.exports = router;