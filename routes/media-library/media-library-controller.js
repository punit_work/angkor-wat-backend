//Models
const mongoose = require('mongoose'); 
const mediaLibrary = require("../../model/MediaLibrary");
const MediaLibraryHelper = require("./media-library-helper");
const fs = require('fs');

//Add media
exports.addMedia = async req => {

  let result = {};

  const { user_id, user_type} = req;
  const {module} = req.body;

  if(['super_admin', 'discover', 'author','editor','commerce','business_owner'].includes(user_type)){

    try {

      const {images,videos} = req.files;

      //extract image file path from images and save image

      let imageData,videoData,imageUploadStatus,videoUploadStatus;

        if (images) {
          imageData = images.map(({ path }) =>
              path.replace(/\\/g, "/")
          );
        }

        if (videos) {
          videoData = videos.map(({ path }) =>
              path.replace(/\\/g, "/")
          );

        }

        if(imageData && imageData.length > 0){

          const data = imageData.map((path)=>{

            const mappedData = {

              path: path,
              added_by : user_id,
              module:module,
              type:"Image"

            }

            return mappedData;

          })

            try{

            imageUploadStatus = await mediaLibrary.insertMany(data);

            }
            catch(e){
              console.log(e);
              result.databaseError = true;
              return result;
            }

        
        }
        if(videoData && videoData.length > 0){

          const data = videoData.map((path)=>{

            const mappedData = {

              path: path,
              added_by : user_id,
              module:module,
              type:"Video"

            }

            return mappedData;

          })

            try{

            videoUploadStatus = await mediaLibrary.insertMany(data);

            }
            catch(e){
              console.log(e);
              result.databaseError = true;
              return result;
            }

 

        }

        let message = "";
        let responseData = {};

        if(imageUploadStatus && videoUploadStatus){

          message = `${imageUploadStatus.length} Images and ${videoUploadStatus.length} Videos are uploaded successfully !`;
          responseData.imageData = imageUploadStatus;
          responseData.videoData = videoUploadStatus;

        }
        else if(imageUploadStatus){

          console.log(imageUploadStatus);

          message = `${imageUploadStatus.length} Images are uploaded successfully !`
          responseData.imageData = imageUploadStatus;

        }
        else if(videoUploadStatus){

          message = `${videoUploadStatus.length} Videos are uploaded successfully !`
          responseData.videoData = videoUploadStatus;

        }

        result.success =true;
        result.message = message;
        result.data = responseData;
        return result
        

    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }

  }
  else{
    result.unAuthorised = true;
    return result;

  }
 
};


//Get media
exports.getMedia = async req =>{

  let result = {};

  const { user_id, user_type} = req;
  const {module,type} = req.query;
  let page,limit;

  if(req.query.page && req.query.limit){

    page = parseInt(req.query.page);
    limit = parseInt(req.query.limit);

  }

  

  try{

    let query = {};
    if(module && type){

      query = {module:module,type:type}
    }
    else if(module){
      query = {module:module}

    }
    else if(type){
      query = {type:type}
    }


    if(page && limit){

      let TotalData,MediaData;

      TotalData = await mediaLibrary.countDocuments(query);
      MediaData = await mediaLibrary.find(query).sort({createdAt:1}).skip((page - 1) * limit).limit(limit);


      const response = MediaLibraryHelper.paginateData(page, limit, TotalData);
      response.totalData = TotalData;
      response.mediaData = MediaData;

      if(TotalData > 0){

        result.success = true;
        result.data = response;
        return result;
  
        
      }
      else{
  
        result.dataNotFound = true;
        return result;
  
      }

      }
    else{

    const data = await mediaLibrary.find(query);

    if(data && data.length > 0){

      const response = {};
      response.mediaData = data; 

      result.success = true;
      result.data = response;
      return result;

      
    }
    else{

      result.dataNotFound = true;
      return result;

    }
  }


  }
  catch(err){

    console.log(err);
    result.databaseError = true;
    return result;


  }




}


//Delete media
exports.deleteMedia = async req =>{

  let result = {};
  const {user_id,user_type} = req;

  const {media_ids} = req.body;

 
  try{

    const mappedMediaIds = media_ids.map((id) => {
      return mongoose.Types.ObjectId(id)
    })
  

    const mediaData = await mediaLibrary.find({ _id: { $in: mappedMediaIds } });

    // Delete from local
    if(mediaData && mediaData.lenght > 0){

      for(data of mediaData){

        fs.unlink(data.path, err => console.log(err));
  
      }
    }

    // Delete from db
    const deleteStatus = await mediaLibrary.deleteMany( { _id: { $in: mappedMediaIds } });

    deleteStatus.deletedCount > 0 ? result.success = true : result.error = true;
  
    if(result.success){
      result.message = `Deleted ${deleteStatus.deletedCount} media`;
    } 
    if(result.error){
      result.message = `Noting deleted !`;
    }
  
    return result;

  }
  catch(err){

    console.log(err);
    result.databaseError = true;
    return result;


  }


}