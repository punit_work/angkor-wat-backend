const { removeFilesIfExist, doFileExist, doFilesExist } = require('../../middleware/check-file.js');

 function RemoveFiles (files){

    if (files.images) {
        doFilesExist(files.images);
      }
      if (files.videos) {
        doFilesExist(files.videos);
      }

}

function paginateData(page, limit, data) {

  const paginationResult = {};

  if (page > 0 && limit > 0) {

      const startIndex = (page - 1) * limit;
      const endIndex = page * limit;

      if (startIndex > 0) {

          paginationResult.previousPage = {
              page: parseInt(page) - 1,
              limit: parseInt(limit)
          }
      }

      if (endIndex < data) {

          paginationResult.nextPage = {
              page: parseInt(page) + 1,
              limit: parseInt(limit)
          }
      }

      return paginationResult;
  }
}


module.exports = {
    RemoveFiles,
    paginateData
}