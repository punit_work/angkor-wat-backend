const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const MediaLibraryController = require("./media-library-controller");
const MediaLibraryHelper = require("./media-library-helper");

const authCheck = require("../../middleware/check-auth");
const { removeFilesIfExist, doFileExist, doFilesExist } = require('../../middleware/check-file.js');

const uploadMedia = require("../../middleware/upload-media");


// Path - /v1/media-library/
// All Routes for media library
router.post(
  "/add-media",
  uploadMedia.fields([{ name: 'images' }, { name: 'videos' }]),
  authCheck,
  [
    body("module")
    .notEmpty()
    .withMessage("Please select valid module from ['Discover', 'Shop', 'Experience','Organization'] "),
  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      MediaLibraryHelper.RemoveFiles(req.files);
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    if(!['Discover', 'Shop', 'Experience','Organization'].includes(req.body.module)){

      MediaLibraryHelper.RemoveFiles(req.files);

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide valid module name from [ 'Discover', 'Shop', 'Experience','Organization' ]"
      )

    }

    const {images,videos} = req.files;

    if(!(images || videos)){

      MediaLibraryHelper.RemoveFiles(req.files);

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please upload image or video"
      )


    }
    if(images && images.length > 5){

      MediaLibraryHelper.RemoveFiles(req.files);

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Cannot upload more than 5 images"
      )

    }
    if(videos && videos.length > 5){

      MediaLibraryHelper.RemoveFiles(req.files);

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Cannot upload more than 5 vidoes"
      )

    }

    const response = await MediaLibraryController.addMedia(req);

    if(response.unAuthorised){

      MediaLibraryHelper.RemoveFiles(req.files);

      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      )

    }

    if (response.error) {
      MediaLibraryHelper.RemoveFiles(req.files);
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
       response.message
      );
    }

    if (response.databaseError) {

      MediaLibraryHelper.RemoveFiles(req.files);

      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.success)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        response.message,
        response.data
      );
  }
);

//Get Media
//v1/media-library/get-media
router.get("/get-media", 

authCheck,

[
  query('module').optional().isString().withMessage("module param is required !"),
  query('type').optional().isString().withMessage("type param is required !"),
  query('page').optional().isFloat({ min: 1}),
  query('limit').optional().isFloat({ min: 1 })
],

async (req, res) => {

  const {module,type} = req.query;

  if(type && !['Image','Video'].includes(type)){

    return Response.error(
      res,
      ResponseCode.UNPROCESSABLE_ENTITY,
      "Type must be Image or Video"
    );

  }

  const response = await MediaLibraryController.getMedia(req);

  if(response.dataNotFound){
      return Response.error(
          res,
          ResponseCode.NOT_FOUND,
          ResponseMessage.ERROR_NOT_FOUND
      );
  }

  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.success)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      "Data retrived successfully !!",
      response.data
    );
});

// Delete files

router.post("/delete-media", 

  authCheck,
  
  [

    body('media_ids').isArray({min: 1})

  ],

  async (req, res) => {

    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    const response = await MediaLibraryController.deleteMedia(req);

    if(response.unAuthorised){

      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      )

    }

    if (response.error) {

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
       response.message
      );

    }

    if (response.databaseError) {

      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } 
    else if (response.success){

      return Response.success(
        res,
        ResponseCode.SUCCESS,
        response.message,
        response.data
      );
    }
    
  

})



module.exports = router;
