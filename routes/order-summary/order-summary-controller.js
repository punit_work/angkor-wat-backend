//Models
const orderSummary = require("../../model/OrderSummary");
const cart = require("../../model/Cart");
const shipping = require("../../model/Shipping");
const experienceShipping = require('../../model/ExperienceShipping');
const experienceCart = require('../../model/ExperienceCart');
const experienceSummary = require("../../model/ExperienceSummary");
const ShippingController = require('../shipping/shipping-controller');
const CartController = require('../cart/cart-controller');
const moment = require('moment');
const service = require("../../model/Service");
const mongoose = require('mongoose'); 
const { v4: uuidv4 } = require('uuid');


 
// Helper function

const {getBookingInsights,generateOrderId} = require('./order-summary-helper');


//Add order summary 
exports.addOrderSummary = async req => {
  let result = {};
  const {user_type,user_id} = req;

  if (user_type === "end_user") {
    try {

      const summaryData = {};

      summaryData.user_id = user_id;

      // Get Shipping details
      try {

        const response = await ShippingController.getShippingDetails(req);

        if(response.shipping.length>0){

          const shippingData = response.shipping[0];

          const shippingDetails = {};

          shippingDetails.delivery_type = shippingData.delivery_type;

          if(shippingData.delivery_type === 'hotel'){

            let hotel_details = {};

            hotel_details.address = shippingData.hotel_details.address;
            hotel_details.commune = shippingData.hotel_details.commune.commune_name;
            hotel_details.district = shippingData.hotel_details.district.district_name;
            hotel_details.province = shippingData.hotel_details.province.province_name;
            hotel_details.postal_code = shippingData.hotel_details.commune.postal_code;

            shippingDetails.hotel_details = hotel_details;
            shippingDetails.billing_details = shippingData.billing_details


          }
          else if(shippingData.delivery_type === 'user-address'){

            shippingDetails.billing_details = shippingData.billing_details

          }
          else if(shippingData.delivery_type === 'self-collection'){

            shippingDetails.self_pickup = shippingData.self_collection
            shippingDetails.billing_details = shippingData.billing_details

          }

          summaryData.shipping_details = shippingDetails;


        }
        else{

          console.log("Error at shipping details : ",error);
          result.noShippingDetails = true;
          return result;

        }
        
        
      } catch (error) {

        console.log("Error at shipping details : ",error);
        result.noShippingDetails = true;
        return result;
        
      }
    
    
      // Get Cart details
      try{

        const response = await CartController.getAllCartProducts(req);

        if(response.cart){

          summaryData.cart_details = response.cart.cartItems; 
          summaryData.order_total =  response.cart.subTotal;
          
        }
        else{

          result.noCartDetails = true;
          return result;

        }

      }
      catch(error){

        console.log("Error at cart details : ",error);
        result.noCartDetails = true;
        return result;

      }

      
      // Generate order date
      summaryData.order_date = new Date()

      // Add payment details 

      // Add dispatched date , that will be expected date if delivery status is not cancelled or dispatched

      // Generate order id
      summaryData.order_id = generateOrderId(user_id);
      summaryData.is_active = true // to identify current order


      result.summaryData = await new orderSummary(summaryData).save()
      return result
      
    } catch (error) {
      console.log(error)
      result.databaseError = true;
      return result;
    }
  }
  else {
    result.unAuthorised = true;
    return result;
  }
};

//Get order summary
exports.getOrderSummary = async req => {
  let result = {};

  if (req.user_type === "end_user") {
    try {

      if (req.body.show_order_history === true) {

        // show order history
        const orderHistory = await orderSummary.find({ user_id: req.user_id}).sort({ createdAt: -1 });

        if (orderHistory.length) {
          result.summary = orderHistory;
          return result;
        }
        else {
          result.notFound = true;
          return result;
        }

      }
      //show only latest summary
      const summaryData = await orderSummary.find({ user_id: req.user_id,is_active:true}).sort({ createdAt: -1 });

      if (summaryData) {
        result.summary = summaryData;
        return result;
      } else {
        result.notFound = true;
        return result;
      }
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};

///FOR EXPERIENCE SUMMARY

//Add experience summary
exports.addExperienceSummary = async req => {
  let result = {};

  if (req.user_type === "end_user") {
    try {
      const summaryData = {};

      // storing user id
      summaryData.user_id = req.user_id;

      //storing latest shipping id
      summaryData.shipping_details = await (await experienceShipping.findOne({ user_id: req.user_id }, { _id: 1 }).sort({ createdAt: -1 }))._id;
      if (!summaryData.shipping_details) {
        result.noShippingDetails = true;
        return result;
      }

      //storing product details from cart
      const cartItems = await experienceCart.find({ user_id: req.user_id }).populate("service");

      if (cartItems.length) {
        summaryData.service_details = cartItems.map(data => {
          const { _id, price,service_provider_id } = data.service;
          const for_person = data.quantity;
          const service_name = data.service.service_title;
          const service_img = data.service.service_images;
          return { _id, price, for_person, service_name, service_img , service_provider_id };
        });

        //function to find subtotal
        summaryData.sub_total = cartItems.reduce((total, data) => total + data.service.price * data.quantity, 0);
        summaryData.order_total = summaryData.sub_total;

        // tax - promocode - payment and other details

      }

      result.summary = await new experienceSummary(summaryData).save();
      return result;

    } catch (error) {
      result.databaseError = true;
      return result;
    }
  }
  else {
    result.unAuthorised = true;
    return result;
  }
};



//Get experience summary
exports.getExperienceSummary = async req => {
  let result = {};

  if (req.user_type === "end_user") {
    try {

      if (req.body.show_order_history === true) {

        // show order history
        const orderHistory = await experienceSummary.find({ user_id: req.user_id }).populate("shipping_details");

        if (orderHistory.length) {
          result.summary = orderHistory;
          return result;
        }
        else {
          result.notFound = true;
          return result;
        }

      }
      //show only latest summary
      const summaryData = await experienceSummary
        .findOne({ user_id: req.user_id })
        .populate("shipping_details").sort({ createdAt: -1 });

      if (summaryData) {
        result.summary = summaryData;
        return result;
      } else {
        result.notFound = true;
        return result;
      }
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
};


//Get experience service data by filter
exports.getExperienceDataByFilter = async req =>{

  let result = {};
  let filters = {};
  let projection = {};

  if(req.user_type === "service_provider"){

    try{


      const serviceData = await service.find({service_provider_id : req.user_id},{service_title:1});

      // give all experiences booked by all user    
      const experienceData = await experienceSummary.find({ "service_details.service_provider_id" : req.user_id },{createdAt:1, service_details : { $elemMatch : { service_provider_id : req.user_id }}});
    


      if(experienceData){

        // 1. last week/month/year 2. this week/month/year 3. last-7 days , last-30 days , last-12 months


        // common filters and projection

        filters["service_details.service_provider_id" ] = req.user_id;
        projection["createdAt"] = 1;
        projection["service_details"] = { $elemMatch : { service_provider_id : req.user_id }};

        // 1.
        if(req.body.filterby === "last-week"){

          const lastWeekFromNow =  moment().subtract(1, 'week') ;
          const lastWeekStartDate = moment(lastWeekFromNow).startOf('week') ;
          const lastWeekEndDate =moment(lastWeekStartDate).endOf('week');

          filters["createdAt"] = {$gte : lastWeekStartDate , $lte : lastWeekEndDate };
         
          const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });


          result.experienceData = getBookingInsights(serviceData,filteredData);
          return result;
  
  
        }

        if(req.body.filterby === "last-month"){

          const lastMonthFromNow =  moment().subtract(1, 'month') ;
          const lastMonthStartDate = moment(lastMonthFromNow).startOf('month') ;
          const lastMonthEndDate =moment(lastMonthStartDate).endOf('month');
        
          filters["createdAt"] = {$gte : lastMonthStartDate , $lte : lastMonthEndDate };

          const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });

          result.experienceData = getBookingInsights(serviceData,filteredData);
          return result;
  
        }

        if(req.body.filterby === "last-year"){

          const lastYearFromNow =  moment().subtract(1, 'month') ;
          const lastYearStartDate = moment(lastYearFromNow).startOf('month') ;
          const lastYearEndDate =moment(lastYearStartDate).endOf('month');
         

          filters["createdAt"] = {$gte : lastYearStartDate , $lte : lastYearEndDate };


          filters["createdAt"] = { $lte: moment().toDate(), $gte: moment().subtract(1, 'year') };
        
          const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });

          result.experienceData = getBookingInsights(serviceData,filteredData);
          return result;
        }

        //2.

        if(req.body.filterby === "this-week"){

          filters["createdAt"] = { $lte: moment().toDate(), $gt: moment().startOf('week') };
         
          const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });


          result.experienceData = getBookingInsights(serviceData,filteredData);
          return result;
  
        }

        if(req.body.filterby === "this-month"){

          filters["createdAt"] = { $lte: moment().toDate(), $gt: moment().startOf('month') };
         
          const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });

          result.experienceData = getBookingInsights(serviceData,filteredData);
          return result;
  
        }

        if(req.body.filterby === "this-year"){

          filters["createdAt"] = { $lte: moment().toDate(), $gt: moment().startOf('year') };
         
          const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });
        

          

          result.experienceData = getBookingInsights(serviceData,filteredData);
          return result;

  
  
        }

        //3.
          
          if(req.body.filterby === "last-7-days"){

            filters["createdAt"] = { $lte: moment().toDate(), $gt: moment().subtract(7, 'days') };
           
            const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });

            result.experienceData = getBookingInsights(serviceData,filteredData);
            return result;
    
          }
  
          if(req.body.filterby === "last-30-days"){
  
            filters["createdAt"] = { $lte: moment().toDate(), $gt: moment().subtract(30, 'days') };
           
            const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });

            result.experienceData = getBookingInsights(serviceData,filteredData);
            return result;
    
          }
  
          if(req.body.filterby === "last-12-months"){
  
            filters["createdAt"] = { $lte: moment().toDate(), $gt: moment().subtract(12, 'month') };
          
            const filteredData = await experienceSummary.find(filters,projection).sort({ createdAt: 1 });
  

            result.experienceData = getBookingInsights(serviceData,filteredData);
            return result;
    
          }
  

          result.experienceData = getBookingInsights(serviceData,experienceData);
          return result;
      }
      else{
        result.notFound = true;
        return result;
      }

     
    }
    catch(err){
      result.databaseError = true;
      return result;
    }



  }
  else if (req.user_type === "business_owner"){
    result.unAuthorised = true;
    return result;


  }
  else{

    // INVALID USER FOR FILTERS
    result.unAuthorised = true;
    return result;



  }

}