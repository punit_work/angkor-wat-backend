function getBookingInsights(services,bookingData) {

  // set initial count to 0  
  const serviceData = {} 
  services.map(({service_title})=> serviceData[service_title] = 0 );


 bookingData.map(({service_details}) => {

    

    service_details.map(({service_name,for_person}) => { 

        if(serviceData[service_name] >= 0){

            serviceData[service_name] =  serviceData[service_name] + for_person 

        }
        
        
      
    
    })

  })

  return serviceData

}

function generateOrderId(user_id){

  const ts = + new Date() 
  const min = parseInt(ts.toString().slice(-5,-1))
  const unique =  Math.floor(Math.random() * (9999 - min)) + min
  const order_id = user_id.slice(-4) +"-"+ ts.toString().slice(-4) +"-"+ unique;


  return order_id;
}

module.exports = {
    getBookingInsights,
    generateOrderId
}