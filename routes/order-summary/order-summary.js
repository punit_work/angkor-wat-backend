const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const OrderSummaryController = require("./order-summary-controller");
const OrderSummaryHelper = require("./order-summary-helper");

//middleware
const authCheck = require("../../middleware/check-auth");

// Path - /v1/order-summary/
// All Routes for order-summary

router.post("/add-order-summary", authCheck, async (req, res) => {

    const response = await OrderSummaryController.addOrderSummary(req);

    if (response.unAuthorised) {
        return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }

    if (response.noShippingDetails) {
        return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_NOT_FOUND + 'For shipping details or shipping data missing')
    }
    if (response.noCartDetails) {
        return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_NOT_FOUND + 'For cart details or cart data missing')
    }
    if (response.databaseError) {
        return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.summaryData) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SUMMARY_ADDED, response.summaryData)

});

router.post("/get-order-summary", authCheck, async (req, res) => {

    const response = await OrderSummaryController.getOrderSummary(req);

    if (response.notFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_NOT_FOUND);

    if (response.unAuthorised) {
        return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }
    if (response.databaseError) {
        return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.summary) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SUMMARY_RETRIVED, response.summary)


});


///FOR EXPERIENCE SUMMARY


router.post("/add-experience-summary", authCheck, async (req, res) => {

    const response = await OrderSummaryController.addExperienceSummary(req);

    if (response.unAuthorised) {
        return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }

    if (response.noShippingDetails) {
        return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_NOT_FOUND + 'For shipping details or shipping data missing')
    }
    if (response.databaseError) {
        return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.summary) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SUMMARY_ADDED, response.summary)

});

router.post("/get-experience-summary", authCheck, async (req, res) => {

    const response = await OrderSummaryController.getExperienceSummary(req);

    if (response.notFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_NOT_FOUND);

    if (response.unAuthorised) {
        return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }
    if (response.databaseError) {
        return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.summary) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SUMMARY_RETRIVED, response.summary)


});

router.post("/get-experience-data-by-filter", authCheck , async (req, res) =>{

    const response = await OrderSummaryController.getExperienceDataByFilter(req);

    if (response.notFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_NOT_FOUND);

    if (response.unAuthorised) {
        return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }
    if (response.databaseError) {
        return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.experienceData) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SUMMARY_RETRIVED, response.experienceData)




})

module.exports = router;
