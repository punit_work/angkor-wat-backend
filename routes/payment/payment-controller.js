const PaymentMethods = require('../../model/PaymentMethods');

// Function for adding payment method
async function addPaymentMethod(payment_method_name) {

    let result = {}, paymentMethod;

    try {

        
        paymentMethod = await PaymentMethods.findOne({ payment_method_name: new RegExp(payment_method_name.trim(), 'i')}); 
        
        if (paymentMethod) {

            result.message = 'Payment Method already exist';
            result.paymentMethodAlreadyExist = true;
            return result;
        }

        paymentMethod = await new PaymentMethods({payment_method_name}).save();

        if (paymentMethod) {

            result.paymentMethod = paymentMethod;
            result.message = 'Payment Method added successfully!'
            return result;
        }

    } catch (err) {

        result.databaseError = true;
        return result;
    }
}

// Function for updaing payment method
async function updatePaymentMethod(payment_method_name, payment_method_id) {

    let result = {}, paymentMethod;

    try {

        let existingPaymentMethod = await PaymentMethods.findById(payment_method_id);

        if (!existingPaymentMethod) {

            result.noPaymentMethodFound = true;
            result.message = 'No Payment Method found with provided payment method ID';
            return result;
        }

        paymentMethod = await PaymentMethods.findOne({ payment_method_name: new RegExp(payment_method_name.trim(), 'i')}); 
        
        if (paymentMethod && paymentMethod._id.toString() !== existingPaymentMethod._id.toString()) {

            result.message = 'Payment Method already exist';
            result.paymentMethodAlreadyExist = true;
            return result;
        }

        existingPaymentMethod.payment_method_name = payment_method_name;
        await existingPaymentMethod.save();

        result.paymentMethod = paymentMethod;
        result.message = 'Payment Method updated successfully!'
        return result;

    } catch (err) {

        result.databaseError = true;
        return result;
    }
}

// Function for fetching payment methods
async function getPaymentMethods() {

    let result = {};

    try {

        let paymentMethods = await PaymentMethods.find({is_deleted: false});

        if (!paymentMethods.length) {

            result.noPaymentMethodsFound = true;
            result.message = 'No payment methods found';
            return result;
        }

        result.paymentMethods = paymentMethods;
        result.message = 'Payment Methods fetched successfully';
        return result;

    } catch (err) {

        result.databaseError = true;
        return result;
    }
}

// Function for deleting payment method
async function deletePaymentMethod(payment_method_id) {

    let result = {};

    try {

        let paymentMethod = await PaymentMethods.findOne({is_deleted: false, _id: payment_method_id});

        if (!paymentMethod) {

            result.noPaymentMethodFound = true;
            result.message = 'No Payment method found with provided ID';
            return result;
        }

        paymentMethod.is_deleted = true;
        await paymentMethod.save();

        result.paymentMethodDeleted = true;
        result.message = 'Payment Method deleted successfully';
        return result;

    } catch (err) {

        result.databaseError = true;
        return result;
    }
}

module.exports = {
    addPaymentMethod,
    updatePaymentMethod,
    getPaymentMethods,
    deletePaymentMethod
}