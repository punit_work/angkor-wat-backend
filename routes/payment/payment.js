const PaymentController = require('./payment-controller');
const express = require('express');
const router = express.Router();
const Response = require('../../global/response');
const ResponseCode = require('../../global/code');
const ResponseMessage = require('../../global/message');
const authCheck = require('../../middleware/check-auth');

router.get('/get-payment-methods',

    authCheck,

    async (req, res) => {

        const response = await PaymentController.getPaymentMethods();

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no payment method found
        else if (response.noPaymentMethodsFound) return Response.error(res, ResponseCode.NOT_FOUND, response.message);

        // Sending success response if payment methods fetched
        else if (response.paymentMethods) return Response.success(res, ResponseCode.SUCCESS, response.message, response.paymentMethods);
    }
)

module.exports = router;