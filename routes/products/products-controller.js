//models
const shop = require("../../model/Shop");
const category = require("../../model/Category");
const Product = require("../../model/Product");
const Ratings = require("../../model/Ratings");
const moment = require("moment");
const Reviews = require("../../model/Reviews");
const Users = require("../../model/Users");
const Wishlist = require("../../model/Wishlist");
const mongoose = require("mongoose");
const { populateProductFields } = require("./products-helper");
const { addTranslatedData } = require("../../global/util");

exports.getAllProducts = async (req) => {
  let productList = [],
    result = {};

  try {
    productList = await Product.aggregate([
      {
        $match: {
          shop_id: mongoose.Types.ObjectId(req.params.shop_id),
        },
      },
      ...populateProductFields,
      {
        $lookup: {
          from: "wishlists",
          let: {
            current_user: mongoose.Types.ObjectId(req.user_id),
            current_product: "$_id",
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ["$user_id", "$$current_user"] },
                    { $eq: ["$product", "$$current_product"] },
                  ],
                },
              },
            },
            {
              $project: {
                product: 1,
                product_variant: 1,
              },
            },
          ],
          as: "wishlist",
        },
      },
      {
        $unwind: {
          path: "$wishlist",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          "product_images": 1,
          "tags": 1,
          "translations":1,
          "category": 1,
          "sub_category": 1,
          "is_featured": 1,
          "createdAt": 1,
          "updatedAt": 1,
          "shop.shop_name": 1,
          "shop.province": 1,
          "rating.average_rating": 1,
          "variants": 1,
          "wishlist": 1,
        },
      },
    ]);

    if (productList.length) {
      result.products = productList;
      return result;
    } else {
      result.noProductFound = true;
      return result;
    }
  } catch (error) {
    console.log(error);
    result.databaseError = true;
    return result;
  }
};

exports.getProductById = async (req) => {
  let result = {};

  try {
    const basicFilter = [
      {
        $match: {
          _id: mongoose.Types.ObjectId(req.params.product_id),
        },
      },
      ...populateProductFields,
      {
        $lookup: {
          from: "system_users",
          as: "business_owner",
          localField: "shop.business_owner_id",
          foreignField: "_id",
        },
      },
      {
        $unwind: "$business_owner",
      },
      {
        $unwind: {
          path: "$rating",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $addFields: {
          "rating.rated_by": {
            $filter: {
              input: "$rating.rated_by",
              as: "var",
              cond: {
                $eq: ["$$var.user_id", mongoose.Types.ObjectId(req.user_id)],
              },
            },
          },
          "user_rating": { $first: "$rating.rated_by" },
        },
      },
      {
        $lookup: {
          from: "wishlists",
          let: {
            current_user: mongoose.Types.ObjectId(req.user_id),
            current_product: "$_id",
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ["$user_id", "$$current_user"] },
                    { $eq: ["$product", "$$current_product"] },
                  ],
                },
              },
            },
            {
              $project: {
                product: 1,
                product_variant: 1,
              },
            },
          ],
          as: "wishlist",
        },
      },
      {
        $unwind: {
          path: "$wishlist",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          "product_images": 1,
          "tags": 1,
          "shop_id": 1,
          "translations":1,
          "category": 1,
          "sub_category": 1,
          "sub_sub_category": 1,
          "is_featured": 1,
          "createdAt": 1,
          "updatedAt": 1,
          "shop.translations": 1,
          "shop.province": 1,
          "shop._id": 1,
          "show_reviews": 1,
          "variants": 1,
          "rating.no_of_ratings": 1,
          "rating.average_rating": 1,
          "rating.user_rating": "$user_rating.rating",
          "business_owner": "$business_owner._id",
          "wishlist": 1,
        },
      },
    ];

    const productData = await Product.aggregate(basicFilter);

    if (productData) {
      result.product = productData[0];
    } else {
      result.noProductFound = true;
    }
    return result;
  } catch (error) {
    console.log(error);

    result.databaseError = true;
    return result;
  }
};

exports.addProduct = async (req) => {
  let result = {};

  // CHECK FOR IMAGE FILE AND AUTHORIZED USER
  if (req.user_type === "end_user") {
    result.unAuthorized = true;
    return result;
  }

  // product_name: req.body.product_name,

  const productData = {
    shop_id: req.body.shop_id,
    category: req.body.category,
    sub_category: req.body.sub_category,
  };

  // if (req.body.product_description) {
  //   // const productDesc = [];
  //   // for (const iterator of req.body.product_description) {
  //   //   productDesc.push(JSON.parse(iterator))
  //   // }
  //   productData.product_description = JSON.parse(req.body.product_description);
  // }

  const translations = {
    en:{},
    kh:{}
  };

  let hasError;

   await addTranslatedData({
     data:JSON.parse(req.body.en),
     lang:'en',
     requiredFields:['product_name','product_description'],
     translationObject:translations
    }).catch(err => {
     hasError = true;
     result['validationError'] = true;
     result['message'] = `${err}`;
   });

   if(hasError){
    return result;
  }

   await addTranslatedData({
     data:JSON.parse(req.body.kh),
     lang:'kh',
     requiredFields:['product_name','product_description'],
     translationObject:translations
    }).catch(err => {
    hasError = true;
    result['validationError'] = true;
    result['message'] = `${err}`;
  });

  if(hasError){
    return result;
  }

  productData.translations = translations;

  if (req.body.variants) {
    // const variant = [];
    // for (const iterator of req.body.variants) {
    //   variant.push(JSON.parse(iterator))
    // }
    productData.variants = JSON.parse(req.body.variants);
  }

  try {
    //check if owner is valid or not
    const isValidOwner = await shop.findOne({
      business_owner_id: req.user_id,
    });

    if (!isValidOwner) {
      result.notValidOwner = true;
      return result;
    }

    //verify category and sub category
    const isValidCategory = await category.findOne({
      _id: productData.category,
      sub_category: { $elemMatch: { _id: productData.sub_category } },
    });

    if (!isValidCategory) {
      result.notValidCategory = true;
      return result;
    }

    //extract image file path from images and save product
    if (
      req.files &&
      req.files.product_images &&
      req.files.product_images.length > 0
    ) {
      productData.product_images = req.files.product_images.map((image) =>
        image.path.replace(/[\\]/g, "/")
      );
    }

    let modifiedVariants = [],
      color_images = [];

    if (req.files.color_images) {
      color_images = req.files.color_images;
      for (let data of productData.variants) {
        if (data.images_count) {
          let obj = {
            ...data,
            images: color_images
              .splice(0, data.images_count)
              .map((image) => image.path.replace(/\\/g, "/")),
          };

          modifiedVariants.push(obj);
        }
      }

      productData.variants = modifiedVariants;
    }
    result.product = await new Product(productData).save();
  } catch (error) {
    console.log(error);
    result.databaseError = true;
    return result;
  }

  return result;
};

exports.deleteProduct = async (req) => {
  let productData,
    result = {};

  if (req.user_type === "end_user") {
    result.notValidOwner = true;
    return result;
  }

  try {
    productData = await Product.findOne({
      _id: req.params.product_id,
    });

    if (!productData) {
      result.noProductFound = true;
      return result;
    } else {
      //check if authorized shop owner
      const isAuthorizedOwner = await shop.find({
        _id: req.user_id,
      });
      if (isAuthorizedOwner.length === 0) {
        result.notValidOwner = true;
        return result;
      }
    }

    await ProductData.remove();
    result.productDeleted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.updateProduct = async (req) => {
  let products,
    isProductExist,
    result = {};


  if (req.user_type === "end_user") {
    result.notValidOwner = true;
    return result;
  }

  const productData = { ...req.body };

  try {
    //product not found
    isProductExist = await Product.findOne({ _id: req.params.product_id });

    if (isProductExist === null) {
      result.noProductFound = true;
      return result;
    }

    //if product exist than check for valid owner

    const isAuthorizedOwner = await shop.find({
      $or:[
        {business_owner_id: req.user_id},
        {owner_id: req.user_id},
      ]
    });

    if (isAuthorizedOwner.length === 0) {
      result.notValidOwner = true;
      return result;
    }
    //check for valid category
    //verify category and sub category if any of this is provided
    if (req.body.category) {
      if (req.body.sub_sub_category && req.body.sub_category) {
        const isValidCategoryAndSubCategoryAndSubSubCategory =
          await category.findOne({
            _id: productData.category,
            sub_category: { $elemMatch: { _id: productData.sub_category } },
            sub_sub_category: {
              $elemMatch: { _id: productData.sub_sub_category },
            },
          });
        if (!isValidCategoryAndSubCategoryAndSubSubCategory) {
          result.notValidCategory = true;
          return result;
        }
      } else if (req.body.sub_category) {
        const isValidCategoryAndSubCategory = await category.findOne({
          _id: productData.category,
          sub_category: { $elemMatch: { _id: productData.sub_category } },
        });
        if (!isValidCategoryAndSubCategory) {
          result.notValidCategory = true;
          return result;
        }
      } else if (req.body.sub_sub_category) {
        const isValidCategoryAndSubSubCategory = await category.findOne({
          _id: productData.category,
          sub_sub_category: {
            $elemMatch: { _id: productData.sub_sub_category },
          },
        });
        if (!isValidCategoryAndSubSubCategory) {
          result.notValidCategory = true;
          return result;
        }
      } else {
        const isValidCategory = await category.findOne({
          _id: productData.category,
        });
        if (!isValidCategory) {
          result.notValidCategory = true;
          return result;
        }
      }
    }
    // check for image
    if (req.files && req.files && req.files.length > 0) {
      productData.product_images = req.files.map((image) =>
        image.path.replace(/[\\]/g, "/")
      );
    }

    if (
      productData.remove_product_images &&
      JSON.parse(productData.remove_product_images).length > 0
    ) {
      productData.product_images = isProductExist.product_images.filter(
        (img) => {
          if (JSON.parse(productData.remove_product_images).includes(img))
            fs.unlink(img, (err) => console.log(err));
          else return img;
        }
      );
    }

    // let modifiedVariants = [], color_images = [];

    // if (req.files.color_images) {

    //   color_images = req.files.color_images;
    //   for (let data of productData.variants) {
    //     if (data.images_count) {

    //       let obj = {
    //         ...data,
    //         images: isProductExist.variants && isProductExist.variants.length && isProductExist.variants.findIndex(variant => variant._id.toString() == data.variant_id) !== -1 ? isProductExist.variants[isProductExist.variants.findIndex(variant => variant._id.toString() == data.variant_id)].images.concat(color_images.splice(0, data.images_count).map(image => image.path.replace(/\\/g, '/'))) : color_images.splice(0, data.images_count).map(image => image.path.replace(/\\/g, '/'))
    //       };

    //       modifiedVariants.push(obj);
    //     }
    //   }

    //   productData.variants = modifiedVariants;
    // }

    const translations = {
      en:{},
      kh:{}
    };
  
    let hasError;
  
     await addTranslatedData({
       data:JSON.parse(req.body.en),
       lang:'en',
       requiredFields:['product_name','product_description'],
       translationObject:translations
      }).catch(err => {
       hasError = true;
       result['validationError'] = true;
       result['message'] = `${err}`;
     });
  
     if(hasError){
      return result;
    }

     await addTranslatedData({
       data:JSON.parse(req.body.kh),
       lang:'kh',
       requiredFields:['product_name','product_description'],
       translationObject:translations
      }).catch(err => {
      hasError = true;
      result['validationError'] = true;
      result['message'] = `${err}`;
    });
  
    if(hasError){
      return result;
    }

    productData.translations = translations;

    delete productData.en;
    delete productData.kh;


    // update product
    products = await Product.findOneAndUpdate(
      { _id: req.params.product_id },
      { ...productData },
      {
        new: true,
      }
    );

    result.products = products;
    return result;
  } catch (error) {
    result.databaseError = true;
    return result;
  }
};

exports.filterBy = async (req) => {
  let productsList = [],
    result = {};

  if (req.user_type !== "end_user") {
    // no user other then 'end_user' is allowed to access this api
    result.invalidUser = true;
    return result;
  }
  // const perPage = 10;
  // const currentPage = req.query.page || 1;

  // const totalProducts = 0;
  const filterType = req.query.filter;

  if (filterType === "category") {
    let seperatedValues = [];
    let input = await req.query.category.split(",");

    input.forEach((element) => {
      seperatedValues.push(element);
    });

    // filtering by category
    // productsList = await Product.find({ $or : [{ 'category' : req.query.category} , {'sub_category' : req.query.sub_category}]})

    try {
      productsList = await Product.find({
        $or: [
          { category: { $in: seperatedValues } },
          { category: { $in: seperatedValues }, shop_id: req.query.shop_id },
        ],
      });
    } catch (err) {
      result.databaseError = true;
      return result;
    }

    // if no products are found
    if (!productsList.length) {
      result.noProductsFound = true;
      return result;
    }

    let productList = [];

    for (let prod of productsList) {
      const product_id = prod._id;

      let productObject = {
        product_images: prod.product_images,
        tags: prod.tags,
        _id: product_id,
        price: prod.price,
        shop_id: prod.shop_id,
        product_name: prod.product_name,
        product_description: prod.product_description,
        category: prod.category,
        available_quantity: prod.available_quantity,
        in_stock: prod.in_stock,
        sub_category: prod.sub_category,
        no_of_ratings: prod.no_of_ratings,
        average_rating: prod.average_rating,
        is_product_in_wishlist: await Wishlist.countDocuments({
          product: product_id,
          user_id: req.user_id,
        }),
      };

      productList.push(productObject);
    }

    result.list = productList;
    return result;
  } else if (filterType === "sub_category") {
    let seperatedValues = [];
    let input = await req.query.sub_category.split(",");

    input.forEach((element) => {
      seperatedValues.push(element);
    });

    try {
      if (req.query.new_arrivals !== "true") {
        productsList = await Product.find({
          $or: [
            { sub_category: { $in: seperatedValues } },
            {
              sub_category: { $in: seperatedValues },
              shop_id: req.query.shop_id,
            },
          ],
        });
      } else {
        const start_date = moment(
          new Date(new Date().setDate(new Date().getDate() - 7))
        )
          .startOf("day")
          .toDate();
        const end_date = moment(
          new Date(new Date().setDate(new Date().getDate()))
        )
          .endOf("day")
          .toDate();

        productsList = await Product.find({
          sub_category: { $in: seperatedValues },
          createdAt: { $gte: start_date, $lte: end_date },
        }).sort({ createdAt: -1 });
      }
    } catch (err) {
      result.databaseError = true;
      return result;
    }

    // if no products are found
    if (!productsList.length) {
      result.noProductsFound = true;
      return result;
    }

    let productList = [];

    for (let prod of productsList) {
      const product_id = prod._id;

      let productObject = {
        product_images: prod.product_images,
        tags: prod.tags,
        _id: product_id,
        price: prod.price,
        shop_id: prod.shop_id,
        product_name: prod.product_name,
        product_description: prod.product_description,
        category: prod.category,
        available_quantity: prod.available_quantity,
        in_stock: prod.in_stock,
        sub_category: prod.sub_category,
        no_of_ratings: prod.no_of_ratings,
        average_rating: prod.average_rating,
        is_product_in_wishlist: await Wishlist.countDocuments({
          product: product_id,
          user_id: req.user_id,
        }),
      };

      productList.push(productObject);
    }

    result.list = productList;
    return result;
  } else if (filterType === "price") {
    let min_price = req.query.min_price || 0;
    let max_price = 0;

    // process this block of code if max price is not provided
    if (isNaN(min_price)) {
      result.minPriceIsNaN = true;
      return result;
    }

    if (req.query.max_price) {
      if (isNaN(req.query.max_price)) {
        result.maxPriceIsNaN = true;
        return result;
      }
      max_price = req.query.max_price;

      // find products if max price is also provided
      try {
        productsList = await Product.find({
          price: { $gte: min_price, $lte: max_price },
        });
      } catch (err) {
        result.databaseError = true;
        return result;
      }

      // if no products are found
      if (!productsList.length) {
        result.noProductsFound = true;
        return result;
      }

      let productList = [];

      for (let prod of productsList) {
        const product_id = prod._id;

        let productObject = {
          product_images: prod.product_images,
          tags: prod.tags,
          _id: product_id,
          price: prod.price,
          shop_id: prod.shop_id,
          product_name: prod.product_name,
          product_description: prod.product_description,
          category: prod.category,
          available_quantity: prod.available_quantity,
          in_stock: prod.in_stock,
          sub_category: prod.sub_category,
          no_of_ratings: prod.no_of_ratings,
          average_rating: prod.average_rating,
          is_product_in_wishlist: await Wishlist.countDocuments({
            product: product_id,
            user_id: req.user_id,
          }),
        };

        productList.push(productObject);
      }

      result.list = productList;
      return result;
    }

    //filtering by min price
    try {
      productsList = await Product.find({ price: { $gte: min_price } });
    } catch (err) {
      result.databaseError = true;
      return result;
    }

    // if no products are found
    if (!productsList.length) {
      result.noProductsFound = true;
      return result;
    }

    let productList = [];

    for (let prod of productsList) {
      const product_id = prod._id;

      let productObject = {
        product_images: prod.product_images,
        tags: prod.tags,
        _id: product_id,
        price: prod.price,
        shop_id: prod.shop_id,
        product_name: prod.product_name,
        product_description: prod.product_description,
        category: prod.category,
        available_quantity: prod.available_quantity,
        in_stock: prod.in_stock,
        sub_category: prod.sub_category,
        no_of_ratings: prod.no_of_ratings,
        average_rating: prod.average_rating,
        is_product_in_wishlist: await Wishlist.countDocuments({
          product: product_id,
          user_id: req.user_id,
        }),
      };

      productList.push(productObject);
    }

    result.list = productList;
    return result;
  } else {
    // if none of the category matches
    result.categoryError = true;
    return result;
  }
};

exports.getFilteredProducts = async (req) => {
  let productsList = [],
    result = {};

  if (req.user_type !== "end_user") {
    // no user other then 'end_user' is allowed to access this api
    result.invalidUser = true;
    return result;
  }

  try {
    // Multiple filter and sort
    if (req.body.filterBy === "multipleFilter") {
      // Apply multiple filter individual & with sorting option.
      // Without sorting option.
      const filters = req.body.multipleFilter.split(",");

      // cases -> sub_sub_category & price , sub_sub_Category & ratings , price & ratings  , sub_sub_Category,price,ratings.
      // if at any case sub_sub_category is present in multiple filters we must have req.body.sub_sub_category.

      // for category & sub category
      const basicFilter = [
        {
          $match: {
            category: mongoose.Types.ObjectId(req.body.category_id),
            sub_category: mongoose.Types.ObjectId(req.body.sub_category_id),
          },
        },
        ...populateProductFields,
        {
          $lookup: {
            from: "wishlists",
            let: {
              current_user: mongoose.Types.ObjectId(req.user_id),
              current_product: "$_id",
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ["$user_id", "$$current_user"] },
                      { $eq: ["$product", "$$current_product"] },
                    ],
                  },
                },
              },
              {
                $project: {
                  product: 1,
                  product_variant: 1,
                },
              },
            ],
            as: "wishlist",
          },
        },
        {
          $unwind: {
            path: "$wishlist",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            "product_images": 1,
            "tags": 1,
            "product_name": 1,
            "product_description": 1,
            "category": 1,
            "sub_category": 1,
            "sub_sub_category": 1,
            "available_quantity": 1,
            "in_stock": 1,
            "is_featured": 1,
            "createdAt": 1,
            "updatedAt": 1,
            "shop.shop_name": 1,
            "shop.province": 1,
            "variants": 1,
            "rating.average_rating": 1,
            "wishlist": 1,
          },
        },
      ];
      let basicFilterByProvince = [];
      let sortBy = {};
      let sub_sub_category = [];

      if (req.body.sub_sub_category) {
        sub_sub_category = req.body.sub_sub_category.split(",");
        sub_sub_category = sub_sub_category.map((id) =>
          mongoose.Types.ObjectId(id)
        );
        console.log(sub_sub_category);
      }

      // nested filteres
      let nestedFilter = {};

      if (filters.length === 2) {
        if (filters.includes("sub_sub_category") && filters.includes("price")) {
          // Filter by sub_sub_category and price
          nestedFilter = {
            $match: {
              "sub_sub_category": { $in: sub_sub_category },
              "variants.0.price": {
                $gte: req.body.min_price,
                $lte: req.body.max_price,
              },
            },
          };
        } else if (
          filters.includes("sub_sub_category") &&
          filters.includes("ratings")
        ) {
          // Filter by sub_sub_category and ratings
          nestedFilter = {
            $match: {
              "sub_sub_category": { $in: sub_sub_category },
              "rating.average_rating": { $exists: true },
              "rating.average_rating": { $gte: req.body.ratings },
            },
          };
        } else if (filters.includes("price") && filters.includes("ratings")) {
          // Filter by price and ratings
          nestedFilter = {
            $match: {
              "rating.average_rating": { $exists: true },
              "rating.average_rating": { $gte: req.body.ratings },
              "variants.0.price": {
                $gte: req.body.min_price,
                $lte: req.body.max_price,
              },
            },
          };
        } else {
          result.inValidFilterSelection = true;
          return result;
        }
      } else if (filters.length === 3) {
        if (
          filters.includes("price") &&
          filters.includes("ratings") &&
          filters.includes("sub_sub_category")
        ) {
          // Filter by sub_sub_category and ratings and price
          nestedFilter = {
            $match: {
              "sub_sub_category": { $in: sub_sub_category },
              "variants.0.price": {
                $gte: req.body.min_price,
                $lte: req.body.max_price,
              },
              "rating.average_rating": { $exists: true },
              "rating.average_rating": { $gte: req.body.ratings },
            },
          };
        } else {
          result.inValidFilterSelection = true;
          return result;
        }
      } else {
        result.inValidFilterSelection = true;
        return result;
      }

      if (req.body.province) {
        basicFilterByProvince = [
          ...populateProductFields,
          {
            $lookup: {
              from: "wishlists",
              let: {
                current_user: mongoose.Types.ObjectId(req.user_id),
                current_product: "$_id",
              },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ["$user_id", "$$current_user"] },
                        { $eq: ["$product", "$$current_product"] },
                      ],
                    },
                  },
                },
                {
                  $project: {
                    product: 1,
                    product_variant: 1,
                  },
                },
              ],
              as: "wishlist",
            },
          },
          {
            $unwind: {
              path: "$wishlist",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $match: {
              "shop.province": mongoose.Types.ObjectId(req.body.province),
            },
          },
          {
            $project: {
              "product_images": 1,
              "tags": 1,
              "product_name": 1,
              "product_description": 1,
              "category": 1,
              "variants": 1,
              "available_quantity": 1,
              "in_stock": 1,
              "sub_category": 1,
              "is_featured": 1,
              "createdAt": 1,
              "updatedAt": 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.average_rating": 1,
              "wishlist": 1,
            },
          },
        ];
      }
      if (req.body.sortBy) {
        const sortByPriceLowToHigh = {
          $sort: {
            "variants.0.price": 1,
          },
        };
        const sortByPriceHighToLow = {
          $sort: {
            "variants.0.price": -1,
          },
        };
        const sortByPopularity = {
          $sort: {
            "rating.average_rating": -1,
          },
        };

        const sortByNewArrivals = {
          $sort: {
            createdAt: -1,
          },
        };

        sortBy = {
          "price-high-to-low": sortByPriceHighToLow,
          "price-low-to-high": sortByPriceLowToHigh,
          "popularity": sortByPopularity,
          "new-arrivals": sortByNewArrivals,
        };
      }

      if (Object.keys(nestedFilter).length) {
        // by province
        if (req.body.province) {
          if (req.body.sortBy) {
            productList = await Product.aggregate([
              ...basicFilterByProvince,
              nestedFilter,
              sortBy[req.body.sortBy],
            ]);
          } else {
            productList = await Product.aggregate([
              ...basicFilterByProvince,
              nestedFilter,
            ]);
          }
        }

        // by category sub category
        else {
          if (req.body.sortBy) {
            productList = await Product.aggregate([
              ...basicFilter,
              nestedFilter,
              sortBy[req.body.sortBy],
            ]);
          } else {
            productList = await Product.aggregate([...basicFilter]);
            console.log(productList);
          }
        }
      }

      if (productList.length) {
        result.list = productList;
        return result;
      }

      result.noProductsFound = true;
      return result;
    } else {
      let sub_sub_category = [];
      if (req.body.filterBy && req.body.filterBy === "sub_sub_category") {
        sub_sub_category = req.body.sub_sub_category.split(",");
        sub_sub_category = sub_sub_category.map((value) =>
          mongoose.Types.ObjectId(value)
        );
      }
      // Filter and Sort
      if (req.body.filterBy && req.body.sortBy) {
        const sortByPriceLowToHigh = {
          $sort: {
            "variants.0.price": 1,
          },
        };
        const sortByPriceHighToLow = {
          $sort: {
            "variants.0.price": -1,
          },
        };
        const sortByPopularity = {
          $sort: {
            "rating.average_rating": -1,
          },
        };

        const sortByNewArrivals = {
          $sort: {
            createdAt: -1,
          },
        };

        const sortByAlphabetically = {
          $sort: {
            product_name: 1,
          },
        };

        let basicFilter = [];

        // Filter by price and sort
        if (req.body.filterBy === "price") {
          basicFilter = [
            {
              $match: {
                "variants.0.price": {
                  $gte: req.body.min_price,
                  $lte: req.body.max_price,
                },
                "category": mongoose.Types.ObjectId(req.body.category_id),
                "sub_category": mongoose.Types.ObjectId(
                  req.body.sub_category_id
                ),
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "product_title": { $toLower: "$product_name" },
                "wishlist": 1,
              },
            },
          ];
        }

        // Filter by reviews & sort
        if (req.body.filterBy === "ratings") {
          basicFilter = [
            {
              $match: {
                "rating.average_rating": { $exists: true },
                "rating.average_rating": { $gte: req.body.ratings },
                "category": mongoose.Types.ObjectId(req.body.category_id),
                "sub_category": mongoose.Types.ObjectId(
                  req.body.sub_category_id
                ),
              },
            },
            {
              $unwind: "$rating",
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "product_title": { $toLower: "$product_name" },
                "wishlist": 1,
              },
            },
          ];
        }

        // Filter by sub-sub category
        if (req.body.filterBy === "sub_sub_category") {
          basicFilter = [
            {
              $match: {
                category: mongoose.Types.ObjectId(req.body.category_id),
                sub_category: mongoose.Types.ObjectId(req.body.sub_category_id),
                sub_sub_category: { $in: sub_sub_category },
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "wishlist": 1,
              },
            },
          ];
        }

        // Filter by title & sort
        if (req.body.filterBy === "title") {
          basicFilter = [
            {
              $match: {
                product_name: { $regex: req.body.title, $options: "i" },
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "wishlist": 1,
              },
            },
          ];

          if (req.body.category_id && req.body.sub_category_id) {
            basicFilter = [
              {
                $match: {
                  category: mongoose.Types.ObjectId(req.body.category_id),
                  sub_category: mongoose.Types.ObjectId(
                    req.body.sub_category_id
                  ),
                  product_name: { $regex: req.body.title, $options: "i" },
                },
              },
              ...populateProductFields,
              {
                $lookup: {
                  from: "wishlists",
                  let: {
                    current_user: mongoose.Types.ObjectId(req.user_id),
                    current_product: "$_id",
                  },
                  pipeline: [
                    {
                      $match: {
                        $expr: {
                          $and: [
                            { $eq: ["$user_id", "$$current_user"] },
                            { $eq: ["$product", "$$current_product"] },
                          ],
                        },
                      },
                    },
                    {
                      $project: {
                        product: 1,
                        product_variant: 1,
                      },
                    },
                  ],
                  as: "wishlist",
                },
              },
              {
                $unwind: {
                  path: "$wishlist",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $project: {
                  "product_images": 1,
                  "tags": 1,
                  "product_name": 1,
                  "product_description": 1,
                  "category": 1,
                  "variants": 1,
                  "available_quantity": 1,
                  "in_stock": 1,
                  "sub_category": 1,
                  "is_featured": 1,
                  "createdAt": 1,
                  "updatedAt": 1,
                  "shop.shop_name": 1,
                  "shop.province": 1,
                  "rating.average_rating": 1,
                  "wishlist": 1,
                },
              },
            ];
          }
        }

        if (req.body.sortBy === "price-high-to-low") {
          productList = await Product.aggregate([
            ...basicFilter,
            sortByPriceHighToLow,
          ]);
        }
        if (req.body.sortBy === "price-low-to-high") {
          productList = await Product.aggregate([
            ...basicFilter,
            sortByPriceLowToHigh,
          ]);
        }
        if (req.body.sortBy === "popularity") {
          productList = await Product.aggregate([
            ...basicFilter,
            sortByPopularity,
          ]);
        }
        if (req.body.sortBy === "new-arrivals") {
          productList = await Product.aggregate([
            ...basicFilter,
            sortByNewArrivals,
          ]);
        }
        if (req.body.sortBy === "alphabetically") {
          productList = await Product.aggregate([
            ...basicFilter,
            sortByAlphabetically,
          ]);
        }

        if (productList.length) {
          result.list = productList;
          return result;
        }

        result.noProductsFound = true;
        return result;
      }

      // Filter
      if (req.body.filterBy) {
        // Most popular - Most popular by (category - sub-category)
        if (req.body.filterBy === "most-popular") {
          const sort = {
            $sort: {
              "rating.average_rating": -1,
            },
          };
          const project = {
            $project: {
              "product_images": 1,
              "tags": 1,
              "product_name": 1,
              "product_description": 1,
              "category": 1,
              "variants": 1,
              "available_quantity": 1,
              "in_stock": 1,
              "sub_category": 1,
              "is_featured": 1,
              "createdAt": 1,
              "updatedAt": 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.average_rating": 1,
              "wishlist": 1,
            },
          };

          const basicFilter = [
            ...populateProductFields,
            {
              $match: { "rating.average_rating": { $exists: true } },
            },
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
          ];

          if (req.body.category_id && req.body.sub_category_id) {
            const filterByCategoryAndSubCategory = [
              ...basicFilter,
              {
                $match: {
                  category: mongoose.Types.ObjectId(req.body.category_id),
                  sub_category: mongoose.Types.ObjectId(
                    req.body.sub_category_id
                  ),
                },
              },
              sort,
              project,
            ];

            productsList = await Product.aggregate(
              filterByCategoryAndSubCategory
            );
          } else {
            productsList = await Product.aggregate([
              ...basicFilter,
              sort,
              project,
            ]);
          }

          if (productsList.length) {
            result.list = productsList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // By Title - Product Name // by title , by title & province
        if (req.body.filterBy === "title") {
          const basicFilter = [
            {
              $match: {
                product_name: { $regex: req.body.title, $options: "i" },
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "wishlist": 1,
              },
            },
          ];

          if (req.body.category_id && req.body.sub_category_id) {
            const filterWithCategorySubCategory = [
              ...basicFilter,
              {
                $match: {
                  category: mongoose.Types.ObjectId(req.body.category_id),
                  sub_category: mongoose.Types.ObjectId(
                    req.body.sub_category_id
                  ),
                },
              },
            ];

            productList = await Product.aggregate(
              filterWithCategorySubCategory
            );
          } else if (req.body.province) {
            const filterWithProvince = [
              ...basicFilter,
              {
                $match: {
                  "shop.province": req.body.province,
                },
              },
            ];

            productList = await Product.aggregate(filterWithProvince);
          } else {
            productList = await Product.aggregate(basicFilter);
          }

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // Featured - Featured by (category - sub-category)
        if (req.body.filterBy === "featured") {
          const basicFilter = [
            ...populateProductFields,
            {
              $match: {
                is_featured: true,
              },
            },
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "wishlist": 1,
              },
            },
            {
              $limit: 10,
            },
          ];

          if (req.body.category_id && req.body.sub_category_id) {
            const filterWithCategorySubCategory = [
              ...basicFilter,
              {
                $match: {
                  category: mongoose.Types.ObjectId(req.body.category_id),
                  sub_category: mongoose.Types.ObjectId(
                    req.body.sub_category_id
                  ),
                },
              },
            ];

            productList = await Product.aggregate(
              filterWithCategorySubCategory
            ).limit(5);
          } else {
            productList = await Product.aggregate(basicFilter);
          }

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // Province
        if (req.body.filterBy === "province") {
          const basicFilter = [
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $match: {
                "shop.province": mongoose.Types.ObjectId(req.body.province),
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "wishlist": 1,
              },
            },
          ];

          productList = await Product.aggregate(basicFilter);

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // New arrivals by cat-subcat & normal
        if (req.body.filterBy === "new-arrivals") {
          const basicFilter = [
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "wishlist": 1,
              },
            },
          ];

          const filterByCategoryAndSubCategory = [
            ...basicFilter,
            {
              $match: {
                category: mongoose.Types.ObjectId(req.body.category_id),
                sub_category: mongoose.Types.ObjectId(req.body.sub_category_id),
              },
            },
          ];

          const sort = {
            $sort: {
              createdAt: -1,
            },
          };

          if (req.body.category_id && req.body.sub_category_id) {
            productList = await Product.aggregate([
              ...filterByCategoryAndSubCategory,
              sort,
            ]).limit(5);
          } else {
            productList = await Product.aggregate([...basicFilter, sort]).limit(
              5
            );
          }

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // Price
        if (req.body.filterBy === "price") {
          const basicFilter = [
            {
              $match: {
                "variants.0.price": {
                  $gte: req.body.min_price,
                  $lte: req.body.max_price,
                },
                "category": mongoose.Types.ObjectId(req.body.category_id),
                "sub_category": mongoose.Types.ObjectId(
                  req.body.sub_category_id
                ),
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "wishlist": 1,
              },
            },
          ];

          productList = await Product.aggregate(basicFilter);

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // Ratings
        if (req.body.filterBy === "ratings") {
          const project = {
            $project: {
              "product_images": 1,
              "tags": 1,
              "product_name": 1,
              "product_description": 1,
              "category": 1,
              "variants": 1,
              "available_quantity": 1,
              "in_stock": 1,
              "sub_category": 1,
              "is_featured": 1,
              "createdAt": 1,
              "updatedAt": 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.average_rating": 1,
              "wishlist": 1,
            },
          };

          const basicFilter = [
            {
              $match: {
                "rating.average_rating": { $exists: true },
                "rating.average_rating": { $gte: req.body.ratings },
                "category": mongoose.Types.ObjectId(req.body.category_id),
                "sub_category": mongoose.Types.ObjectId(
                  req.body.sub_category_id
                ),
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
          ];

          productList = await Product.aggregate([...basicFilter, project]);

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // By category sub-category
        if (req.body.filterBy === "category-sub_category") {
          const project = {
            $project: {
              "product_images": 1,
              "tags": 1,
              "product_name": 1,
              "product_description": 1,
              "category": 1,
              "variants": 1,
              "available_quantity": 1,
              "in_stock": 1,
              "sub_category": 1,
              "is_featured": 1,
              "createdAt": 1,
              "updatedAt": 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.average_rating": 1,
              "wishlist": 1,
            },
          };

          const basicFilter = [
            {
              $match: {
                category: mongoose.Types.ObjectId(req.body.category_id),
                sub_category: mongoose.Types.ObjectId(req.body.sub_category_id),
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
          ];

          productList = await Product.aggregate([...basicFilter, project]);

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }

        // By sub-sub-category
        if (req.body.filterBy === "sub_sub_category") {
          const project = {
            $project: {
              "product_images": 1,
              "tags": 1,
              "product_name": 1,
              "product_description": 1,
              "category": 1,
              "variants": 1,
              "available_quantity": 1,
              "in_stock": 1,
              "sub_category": 1,
              "is_featured": 1,
              "createdAt": 1,
              "updatedAt": 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.average_rating": 1,
              "wishlist": 1,
            },
          };

          const basicFilter = [
            {
              $match: {
                category: mongoose.Types.ObjectId(req.body.category_id),
                sub_category: mongoose.Types.ObjectId(req.body.sub_category_id),
                sub_sub_category: { $in: sub_sub_category },
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
          ];

          productList = await Product.aggregate([...basicFilter, project]);

          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }
      }

      // Sort
      if (req.body.sortBy) {
        if (req.body.category_id && req.body.sub_category_id) {
          const basicFilter = [
            {
              $match: {
                category: mongoose.Types.ObjectId(req.body.category_id),
                sub_category: mongoose.Types.ObjectId(req.body.sub_category_id),
              },
            },
            ...populateProductFields,
            {
              $lookup: {
                from: "wishlists",
                let: {
                  current_user: mongoose.Types.ObjectId(req.user_id),
                  current_product: "$_id",
                },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $eq: ["$user_id", "$$current_user"] },
                          { $eq: ["$product", "$$current_product"] },
                        ],
                      },
                    },
                  },
                  {
                    $project: {
                      product: 1,
                      product_variant: 1,
                    },
                  },
                ],
                as: "wishlist",
              },
            },
            {
              $unwind: {
                path: "$wishlist",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $project: {
                "product_images": 1,
                "tags": 1,
                "product_name": 1,
                "product_description": 1,
                "category": 1,
                "variants": 1,
                "available_quantity": 1,
                "in_stock": 1,
                "sub_category": 1,
                "is_featured": 1,
                "createdAt": 1,
                "updatedAt": 1,
                "shop.shop_name": 1,
                "shop.province": 1,
                "rating.average_rating": 1,
                "product_title": { $toLower: "$product_name" },
                "wishlist": 1,
              },
            },
          ];
          const sortByPriceLowToHigh = {
            $sort: {
              "variants.0.price": 1,
            },
          };
          const sortByPriceHighToLow = {
            $sort: {
              "variants.0.price": -1,
            },
          };
          const sortByPopularity = {
            $sort: {
              "rating.average_rating": -1,
            },
          };

          const sortByNewArrivals = {
            $sort: {
              createdAt: -1,
            },
          };

          const sortByAlphabetically = {
            $sort: {
              product_title: 1,
            },
          };
          if (req.body.sortBy === "price-high-to-low") {
            productList = await Product.aggregate([
              ...basicFilter,
              sortByPriceHighToLow,
            ]);
          }
          if (req.body.sortBy === "price-low-to-high") {
            productList = await Product.aggregate([
              ...basicFilter,
              sortByPriceLowToHigh,
            ]);
          }
          if (req.body.sortBy === "popularity") {
            productList = await Product.aggregate([
              ...basicFilter,
              sortByPopularity,
            ]);
          }
          if (req.body.sortBy === "new-arrivals") {
            productList = await Product.aggregate([
              ...basicFilter,
              sortByNewArrivals,
            ]);
          }

          if (req.body.sortBy === "alphabetically") {
            productList = await Product.aggregate([
              ...basicFilter,
              sortByAlphabetically,
            ]);
          }
          if (productList.length) {
            result.list = productList;
            return result;
          }

          result.noProductsFound = true;
          return result;
        }
      }
    }
  } catch (error) {
    console.log(error);

    result.databaseError = true;
    return result;
  }
};

// NOT IS USE
exports.getSortedProducts = async (
  user_type,
  { sort_by, shop_id },
  user_id
) => {
  let result = {};

  try {
    let products = [];

    if (user_type === "end_user") {
      if (sort_by === "price-low-to-high") {
        if (shop_id)
          products = await Product.find({ shop_id }).sort({ price: 1 });
        else products = await Product.find().sort({ price: 1 });
      } else if (sort_by === "price-high-to-low") {
        if (shop_id)
          products = await Product.find({ shop_id }).sort({ price: -1 });
        else products = await Product.find().sort({ price: -1 });
      } else if (sort_by === "popularity") {
        if (shop_id)
          products = await Ratings.find({ product_id: { $ne: null }, shop_id })
            .sort({ average_rating: -1 })
            .select("-rated_by")
            .populate("product_id");
        else
          products = await Ratings.find({
            product_id: { $ne: null },
            shop_id: { $ne: null },
          })
            .sort({ average_rating: -1 })
            .select("-rated_by")
            .populate("product_id");
      } else if (sort_by === "new-arrivals") {
        const start_date = moment(
          new Date(new Date().setDate(new Date().getDate() - 7))
        )
          .startOf("day")
          .toDate();
        const end_date = moment(
          new Date(new Date().setDate(new Date().getDate()))
        )
          .endOf("day")
          .toDate();

        if (shop_id)
          products = await Product.find({
            shop_id,
            createdAt: { $gte: start_date, $lte: end_date },
          }).sort({ createdAt: -1 });
        else
          products = await Product.find({
            createdAt: { $gte: start_date, $lte: end_date },
          }).sort({ createdAt: -1 });
      }

      if (products.length) {
        let productList = [];

        for (let prod of products) {
          const product_id =
            sort_by === "popularity" && prod.product_id
              ? prod.product_id._id
              : prod._id;

          let productObject = {
            product_images:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.product_images
                : prod.product_images,
            tags:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.tags
                : prod.tags,
            _id: product_id,
            shop_id:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.shop_id
                : prod.shop_id,
            price:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.price
                : prod.price,
            product_name:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.product_name
                : prod.product_name,
            product_description:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.product_description
                : prod.product_description,
            category:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.category
                : prod.category,
            available_quantity:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.available_quantity
                : prod.available_quantity,
            in_stock:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.in_stock
                : prod.in_stock,
            sub_category:
              sort_by === "popularity" && prod.product_id
                ? prod.product_id.sub_category
                : prod.sub_category,
            no_of_ratings: prod.no_of_ratings,
            average_rating: prod.average_rating,
            is_product_in_wishlist: await Wishlist.countDocuments({
              product: product_id,
              user_id,
            }),
          };

          productList.push(productObject);
        }

        if (productList.length) {
          result.products = productList;
          return result;
        }

        result.noProductFound = true;
        return result;
      }

      result.noProductFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.addRatingForProduct = async (
  user_type,
  user_id,
  { rating_number, product_id }
) => {
  let result = {};

  try {
    if (user_type === "end_user") {
      const product = await Product.findById(product_id);

      if (product) {
        const rating = await Ratings.findOne({ product_id });

        if (rating) {
          if (
            rating.rated_by.find(
              (users) => users.user_id.toString() === user_id
            )
          ) {
            result.ratingUpdated = true;
            rating.rated_by[
              rating.rated_by.findIndex(
                (users) => users.user_id.toString() === user_id
              )
            ].rating = rating_number;
            const average_rating =
              (rating.rated_by.reduce((acc, curr) => acc + curr.rating, 0) +
                rating_number) /
              (rating.no_of_ratings + 1);
            rating.average_rating = average_rating.toFixed(1);
            await rating.save();
            return result;
          }

          const average_rating =
            (rating.rated_by.reduce((acc, curr) => acc + curr.rating, 0) +
              rating_number) /
            (rating.no_of_ratings + 1);
          rating.no_of_ratings = rating.no_of_ratings + 1;
          rating.average_rating = average_rating.toFixed(1);
          rating.rated_by.push({ user_id, rating: rating_number });
          await rating.save();

          result.ratingAdded = true;
          return result;
        }

        const ratingObject = {
          product_id,
          no_of_ratings: 1,
          average_rating: rating_number,
          rated_by: [{ user_id, rating: rating_number }],
        };

        await new Ratings(ratingObject).save();

        result.ratingAdded = true;
        return result;
      }

      result.noProductFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.addProductReview = async (
  user_type,
  user_id,
  { comment, product_id }
) => {
  let result = {};

  try {
    if (user_type === "end_user") {
      const user = await Users.findById(user_id);
      const product = await Product.findById(product_id);

      if (product) {
        let reviewObject = {
          product_id,
          comment,
          user_id,
        };

        let review = await Reviews(reviewObject).save();

        reviewObject = {
          product_name: product.product_name,
          product_id: product._id,
          user_name: user.fname + " " + user.lname,
          user_avatar: user.avatar,
          comment: review.comment,
          review_id: review._id,
          createdAt: review.createdAt,
          updatedAt: review.updatedAt,
          liked_by: 0,
        };

        result.review = reviewObject;
        return result;
      }

      result.noProductFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.updateProductReview = async (
  user_type,
  user_id,
  { comment, review_id }
) => {
  let result = {};

  try {
    if (user_type === "end_user") {
      let review = await Reviews.findOne({ user_id, _id: review_id })
        .populate("user_id")
        .populate("product_id");

      if (review) {
        review.comment = comment;
        await review.save();

        let reviewObject = {
          product_name: review.product_id.product_name,
          product_id: review.product_id._id,
          user_name: review.user_id.fname + " " + review.user_id.lname,
          user_avatar: review.user_id.avatar,
          comment: review.comment,
          review_id: review._id,
          createdAt: review.createdAt,
          updatedAt: review.updatedAt,
          liked_by: review.liked_by.length,
        };

        result.review = reviewObject;
        return result;
      }

      result.noReviewFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.deleteProductReview = async (user_type, user_id, { review_id }) => {
  let result = {};

  try {
    if (user_type === "end_user") {
      const review = await Reviews.findOne({ user_id, _id: review_id });

      if (review) {
        await review.remove();

        result.reviewDeleted = true;
        return result;
      }

      result.noReviewFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getProductReviews = async (req) => {
  let result = {};

  const { product_id, sortBy, page, limit } = req.body;
  const { user_type, user_id } = req;

  try {
    if (user_type === "end_user") {
      const basicFilter = [
        {
          $match: { product_id: mongoose.Types.ObjectId(product_id) },
        },
        {
          $lookup: {
            from: "users",
            as: "user",
            localField: "user_id",
            foreignField: "_id",
          },
        },
        {
          $lookup: {
            from: "products",
            as: "product",
            localField: "product_id",
            foreignField: "_id",
          },
        },
        {
          $unwind: "$user",
        },
        {
          $unwind: "$product",
        },
        {
          $lookup: {
            from: "ratings",
            as: "rating",
            localField: "product._id",
            foreignField: "product_id",
          },
        },
        {
          $unwind: {
            path: "$rating",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $addFields: {
            "rating.rated_by": {
              $filter: {
                input: "$rating.rated_by",
                as: "var",
                cond: { $eq: ["$$var.user_id", "$user._id"] },
              },
            },
            "user_rating": { $first: "$rating.rated_by" },
            "like_count": { $size: "$liked_by" },
          },
        },
        {
          $project: {
            "user.user_name": { $concat: ["$user.fname", " ", "$user.lname"] },
            "user.avatar": 1,
            "user._id": 1,
            "user.user_rating": "$user_rating.rating",
            "product.product_name": 1,
            "product._id": 1,
            "comment": 1,
            "liked_by": 1,
            "like_count": 1,
            "has_user_liked_review": {
              $in: [mongoose.Types.ObjectId(user_id), "$liked_by"],
            },
            "createdAt": 1,
          },
        },
        {
          $skip: (page - 1) * limit,
        },
        {
          $limit: limit,
        },
      ];

      let sortFilter = {};

      if (sortBy) {
        if (sortBy === "featured") {
          sortFilter = {
            $sort: {
              like_count: -1,
            },
          };
        }
      }

      let reviews = [];
      if (sortBy) {
        reviews = await Reviews.aggregate([...basicFilter, sortFilter]);
      } else {
        reviews = await Reviews.aggregate(basicFilter);
      }

      if (reviews.length) {
        result.reviews = reviews;
        return result;
      }

      result.noReviewsFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
};

exports.changeReviewLikeStatus = async (user_type, user_id, review_id) => {
  let result = {};

  try {
    if (user_type === "end_user") {
      let review = await Reviews.findById(review_id);

      if (review) {
        const index = review.liked_by.findIndex(
          (user) => user.toString() === user_id
        );

        if (index !== -1) {
          review.liked_by.splice(index, 1);
          await review.save();

          result.reviewUnliked = true;
          return result;
        } else {
          review.liked_by.unshift(user_id);
          await review.save();

          result.reviewLiked = true;
          return result;
        }
      }

      result.noReviewFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getRecommendedProductsByProvince = async (req) => {
  let result = {};
  let recommendBy = [];

  const { province } = req.body;

  try {
    if (req.user_type === "end_user") {
      recommendBy = [
        ...populateProductFields,
        {
          $match: {
            $expr: {
              $eq: ["$shop.province", mongoose.Types.ObjectId(province)],
            },
          },
        },
        {
          $sort: { "rating.average_rating": -1 },
        },
        {
          $project: {
            "_id": 1,
            "shop_id": 1,
            "product_name": 1,
            "product_images": 1,
            "product_description": 1,
            "category": 1,
            "sub_category": 1,
            "shop.shop_name": 1,
            "shop.province": 1,
            "rating.average_rating": 1,
            "variants": 1,
            "available_quantity": 1,
            "in_stock": 1,
            "tags": 1,
            "createdAt": 1,
            "updatedAt": 1,
            "__v": 1,
          },
        },
        {
          $limit: 5,
        },
      ];

      result.productData = [];

      if (req.body.category_id) {
        recommendBy = [
          {
            $match: {
              category: mongoose.Types.ObjectId(req.body.category_id),
            },
          },
          ...populateProductFields,
          {
            $match: {
              $expr: {
                $eq: ["$shop.province", mongoose.Types.ObjectId(province)],
              },
            },
          },
          {
            $sort: { "rating.average_rating": -1 },
          },
          {
            $project: {
              "_id": 1,
              "shop_id": 1,
              "product_name": 1,
              "product_images": 1,
              "product_description": 1,
              "category": 1,
              "sub_category": 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.average_rating": 1,
              "variants": 1,
              "available_quantity": 1,
              "in_stock": 1,
              "tags": 1,
              "createdAt": 1,
              "updatedAt": 1,
              "__v": 1,
            },
          },
          {
            $limit: 5,
          },
        ];
      }

      result.productData = await Product.aggregate(recommendBy);

      if (result.productData.length) {
        return result;
      }

      result.noProductFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getProductsAccordingToShopOwner = async (req) => {
  let result = {};

  try {
    if (req.user_type === "business_owner" || req.user_type == "admin") {
      let filterForProduct = [
        {
          $lookup: {
            from: "shops",
            as: "shop_id",
            localField: "shop_id",
            foreignField: "_id",
          },
        },
        { $unwind: "$shop_id" },
        {
          $match: {
            $expr: {
              $eq: [
                "$shop_id.business_owner_id",
                mongoose.Types.ObjectId(req.user_id),
              ],
            },
          },
        },
        {
          $project: {
            product_name: 1,
            product_description: 1,
            shop_id: "$shop_id._id",
            category: 1,
            price: 1,
            available_quantity: 1,
            in_stock: 1,
            sub_category: 1,
            createdAt: 1,
            updatedAt: 1,
            _id: 1,
            product_images: 1,
            tags: 1,
            __v: 1,
          },
        },
      ];
      const obj = await Product.aggregate(filterForProduct);

      if (obj.length) {
        result.products = obj;
        return result;
      }

      result.noProductFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getProductRatings = async (req) => {
  let result = {};

  const { product_id, sortBy, page, limit } = req.body;
  const { user_type, user_id } = req;

  try {
    if (user_type === "end_user") {
      const basicFilter = [
        {
          $match: { product_id: mongoose.Types.ObjectId(product_id) },
        },
        {
          $unwind: "$rated_by",
        },
        {
          $lookup: {
            from: "users",
            let: {
              user_id: "$rated_by.user_id",
              rated_by: "$rated_by",
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ["$_id", "$$user_id"],
                  },
                },
              },
              {
                $replaceRoot: {
                  newRoot: {
                    $mergeObjects: ["$$rated_by", "$$ROOT"],
                  },
                },
              },
            ],
            as: "user",
          },
        },
        {
          $group: {
            _id: "$_id",
            user: {
              $push: {
                $first: "$user",
              },
            },
          },
        },
        {
          $project: {
            "user.user_id": 1,
            "user.rating": 1,
            "user.ratingOn": 1,
            "user.avatar": 1,
            "user.fname": 1,
            "user.lname": 1,
          },
        },

        {
          $skip: (page - 1) * limit,
        },
        {
          $limit: limit,
        },
      ];

      let sortFilter = {};

      if (sortBy) {
        if (sortBy === "featured") {
          sortFilter = {
            $sort: {
              like_count: -1,
            },
          };
        }
      }

      let reviews = [];
      if (sortBy) {
        reviews = await Ratings.aggregate([...basicFilter, sortFilter]);
      } else {
        reviews = await Ratings.aggregate(basicFilter);
      }

      if (reviews.length) {
        result.reviews = reviews;
        return result;
      }

      result.noReviewsFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
};
