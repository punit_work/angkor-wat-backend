exports.populateProductFields = [
  {
    $lookup: {
      from: "shops",
      as: "shop",
      localField: "shop_id",
      foreignField: "_id",
    },
  },
  {
    "$unwind":"$shop"
  },
  {
    $lookup: {
      from: "ratings",
      as: "rating",
      localField: "_id",
      foreignField: "product_id",
    },
  },
  {
    "$unwind":{
      "path":"$rating",
      "preserveNullAndEmptyArrays":true
    }
  },
  
];