const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");
const xlsx = require("node-xlsx");
const mongoose = require('mongoose');

const ProductsController = require("./products-controller");
const ProductsHelper = require("./products-helper");

const uploadProductImage = require("../../middleware/upload-product-image");

//middleware
const authCheck = require("../../middleware/check-auth");
const uploadDocument = require("../../middleware/upload-document");
const Shop = require("../../model/Shop");
const Category = require("../../model/Category");
const Product = require("../../model/Product");

// Path - /v1/products/get-products/:shop_id
// Route for getting all products belongs to shop
router.get(
  "/get-products/:shop_id",
  authCheck,
  [
    param("shop_id")
      .isMongoId()
      .withMessage("Parameter [shop_id] is missing or invalid"),
  ],
  async (req, res) => {
    const response = await ProductsController.getAllProducts(req);
    if (response.isValidOwner)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.products) {
      const successMessage =
        response.products.length > 0
          ? ResponseMessage.RETRIVED_PRODUCT_DATA_SUCCESSFULLY
          : ResponseMessage.NO_PRODUCT_AVAILABLE;
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        successMessage,
        response.products
      );
    }
  }
);

// Path - /v1/products/get-product/:id
// Route for getting product by id
router.get(
  "/get-product/:product_id",
  authCheck,
  [
    param("product_id")
      .isMongoId()
      .withMessage("Parameter [product_id] is missing or invalid"),
  ],
  async (req, res) => {
    const response = await ProductsController.getProductById(req);
    if (response.isValidOwner)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    }
    if (response.noProductFound) {
      return Response.error(res, ResponseCode.NOT_FOUND, "Porduct Not Found");
    } else if (response.product) {
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCTS_FETCHED_SUCCESSFULLY,
        response.product
      );
    }
  }
);

// Path - /v1/products/add-product
// Route for adding shop wise product
router.post(
  "/add-product",
  authCheck,
  uploadProductImage.fields([
    { name: "product_images" },
    { name: "color_images" },
  ]),
  [
    body("shop_id").notEmpty().withMessage("Shop id is required"),
    body("en").notEmpty().isJSON().withMessage("Parameter [en] is required"),
    body("kh").notEmpty().isJSON().withMessage("Parameter [kh] is required"),
    body("category").notEmpty().withMessage("Category is required"),
    body("sub_category").notEmpty().withMessage("Sub Category is required"),
    // body('variants').notEmpty().withMessage('Variants is required'),
  ],

  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ProductsController.addProduct(req);

     // Sending error response if validation error
     if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

     
    if (response.unAuthorized)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.notValidOwner)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.INVALID_OWNER
      );
    if (response.notValidCategory)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.INVALID_CATEGORY
      );
    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.product)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCT_CREATED_SUCCESSFULLY,
        response.product
      );
  }
);

// Path - /v1/products/update-product/:product_id
// Route for updating product by product_id
router.post(
  "/update-product/:product_id",

  authCheck,

  [
    param("product_id")
      .isMongoId()
      .withMessage("Parameter [product_id] is missing or invalid"),
  ],

  uploadProductImage.array("product_images"),

  [
    body("shop_id").notEmpty().withMessage("Shop id is required"),
    body("en").notEmpty().isJSON().withMessage("Parameter [en] is required"),
    body("kh").notEmpty().isJSON().withMessage("Parameter [kh] is required"),
    // body("product_name")
    //   .if(body("product_name").exists())
    //   .notEmpty()
    //   .withMessage("Product name is required"),
    body("category")
      .if(body("category").exists())
      .notEmpty()
      .withMessage("Category is required"),
    body("sub_category")
      .if(body("sub_category").exists())
      .notEmpty()
      .withMessage("Sub category is required"),
  ],
  async (req, res) => {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    if (
      req.body.remove_product_images &&
      !Array.isArray(JSON.parse(req.body.remove_product_images))
    ) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Parameter [remove_product_images] is invalid"
      );
    }

    const response = await ProductsController.updateProduct(req);

     // Sending error response if validation error
     if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

    if (response.unAuthorized)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );
    if (response.noProductFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    if (response.notValidOwner)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.INVALID_OWNER
      );
    if (response.imagesNotFound)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.IMAGE_MISSING
      );
    if (response.notValidCategory)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.INVALID_CATEGORY
      );
    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.products)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCT_UPDATED_SUCCESSFULLY,
        response.products
      );
  }
);

// Path - /v1/products/delete-product/:product_id
// Route for deleting product by product_id
router.delete(
  "/delete-product/:product_id",
  authCheck,
  [
    param("product_id")
      .isMongoId()
      .withMessage("Parameter [product_id] is missing or invalid"),
  ],
  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ProductsController.deleteProduct(req);

    if (response.notValidOwner)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ERROR_UNAUTHORIZED
      );

    // Sending error response if database error
    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_UNKNOWN
      );
    // Sending error response if no product found
    else if (response.noProductFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    // Sending success response if product deleted
    else if (response.productDeleted)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCT_DELETED_SUCCESSFULLY
      );
  }
);

// Path - /v1/products/getFilteredProducts   => NOT IS USE
// Route for fetching filtered products by category and price
router.get(
  "/getFilteredProducts",

  // check if authenticated or not
  authCheck,

  [
    query("filter").not().isEmpty().withMessage("Please provide a filter"),
    query("shop_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter shop_id is invalid"),
    query("category")
      .optional()
      .isMongoId()
      .withMessage("Parameter category is invalid"),
    query("sub_category")
      .optional()
      .isMongoId()
      .withMessage("Parameter sub_category is invalid"),
    query("max_price")
      .optional()
      .not()
      .isEmpty()
      .withMessage("Parameter max_price is invalid"),
    query("min_price")
      .optional()
      .not()
      .isEmpty()
      .withMessage("Parameter min_price is invalid"),
    query("new_arrivals")
      .optional()
      .not()
      .isEmpty()
      .withMessage("Parameter new_arrivals is invalid"),
  ],

  async (req, res) => {
    const errors = validationResult(req);
    // check if user has sent category field to filter
    if (!errors.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        errors.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }
    // else if ( !req.query.page ) {

    //   return Response.error( res , ResponseCode.BAD_REQUEST , 'Please provide a page number.')
    // }

    // apply filter
    // check if category has been provided or not
    const filter = req.query.filter;

    if (filter === "category") {
      if (!req.query.category)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide a category."
        );
    } else if (filter === "sub_category") {
      if (!req.query.sub_category)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide a sub_category."
        );
      if (
        req.query.new_arrivals &&
        !["true", "false"].includes(req.query.new_arrivals.toString())
      )
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Parameter [new_arrivals] needs to be boolean"
        );
    } else if (filter === "price") {
      if (!req.query.min_price)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide minimum price to filter."
        );
    }

    // get all the products according to the filter applied
    const response = await ProductsController.filterBy(req);

    // check if invalid user is trying to access
    if (response.invalidUser)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.INVALID_USER_TYPE
      );
    // check if min price is a valid numeric or not
    else if (response.minPriceIsNaN)
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        ResponseMessage.INVALID_MIN_PRICE
      );
    // check if max price is a valid numeric or not
    else if (response.maxPriceIsNaN)
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        ResponseMessage.INVALID_MAX_PRICE
      );
    // check if category is found or not
    else if (response.categoryError)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.NO_CATEGORY_FOUND
      );
    // check if database Error
    else if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    // check if any products are found using applied category
    else if (response.noProductsFound)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    // send productList in response
    else if (response.list)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCTS_FETCHED_SUCCESSFULLY,
        response.list
      );
  }
);

router.post(
  "/get-filtered-products",
  authCheck,
  [
    body("filterBy")
      .optional()
      .trim()
      .isLength({ min: 1 })
      .withMessage("Please give valid filter option"),
    body("sortBy")
      .optional()
      .trim()
      .isLength({ min: 1 })
      .withMessage("Please give valid sort option"),
    body("shop_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter shop_id is invalid"),
    body("category_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter category is invalid"),
    body("sub_category_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter sub_category is invalid"),
    body("ratings")
      .optional()
      .isFloat({ min: 1, max: 5 })
      .withMessage("Ratings must be number between 1 - 5 & number"),
    body("max_price")
      .optional()
      .isFloat({ max: 1000 })
      .withMessage("Parameter max_price is invalid"),
    body("min_price")
      .optional()
      .isFloat({ min: 0 })
      .withMessage("Parameter min_price is invalid"),
    body("title").optional().not().isEmpty().withMessage("Title is required"),
    body("province")
      .optional()
      .isMongoId()
      .withMessage("Parameter province is invalid"),
    body("multipleFilter")
      .optional()
      .trim()
      .isLength({ min: 1 })
      .withMessage("Parameter multipleFilter is required"),
    body("sub_sub_category")
      .optional()
      .trim()
      .isLength({ min: 1 })
      .withMessage("Parameter sub_sub_category is required"),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    // check if user has sent category field to filter
    if (!errors.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        errors.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

   
    

    if (!(req.body.filterBy || req.body.sortBy))
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide sort or filter options"
      );

    // FILTER

    if (req.body.filterBy) {
      if (
        ![
          "category",
          "category-sub_category",
          "ratings",
          "price",
          "sub_sub_category",
          "new-arrivals",
          "most-popular",
          "featured",
          "province",
          "title",
          "multipleFilter",
        ].includes(req.body.filterBy)
      )
        return Response.error(
          res,
          ResponseCode.BAD_REQUEST,
          "Please provide valid filter options"
        );

      const filterBy = req.body.filterBy;

      if (filterBy === "category-sub_category") {
        if (!(req.body.category_id && req.body.sub_category_id))
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide category and sub_category id"
          );
      }

      if (filterBy === "ratings") {
        if (!(req.body.category_id && req.body.sub_category_id))
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide category and sub_category id"
          );
        if (!req.body.ratings)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide ratings in 1-5"
          );
      }

      if (filterBy === "price") {
        if (!(req.body.category_id && req.body.sub_category_id))
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide category and sub_category id"
          );
        if (!(req.body.min_price && req.body.max_price))
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide min_price and max_price"
          );
      }

      if (filterBy === "title") {
        if (!req.body.title)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide title field"
          );
      }

      if (filterBy === "province") {
        if (!req.body.province)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide province"
          );
      }

      if (filterBy === "sub_sub_category") {
        if (!(req.body.category_id && req.body.sub_category_id))
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide category and sub_category id"
          );
        if (!req.body.sub_sub_category)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide sub_sub_category field is comma separated values"
          );
      }

      if (filterBy === "multipleFilter") {
        if (!req.body.multipleFilter)
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide multiple filters"
          );

        const filters = req.body.multipleFilter.split(",");

        if (
          !filters.some((value) =>
            ["sub_sub_category", "ratings", "price"].includes(value)
          )
        )
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide valid filter options for multiple filters from this list [ 'sub_sub_category','ratings','price' ] "
          );

        if (filters.includes("sub_sub_category")) {
          if (!req.body.sub_sub_category)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide sub_sub_category field"
            );
        }

        if (filters.includes("price")) {
          if (!(req.body.category_id && req.body.sub_category_id))
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide category and sub_category id"
            );
          if (!(req.body.min_price && req.body.max_price))
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide min_price and max_price"
            );
        }

        if (filters.includes("ratings")) {
          if (!req.body.category_id && !req.body.sub_category_id)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide category and sub_category id"
            );
          if (!req.body.ratings)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide ratings in 1-5"
            );
        }
      }
    }

    // SORT

    if (req.body.sortBy) {
      if (
        ![
          "price-high-to-low",
          "price-low-to-high",
          "popularity",
          "new-arrivals",
          "alphabetically",
        ].includes(req.body.sortBy)
      )
        return Response.error(
          res,
          ResponseCode.BAD_REQUEST,
          "Please provide valid sort options from [ price-high-to-low ,price-low-to-high ,popularity ,new-arrivals, alphabetically ]"
        );

        if(req.body.sortBy !== 'popularity'){

          if (!(req.body.category_id && req.body.sub_category_id))
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide category and sub_category id"
          );

        }
     
    }

  

    // get all the products according to the filter & sort applied
    const response = await ProductsController.getFilteredProducts(req);

    // check if invalid user is trying to access
    if (response.invalidUser)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.INVALID_USER_TYPE
      );
    // check if min price is a valid numeric or not
    else if (response.minPriceIsNaN)
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        ResponseMessage.INVALID_MIN_PRICE
      );
    // check if max price is a valid numeric or not
    else if (response.maxPriceIsNaN)
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        ResponseMessage.INVALID_MAX_PRICE
      );
    // check if category is found or not
    else if (response.categoryError)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        ResponseMessage.NO_CATEGORY_FOUND
      );
    // check if database Error
    else if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    // check if any products are found using applied category
    else if (response.noProductsFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    // check if category is found or not
    else if (response.inValidFilterSelection)
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide valid filter selection"
      );
    // send productList in response
    else if (response.list)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCTS_FETCHED_SUCCESSFULLY,
        response.list
      );
  }
);
// Path - /v1/products/get-sorted-products
// Route for getting sorted products by specific type
router.post(
  "/get-sorted-products",

  authCheck,

  [
    body("sort_by")
      .trim()
      .isLength({ min: 1 })
      .withMessage("Parameter [sort_by] is missing or invalid"),
    body("shop_id")
      .optional()
      .isMongoId()
      .withMessage("Parameter [shop_id] is missing or invalid"),
  ],

  async (req, res) => {
    const errors = validationResult(req);
    // check if user has sent category field to filter
    if (!errors.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        errors.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    if (
      ![
        "price-high-to-low",
        "price-low-to-high",
        "new-arrivals",
        "popularity",
        "alphabetically",
      ].includes(req.body.sort_by)
    ) {
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        ResponseMessage.INVALID_PARAMATER
      );
    }
    const response = await ProductsController.getSortedProducts(
      req.user_type,
      req.body,
      req.user_id
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noProductFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    else if (response.products)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCTS_FETCHED_SUCCESSFULLY,
        response.products
      );
  }
);

// Path - /v1/products/add-rating
// Route for adding rating for product
router.post(
  "/add-rating",

  authCheck,

  [
    body("product_id")
      .isMongoId()
      .withMessage("Parameter [product_id] is missing or invalid"),
    body("rating_number")
      .isFloat({ min: 1, max: 5 })
      .withMessage("Parameter [rating_number] must be between 1 to 5"),
  ],

  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ProductsController.addRatingForProduct(
      req.user_type,
      req.user_id,
      req.body
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noProductFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    else if (response.alreadyRated)
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        ResponseMessage.RATING_ALREADY_ADDED
      );
    else if (response.ratingAdded)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.RATING_ADDED
      );
    else if (response.ratingUpdated)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.RATING_UPDATED
      );
  }
);

// Path - /v1/products/add-review
// Route for adding product review
router.post(
  "/add-review",

  authCheck,

  [
    body("product_id")
      .isMongoId()
      .withMessage("Parameter [product_id] is missing or invalid"),
    body("comment")
      .trim()
      .isLength({ min: 1 })
      .withMessage("Parameter [comment] is missing or invalid"),
  ],

  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ProductsController.addProductReview(
      req.user_type,
      req.user_id,
      req.body
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noProductFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    else if (response.review)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.REVIEW_ADDED_SUCCESSFULLY,
        response.review
      );
  }
);

// Path - /v1/products/update-review
// Route for updating product review
router.post(
  "/update-review",

  authCheck,

  [
    body("review_id")
      .isMongoId()
      .withMessage("Parameter [review_id] is missing or invalid"),
    body("comment")
      .trim()
      .isLength({ min: 1 })
      .withMessage("Parameter [comment] is missing or invalid"),
  ],

  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ProductsController.updateProductReview(
      req.user_type,
      req.user_id,
      req.body
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noReviewFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_REVIEW_FOUND
      );
    else if (response.review)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.REVIEW_UPDATED_SUCCESSFULLY,
        response.review
      );
  }
);

// Path - /v1/products/delete-review/:review_id
// Route for deleting product review
router.delete(
  "/delete-review/:review_id",

  authCheck,

  [
    param("review_id")
      .isMongoId()
      .withMessage("Parameter [review_id] is missing or invalid"),
  ],

  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ProductsController.deleteProductReview(
      req.user_type,
      req.user_id,
      req.params
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noReviewFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_REVIEW_FOUND
      );
    else if (response.reviewDeleted)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.REVIEW_DELETED_SUCCESSFULLY
      );
  }
);

// Path - /v1/products/get-all-reviews/:product_id
// Route for fetching all product reviews
router.post(
  "/get-all-product-reviews",

  authCheck,

  [
    body("product_id").isMongoId().withMessage("Parameter [product_id] is missing or invalid"),
    body("sortBy").optional().notEmpty().withMessage("Please provide sort option"),
    body("page").isFloat({ min: 1}).notEmpty().withMessage("Please give valid page number"),
    body('limit').isFloat({ min: 1}).notEmpty().withMessage("Please give valid limit")
  ],

  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    if(req.body.sortBy && !['featured'].includes(req.body.sortBy)){
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provider valid sort options like ['featured']"
      );
    }
    const response = await ProductsController.getProductRatings(req);

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noReviewsFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_REVIEWS_FOUND
      );
    else if (response.reviews)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.REVIEWS_FETCHED_SUCCESSFULLY,
        response.reviews
      );
  }
);

// Path - /v1/products/change-review-likes/:review_id
// Route for changing product review likes status
router.get(
  "/change-review-likes/:review_id",

  authCheck,

  [
    param("review_id")
      .isMongoId()
      .withMessage("Parameter [review_id] is missing or invalid"),
  ],

  async (req, res) => {
    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    const response = await ProductsController.changeReviewLikeStatus(
      req.user_type,
      req.user_id,
      req.params.review_id
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noReviewFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_REVIEW_FOUND
      );
    else if (response.reviewUnliked)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.REVIEW_UNLIKED
      );
    else if (response.reviewLiked)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.REVIEW_LIKED
      );
  }
);

router.post(
  "/get-recommended-products-by-province",

  authCheck,

  [
    body("province").isMongoId().withMessage("Province is required"),
    body("category_id")
      .optional()
      .isMongoId()
      .withMessage("category_id is required"),
  ],

  async (req, res) => {
    // Checking if req.body has any error

    const errors = validationResult(req);
    // check if user has sent category field to filter
    if (!errors.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.BAD_REQUEST,
        errors.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg,
        }))
      );
    }

    // Checking if req.body has any error

    const response = await ProductsController.getRecommendedProductsByProvince(
      req
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noProductFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    else if (response.productData)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.PRODUCTS_FETCHED_SUCCESSFULLY,
        response.productData
      );
  }
);

router.get(
  "/get-products-by-shop-owner",

  authCheck,

  async (req, res) => {
    const response = await ProductsController.getProductsAccordingToShopOwner(
      req
    );

    if (response.databaseError)
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    else if (response.accessRestricted)
      return Response.error(
        res,
        ResponseCode.UNAUTHORIZED_ACCESS,
        ResponseMessage.ACCESS_RETRICTED + " for other than end users"
      );
    else if (response.noProductFound)
      return Response.error(
        res,
        ResponseCode.NOT_FOUND,
        ResponseMessage.NO_PRODUCT_FOUND
      );
    else if (response.products)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        ResponseMessage.RETRIVED_SHOP_DATA_SUCCESSFULLY,
        response.products
      );
  }
);

// router.post('/add-bulk-products',

//   uploadDocument.single('file'),

//   async (req, res, next) => {

//     const parsedFile = xlsx.parse(req.file.path);

//     const pjpRows = parsedFile[0].data;
//     const rowTitles = pjpRows[0];

//     for (let data of pjpRows.slice(44, 51)) {

//       const obj = {
//         shop_id: (await Shop.findOne({shop_name: data[0].trim()}))._id,
//         product_name: data[1].trim(),
//         category: (await Category.findOne({category_name: data[2].trim()}))._id,
//         sub_category: (await Category.findOne({category_name: data[2].trim()})).sub_category[0]._id,
//         variants: [{unit: 'N/A', available_quantity: 100, price: data[7]}],
//         product_description: [{
//           language: 'English',
//           content: data[5].trim()
//         }, {
//           language: 'Khmer',
//           content: data[6].trim()
//         }]
//       }

//       await new Product(obj).save();
//       // console.log(obj);
//     }
//   }
// )

// router.get(
//   "/add-product-reviews-field",

//   async (req, res, next) => {
//     try {
//       const updatedDocs = await Product.updateOne({
//         _id:mongoose.Types.ObjectId("60b621fa4c4daa73acd3f6c8")
//       },{ show_reviews: false });

//       console.log(updatedDocs);
//     } catch (error) {
//       console.log(error);
//     }

//   }
// );

module.exports = router;
