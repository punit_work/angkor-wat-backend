//Models
const promocode = require("../../model/Promocode");

// action model data => add - Hotel

//Add hotel
exports.addPromocode = async req => {
  let result = {};
  
  const {user_type,user_id} = req;

  const promocodeData = req.body;

  try {
    //Check if hotel is already exist or not
    const isPromocodeExist = await promocode.findOne({ code: promocodeData.code });

    if (isPromocodeExist) {
      result.promocodeExists = true;
      return result;
    }

    result.promocode = await new promocode(promocodeData).save();
    return result;

  } catch (err) {
    
    result.databaseError = true;
    return result;
  }
};

//Get products from cart
exports.getAllHotels = async req => {
  let result = {};

  
    try {

      const basicFilter = [
        {
          "$match":{}
        },
        {
          "$lookup": {
            "from": "provinces",
            "as": "province",
            "localField": "province",
            "foreignField": "_id"
          }
      },
      {
        "$unwind":{
          "path":"$province",
          "preserveNullAndEmptyArrays":true
        }
      },
      {
        "$addFields":{
          "district": {"$filter": {
            "input": '$province.district',
           "as": 'var',
            "cond": {"$eq": ['$$var._id', '$district']}
        }},
        }

      },
      {
        "$unwind":{
          "path":"$district",
          "preserveNullAndEmptyArrays":true
        }
      },
      {
        "$addFields":{
          "commune": {"$filter": {
            "input": '$district.commune',
           "as": 'var',
            "cond": {"$eq": ['$$var._id', '$commune']}
        }},
        }

      },
      {
        "$unwind":{
          "path":"$commune",
          "preserveNullAndEmptyArrays":true
        }
      },
      {
        "$project":{
          "hotel_name":1,
          "address":1,
          "createdAt":1,
          "updatedAt":1,
          "province._id":1,
          "province.province_name":"$province.province",
          "district._id":1,
          "district.district_name":1,
          "commune":1,
          "postal_code":1
        }
      }

      ]
      result.hotels = await hotel.aggregate(basicFilter);

      if (!result.hotels.length) {
        result.noHotels = true ; 
        return result ; 
      }

      return result;

    } catch (err) {
      result.databaseError = true;
      return result;
    }
};

// Update hotel
exports.updateHotel = async req => {
  let result = {};

  const hotelData = req.body;
  const {hotel_id} = req.params;

  
    try {

     //Check if hotel is  exist or not
    const isHotelExist = await hotel.findOne({ _id: hotel_id });

    if (!isHotelExist) {
      result.hotelNotFound = true;
      return result;
    }
    
    else{

      const updatedHotel = await hotel.updateOne({ _id:hotel_id},hotelData); 

      console.log(updatedHotel);

      if(updatedHotel.nModified > 0){

        result.hotel = true;
        return result;


      }
      else{
        result.nothingUpdated = true;
        return result;
      }

    }

    } catch (err) {
      result.databaseError = true;
      return result;
    }
};
