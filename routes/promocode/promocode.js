const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const PromocodeController = require("./promocode-controller");
const PromocodeHelper = require("./promocode-helper");

const authCheck = require('../../middleware/check-auth');

// Path - /v1/hotel/
// All Routes for hotel

router.post(
  "/add-promocode",
  authCheck
  ,
  [
    body("shop_id")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Valid shop id is required"),
    body("product_id")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Valid product id is required"),
    body("service_id")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Valid service id is required"),
    body("is_percent")
      .isBoolean()
      .withMessage("Please provide amount for promocode is in percent or not"),
    body("amount")
      .isFloat({min:1})
      .withMessage("Please provide valid amount"),
    body("code")
      .notEmpty()
      .withMessage("Please provide valid code"),
    body("on_total")
      .optional()
      .isFloat({min:1})
      .withMessage("Please provide on_total"),
    body("expire_date")
      .optional()
      .isDate()
      .withMessage("Please valid expire date for code")
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    if(!(req.body.shop_id || req.body.service_id || req.body.product_id)){

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Please provide atleast one option from this for promocode [ shop_id, service_id, product_id ]"
      );

    }

    if(req.body.is_percent && (req.body.amount >= 100)){

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Sorry amount cannot be greater than 100 for promocode have 'is_percent' true"
      );

    }

    const response = await PromocodeController.addPromocode(req);

    if (response.promocodeExists) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "This promocode is already exists in system please add another promocode"
      );
    }

    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    } else if (response.promocode)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Promocode Added successfully !",
        response.promocode
      );
  }
);

//Get hotels
//v1/hotel/get-hotel
router.get("/get-hotel", async (req, res) => {
  const response = await HotelController.getAllHotels(req);

  if(response.noHotels){
      return Response.error(
          res,
          ResponseCode.NOT_FOUND,
          ResponseMessage.ERROR_NOT_FOUND
      );
  }

  if (response.databaseError) {
    return Response.error(
      res,
      ResponseCode.DATABASE_ERROR,
      ResponseMessage.ERROR_DATABASE
    );
  } else if (response.hotels)
    return Response.success(
      res,
      ResponseCode.SUCCESS,
      ResponseMessage.HOTEL_RETRIVED_SUCCESSFULLY,
      response.hotels
    );
});

//Update hotel
//v1/hotel/update-hotel
router.post(
  "/update-hotel/:hotel_id",
  [
    body("hotel_name")
      .optional()
      .notEmpty()
      .withMessage("Hotel name is required"),
    body("location")
      //.isObject()
      .optional()
      .notEmpty()
      .withMessage("Location object is required"),
    body("address")
      .optional()
      .notEmpty()
      .withMessage("Address is required"),
    body("province")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid province id"),
    body("district")
      .optional() 
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid district id"),
    body("commune")
      .optional()
      .notEmpty()
      .isMongoId()
      .withMessage("Please provide valid commune id"),
    body("postal_code")
      .optional()
      .notEmpty()
      .isNumeric()
      .withMessage("Please provide valid postal code")
  ],
  async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        error.array().map(error => ({
          field: error.param,
          errorMessage: error.msg
        }))
      );
    }

    const response = await HotelController.updateHotel(req);

    if (response.hotelNotFound) {
      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Hotel not found"
      );
    }

    if (response.databaseError) {
      return Response.error(
        res,
        ResponseCode.DATABASE_ERROR,
        ResponseMessage.ERROR_DATABASE
      );
    }
    else if (response.nothingUpdated){

      return Response.error(
        res,
        ResponseCode.UNPROCESSABLE_ENTITY,
        "Nothing updated !"
      );
    }
     else if (response.hotel)
      return Response.success(
        res,
        ResponseCode.SUCCESS,
        "Hotel Updated successfully",
        response.hotel
      );
  }
);

module.exports = router;
