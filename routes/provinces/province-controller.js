const Province = require("../../model/Province");
const mongoose = require("mongoose");

// Function for adding new province
async function addNewProvince(
  user_type,
  { latitude, longitude, en, kh , district, province_order }
) {
  let result = {};

  try {
    if (["admin", "business_owner", "super_admin"].includes(user_type)) {
      let provinceObject = {};
      const translations = {
        en:{
          province_name:''
        },
        kh:{
          province_name:''
        }
      }

      const addLanguageBasedData = async (data,lang) => {
        if(data.province_name && data.province_name !== ''){
          const provinceExists = await Province.findOne({[`translations.${lang}.province_name`]:data.province_name});
          if(provinceExists){
            result.validationError = true;
            result.message = `Province name for ${lang} language already exists`
            return result;
          } else {
            translations[lang].province_name = data.province_name;
          }
        }
      }

      let hasError;
      hasError = await addLanguageBasedData(en,'en');
      hasError = await addLanguageBasedData(kh,'kh');
      
      if(hasError){
        return hasError;
      }

      provinceObject.country = "Cambodia";
      provinceObject.translations = translations;
      provinceObject.province_order = province_order ? province_order : 0;
      if (district.length) {
        provinceObject.district = district.map((dist) => {

            const districtTranslations = {
              en:{
                district_name:dist.en.district_name
              },
              kh:{
                district_name:dist.kh.district_name
              }
            }
  
  
            return {
              translations: districtTranslations,
              district_order:
                dist.district_order === 0
                  ? 0
                  : typeof dist.district_order === "number"
                  ? dist.district_order
                  : 0,
              commune: [],
            };
        });
      }

      if (latitude && longitude) {
        provinceObject.location = {
          type: "Point",
          coordinates: [parseFloat(longitude), parseFloat(latitude)],
        };
      }

      await new Province(provinceObject).save();
      result.provinceAdded = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
}

// Function for updating province
async function updateProvince(
  user_type,
  { province_id, latitude, longitude, en,kh, province_order, district }
) {
  let result = {};

  try {
    if (["admin", "business_owner", "super_admin"].includes(user_type)) {
      const provinceList = await Province.find({});
      const districtList = provinceList.reduce((acc,prov) => {
        if(prov.district && prov.district.length > 0){
          return acc.concat(prov.district)
        }
        return acc;
      },[])

      const province_details = provinceList.find(provin => provin._id.toString() === province_id);
      const translations = {
        en:{
          province_name:''
        },
        kh:{
          province_name:''
        }
      }

      const addLanguageBasedData = async (data,lang) => {
        if(data.province_name && data.province_name !== ''){
          const provinceExists = await provinceList.find(ele => ele.translations && ele.translations[lang] && ele.translations[lang].province_name === data.province_name && ele._id.toString() !== province_id);
          if(provinceExists){
            result.validationError = true;
            result.message = `Province name ${data.province_name} for ${lang} language already exists`
            return result;
          } else {
            translations[lang].province_name = data.province_name;
          }
        }
      }

      let hasError;
      hasError = await addLanguageBasedData(en,'en');
      hasError = await addLanguageBasedData(kh,'kh');
      
      if(hasError){
        return hasError;
      }

      if (province_details) {
        province_details.translations = translations;
        const failedUpdates = [];
        if(district.length){
          const updatedDistrict = district.reduce((acc,dist) => {
            if(dist._id){

              const distTranslations = {
                en:{
                  district_name:''
                },
                kh:{
                  district_name:''
                }
              };

              if(districtList.find(di => (di.translations && di.translations.en && di.translations.en.district_name === dist.en.district_name) && di._id.toString() !== dist._id)){
                failedUpdates.push({
                  language:'en',
                  district_name:dist.en.district_name
                })
                return acc;
              }

              if(districtList.find(di => (di.translations && di.translations.kh && di.translations.kh.district_name === dist.kh.district_name) && di._id.toString() !== dist._id)){
                failedUpdates.push({
                  language:'kh',
                  district_name:dist.kh.district_name
                })
                return acc;
              }

              distTranslations.en.district_name = dist.en.district_name;
              distTranslations.kh.district_name = dist.kh.district_name;

              const getOriginalDist = districtList.find(dis => dis._id.toString() === dist._id);
              if(getOriginalDist){
                return acc.concat({
                  _id:getOriginalDist._id,
                  translations:distTranslations,
                  district_order:dist.district_order,
                  commune:getOriginalDist.commune
                })
              } else {
                return acc;
              }
            } else {

              const distTranslations = {
                en:{
                  district_name:''
                },
                kh:{
                  district_name:''
                }
              };

              if(districtList.find(di => (di.translations && di.translations.en && di.translations.en.district_name === dist.en.district_name))){
                failedUpdates.push({
                  language:'en',
                  district_name:dist.en.district_name
                })
                return acc;
              }

              if(districtList.find(di => (di.translations && di.translations.kh && di.translations.kh.district_name === dist.kh.district_name))){
                failedUpdates.push({
                  language:'kh',
                  district_name:dist.kh.district_name
                })
                return acc;
              }

              distTranslations.en.district_name = dist.en.district_name;
              distTranslations.kh.district_name = dist.kh.district_name;


              return acc.concat({
                translations:distTranslations,
                district_order:dist.district_order,
                commune:[]
              })
            }
          },[])
          province_details.district = updatedDistrict
        }
        if (province_order) {
          province_details.province_order = province_order
            ? province_order
            : province_details.province_order;
        }

        if (latitude && longitude) {
          province_details.location = {
            type: "Point",
            coordinates: [parseFloat(longitude), parseFloat(latitude)],
          };
        }

        await province_details.save();

        result.provinceUpdated = true;
        return result;
      }

      result.noProvinceFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for deleting province
async function deleteProvince(user_type, province_id) {
  let result = {};

  try {
    if (["admin", "business_owner", "super_admin"].includes(user_type)) {
      let province = await Province.findById(province_id);

      if (province) {
        await province.remove();
        result.provinceRemoved = true;
        return result;
      } else {
        result.noProvinceFound = true;
        return result;
      }
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for fetching list of province
async function getAllProvince({ district }) {
  let result = {};

  try {
    let projectionFilter = {};
    if (!district) {
      projectionFilter = { _id: 1, translations: 1 };
    }
    const province = await Province.find({}, projectionFilter);

    if (province.length) {
      result.province = province;
      return result;
    }

    result.noProvinceFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for fetching list of province
async function getProvinceById(req) {
  let result = {};

  try {
    const province = await Province.find(
      { _id: req.params.province_id },
      { _id: 1, translations: 1, district: 1 }
    );
    if (province.length) {
      result.province = province;
      return result;
    }

    result.noProvinceFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function changeProvinceOrder(user_type, province_id, province_order) {
  let result = {};
  try {
    if (["admin", "business_user", "super_admin"].includes(user_type)) {
      const province = await Province.findByIdAndUpdate(
        province_id,
        {
          $set: { province_order },
        },
        { new: true }
      );

      if (province) {
        result.province = province;
        return result;
      }

      result.noProvinceFound = true;
      return result;
    }
    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function to add district to a province
async function addDistrict(
  user_type,
  { province_id, en,kh, district_order, commune }
) {
  let result = {};

  try {
    if (["admin", "business_owner", "super_admin"].includes(user_type)) {
      const province_details = await Province.findById(province_id);
      if (province_details) {
        const districtAlreadyExists = province_details.district.find(
          (dist) => dist.translations && dist.translations.en && dist.translations.en.district_name === en.district_name
        );

        if (!districtAlreadyExists) {
          let filteredCommune = [];
          if (commune && commune.length > 0) {
            const hasSeenEnCommune = {};
            const hasSeenKhCommune = {};
            filteredCommune = commune.reduce((acc, counter) => {
              const communeTranslations = {
                en:{
                  commune_name:''
                },
                kh:{
                  commune_name:''
                }
              }

              if (
                counter.en.commune_name &&
                counter.en.commune_name !== "" &&
                !hasSeenEnCommune[counter.en.commune_name]
              ) {
                hasSeenEnCommune[counter.en.commune_name] = true;
                communeTranslations.en.commune_name = counter.en.commune_name;
              }

              if (
                counter.kh.commune_name &&
                counter.kh.commune_name !== "" &&
                !hasSeenKhCommune[counter.kh.commune_name]
              ) {
                hasSeenKhCommune[counter.kh.commune_name] = true;
                communeTranslations.kh.commune_name = counter.kh.commune_name;
              }
              
              if(communeTranslations.en.commune_name !== '' && communeTranslations.kh.commune_name !== '') {
                return acc.concat({
                  translations: communeTranslations,
                  commune_order: counter.commune_order,
                  postal_code: counter.postal_code,
                });
              } else {
                return acc;
              }
            }, []);
          }

          province_details.district.push({
            translations:{
              en:{
                district_name:en.district_name
              },
              kh:{
                district_name:kh.district_name
              }
            },
            district_order,
            commune: filteredCommune,
          });

          await province_details.save();

          result.districtAdded = true;
          return result;
        } else {
          result.districtAlreadyExists = true;
          return result;
        }
      }
      result.noProvinceFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function to update district of a province
async function updateDistrict(
  user_type,
  { province_id, district_id, en,kh, district_order, commune }
) {
  let result = {};

  try {
    if (["admin", "business_owner", "super_admin"].includes(user_type)) {

      const provinceList = await Province.find({});
      const districtList = provinceList.reduce((acc,prov) => {
        if(prov.district && prov.district.length > 0){
          return acc.concat(prov.district)
        }
        return acc;
      },[]);

      const communeList = districtList.reduce((acc,dist) => {
        if(dist.commune && dist.commune.length > 0){
          return acc.concat(dist.commune)
        }
        return acc;
      },[]);

      const province_details = await Province.findById(province_id);

      if (province_details) {
        const districtIndex = province_details.district.findIndex(
          (dist) => dist._id.toString() === district_id
        );
        if (districtIndex !== -1) {
          const updateData = {};

          // create function
          // create default translation object
          // check if the new values are unique
          // if values are unique return translated object
          // return error;

          const getTranslatedData = ({dataList,id,newData,field}) => {
            const result = {
              success:false,
              data:null
            }

              const translations = {
                en:{
                  [field]:''
                },
                kh:{
                  [field]:''
                }
              }

              const validateLangBasedField = (lang) => {
                if(id){
                  return dataList.find(ele => ele._id.toString() !== id  && ele.translations && ele.translations[lang] && ele.translations[lang][field] === newData[lang][field])
                } 
                return dataList.find(ele => ele.translations && ele.translations[lang] && ele.translations[lang][field] === newData[lang][field])
              }
              if(validateLangBasedField('en')){
                result.success = false;
                result.data = `${newData['en'][field]} for language en already exists`
                return result;
              } else {
                translations.en[field] = newData['en'][field]
              }
              if(validateLangBasedField('kh')){
                result.success = false;
                result.data = `${newData['kh'][field]} for language en already exists`
                return result;
              } else {
                translations.kh[field] = newData['kh'][field]
              }

              result.success = true;
              result.data = translations
             return result;
          }

         const uniqueDistrict = getTranslatedData({
           dataList:districtList,
           id:district_id,
           newData:{en,kh},
           field:'district_name'
          })

          if(uniqueDistrict && uniqueDistrict.success){
            updateData.translations = uniqueDistrict.data;
          } else {
            result.validationError = true;
            result.message = uniqueDistrict.data;
            return result;
          }

          if (district_order && district_order !== "") {
            updateData.district_order = district_order;
          }

          let filteredCommune = [];
          if (commune && commune.length > 0) {
            // const hasSeenCommune = {};
            filteredCommune = commune.reduce((acc,ele,ind) => {
              if(ele._id){
                if(communeList.find(com => com._id.toString() === ele._id)){
                  const translatedCommune = getTranslatedData({dataList:communeList,newData:{en:ele.en,kh:ele.kh},id:ele._id,field:'commune_name'});
                  if(translatedCommune && translatedCommune.success){
                    return acc.concat({
                      _id: mongoose.Types.ObjectId(ele._id),
                      commune_order:ele.commune_order,
                      translations:translatedCommune.data,
                      postal_code:ele.postal_code
                    });
                  }
                }
                return acc;
              } else {
                const translatedCommune = getTranslatedData({dataList:communeList,newData:{en:ele.en,kh:ele.kh},id:ele._id,field:'commune_name'});
                if(translatedCommune && translatedCommune.success){
                  return acc.concat({
                    commune_order:ele.commune_order,
                    translations:translatedCommune.data,
                    postal_code:ele.postal_code
                  })
                }
                return acc;
              }
            },[])
            // filteredCommune = commune.reduce((acc, ele, ind) => {
            //   const isUniqueName =
            //     ele.commune_name &&
            //     ele.commune_name !== "" &&
            //     !hasSeenCommune[ele.commune_name];
            //   const currentCommune = province_details.district[
            //     districtIndex
            //   ].commune.find((com) => com._id.toString() === ele._id);

            //   if (ele._id && currentCommune) {
            //     // CHECK IF THE COMMUNE HAS ID AND IF IT DOES THEN ITS AN UPDATE FLOW
            //     if (isUniqueName) {
            //       hasSeenCommune[ele.commune_name] = true;
            //       return acc.concat({
            //         _id: currentCommune._id,
            //         commune_name: ele.commune_name,
            //         commune_order: ele.commune_order,
            //         postal_code: ele.postal_code,
            //       });
            //     } else {
            //       return acc.concat(currentCommune);
            //     }
            //   } else {
            //     //  ADD COMMUNE FLOW
            //     if (isUniqueName) {
            //       return acc.concat(ele);
            //     } else {
            //       return acc;
            //     }
            //   }
            // }, []);
          }
          updateData.commune = filteredCommune;
          if (updateData.translations) {
            province_details.district[districtIndex].translations =
              updateData.translations;
          }
          if (updateData.district_order) {
            province_details.district[districtIndex].district_order =
              updateData.district_order;
          }
          if (updateData.commune) {
            province_details.district[districtIndex].commune =
              updateData.commune;
          }

          await province_details.save();
          result.provinceUpdated = true;
          return result;
        } else {
          result.districtAlreadyExists = true;
          return result;
        }
      }
      result.noProvinceFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function to add district to a province
// async function addDistrict(user_type, { province_id, district_name, district_order }) {
//   let result = {};

//   try {
//     if (["admin", "business_owner", "super_admin"].includes(user_type)) {
//       const province_details = await Province.findById(province_id);
//       if (province_details) {
//         let updatedDistrictArray = [];
//         updatedDistrictArray = district.map((dist) => {
//           let validatedCommune = [];
//           if (dist.commune && Array.isArray(dist.commune)) {
//             validatedCommune = dist.commune.map((com) => ({
//               commune_name: com.commune_name,
//               commune_order: com.commune_order,
//               postal_code: com.postal_code,
//             }));
//           }
//           return {
//             district_name: dist.district_name,
//             district_order: dist.district_order,
//             commune: validatedCommune,
//           };
//         });
//         if (province_details.district && province_details.district === 0) {
//           province_details.district = updatedDistrictArray;
//           await province_details.save();

//           result.provinceUpdated = true;
//           return result;
//         } else {
//           const newDistricts = updatedDistrictArray.filter(
//             (upDist) =>
//               !province_details.district.find(
//                 (dist) => dist.district_name === upDist.district_name
//               )
//           );
//           if (newDistricts && newDistricts.length > 0) {
//             province_details.district =
//               province_details.district.concat(newDistricts);
//           }
//           await province_details.save();

//           result.provinceUpdated = true;
//           return result;
//         }
//       }
//       result.noProvinceFound = true;
//       return result;
//     }

//     result.accessRestricted = true;
//     return result;
//   } catch (err) {
//     result.databaseError = true;
//     return result;
//   }
// }

// Function to update district of a province
// async function updateDistrict(user_type, { province_id, district, commune }) {
//   let result = {};

//   try {
//     if (["admin", "business_owner", "super_admin"].includes(user_type)) {
//       const province_details = await Province.findById(province_id);
//       if (province_details) {
//         let updatedDistrictArray = [];

//         // CLEAR AND VALIDATE USER DATA
//         updatedDistrictArray = district.map((dist) => {
//           let validatedCommune = [];
//           if (dist.commune && Array.isArray(dist.commune)) {
//             validatedCommune = dist.commune.map((com) => {
//               const data = {};
//               if (com._id) {
//                 data._id = com._id;
//               }
//               data.commune_name = com.commune_name;
//               data.commune_order = com.commune_order;
//               data.postal_code = com.postal_code;
//               return data;
//             });
//           }

//           let districtData = {};
//           if (dist._id) {
//             districtData._id = dist._id;
//           }

//           districtData.district_name = dist.district_name;
//           districtData.district_order = dist.district_order;
//           districtData.commune = validatedCommune;
//           return districtData;
//         });

//         // CHECK IF THERE ARE ANY DISTRICTS IN THE DB
//         if (province_details.district && province_details.district === 0) {
//           province_details.district = updatedDistrictArray;
//           await province_details.save();

//           result.provinceUpdated = true;
//           return result;
//         } else {
//           let updatedList = [];
//           if (updatedDistrictArray && updatedDistrictArray.length > 0) {
//             updatedList = updatedDistrictArray.reduce((acc, upDist) => {
//               if (upDist._id) {
//                 if (
//                   province_details.district.find(
//                     (provDist) => provDist._id.toString() === upDist._id
//                   )
//                 ) {
//                   return acc.concat(upDist);
//                 }
//                 return acc;
//               } else {
//                 if (
//                   !province_details.district.find(
//                     (provDist) =>
//                       provDist.district_name === upDist.district_name
//                   )
//                 ) {
//                   return acc.concat(upDist);
//                 }
//                 return acc;
//               }
//             }, []);
//           }

//           province_details.district = updatedList;

//           await province_details.save();
//           result.provinceUpdated = true;
//           return result;
//         }
//       }
//       result.noProvinceFound = true;
//       return result;
//     }

//     result.accessRestricted = true;
//     return result;
//   } catch (err) {
//     result.databaseError = true;
//     return result;
//   }
// }

// Function to add commune to a district
async function addCommune(user_type, { province_id, district_id, commune }) {
  let result = {};

  try {
    if (["discover", "super_admin"].includes(user_type)) {
      const province_details = await Province.findById(province_id);
      if (province_details) {
        const districtIndex = province_details.district.findIndex(
          (dist) => dist._id.toString() === district_id
        );
        console.log(districtIndex);
        if (districtIndex !== -1) {
          let updatedCommuneList = [];

          const existingCommuneList = [
            ...province_details.district[districtIndex].commune,
          ];
          updatedCommuneList = commune.map((com) => ({
            commune_name: com.commune_name,
            postal_code: com.postal_code,
            commune_order: com.commune_order,
          }));

          if (commune && commune.length === 0) {
            province_details.district[districtIndex].commune =
              updatedCommuneList;
            await province_details.save();
            result.communeAdded = true;
            return result;
          } else {
            const newProvinces = updatedCommuneList.filter(
              (upCom) =>
                !existingCommuneList.find(
                  (exCom) => exCom.commune_name === upCom.commune_name
                )
            );
            province_details.district[districtIndex].commune =
              province_details.district[districtIndex].commune.concat(
                newProvinces
              );
            await province_details.save();
            result.communeAdded = true;
            return result;
          }
        } else {
          result.noDistrictFound = true;
          return result;
        }
      }
      result.noProvinceFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function to update commune to a district
async function updateCommune(user_type, { province_id, district_id, commune }) {
  let result = {};

  try {
    if (["discover", "super_admin"].includes(user_type)) {
      const province_details = await Province.findById(province_id);
      if (province_details) {
        const districtIndex = province_details.district.findIndex(
          (dist) => dist._id.toString() === district_id
        );
        console.log(districtIndex);
        if (districtIndex !== -1) {
          let updatedCommuneList = [];

          const existingCommuneList = [
            ...province_details.district[districtIndex].commune,
          ];
          updatedCommuneList = commune.map((com) => {
            const data = {};
            if (com._id) {
              data._id = com._id;
            }

            data.commune_name = com.commune_name;
            data.postal_code = com.postal_code;
            data.commune_order = com.commune_order;
            return data;
          });

          if (commune && commune.length === 0) {
            province_details.district[districtIndex].commune =
              updatedCommuneList;
            await province_details.save();
            result.communeUpdated = true;
            return result;
          } else {
            const newProvinces = updatedCommuneList.reduce((acc, upCom) => {
              if (upCom._id) {
                if (
                  province_details.district[districtIndex].commune.find(
                    (com) => com._id.toString() === upCom._id
                  )
                ) {
                  return acc.concat(upCom);
                }
                return acc;
              } else {
                if (
                  !province_details.district[districtIndex].commune.find(
                    (com) => com.commune_name === upCom.commune_name
                  )
                ) {
                  return acc.concat(upCom);
                }
                return acc;
              }
            }, []);
            province_details.district[districtIndex].commune = newProvinces;
            await province_details.save();
            result.communeUpdated = true;
            return result;
          }
        } else {
          result.noDistrictFound = true;
          return result;
        }
      }
      result.noProvinceFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function deleteCommune(
  user_type,
  { province_id, district_id, commune_id }
) {
  let result = {};

  try {
    if (["discover", "super_admin"].includes(user_type)) {
      const province_details = await Province.findById(province_id);
      if (province_details) {
        const selectedDistrictIndex = province_details.district.findIndex(
          (dist) => dist._id.toString() === district_id
        );
        if (selectedDistrictIndex >= 0) {
          const filteredCommune = province_details.district[
            selectedDistrictIndex
          ].commune.filter((com) => com._id.toString() !== commune_id);
          if (filteredCommune && filteredCommune.length === 0) {
            province_details.district[selectedDistrictIndex].commune = [];
          } else {
            province_details.district[selectedDistrictIndex].commune =
              filteredCommune;
          }

          await province_details.save();
          result.communeDeleted = true;
          return result;
        }
        result.noDistrictFound = true;
        return result;
      }
      resilt.noProvinceFound = true;
      return result;
    }
    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function deleteDistrict(user_type, { province_id, district_id }) {
  let result = {};

  try {
    if (["discover", "super_admin"].includes(user_type)) {
      const province_details = await Province.findById(province_id);
      if (province_details) {
        const filteredDistrict = province_details.district.filter(
          (dist) => dist._id.toString() !== district_id
        );
        if (filteredDistrict && filteredDistrict.length === 0) {
          province_details.district = [];
        } else {
          province_details.district = filteredDistrict;
        }
        await province_details.save();
        result.districtDeleted = true;
        return result;
      }
      resilt.noProvinceFound = true;
      return result;
    }
    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function changeDistrictOrder(
  user_type,
  { province_id, district_id, district_order }
) {
  let result = {};
  try {
    if (["admin", "business_user", "super_admin"].includes(user_type)) {
      const province = await Province.findByIdAndUpdate(
        province_id,
        {
          $set: { "district.$[element].district_order": district_order },
        },
        {
          arrayFilters: [
            { "element._id": { $eq: mongoose.Types.ObjectId(district_id) } },
          ],
        }
      );

      if (province) {
        result.province = province;
        return result;
      }

      result.noProvinceFound = true;
      return result;
    }
    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

module.exports = {
  addNewProvince,
  getAllProvince,
  deleteProvince,
  updateProvince,
  getProvinceById,
  changeProvinceOrder,
  addDistrict,
  updateDistrict,
  deleteDistrict,
  addCommune,
  updateCommune,
  deleteCommune,
  changeDistrictOrder,
};
