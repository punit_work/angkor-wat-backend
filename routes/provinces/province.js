const express = require('express')
const router = express.Router();
const authCheck = require('../../middleware/check-auth');
const Response = require('../../global/response');
const ResponseMessage = require('../../global/message');
const ResponseCode = require('../../global/code');
const ProvinceController = require('./province-controller');
const {body, param, query,validationResult} = require('express-validator');

// Path - /v1/province/get-all-province
// Route for fetching all province
router.get('/get-all-province',

    // Middleware to check if user is authenticated
    authCheck,
    [
      query('district').optional().isBoolean().withMessage('Parameter [province_id] is should be a boolean value')
    ],
    async (req, res) => {
      const error = validationResult(req);
        
      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }
      
        const response = await ProvinceController.getAllProvince(req.query);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner found
        else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

        // Sending error response if user enters start date as past date
        else if (response.province) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_FETCHED, response.province);        
    }
)


// Path - /v1/province/get-province-by-id
// Route for fetching province information by id
router.get('/get-province-by-id/:province_id',

    // Middleware to check if user is authenticated
    authCheck,

    async (req, res) => {
     
        const response = await ProvinceController.getProvinceById(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner found
        else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

        // Sending error response if user enters start date as past date
        else if (response.province) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_FETCHED, response.province);        
    }
)


module.exports = router;