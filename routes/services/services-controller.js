const Service = require("../../model/Service");
const moment = require("moment");
const SystemUsers = require("../../model/SystemUsers");
const Users = require("../../model/Users");
const Reviews = require("../../model/Reviews");
const Ratings = require("../../model/Ratings");
const Shop = require("../../model/Shop");
const mongoose = require("mongoose");

// Function for adding new service
async function addNewService(req) {
  let service_provider,
    result = {};
  const service_data = req.body;

  try {
    //check if owner is valid or not
    const isValidOwner = await Shop.findOne({
      _id: req.user_id,
      vendor_type: "service_provider",
    });

    if (!isValidOwner) {
      result.notValidOwner = true;
      return result;
    }

    const todaysDate = new Date().setHours(0, 0, 0, 0);

    const startDate = new Date(service_data.start_date).setHours(0, 0, 0, 0);

    const endDate = new Date(service_data.end_date).setHours(0, 0, 0, 0);

    // Checking if user has entered start date as past date
    if (startDate < todaysDate) {
      result.cannotUseStartDateAsPastDate = true;
      return result;
    }

    // Checking if user has entered end date as past date
    else if (endDate < todaysDate) {
      result.cannotUseEndDateAsPastDate = true;
      return result;
    }

    // Checking if user has entered end date before start date
    else if (endDate < startDate) {
      result.cannotUserEndDateBeforeStartDate = true;
      return result;
    }

    if (service_data.tags) service_data.tags = JSON.parse(service_data.tags);
    if (service_data.payment_methods)
      service_data.payment_methods = JSON.parse(service_data.payment_methods);
    if (service_data.promocode)
      service_data.promocode = JSON.parse(service_data.promocode);
    if (service_data.tax) service_data.tax = JSON.parse(service_data.tax);
    service_data.service_details = JSON.parse(service_data.service_details);
    service_data.available_slot = JSON.parse(service_data.available_slot);
    service_data.package = JSON.parse(service_data.package);

    let serviceObject = {
      ...service_data,
      shop_id: req.user_id,
      start_date: new Date(service_data.start_date).toISOString(),
      end_date: new Date(service_data.end_date).toISOString(),
      location: {
        type: "Point",
        coordinates: [
          parseFloat(service_data.longitude),
          parseFloat(service_data.latitude),
        ],
      },
    };

    serviceObject.service_images = await req.files.map(({ path }) =>
      path.replace(/\\/g, "/")
    );

    service_result = await new Service(serviceObject).save();

    if (service_result) {
      result.serviceAdded = service_result;
      return result;
    }
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
}

// Function for fetching all services (based on admin/business_owner/end-users)
async function getAllServices(user_id, user_type) {
  let services,
    result = {};

  try {
    if (user_type === "business_owner")
      services = await Service.find({ business_owner_id: user_id });

    if (user_type === "service_provider_id")
      services = await Service.find({ service_provider_id: user_id });
    else services = await Service.find();

    if (services.length === 0) {
      result.noServiceFound = true;
      return result;
    }

    result.services = services;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function getServiceByID(req) {
  let result = {};

  const { service_id } = req.params;
  const { user_id, user_type } = req;

  if (user_type == "end_user") {
    try {
      service = await Service.findById(service_id);

      if (service) {
        const basicFilter = [
          {
            $match: {
              _id: mongoose.Types.ObjectId(service_id),
            },
          },
          {
            $lookup: {
              from: "shops",
              as: "shop",
              localField: "shop_id",
              foreignField: "_id",
            },
          },
          {
            $lookup: {
              from: "ratings",
              as: "rating",
              localField: "_id",
              foreignField: "service_id",
            },
          },
          {
            $unwind: {
              path: "$rating",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "wishlists",
              as: "wishlist",
              localField: "_id",
              foreignField: "service",
            },
          },
          {
            $addFields: {
              wishlist: {
                $filter: {
                  input: "$wishlist",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              "rating.rated_by": {
                $filter: {
                  input: "$rating.rated_by",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              user_rating: { $first: "$rating.rated_by" },
            },
          },
          {
            $unwind: "$shop",
          },
          {
            $project: {
              service_images: 1,
              tags: 1,
              shop_id: 1,
              service_title: 1,
              category: 1,
              sub_category: 1,
              sub_sub_category: 1,
              package: 1,
              available_slot: 1,
              service_details: 1,
              start_date: 1,
              end_date: 1,
              city: 1,
              address: 1,
              is_featured: 1,
              createdAt: 1,
              updatedAt: 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.no_of_ratings": 1,
              "rating.average_rating": 1,
              "rating.user_rating": "$user_rating.rating",
              in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
            },
          },
        ];

        const serviceData = await Service.aggregate(basicFilter);

        result.service = serviceData;
        return result;
      } else {
        result.noServiceFound = true;
        return result;
      }
    } catch (e) {
      console.log(e);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unauthorizedAccess = true;
    return result;
  }
}

// Function for updating service
async function updateService(service_provider_id, service_data) {
  let service,
    result = {};

  try {
    service = await Service.findOne({
      service_provider_id: service_provider_id,
      _id: service_data.body.service_id,
    });

    if (!service) {
      result.noServiceFound = true;
      return result;
    }

    const {
      start_date,
      end_date,
      province,
      pincode,
      service_title,
      service_description,
      city,
      address,
      latitude,
      longitude,
      tags,
      price,
      category,
      sub_category,
      available_quantity,
      is_available,
      payment_methods,
    } = service_data.body;

    const { service_images } = service_data.files;

    if (start_date || end_date) {
      const todaysDate = new Date().setHours(0, 0, 0, 0);

      const startDate = new Date(start_date).setHours(0, 0, 0, 0);

      const endDate = new Date(end_date).setHours(0, 0, 0, 0);

      if (start_date) {
        // Checking if user has entered start date as past date
        if (startDate < todaysDate) {
          result.cannotUseStartDateAsPastDate = true;
          return result;
        }
      }

      if (end_date) {
        // Checking if user has entered end date as past date
        if (endDate < todaysDate) {
          result.cannotUseEndDateAsPastDate = true;
          return result;
        }
      }

      if (start_date && end_date) {
        // Checking if user has entered end date before start date
        if (endDate < startDate) {
          result.cannotUserEndDateBeforeStartDate = true;
          return result;
        }
      }
    }

    let new_file = [];

    if (service_data.files && service_data.files.length) {
      new_file = await service_data.files.map(({ path }) =>
        path.replace(/\\/g, "/")
      );
    }

    service.available_quantity = available_quantity
      ? available_quantity
      : service.available_quantity;
    service.is_available = is_available ? is_available : service.is_available;
    service.service_title = service_title
      ? service_title
      : service.service_title;
    service.service_description = service_description
      ? service_description
      : service.service_description;
    service.province = province ? province : service.province;
    service.pincode = pincode ? pincode : service.pincode;
    service.city = city ? city : service.city;
    service.price = price ? price : service.price;
    service.address = address ? address : service.address;
    service.tags = tags && JSON.parse(tags) ? JSON.parse(tags) : service.tags;
    service.category = category ? category : service.category;
    service.sub_category = sub_category ? sub_category : service.sub_category;
    service.service_images = new_file.length
      ? service.service_images.concat(new_file)
      : service.service_images;
    service.payment_methods =
      payment_methods && JSON.parse(payment_methods)
        ? JSON.parse(payment_methods)
        : service.payment_methods;
    service.start_date = start_date
      ? new Date(start_date).toISOString()
      : service.start_date;
    service.end_date = end_date
      ? new Date(end_date).toISOString()
      : service.end_date;
    service.location =
      latitude && longitude
        ? {
            type: "Point",
            coordinates: [parseFloat(longitude), parseFloat(latitude)],
          }
        : service.location;

    let updatedService = await service.save();

    if (updatedService) {
      result.serviceUpdated = updatedService;
      return result;
    }
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
}

// Function for deleting service
async function deleteService(service_provider_id, service_id) {
  let service,
    result = {};

  try {
    service = await Service.findOne({
      service_provider_id: service_provider_id,
      _id: service_id,
    });

    if (!service) {
      result.noServiceFound = true;
      return result;
    }

    await service.remove();
    result.serviceDeleted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// function for adding ratings for a service
async function addServiceRating(req) {
  let result = {};

  try {
    if (req.user_type === "end_user") {
      const service = await Service.findById(req.body.service_id);

      if (service) {
        const rating = await Ratings.findOne({
          service_id: req.body.service_id,
        });

        if (rating) {
          if (
            rating.rated_by.length &&
            rating.rated_by.find(
              (users) => users.user_id.toString() === req.user_id
            )
          ) {
            result.ratingUpdated = true;
            rating.rated_by[
              rating.rated_by.findIndex(
                (users) => users.user_id.toString() === req.user_id
              )
            ].rating = req.body.rating_number;
            const average_rating =
              (rating.rated_by.reduce((acc, curr) => acc + curr.rating, 0) +
                req.body.rating_number) /
              (rating.no_of_ratings + 1);
            rating.average_rating = average_rating.toFixed(1);
            await rating.save();
            return result;
          }

          const average_rating =
            (rating.rated_by.reduce((acc, curr) => acc + curr.rating, 0) +
              req.body.rating_number) /
            (rating.no_of_ratings + 1);
          rating.no_of_ratings = rating.no_of_ratings + 1;
          rating.average_rating = average_rating.toFixed(1);
          rating.rated_by.push({
            user_id: req.user_id,
            rating: req.body.rating_number,
          });
          await rating.save();

          result.ratingAdded = true;
          return result;
        }

        const ratingObject = {
          user_id: req.user_id,
          service_id: req.body.service_id,
          no_of_ratings: 1,
          average_rating: req.body.rating_number,
          rated_by: [{ user_id: req.user_id, rating: req.body.rating_number }],
        };

        await new Ratings(ratingObject).save();

        result.ratingAdded = true;
        return result;
      }

      result.serviceNotFound = true;
      return result;
    } else {
      result.unauthorizedAccess = true;
      return result;
    }
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// function for adding service review
async function addServiceReview(req) {
  let result = {};

  try {
    if (req.user_type === "end_user") {
      const user = await Users.findById(req.user_id);
      const service = await Service.findById(req.body.service_id);

      if (service) {
        let reviewObject = {
          service_id: req.body.service_id,
          comment: req.body.comment,
          user_id: req.user_id,
        };

        let review = await Reviews(reviewObject).save();

        reviewObject = {
          service_title: service.service_title,
          service_id: service._id,
          // user_name: user.fname + " " + user.lname,
          // user_avatar: user.avatar,
          comment: review.comment,
          review_id: review._id,
          createdAt: review.createdAt,
          updatedAt: review.updatedAt,
          liked_by: 0,
        };

        user.fname != null
          ? (reviewObject.user_name = user.fname + " " + user.lname)
          : (reviewObject.user_id = req.user_id);
        user.avatar != null ? (reviewObject.user_avatar = user.avatar) : null;

        result.review = reviewObject;
        return result;
      }

      result.noServiceFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function updateServiceReview(user_type, user_id, { comment, review_id }) {
  let result = {};

  try {
    if (user_type === "end_user") {
      let review = await Reviews.findOne({ user_id, _id: review_id })
        .populate("user_id")
        .populate("service_id");

      if (review) {
        review.comment = comment;
        await review.save();

        let reviewObject = {
          service_title: review.service_id.service_title,
          service_id: review.service_id._id,
          // user_name: review.user_id.fname + " " + review.user_id.lname,
          // user_avatar: review.user_id.avatar,
          comment: review.comment,
          review_id: review._id,
          createdAt: review.createdAt,
          updatedAt: review.updatedAt,
          liked_by: review.liked_by.length,
        };

        review.user_id.fname != null
          ? (reviewObject.user_name =
              review.user_id.fname + " " + review.user_id.lname)
          : (reviewObject.user_id = user_id);
        review.user_id.avatar != null
          ? (reviewObject.user_avatar = review.user_id.avatar)
          : null;

        result.review = reviewObject;
        return result;
      }

      result.noReviewFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function deleteServiceReview(user_type, user_id, { review_id }) {
  let result = {};

  try {
    if (user_type === "end_user") {
      const review = await Reviews.findOne({ user_id, _id: review_id });

      if (review) {
        await review.remove();

        result.reviewDeleted = true;
        return result;
      }

      result.noReviewFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function getServiceReviews(user_type, user_id, service_id) {
  let result = {};

  try {
    if (user_type === "end_user") {
      const reviews = await Reviews.find({ service_id })
        .populate("user_id")
        .populate("service_id");

      if (reviews.length) {
        let reviewList = [];

        for (let review of reviews) {
          let reviewObject = {
            service_title: review.service_id.service_title,
            service_id: review.service_id._id,
            // user_name: review.user_id.fname + " " + review.user_id.lname,
            // user_avatar: review.user_id.avatar,
            comment: review.comment,
            review_id: review._id,
            createdAt: review.createdAt,
            updatedAt: review.updatedAt,
            liked_by: review.liked_by.length,
            has_user_commented:
              review.user_id._id.toString() === user_id.toString(),
            has_user_liked_review: review.liked_by.includes(user_id),
          };

          review.user_id.fname != null
            ? (reviewObject.user_name =
                review.user_id.fname + " " + review.user_id.lname)
            : (reviewObject.user_id = user_id);
          review.user_id.avatar != null
            ? (reviewObject.user_avatar = review.user_id.avatar)
            : null;

          reviewList.push(reviewObject);
        }

        result.reviews = reviewList;
        return result;
      }

      result.noReviewsFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function changeReviewLikeStatus(req) {
  let result = {};

  try {
    if (req.user_type === "end_user") {
      let review = await Reviews.findById(req.params.review_id);

      if (review) {
        const index = review.liked_by.findIndex(
          (user) => user.toString() === req.user_id
        );

        if (index !== -1) {
          review.liked_by.splice(index, 1);
          await review.save();

          result.reviewUnliked = true;
          return result;
        } else {
          review.liked_by.unshift(req.user_id);
          await review.save();

          result.reviewLiked = true;
          return result;
        }
      }

      result.noReviewFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for fetching all services (based on admin/business_owner/end-users)
async function getServiceByCategory(req) {
  let services,
    result = {};

  try {
    const { category_id, subcategory_id } = req.body;

    if (req.user_type === "business_owner")
      services = await Service.find({
        business_owner_id: req.user_id,
        category: category_id,
        sub_category: subcategory_id,
      });

    if (req.user_type === "service_provider")
      services = await Service.find({
        service_provider_id: req.user_id,
        category: category_id,
        sub_category: subcategory_id,
      });
    else
      services = await Service.find({
        category: category_id,
        sub_category: subcategory_id,
      });

    if (services.length === 0) {
      result.noServiceFound = true;
      return result;
    }

    let serviceList = [];

    for (let service of services) {
      let single_service = JSON.parse(JSON.stringify(service));
      const rating = await Ratings.findOne({ service_id: service._id }).select(
        "-_id no_of_ratings average_rating"
      );

      if (rating) single_service.service_rating = rating;

      serviceList.push(single_service);
    }

    result.services = serviceList;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for fetching ratings by service
async function getRatingForService(user_id, user_type, service_id) {
  let result = {};

  try {
    if (user_type === "end_user") {
      let rating = await Ratings.findOne({ service_id: service_id });

      if (rating) {
        let ratingObject = {
          no_of_ratings: rating.no_of_ratings,
          average_rating: rating.average_rating,
          has_user_rated: rating.rated_by.find(
            (user) => user.user_id.toString() === user_id
          )
            ? true
            : false,
          user_ratings: rating.rated_by.find(
            (user) => user.user_id.toString() === user_id
          )
            ? rating.rated_by.find(
                (user) => user.user_id.toString() === user_id
              ).rating
            : 0,
        };

        result.rating = ratingObject;
        return result;
      }

      result.noRatingsFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function filterBy(req) {
  result = {};

  if (req.user_type !== "end_user") {
    // no user other then 'end_user' is allowed to access this api
    result.invalidUser = true;
    return result;
  }

  const { category_id, subcategory_id } = req.body;

  if (
    req.body.tag ||
    req.body.city ||
    req.body.rating ||
    req.body.title ||
    (req.body.min_price && req.body.max_price)
  ) {
    let servicesData = [];

    try {
      servicesData = await Service.find({
        category: category_id,
        sub_category: subcategory_id,
      });

      // by tag
      if (req.body.tag) {
        if (req.body.tag.toUpperCase() === "POPULAR") {
          let serviceList = [];

          for (let service of servicesData) {
            let single_service = JSON.parse(JSON.stringify(service));
            const rating = await Ratings.findOne({
              service_id: service._id,
            }).select("-_id no_of_ratings average_rating");

            if (rating) single_service.service_rating = rating;

            serviceList.push(single_service);
          }

          servicesData = await serviceList.sort(
            (a, b) =>
              b.service_rating.average_rating - a.service_rating.average_rating
          );
        } else if (req.body.tag.toUpperCase() === "LATEST") {
          servicesData = await Service.find({
            category: category_id,
            sub_category: subcategory_id,
          })
            .sort({ createdAt: -1 })
            .limit(10);

          let serviceList = [];

          for (let service of servicesData) {
            let single_service = JSON.parse(JSON.stringify(service));
            const rating = await Ratings.findOne({
              service_id: service._id,
            }).select("-_id no_of_ratings average_rating");

            if (rating) single_service.service_rating = rating;

            serviceList.push(single_service);
          }

          servicesData = serviceList;
        }
      }

      //by name
      if (req.body.title) {
        if (category_id && subcategory_id) {
          servicesData = await Service.find({
            category: category_id,
            sub_category: subcategory_id,
            service_title: {
              $regex: req.body.title,
              $options: "i",
            },
          });
        } else {
          servicesData = await Service.find({
            service_title: {
              $regex: req.body.title,
              $options: "i",
            },
          });
        }

        let serviceList = [];

        for (let service of servicesData) {
          let single_service = JSON.parse(JSON.stringify(service));
          const rating = await Ratings.findOne({
            service_id: service._id,
          }).select("-_id no_of_ratings average_rating");

          if (rating) single_service.service_rating = rating;

          serviceList.push(single_service);
        }

        servicesData = serviceList;
      }
      //by city
      if (req.body.city) {
        servicesData = await Service.find({
          category: category_id,
          sub_category: subcategory_id,
          city: {
            $regex: req.body.city,
            $options: "i",
          },
        });

        let serviceList = [];

        for (let service of servicesData) {
          let single_service = JSON.parse(JSON.stringify(service));
          const rating = await Ratings.findOne({
            service_id: service._id,
          }).select("-_id no_of_ratings average_rating");

          if (rating) single_service.service_rating = rating;

          serviceList.push(single_service);
        }

        servicesData = serviceList;
      }

      //by ratings
      if (req.body.rating) {
        let serviceList = [];

        for (let service of servicesData) {
          let single_service = JSON.parse(JSON.stringify(service));
          const rating = await Ratings.findOne({
            service_id: service._id,
          }).select("-_id no_of_ratings average_rating");

          if (rating) single_service.service_rating = rating;

          serviceList.push(single_service);
        }
        servicesData = await serviceList.filter(
          (data) => data.service_rating.average_rating >= req.body.rating
        );
      }

      if (req.body.min_price && req.body.max_price) {
        servicesData = await Service.find({
          $and: [
            { price: { $gte: req.body.min_price } },
            { price: { $lte: req.body.max_price } },
            { category: { $eq: mongoose.Types.ObjectId(category_id) } },
            { sub_category: { $eq: mongoose.Types.ObjectId(subcategory_id) } },
          ],
        });
      }

      if (servicesData.length !== 0) {
        result.services = servicesData;
        return result;
      } else {
        result.noServicesFound = true;
        return result;
      }
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }
  } else {
    result.invalidFilter = true;
    return result;
  }
}

async function getSortedServices(req) {
  let result = {};
  if (req.user_type == "end_user") {
    try {
      if (req.body.high_to_low) {
        if (req.body.category_id && req.body.subcategory_id) {
          data = await Service.find({
            sub_category: req.body.subcategory_id,
            category: req.body.category_id,
          }).sort({ price: -1 });
        } else {
          data = await Service.find().sort({ price: -1 });
        }
      }
      if (req.body.low_to_high) {
        if (req.body.category_id && req.body.subcategory_id) {
          data = await Service.find({
            sub_category: req.body.subcategory_id,
            category: req.body.category_id,
          }).sort({ price: 1 });
        } else {
          data = await Service.find().sort({ price: 1 });
        }
      }

      if (data) {
        result.services = data;
        return result;
      } else {
        result.noServiceFound = true;
        return result;
      }
    } catch (e) {
      console.log(e);
      result.databaseError = true;
      return result;
    }
  } else {
    result.unauthorizedAccess = true;
    return result;
  }
}


async function markServiceAsFeatured(req) {
  let result = {};

  try {
    if (req.user_type === "admin") {
      for (let id of req.body.service_ids) {
        try {
          const obj = await Service.findById(id);

          if (obj) {
            obj.is_featured = true;
            await obj.save();
          }
        } catch (err) {
          continue;
        }
      }

      result.servicesMarkedAsFeatured = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
}

async function unmarkServicesFromFeatured(req) {
  let result = {};

  try {
    if (req.user_type === "admin") {
      for (let id of req.body.service_ids) {
        try {
          const obj = await Service.findById(id);

          if (obj) {
            obj.is_featured = false;
            await obj.save();
          }
        } catch (err) {
          continue;
        }
      }

      result.servicesUnMarkedFromFeatured = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
}

async function getFeaturedServices(user_type) {
  let result = {};

  try {
    if (user_type === "end_user") {
      let featuredServices = [
        { $match: { is_featured: { $eq: true } } },
        { $limit: 5 },
        {
          $lookup: {
            from: "categories",
            let: { category_id: "$category" },

            as: "category",
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$category_id"] } } },

              {
                $project: {
                  category_name: 1,
                  _id: 1,
                  sub_category: 1,
                },
              },
            ],
          },
        },

        { $unwind: "$category" },
        { $unwind: "$category.sub_category" },
        {
          $match: {
            $expr: { $eq: ["$category.sub_category._id", "$sub_category"] },
          },
        },
      ];

      const obj = await Service.aggregate(featuredServices);

      if (obj.length) {
        result.featuredServices = obj;
        return result;
      }

      result.noServiceFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
}

async function getPopularServices(user_type) {
  let results = [];

  if (user_type == "end_user") {
    try {
      let filterPopular = [
        { $match: { service_id: { $ne: null } } },
        {
          $lookup: {
            from: "services",
            let: { service_id: "$service_id" },
            as: "services",
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$service_id"] } } },
              {
                $lookup: {
                  from: "categories",
                  let: { category_id: "$category" },

                  as: "category",
                  pipeline: [
                    { $match: { $expr: { $eq: ["$_id", "$$category_id"] } } },

                    {
                      $project: {
                        category_name: 1,
                        _id: 1,
                        sub_category: 1,
                      },
                    },
                  ],
                },
              },
            ],
          },
        },
        {
          $match: {
            "services._id": {
              $exists: true,
              $ne: null,
            },
          },
        },
        { $unwind: "$services" },
        { $unwind: "$services.category" },
        { $unwind: "$services.category.sub_category" },
        { $sort: { average_rating: -1 } },
        {
          $match: {
            $expr: {
              $eq: [
                "$services.category.sub_category._id",
                "$services.sub_category",
              ],
            },
          },
        },
        {
          $project: {
            services: 1,
          },
        },
      ];

      results.popularServices = await Ratings.aggregate(filterPopular);

      if (results) {
        return results;
      }
      result.noServiceFound = true;
      return result;
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return result;
    }
  } else {
    result.accessRestricted = true;
    return result;
  }
}

async function getRecommendedServices(req) {

  const { user_type, user_id } = req;

  const {recommendedBy,province_id,service_category_id} = req.body;

  let basicFilter = [];
  
  const result = {};


  if (user_type == "end_user") {

    const recommendedOptions = {

      service_category_id:{
        "$match":{
          category:mongoose.Types.ObjectId(service_category_id)
        }
      },
      province_id:{
        "$match":{
          "shop.province":mongoose.Types.ObjectId(province_id)
        }
      }

    }

      basicFilter = [
        {
          $lookup: {
            from: "shops",
            as: "shop",
            localField: "shop_id",
            foreignField: "_id",
          }
        },
        {
          $unwind:"$shop"

        },
        recommendedOptions[recommendedBy]
       ,
       
        {
          $lookup: {
            from: "ratings",
            as: "rating",
            localField: "_id",
            foreignField: "service_id",
          }
        },
        {
          $unwind: {
            path: "$rating",
            preserveNullAndEmptyArrays: true,
          }
        },
        {
          $addFields: {
            service_details: { $first: "$service_details" },
          }
        },
        {
          $project: {
            _id:1,
            service_image: {"$first":"$service_images"},
            tags: 1,
            shop_id: 1,
            service_title: 1,
            service_description: {
              $substr: ["$service_details.highlights", 0, 80],
            },
            category: 1,
            sub_category: 1,
            sub_sub_category: 1,
            package: { $first: "$package.cost" },
            "shop.shop_name": 1,
            "shop.province": 1,
            "rating.no_of_ratings": 1,
            "rating.average_rating": 1
          }
        },
        {
          $sort: { "rating.average_rating": -1 }
        },
        {
          "$limit":5
        }
      ]
  
    
    try{

      const recommendedServices = await Service.aggregate(basicFilter) ;
    

      if(recommendedServices){

        result.recommendedServices = recommendedServices;
        return result;

      }
      else{
        result.noServiceFound = true;
        return result;
      }

  }
  catch(err){
    console.log(err);
    result.databaseError = true;
    return result;
  }
    
     
  } else {
    result.accessRestricted = true;
    return result;
  }
}

async function getFilteredServices(req) {
  const { user_type, user_id } = req;

  if (user_type === "end_user") {
    const {
      filterBy,
      sortBy,
      multipleFilter,
      category_id,
      sub_category_id,
      province,
      title,
      ratings,
      min_price,
      max_price,
    } = req.body;

    let basicFilter = [];

    let serviceList = [];
    let result = {};

    const filterOptions = {
      "category-sub_category": {
        $match: {
          category: mongoose.Types.ObjectId(category_id),
          sub_category: mongoose.Types.ObjectId(sub_category_id),
        },
      },
      province: {
        $match: { province: mongoose.Types.ObjectId(province) },
      },
      sub_sub_category: {},
      ratings: {
        $match: {
          category: mongoose.Types.ObjectId(category_id),
          sub_category: mongoose.Types.ObjectId(sub_category_id),
          "rating.average_rating": { $exists: true },
          "rating.average_rating": { $gte: ratings },
        },
      },
      price: {
        $match: {
          "package.0.cost": { $gte: min_price, $lte: max_price },
          category: mongoose.Types.ObjectId(category_id),
          sub_category: mongoose.Types.ObjectId(sub_category_id),
        },
      },
      featured: {
        $match: { is_featured: true },
      },
      "featured-in-category-and-sub-category": {
        $match: {
          category: mongoose.Types.ObjectId(category_id),
          sub_category: mongoose.Types.ObjectId(sub_category_id),
          is_featured: true,
        },
      },
      title: {
        $match: { service_title: { $regex: title, $options: "i" } },
      },
      "title-in-category-and-sub-category": {
        $match: {
          category: mongoose.Types.ObjectId(category_id),
          sub_category: mongoose.Types.ObjectId(sub_category_id),
          service_title: { $regex: title, $options: "i" },
        },
      },
    };

    const sortOptions = {
      "most-popular": {
        $sort: { "rating.average_rating": -1 },
      },
      "most-popular-in-category-and-sub-category": {
        $sort: { "rating.average_rating": -1 },
      },
      "new-arrival": {
        $sort: { createdAt: -1 },
      },
      "price-high-to-low": {
        $sort: { package: -1 },
      },
      "price-low-to-high": {
        $sort: { package: 1 },
      },
      alphabetically: {
        $sort: {
          insensitive_service_title: 1,
        },
      },
    };

    if (multipleFilter) {

      const filters = multipleFilter.split(",");
      let preserveNullAndEmptyArrays = true;

      if(sortBy){
        if (["most-popular", "most-popular-in-category-and-sub-category"].includes(sortBy)) 
        {
          preserveNullAndEmptyArrays = false;
        }
      }

      // filter by price and ratings.
      if (filters.includes("price") && filters.includes("ratings")) {

        // Filter by price and ratings , sort by any valid option
        if(sortBy){

            basicFilter = [
                filterOptions["price"],
                {
                  $lookup: {
                    from: "shops",
                    as: "shop",
                    localField: "shop_id",
                    foreignField: "_id",
                  },
                },
                {
                  $lookup: {
                    from: "ratings",
                    as: "rating",
                    localField: "_id",
                    foreignField: "service_id",
                  },
                },
                {
                  $unwind: {
                    path: "$rating",
                    preserveNullAndEmptyArrays: preserveNullAndEmptyArrays,
                  },
                },
                {
                    $match:{
                        "rating.average_rating": { $exists: true },
                        "rating.average_rating": { $gte: ratings }
                    }

                },
                {
                  $lookup: {
                    from: "wishlists",
                    as: "wishlist",
                    localField: "_id",
                    foreignField: "service",
                  },
                },
                {
                  $addFields: {
                    wishlist: {
                      $filter: {
                        input: "$wishlist",
                        as: "var",
                        cond: {
                          $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                        },
                      },
                    },
                    "rating.rated_by": {
                      $filter: {
                        input: "$rating.rated_by",
                        as: "var",
                        cond: {
                          $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                        },
                      },
                    },
                    user_rating: { $first: "$rating.rated_by" },
                  },
                },
                {
                  $unwind: "$shop",
                },
                {
                  $project: {
                    service_images: 1,
                    tags: 1,
                    shop_id: 1,
                    service_title: 1,
                    insensitive_service_title: { $toLower: "$service_title" },
                    category: 1,
                    sub_category: 1,
                    sub_sub_category: 1,
                    package: { $first: "$package.cost" },
                    is_featured: 1,
                    createdAt: 1,
                    updatedAt: 1,
                    "shop.shop_name": 1,
                    "shop.province": 1,
                    "rating.no_of_ratings": 1,
                    "rating.average_rating": 1,
                    "rating.user_rating": "$user_rating.rating",
                    in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
                  },
                },
                sortOptions[sortBy],
              ];
        }
        // Filter by price and ratings
        else{

            basicFilter = [
                filterOptions["price"],
                {
                  $lookup: {
                    from: "shops",
                    as: "shop",
                    localField: "shop_id",
                    foreignField: "_id",
                  },
                },
                {
                  $lookup: {
                    from: "ratings",
                    as: "rating",
                    localField: "_id",
                    foreignField: "service_id",
                  },
                },
                {
                  $unwind: {
                    path: "$rating",
                    preserveNullAndEmptyArrays: preserveNullAndEmptyArrays,
                  },
                },
                {
                    $match:{
                        "rating.average_rating": { $exists: true },
                        "rating.average_rating": { $gte: ratings }
                    }

                },
                {
                  $lookup: {
                    from: "wishlists",
                    as: "wishlist",
                    localField: "_id",
                    foreignField: "service",
                  },
                },
                {
                  $addFields: {
                    wishlist: {
                      $filter: {
                        input: "$wishlist",
                        as: "var",
                        cond: {
                          $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                        },
                      },
                    },
                    "rating.rated_by": {
                      $filter: {
                        input: "$rating.rated_by",
                        as: "var",
                        cond: {
                          $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                        },
                      },
                    },
                    user_rating: { $first: "$rating.rated_by" },
                  },
                },
                {
                  $unwind: "$shop",
                },
                {
                  $project: {
                    service_images: 1,
                    tags: 1,
                    shop_id: 1,
                    service_title: 1,
                    insensitive_service_title: { $toLower: "$service_title" },
                    category: 1,
                    sub_category: 1,
                    sub_sub_category: 1,
                    package: { $first: "$package.cost" },
                    is_featured: 1,
                    createdAt: 1,
                    updatedAt: 1,
                    "shop.shop_name": 1,
                    "shop.province": 1,
                    "rating.no_of_ratings": 1,
                    "rating.average_rating": 1,
                    "rating.user_rating": "$user_rating.rating",
                    in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
                  },
                }
              ];

        }

      }
      else {
          console.log("Fix multiple filters")
      }

      try {
        serviceList = await Service.aggregate(basicFilter);
      } catch (error) {
        console.log("Error at multipleFilters : ", error);
        result.databaseError = true;
        return result;
      }



    } 
    else if (filterBy && sortBy) {

      let preserveNullAndEmptyArrays = true;

      if ( ["most-popular", "most-popular-in-category-and-sub-category"].includes(sortBy) ) 
      {
        preserveNullAndEmptyArrays = false;
      }

      if (filterBy === "ratings") {
        basicFilter = [
          {
            $lookup: {
              from: "shops",
              as: "shop",
              localField: "shop_id",
              foreignField: "_id",
            },
          },
          {
            $lookup: {
              from: "ratings",
              as: "rating",
              localField: "_id",
              foreignField: "service_id",
            },
          },
          {
            $unwind: {
              path: "$rating",
              preserveNullAndEmptyArrays: preserveNullAndEmptyArrays,
            },
          },
          filterOptions[filterBy],
          {
            $lookup: {
              from: "wishlists",
              as: "wishlist",
              localField: "_id",
              foreignField: "service",
            },
          },
          {
            $addFields: {
              wishlist: {
                $filter: {
                  input: "$wishlist",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              "rating.rated_by": {
                $filter: {
                  input: "$rating.rated_by",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              user_rating: { $first: "$rating.rated_by" },
            },
          },
          {
            $unwind: "$shop",
          },
          {
            $project: {
              service_images: 1,
              tags: 1,
              shop_id: 1,
              service_title: 1,
              insensitive_service_title: { $toLower: "$service_title" },
              category: 1,
              sub_category: 1,
              sub_sub_category: 1,
              package: { $first: "$package.cost" },
              is_featured: 1,
              createdAt: 1,
              updatedAt: 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.no_of_ratings": 1,
              "rating.average_rating": 1,
              "rating.user_rating": "$user_rating.rating",
              in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
            },
          },
          sortOptions[sortBy],
        ];
      } else {
        basicFilter = [
          filterOptions[filterBy],
          {
            $lookup: {
              from: "shops",
              as: "shop",
              localField: "shop_id",
              foreignField: "_id",
            },
          },
          {
            $lookup: {
              from: "ratings",
              as: "rating",
              localField: "_id",
              foreignField: "service_id",
            },
          },
          {
            $unwind: {
              path: "$rating",
              preserveNullAndEmptyArrays: preserveNullAndEmptyArrays,
            },
          },
          {
            $lookup: {
              from: "wishlists",
              as: "wishlist",
              localField: "_id",
              foreignField: "service",
            },
          },
          {
            $addFields: {
              wishlist: {
                $filter: {
                  input: "$wishlist",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              "rating.rated_by": {
                $filter: {
                  input: "$rating.rated_by",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              user_rating: { $first: "$rating.rated_by" },
            },
          },
          {
            $unwind: "$shop",
          },
          {
            $project: {
              service_images: 1,
              tags: 1,
              shop_id: 1,
              service_title: 1,
              insensitive_service_title: { $toLower: "$service_title" },
              category: 1,
              sub_category: 1,
              sub_sub_category: 1,
              package: { $first: "$package.cost" },
              is_featured: 1,
              createdAt: 1,
              updatedAt: 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.no_of_ratings": 1,
              "rating.average_rating": 1,
              "rating.user_rating": "$user_rating.rating",
              in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
            },
          },
          sortOptions[sortBy],
        ];
      }

      try {
        serviceList = await Service.aggregate(basicFilter);
      } catch (error) {
        console.log("Error at sortBy : ", error);
        result.databaseError = true;
        return result;
      }
    } 
    else if (filterBy) {
      basicFilter = [
        filterOptions[filterBy],
        {
          $lookup: {
            from: "shops",
            as: "shop",
            localField: "shop_id",
            foreignField: "_id",
          },
        },
        {
          $lookup: {
            from: "ratings",
            as: "rating",
            localField: "_id",
            foreignField: "service_id",
          },
        },
        {
          $unwind: {
            path: "$rating",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "wishlists",
            as: "wishlist",
            localField: "_id",
            foreignField: "service",
          },
        },
        {
          $addFields: {
            wishlist: {
              $filter: {
                input: "$wishlist",
                as: "var",
                cond: {
                  $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                },
              },
            },
            "rating.rated_by": {
              $filter: {
                input: "$rating.rated_by",
                as: "var",
                cond: {
                  $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                },
              },
            },
            user_rating: { $first: "$rating.rated_by" },
          },
        },
        {
          $unwind: "$shop",
        },
        {
          $addFields: {
            service_details: { $first: "$service_details" },
          },
        },
        {
          $project: {
            service_images: 1,
            tags: 1,
            shop_id: 1,
            service_title: 1,
            service_description: {
              $substr: ["$service_details.highlights", 0, 80],
            },
            category: 1,
            sub_category: 1,
            sub_sub_category: 1,
            package: { $first: "$package.cost" },
            is_featured: 1,
            createdAt: 1,
            updatedAt: 1,
            "shop.shop_name": 1,
            "shop.province": 1,
            "rating.no_of_ratings": 1,
            "rating.average_rating": 1,
            "rating.user_rating": "$user_rating.rating",
            in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
          },
        },
      ];

      if (filterBy === "ratings") {
        basicFilter = [
          {
            $lookup: {
              from: "shops",
              as: "shop",
              localField: "shop_id",
              foreignField: "_id",
            },
          },
          {
            $lookup: {
              from: "ratings",
              as: "rating",
              localField: "_id",
              foreignField: "service_id",
            },
          },
          {
            $unwind: {
              path: "$rating",
              preserveNullAndEmptyArrays: true,
            },
          },
          filterOptions["ratings"],
          {
            $lookup: {
              from: "wishlists",
              as: "wishlist",
              localField: "_id",
              foreignField: "service",
            },
          },
          {
            $addFields: {
              wishlist: {
                $filter: {
                  input: "$wishlist",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              "rating.rated_by": {
                $filter: {
                  input: "$rating.rated_by",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              user_rating: { $first: "$rating.rated_by" },
            },
          },
          {
            $unwind: "$shop",
          },
          {
            $addFields: {
              service_details: { $first: "$service_details" },
            },
          },
          {
            $project: {
              service_images: 1,
              tags: 1,
              shop_id: 1,
              service_title: 1,
              service_description: {
                $substr: ["$service_details.highlights", 0, 80],
              },
              category: 1,
              sub_category: 1,
              sub_sub_category: 1,
              package: { $first: "$package.cost" },
              is_featured: 1,
              createdAt: 1,
              updatedAt: 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.no_of_ratings": 1,
              "rating.average_rating": 1,
              "rating.user_rating": "$user_rating.rating",
              in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
            },
          },
        ];
      }

      try {
        serviceList = await Service.aggregate(basicFilter);
      } catch (error) {
        console.log("Error at filterBy : ", error);
        result.databaseError = true;
        return result;
      }
    } 
    else if (sortBy) {
      let preserveNullAndEmptyArrays = true;

      if (
        ["most-popular", "most-popular-in-category-and-sub-category"].includes(
          sortBy
        )
      ) {
        preserveNullAndEmptyArrays = false;
      }

      // for home page
      if (sortBy === "most-popular") {
        basicFilter = [
          { $match: {} },
          {
            $lookup: {
              from: "shops",
              as: "shop",
              localField: "shop_id",
              foreignField: "_id",
            },
          },
          {
            $lookup: {
              from: "ratings",
              as: "rating",
              localField: "_id",
              foreignField: "service_id",
            },
          },
          {
            $unwind: {
              path: "$rating",
              preserveNullAndEmptyArrays: preserveNullAndEmptyArrays,
            },
          },
          {
            $lookup: {
              from: "wishlists",
              as: "wishlist",
              localField: "_id",
              foreignField: "service",
            },
          },
          {
            $addFields: {
              wishlist: {
                $filter: {
                  input: "$wishlist",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              "rating.rated_by": {
                $filter: {
                  input: "$rating.rated_by",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              user_rating: { $first: "$rating.rated_by" },
            },
          },
          {
            $unwind: "$shop",
          },
          {
            $project: {
              service_images: 1,
              tags: 1,
              shop_id: 1,
              service_title: 1,
              insensitive_service_title: { $toLower: "$service_title" },
              category: 1,
              sub_category: 1,
              sub_sub_category: 1,
              package: { $first: "$package.cost" },
              is_featured: 1,
              createdAt: 1,
              updatedAt: 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.no_of_ratings": 1,
              "rating.average_rating": 1,
              "rating.user_rating": "$user_rating.rating",
              in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
            },
          },
          sortOptions[sortBy],
        ];
      } else {
        basicFilter = [
          filterOptions["category-sub_category"],
          {
            $lookup: {
              from: "shops",
              as: "shop",
              localField: "shop_id",
              foreignField: "_id",
            },
          },
          {
            $lookup: {
              from: "ratings",
              as: "rating",
              localField: "_id",
              foreignField: "service_id",
            },
          },
          {
            $unwind: {
              path: "$rating",
              preserveNullAndEmptyArrays: preserveNullAndEmptyArrays,
            },
          },
          {
            $lookup: {
              from: "wishlists",
              as: "wishlist",
              localField: "_id",
              foreignField: "service",
            },
          },
          {
            $addFields: {
              wishlist: {
                $filter: {
                  input: "$wishlist",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              "rating.rated_by": {
                $filter: {
                  input: "$rating.rated_by",
                  as: "var",
                  cond: {
                    $eq: ["$$var.user_id", mongoose.Types.ObjectId(user_id)],
                  },
                },
              },
              user_rating: { $first: "$rating.rated_by" },
            },
          },
          {
            $unwind: "$shop",
          },
          {
            $project: {
              service_images: 1,
              tags: 1,
              shop_id: 1,
              service_title: 1,
              insensitive_service_title: { $toLower: "$service_title" },
              category: 1,
              sub_category: 1,
              sub_sub_category: 1,
              package: { $first: "$package.cost" },
              is_featured: 1,
              createdAt: 1,
              updatedAt: 1,
              "shop.shop_name": 1,
              "shop.province": 1,
              "rating.no_of_ratings": 1,
              "rating.average_rating": 1,
              "rating.user_rating": "$user_rating.rating",
              in_wishlist: { $eq: [{ $size: "$wishlist" }, 1] },
            },
          },
          sortOptions[sortBy],
        ];
      }

      try {
        serviceList = await Service.aggregate(basicFilter);
      } catch (error) {
        console.log("Error at sortBy : ", error);
        result.databaseError = true;
        return result;
      }
    } else {
      console.log("No options were selected ! ");
    }

    if (serviceList.length) {
      result.serviceList = serviceList;
      return result;
    } else {
      result.servicesNotFound = true;
      return result;
    }
  } else {
    result.unauthorizedAccess = true;
    return result;
  }
}

async function serviceHelper(req){

  const updateStatus = await Service.updateMany({},{
    available_slot:[{
      day:'Monday',
      slot:{
        start_time:'8 AM',
        end_time:'10 AM'
      }
    },
    {
      day:'Tuesday',
      slot:{
        start_time:'8 AM',
        end_time:'10 AM'
      }
    }
  ]
  });
  console.log(updateStatus);

}

module.exports = {
  addNewService,
  getAllServices,
  getServiceByID,
  updateService,
  deleteService,
  addServiceRating,
  addServiceReview,
  changeReviewLikeStatus,
  updateServiceReview,
  deleteServiceReview,
  getServiceReviews,
  getServiceByCategory,
  getRatingForService,
  filterBy,
  markServiceAsFeatured,
  unmarkServicesFromFeatured,
  getFeaturedServices,
  getPopularServices,
  getSortedServices,
  getRecommendedServices,
  getFilteredServices,
  serviceHelper
};
