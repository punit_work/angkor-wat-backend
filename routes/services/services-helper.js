// Function for validating lat-long pair
function isLatLngValid(lat, long) {

    const regexForLatLng = new RegExp('^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$');
    return (regexForLatLng.test(lat) && regexForLatLng.test(long));    
}

// Function for checking if given date is valid
function isDateValid(date) {

    if (new Date(date) == "Invalid Date") return false;
    else return true;
}

module.exports = {
    isLatLngValid,
    isDateValid
}