const express = require('express')
const router = express.Router();
const ServiceController = require('./services-controller');
const ServiceHelper = require('./services-helper');
const authCheck = require('../../middleware/check-auth');
const Response = require('../../global/response');
const ResponseMessage = require('../../global/message');
const ResponseCode = require('../../global/code');
const uploadServiceImage = require('../../middleware/upload-service-image')
const { checkFileMimeType } = require('../../global/mimeTypeCheck');
const { doFileExist } = require('../../global/fileCheck');
const { removeMultipleFiles } = require('../../global/fileCheck')
const { body, param, validationResult, query } = require('express-validator');
const Ratings = require('../../model/Ratings');

// Path - /v1/services/add-service
// Route for adding new service 
router.post('/add-service',

    // Middleware to check if user is authenticated
    authCheck,

    uploadServiceImage.array('service_images'),

    [
        body('service_title').trim().isLength({ min: 3 }).withMessage('Service title should be minimum 3 characters long.'),
        body('service_details').trim().isLength({ min: 3 }).withMessage('Service service_details should be minimum 3 characters long.'),
        body('start_date').exists().withMessage('Start date is required'),
        body('end_date').exists().withMessage('End date is required'),
        body('package').trim().isLength({ min: 1 }).withMessage('Paramater [package] is missing or invalid'),
        body('province').trim().isLength({ min: 1 }).withMessage('Paramater [province] is missing'),
        body('city').trim().isLength({ min: 1 }).withMessage('Paramater [city] is missing'),
        body('address').trim().isLength({ min: 1 }).withMessage('Paramater [address] is missing'),
        body('latitude').trim().isLength({ min: 1 }).withMessage('Paramater [latitude] is missing'),
        body('longitude').trim().isLength({ min: 1 }).withMessage('Paramater [longitude] is missing'),
        body('pincode').trim().isLength({ min: 1 }).withMessage('Paramater [pincode] is missing'),
        // body('tags').isArray({ min: 1 }).withMessage('Parameter [tags] is missing or invalid'),
        body('category').isMongoId().withMessage('Parameter [category] is missing or invalid'),
        body('sub_category').isMongoId().withMessage('Parameter [sub_category] is missing or invalid'),
        // body('payment_methods').optional().isArray({ min: 1 }).withMessage('Parameter [payment_methods] is missing or invalid'),
        body('max_persons_allowed').isFloat({ min: 1 }).withMessage('Available quantity is required'),
        body('is_available').isBoolean().withMessage('Stock information is required')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            // remove file/files if they exists
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        // validation for checking if files are sent in paramter or not
        if (!req.files || (req.files && !req.files.length)) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.FILE_NOT_FOUND);

        // check file mimetype
        for (let file of req.files) {

            const isValid = await checkFileMimeType(file.mimetype);

            if (!isValid) {
                // remove/unlink file/files
                removeMultipleFiles(req.files)
                return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_FILE_TYPE)
            }

        }

        const isStartDateValid = ServiceHelper.isDateValid(req.body.start_date);
        const isEndDateValid = ServiceHelper.isDateValid(req.body.end_date);

        // Sending error response on invalid date
        if (!isStartDateValid || !isEndDateValid) {

            // remove images files if they exists
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_DATE);
        }

        const isLatLngInvalid = ServiceHelper.isLatLngValid(req.body.latitude, req.body.longitude);

        // Sending error response if lat-lng is invalid
        if (isLatLngInvalid) {

            // remove images files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_LAT_LNG);
        }

        const response = await ServiceController.addNewService(req);

        // Sending error response if database error
        if (response.databaseError) {

            // remove images files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        // Sending error response if no business owner found
        else if (response.noServiceProviderFound) {

            // remove images files if they exists
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.ACCESS_RETRICTED + ' for other than service provider');
        }

        // Sending error response if user enters start date as past date
        else if (response.cannotUseStartDateAsPastDate) {

            // remove images files if they exists
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.CANNOT_USE_START_DATE_AS_PAST_DATE);
        }

        // Sending error response if user enters end date as past date
        else if (response.cannotUseEndDateAsPastDate) {

            // remove images files if they exists
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.CANNOT_USE_END_DATE_AS_PAST_DATE);
        }

        // Sending error response if user enters end date before start date
        else if (response.cannotUserEndDateBeforeStartDate) {

            // remove images files if they exists
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.CANNOT_USER_END_DATE_BEFORE_START_DATE);
        }

        // Sending success response if new service added  
        else if (response.serviceAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICE_ADDED, response.serviceAdded);
    }
)

// Route for getting service by id

router.get('/get-service-by-id/:service_id',
    authCheck,
    [
        param('service_id').notEmpty().isMongoId().withMessage("Parameter [service_id] missing or invalid")
    ],
    async (req, res) => {
        const error = validationResult(req);

        if (!error.isEmpty()) {

            // remove file/files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        var response = await ServiceController.getServiceByID(req);

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        else if (response.service) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.service);


    });

// Path - /v1/services/update-service
// Route for updating service
router.post('/update-service',

    // Middleware to check if user is authenticated
    authCheck,

    uploadServiceImage.array('service_images'),

    [
        body('service_id').isMongoId().withMessage('Paramater [service_id] is missing or invalid'),
        body('service_title').optional().trim().isLength({ min: 3 }).withMessage('Service title should be minimum 3 characters long.'),
        body('service_description').optional().trim().isLength({ min: 3 }).withMessage('Service description should be minimum 3 characters long.'),
        body('start_date').optional().exists().withMessage('Start date is required'),
        body('end_date').optional().exists().withMessage('End date is required'),
        body('price').optional().isFloat({ min: 1 }).withMessage('Paramater [price] is missing or invalid'),
        body('province').optional().trim().isLength({ min: 1 }).withMessage('Paramater [province] is missing'),
        body('city').optional().trim().isLength({ min: 1 }).withMessage('Paramater [city] is missing'),
        body('address').optional().trim().isLength({ min: 1 }).withMessage('Paramater [address] is missing'),
        body('latitude').optional().trim().isLength({ min: 1 }).withMessage('Paramater [latitude] is missing'),
        body('longitude').optional().trim().isLength({ min: 1 }).withMessage('Paramater [longitude] is missing'),
        body('pincode').optional().trim().isLength({ min: 1 }).withMessage('Paramater [pincode] is missing'),
        // body('tags').optional().isArray({ min: 1 }).withMessage('Parameter [tags] is missing or invalid'),
        body('category').isMongoId().withMessage('Parameter [category] is missing or invalid'),
        body('sub_category').isMongoId().withMessage('Parameter [sub_category] is missing or invalid'),
        // body('payment_methods').optional().isArray({ min: 1 }).withMessage('Parameter [payment_methods] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            // remove file/files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (req.body.start_date) {

            const isStartDateValid = ServiceHelper.isDateValid(req.body.start_date);

            // Sending error response on invalid date
            if (!isStartDateValid) {
                // remove file/files if they exist
                removeMultipleFiles(req.files)
                return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_DATE);
            }
        }

        if (req.body.end_date) {

            const isEndDateValid = ServiceHelper.isDateValid(req.body.end_date);

            // Sending error response on invalid date
            if (!isEndDateValid) {
                // remove file/files if they exist
                removeMultipleFiles(req.files)
                return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_DATE);
            }
        }

        if (req.body.latitude && req.body.longitude) {

            const isLatLngInvalid = ServiceHelper.isLatLngValid(req.body.latitude, req.body.longitude);

            // Sending error response if lat-lng is invalid
            if (isLatLngInvalid) {
                // remove file/files if they exist
                removeMultipleFiles(req.files)
                return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_LAT_LNG);
            }
        }

        // check if service_images is empty 
        //if(!req.body.service_images) return Response.error( res , ResponseCode.UNPROCESSABLE_ENTITY , 'Service image parameter cannot be empty.')

        // check if files exist and are valid or not
        if (req.files || (req.files && req.files.length > 0)) {

            // check file mimetype
            for (let file of req.files) {

                const isValid = await checkFileMimeType(file.mimetype);

                if (!isValid) {
                    // remove/unlink file/files
                    removeMultipleFiles(req.files)
                    return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_FILE_TYPE)
                }
            }
        }


        const response = await ServiceController.updateService(req.user_id, req);

        // Sending error response if database error
        if (response.databaseError) {
            // remove file/files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        // Sending error response if no service found
        else if (response.noServiceFound) {
            // remove file/files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);
        }

        // Sending error response if user enters start date as past date
        else if (response.cannotUseStartDateAsPastDate) {
            // remove file/files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.CANNOT_USE_START_DATE_AS_PAST_DATE);
        }

        // Sending error response if user enters end date as past date
        else if (response.cannotUseEndDateAsPastDate) {
            // remove file/files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.CANNOT_USE_END_DATE_AS_PAST_DATE);
        }

        // Sending error response if user enters end date before start date
        else if (response.cannotUserEndDateBeforeStartDate) {
            // remove file/files if they exist
            removeMultipleFiles(req.files)
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.CANNOT_USER_END_DATE_BEFORE_START_DATE);
        }

        // Sending success response if service updated 
        else if (response.serviceUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICE_UPDATED_SUCCESSFULLY, response.serviceUpdated);
    }
)

// Path - /v1/services/get-all-services
// Route for fetching all services
router.get('/get-all-services',

    // Middleware to check if user is authenticated
    authCheck,

    async (req, res) => {

        const response = await ServiceController.getAllServices(req.user_id, req.user_type);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no service found
        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        // Sending success response if services fetched 
        else if (response.services) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.services);
    }
)

// Path - /v1/services/delete-service
// Route for deleting a service
router.delete('/delete-service/:service_id',

    // Middleware to check if user is authenticated
    authCheck,

    [
        param('service_id').isMongoId().withMessage('Parameter [service_id] is missing or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.deleteService(req.user_id, req.params.service_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no service found
        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        // Sending success response if service deleted 
        else if (response.serviceDeleted) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICE_DELETED_SUCCESSFULLY);
    }
)

// Path - /v1/services/addRating
// Route for adding service rating
router.post('/addRating',

    // Middleware to check if user is authenticated
    authCheck,

    [
        body('service_id').isMongoId().withMessage('Parameter [service_id] is missing or invalid'),
        body('rating_number').isFloat({ min: 1, max: 5 }).withMessage('Parameter [rating_number] must be between 1 to 5')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.addServiceRating(req);

        // check if user is unauthorized 
        if (response.unauthorizedAccess) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED)

        // check if database error
        else if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

        // check if no service is found
        else if (response.serviceNotFound) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.NO_SERVICE_FOUND);

        // check if ratings have already been added
        else if (response.ratingAlreadyAdded) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.RATING_ALREADY_ADDED)

        // check if
        else if (response.ratingAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RATING_ADDED)

        else if (response.ratingUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RATING_UPDATED)
    }
)

// Path - /v1/services/addReview
// Route for adding service review 
router.post('/addReview',

    authCheck,

    [
        body('service_id').isMongoId().withMessage('Parameter [service_id] is missing or invalid'),
        body('comment').trim().isLength({ min: 3 }).withMessage('Parameter [comment] is missing or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.addServiceReview(req);


        // check if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        // check if a valid user is trying to access or not
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other then end users');

        // check if any review is found or not
        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        // sending success response if review is added to the database
        else if (response.review) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.REVIEW_ADDED_SUCCESSFULLY, response.review);
    }
)

// Path - /v1/services/update-review
// Route for updating service review
router.post('/update-review',

    authCheck,

    [
        body('review_id').isMongoId().withMessage('Parameter [review_id] is missing or invalid'),
        body('comment').trim().isLength({ min: 1 }).withMessage('Parameter [comment] is missing or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.updateServiceReview(req.user_type, req.user_id, req.body);

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noReviewFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_REVIEW_FOUND);

        else if (response.review) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.REVIEW_UPDATED_SUCCESSFULLY, response.review);
    }
);

// Path - /v1/services/delete-review/:review_id
// Route for deleting service review
router.delete('/delete-review/:review_id',

    authCheck,

    [
        param('review_id').isMongoId().withMessage('Parameter [review_id] is missing or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.deleteServiceReview(req.user_type, req.user_id, req.params);

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noReviewFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_REVIEW_FOUND);

        else if (response.reviewDeleted) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.REVIEW_DELETED_SUCCESSFULLY);
    }
);

// Path - /v1/services/get-all-reviews/:service_id
// Route for fetching all service reviews
router.get('/get-all-reviews/:service_id',

    authCheck,

    [
        param('service_id').isMongoId().withMessage('Parameter [service_id] is missing or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.getServiceReviews(req.user_type, req.user_id, req.params.service_id);

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noReviewsFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_REVIEWS_FOUND);

        else if (response.reviews) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.REVIEWS_FETCHED_SUCCESSFULLY, response.reviews);
    }
);


// Path - /v1/services/change-review-likes/:service_id
// Route for changing service review likes status
router.get('/change-review-likes/:review_id',

    authCheck,

    [
        param('review_id').isMongoId().withMessage('Parameter [review_id] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.changeReviewLikeStatus(req);

        // check if database error exists
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        // check if unauthorised user is trying to access 
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        // check if no service is found
        else if (response.noReviewFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_REVIEW_FOUND);

        // sending review unliked response
        else if (response.reviewUnliked) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.REVIEW_UNLIKED);

        // sending review liked response
        else if (response.reviewLiked) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.REVIEW_LIKED);
    }
);

// Path - /v1/services/get-service-by-category
// Route for getting services by category and sub category
router.post('/get-services-by-category',

    // Middleware to check if user is authenticated
    authCheck,

    [
        body('category_id').exists().withMessage('Parameter [category_id] is missing or invalid'),
        body('subcategory_id').exists().withMessage('Parameter [subcategory_id] is missing or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.getServiceByCategory(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // check if unauthorised user is trying to access 
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        // Sending error response if no service found
        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        // Sending success response if services fetched 
        else if (response.services) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.services);
    }
);

// Path - /v1/services/get-ratings-for-service/:service_id
// Route for getting ratings for service
router.get('/get-ratings-for-service/:service_id',

    // Middleware to check if user is authenticated
    authCheck,

    [
        param('service_id').exists().withMessage('Parameter [service_id] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.getRatingForService(req.user_id, req.user_type, req.params.service_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // check if unauthorised user is trying to access 
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        // Sending error response if no ratings found
        else if (response.noRatingsFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_RATINGS_FOUND);

        // Sending success response if ratings fetched 
        else if (response.rating) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RATINGS_FETCHED_SUCCESSFULLY, response.rating);
    }
)


// Path - /v1/services/getFilteredServices
// Route for fetching filtered services by tag, ratings and location
// router.post('/getFilteredServices',

//     // check if authenticated or not
//     authCheck,

//     [
//         body('tag').optional().notEmpty().withMessage('Parameter tag is invalid'),
//         body('city').optional().notEmpty().withMessage('Parameter city is invalid'),
//         body('rating').optional().notEmpty().withMessage('Parameter rating is invalid'),
//         body('title').optional().notEmpty().withMessage('Parameter title is invalid'),
//         body('min_price').optional().isNumeric().withMessage('Parameter min_price is invalid'),
//         body('max_price').optional().isNumeric().withMessage('Parameter max_price is invalid')
//     ],

//     async (req, res) => {

//         const errors = validationResult(req);
//         // check if user has sent category field to filter
//         if (!errors.isEmpty()) {

//             return Response.error(res, ResponseCode.BAD_REQUEST, errors.array().map((error) => ({
//                 field: error.param,
//                 errorMessage: error.msg
//             })));
//         }

//         // get all the services according to the filter applied
//         const response = await ServiceController.filterBy(req);

//         // check if invalid user is trying to access 
//         if (response.invalidUser) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.INVALID_USER_TYPE)

//         // check for invalid filter
//         if (response.invalidFilter) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_FILTER)

//         // check if database Error
//         else if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

//         // check if any products are found using applied category
//         else if (response.noServicesFound) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.NO_SERVICE_FOUND)

//         // send productList in response
//         else if (response.services) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PRODUCTS_FETCHED_SUCCESSFULLY, response.services);

//     }
// );

router.post('/get-sorted-services', authCheck,
    [
        body('high_to_low').isBoolean().optional().withMessage('Parameter high_to_low invalid.'),
        body('low_to_high').isBoolean().optional().withMessage('Parameter low_to_high invalid.'),
        body('category_id').optional().isMongoId().withMessage('Parameter category_id is invalid'),
        body('subcategory_id').optional().isMongoId().withMessage('Parameter subcategory_id is invalid')
    ],
    async (req, res) => {
        const errors = validationResult(req);
        // check if user has sent category field to filter
        if (!errors.isEmpty()) {

            return Response.error(res, ResponseCode.BAD_REQUEST, errors.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.getSortedServices(req);

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        else if (response.services) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.services);

    });

router.post('/get-recommended-services',

    authCheck,

    [
        body('recommendedBy').optional().isString().withMessage("please provide recommendedBy options [ 'province_id','service_category_id' ]"),
        body('province_id').optional().isMongoId().withMessage("Please provide valid province id"),
        body('service_category_id').optional().isMongoId().withMessage("Please provide valid service_category_id"),
    ],

    async (req, res) => {

        // Checking if req.body has any error

        const errors = validationResult(req);
        // check if user has sent category field to filter
        if (!errors.isEmpty()) {

            return Response.error(res, ResponseCode.BAD_REQUEST, errors.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const {recommendedBy,province_id,service_category_id} = req.body;

        if(!['province_id', 'service_category_id'].includes(recommendedBy)){

          return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide valid option for recommendation from [ 'province_id','service_category_id' ]");

        }
        else if(recommendedBy === 'province_id' && !province_id){
          return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide province_id");
        }
        else if(recommendedBy === 'service_category_id' && !service_category_id){
          return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide service_category_id");
        }


        const response = await ServiceController.getRecommendedServices(req);

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        else if (response.recommendedServices) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.recommendedServices);

    }
);


router.get('/get-featured-services',

    authCheck,

    async (req, res) => {

        const response = await ServiceController.getFeaturedServices(req.user_type)

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        else if (response.featuredServices) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.featuredServices);

    }
);

router.get("/get-popular-services",
    authCheck,
    async (req, res) => {
        const response = await ServiceController.getPopularServices(req.user_type);

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        else if (response.popularServices) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.popularServices);
    }
);

router.post('/get-recommended-ratings',
    authCheck,
    [
        body('average_rating').isNumeric().notEmpty().withMessage('Parameter average_rating is missing or invalid'),
        body('province').isString().notEmpty().withMessage('Parameter province is missing or invalid'),
    ],
    async (req, res) => {
        const errors = validationResult(req);
        // check if user has sent category field to filter
        if (!errors.isEmpty()) {

            return Response.error(res, ResponseCode.BAD_REQUEST, errors.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.getRecommendedServices(req)

        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

        else if (response.noServiceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SERVICE_FOUND);

        else if (response.popularServices) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_FETCHED_SUCCESSFULLY, response.popularServices);
    }
);

router.post(
    "/get-filtered-services",
    authCheck,
    [
      body("filterBy")
        .optional()
        .trim()
        .isLength({ min: 1 })
        .withMessage("Please give valid filter option"),
      body("sortBy")
        .optional()
        .trim()
        .isLength({ min: 1 })
        .withMessage("Please give valid sort option"),
      body("shop_id")
        .optional()
        .isMongoId()
        .withMessage("Parameter shop_id is invalid"),
      body("category_id")
        .optional()
        .isMongoId()
        .withMessage("Parameter category is invalid"),
      body("sub_category_id")
        .optional()
        .isMongoId()
        .withMessage("Parameter sub_category is invalid"),
      body("ratings")
        .optional()
        .isFloat({ min: 1, max: 5 })
        .withMessage("Ratings must be number between 1 - 5 & number"),
      body("max_price")
        .optional()
        .isFloat({ max: 1000 })
        .withMessage("Parameter max_price is invalid"),
      body("min_price")
        .optional()
        .isFloat({min:0})
        .withMessage("Parameter min_price is invalid"),
      body("title").optional().not().isEmpty().withMessage("Title is required"),
      body("province")
        .optional()
        .isMongoId()
        .withMessage("Parameter province is invalid"),
      body("multipleFilter")
        .optional()
        .trim()
        .isLength({ min: 1 })
        .withMessage("Parameter multipleFilter is required"),
      body("sub_sub_category")
        .optional()
        .trim()
        .isLength({ min: 1 })
        .withMessage("Parameter sub_sub_category is required"),
    ],
    async (req, res) => {
      const errors = validationResult(req);
      // check if user has sent category field to filter
      if (!errors.isEmpty()) {
        return Response.error(
          res,
          ResponseCode.BAD_REQUEST,
          errors.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg,
          }))
        );
      }
  
      if (!(req.body.filterBy || req.body.sortBy))
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide sort or filter options"
        );
  
      // FILTER
  
      if (req.body.filterBy) {
        if (
          ![
            "category",
            "category-sub_category",
            "ratings",
            "price",
            "sub_sub_category",
            "featured",
            "featured-in-category-and-sub-category",
            "title-in-category-and-sub-category",
            "province",
            "title",
            "multipleFilter",
          ].includes(req.body.filterBy)
        )
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide valid filter options"
          );
  
        const filterBy = req.body.filterBy;
  
        if ( ["category-sub_category","featured-in-category-and-sub-category","title-in-category-and-sub-category","price","ratings"].includes(filterBy)) {
          if (!(req.body.category_id && req.body.sub_category_id))
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide category and sub_category id for filter by "+filterBy
            );
        }
  
        if (filterBy === "ratings") {

          if (!req.body.ratings)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide ratings in 1-5"
            );
        }
  
        if (filterBy === "price") {
          if (!(req.body.min_price && req.body.max_price))
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide min_price and max_price"
            );
        }
  
        if (filterBy === "title") {
          if (!req.body.title)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide title field"
            );
        }
  
        if (filterBy === "province") {
          if (!req.body.province)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide province"
            );
        }
  
        if (filterBy === "sub_sub_category") {
          if (!req.body.sub_sub_category)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide sub_sub_category field is comma separated values"
            );
        }

  
        if (filterBy === "multipleFilter") {
          if (!req.body.multipleFilter)
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide multiplefilter"
            );
  
          const filters = req.body.multipleFilter.split(",");
  
          if (
            !filters.some((value) =>
              ["sub_sub_category", "ratings", "price"].includes(value)
            )
          )
            return Response.error(
              res,
              ResponseCode.BAD_REQUEST,
              "Please provide valid filter options for multiple filters from this list [ 'sub_sub_category','ratings','price' ] "
            );
  
          if (filters.includes("sub_sub_category")) {
            if (!req.body.sub_sub_category)
              return Response.error(
                res,
                ResponseCode.BAD_REQUEST,
                "Please provide sub_sub_category field"
              );
          }
  
          if (filters.includes("price")) {
            if (!(req.body.category_id && req.body.sub_category_id))
              return Response.error(
                res,
                ResponseCode.BAD_REQUEST,
                "Please provide category and sub_category id"
              );
            if (!(req.body.min_price && req.body.max_price))
              return Response.error(
                res,
                ResponseCode.BAD_REQUEST,
                "Please provide min_price and max_price"
              );
          }
  
          if (filters.includes("ratings")) {
            if (!req.body.category_id && !req.body.sub_category_id)
              return Response.error(
                res,
                ResponseCode.BAD_REQUEST,
                "Please provide category and sub_category id"
              );
            if (!req.body.ratings)
              return Response.error(
                res,
                ResponseCode.BAD_REQUEST,
                "Please provide ratings in 1-5"
              );
          }
        }
      }
  
      // SORT
  
      if (req.body.sortBy) {
        if (
          ![
            "price-high-to-low",
            "price-low-to-high",
            "most-popular",
            "most-popular-in-category-and-sub-category",
            "new-arrival",
            "alphabetically",
          ].includes(req.body.sortBy)
        )
          return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            "Please provide valid sort options from [ price-high-to-low ,price-low-to-high, most-popular, most-popular-in-category-and-sub-category, new-arrivals, alphabetically ]"
          );
        if(req.body.sortBy !== "most-popular"){
            if (!(req.body.category_id && req.body.sub_category_id))
            return Response.error(
                res,
                ResponseCode.BAD_REQUEST,
                "Please provide category and sub_category id"
            );
          }
      }
  
      // get all the services according to the filter & sort applied
      const response = await ServiceController.getFilteredServices(req);
  
      // check if invalid user is trying to access
      if (response.invalidUser)
        return Response.error(
          res,
          ResponseCode.UNAUTHORIZED_ACCESS,
          ResponseMessage.INVALID_USER_TYPE
        );
      // check if min price is a valid numeric or not
      else if (response.minPriceIsNaN)
        return Response.error(
          res,
          ResponseCode.BAD_REQUEST,
          ResponseMessage.INVALID_MIN_PRICE
        );
      // check if max price is a valid numeric or not
      else if (response.maxPriceIsNaN)
        return Response.error(
          res,
          ResponseCode.BAD_REQUEST,
          ResponseMessage.INVALID_MAX_PRICE
        );
      // check if category is found or not
      else if (response.categoryError)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          ResponseMessage.NO_CATEGORY_FOUND
        );
      // check if database Error
      else if (response.databaseError)
        return Response.error(
          res,
          ResponseCode.DATABASE_ERROR,
          ResponseMessage.ERROR_DATABASE
        );
      // check if any products are found using applied category
      else if (response.servicesNotFound)
        return Response.error(
          res,
          ResponseCode.NOT_FOUND,
          ResponseMessage.NO_SERVICE_FOUND
        );
      // check if category is found or not
      else if (response.inValidFilterSelection)
        return Response.error(
          res,
          ResponseCode.UNPROCESSABLE_ENTITY,
          "Please provide valid filter selection"
        );
      // send productList in response
      else if (response.serviceList)
        return Response.success(
          res,
          ResponseCode.SUCCESS,
          ResponseMessage.PRODUCTS_FETCHED_SUCCESSFULLY,
          response.serviceList
        );
    }
  );


// router.get('/service-helper',
// async (req, res) => {
 
//     var response = await ServiceController.serviceHelper(req);

// });





module.exports = router;
