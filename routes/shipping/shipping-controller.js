//Models
const shipping = require('../../model/Shipping');
const cart = require('../../model/Cart');
const experienceShipping = require('../../model/ExperienceShipping');
const hotel = require('../../model/Hotel');
const mongoose = require('mongoose'); 
const BillingController = require("../billing-details/billing-details-controller");

// action model data => add - Shipping - Details

//Add products to wishlist 
exports.addShippingDetails = async (req) => {

    let result = {}

    if (req.user_type === "end_user") {

        try {

            const shippingData = { ...req.body };
            shippingData.user_id = req.user_id;

            const shippingDetails = await shipping.findOne({user_id:req.user_id});

            if(shippingDetails){

                shippingDetails.delivery_type = shippingData.delivery_type
                shippingDetails.billing_details = shippingData.billing_details

                if(shippingData.hotel_id){
                    shippingDetails.hotel_id = shippingData.hotel_id
                }

                result.shipping = await shippingDetails.save()

            }
            else{

                result.shipping = await new shipping(shippingData).save();

            }

            return result

        }
        catch (err) {
            console.log(err);
            result.databaseError = true;
            return result;
        }

    }
    else {

        result.unAuthorised = true;
        return result;
    }

}

//Get all products from wishlist
exports.getShippingDetails = async (req) => {

    let result = {};

    const {user_id,user_type} = req;

    if (user_type === "end_user") {

        try {

            // need three filters according to what user has seleted

            const shippingDetails = await shipping.findOne({user_id:user_id});

            if(!shippingDetails){

                result.noShippingDetailsFound = true;
                return result;
            }
            
            const delivery_type = shippingDetails.delivery_type;

            let basicFilter = [];

            // hotel
            if(delivery_type === 'hotel'){

                basicFilter = [
                    {
                        "$match": { 
                             user_id:mongoose.Types.ObjectId(req.user_id)
                        }
                    },
                    { "$lookup": {
                        "from": "billingdetails",
                        "localField": "billing_details",
                        "foreignField": "_id",
                        "as": "billing_details"
                      }},
                      {
                          "$unwind":{
                              "path":"$billing_details",
                              "preserveNullAndEmptyArrays":true
                          }
                      },
                      { "$lookup": {
                        "from": "hotels",
                        "localField": "hotel_id",
                        "foreignField": "_id",
                        "as": "hotel_details"
                      }},
                      {
                          "$unwind":{
                              "path":"$hotel_details",
                              "preserveNullAndEmptyArrays":true
                          }
                      },
                      {
                        "$lookup": {
                          "from": "provinces",
                          "as": "hotel_details.province",
                          "localField": "hotel_details.province",
                          "foreignField": "_id"
                        }
                    },
                    {
                        "$unwind":{
                          "path":"$hotel_details.province",
                          "preserveNullAndEmptyArrays":true
                        }
                      },
                      {
                        "$addFields":{
                          "hotel_details.district": {"$filter": {
                            "input": '$hotel_details.province.district',
                           "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$hotel_details.district']}
                        }},
                        }
                
                      },
                      {
                        "$unwind":{
                          "path":"$hotel_details.district",
                          "preserveNullAndEmptyArrays":true
                        }
                      },
                      {
                        "$addFields":{
                          "hotel_details.commune": {"$filter": {
                            "input": '$hotel_details.district.commune',
                           "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$hotel_details.commune']}
                        }},
                        }
                
                      },
                      {
                        "$unwind":{
                          "path":"$hotel_details.commune",
                          "preserveNullAndEmptyArrays":true
                        }
                      },
                    {
                        "$project":{
                          "delivery_type":1,
                          "billing_details.full_name":1,
                          "billing_details.email":1,
                          "billing_details.phone_number":1,
                          "billing_details.user_id":1,
                          "createdAt":1,
                          "updatedAt":1,
                          "hotel_details.address":1,
                          "hotel_details.province._id":1,
                          "hotel_details.province.province_name":"$hotel_details.province.province",
                          "hotel_details.province.country":1,
                          "hotel_details.district._id":1,
                          "hotel_details.district.district_name":1,
                          "hotel_details.commune":1,
                          "hotel_details.postal_code":1
                        }
                      }
                    
                 ]

            }

            // user - address
            else if(delivery_type === 'user-address'){

                 basicFilter = [
                {
                    "$match": { 
                         user_id:mongoose.Types.ObjectId(req.user_id)
                    }
                },
                { "$lookup": {
                    "from": "billingdetails",
                    "localField": "billing_details",
                    "foreignField": "_id",
                    "as": "billing_details"
                  }},
                  {
                      "$unwind":{
                          "path":"$billing_details",
                          "preserveNullAndEmptyArrays":true
                      }
                  },
                  {
                    "$lookup": {
                      "from": "provinces",
                      "as": "billing_details.province",
                      "localField": "billing_details.province",
                      "foreignField": "_id"
                    }
                },
                {
                    "$unwind":{
                      "path":"$billing_details.province",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                  {
                    "$addFields":{
                      "billing_details.district": {"$filter": {
                        "input": '$billing_details.province.district',
                       "as": 'var',
                        "cond": {"$eq": ['$$var._id', '$billing_details.district']}
                    }},
                    }
            
                  },
                  {
                    "$unwind":{
                      "path":"$billing_details.district",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                  {
                    "$addFields":{
                      "billing_details.commune": {"$filter": {
                        "input": '$billing_details.district.commune',
                       "as": 'var',
                        "cond": {"$eq": ['$$var._id', '$billing_details.commune']}
                    }},
                    }
            
                  },
                  {
                    "$unwind":{
                      "path":"$billing_details.commune",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                {
                    "$project":{
                      "delivery_type":1,
                      "billing_details.full_name":1,
                      "billing_details.email":1,
                      "billing_details.phone_number":1,
                      "billing_details.user_id":1,
                      "createdAt":1,
                      "updatedAt":1,
                      "billing_details.province._id":1,
                      "billing_details.province.province_name":"$billing_details.province.province",
                      "billing_details.province.country":1,
                      "billing_details.district._id":1,
                      "billing_details.district.district_name":1,
                      "billing_details.commune":1,
                      "billing_details.postal_code":1
                    }
                  }
                
             ]

            }

            // self - collection 
            else if(delivery_type === 'self-collection'){
                
                const selfPickupFilter = [

                    {
                        "$match":{
                            user_id: mongoose.Types.ObjectId(user_id)
                        }
                    
                    
                    },
                    {
                        "$lookup": {
                        "from": "shops",
                        "as": "shop",
                        "localField": "shop",
                        "foreignField": "_id"
                        }
                    },
                    {
                        "$unwind":"$shop"
                    },
                    {
                        "$lookup": {
                        "from": "provinces",
                        "as": "shop.province",
                        "localField": "shop.province",
                        "foreignField": "_id"
                        }
                    },
                    {
                        "$unwind":{
                        "path":"$shop.province",
                        "preserveNullAndEmptyArrays":true
                        }
                    },
                    {
                        "$addFields":{
                        "shop.district": {"$filter": {
                            "input": '$shop.province.district',
                        "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$shop.district']}
                        }},
                        }
                
                    },
                    {
                        "$unwind":{
                        "path":"$shop.district",
                        "preserveNullAndEmptyArrays":true
                        }
                    },
                    {
                        "$addFields":{
                        "shop.commune": {"$filter": {
                            "input": '$shop.district.commune',
                        "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$shop.commune']}
                        }},
                        }
                
                    },
                    {
                        "$unwind":{
                        "path":"$shop.commune",
                        "preserveNullAndEmptyArrays":true
                        }
                    },
                    {
                        "$lookup": {
                        "from": "products",
                        "as": "product",
                        "localField": "product",
                        "foreignField": "_id"
                        }
                    },
                    {"$unwind":"$product"},
                    {
                        "$project":{
                            "product.variants": {"$filter": {
                                "input": '$product.variants',
                            "as": 'var',
                                "cond": {"$eq": ['$$var._id', '$variant']}
                            }},
                            "product._id":1,
                            "product.product_images":1,
                            "product.quantity":"$quantity",
                            "product.product_name":1,
                            "product.category":1,
                            "product.sub_category":1,
                            "product.createdAt":1,
                            "product.updatedAt":1,
                            "shop.shop_name":1,
                            "shop.district":1,
                            "shop.commune":1,
                            "shop.address":1,
                            "shop.postal_code":1,
                            "shop.province":1,
                            "shop.business_owner_id":1,
                            "shop.is_pickup_point":1,
                            "shop._id":1
                        }
                    },
                    {
                        "$unwind":"$product.variants"
                    },
                    {
                        "$group":{
                            "_id":"$shop._id",
                            "cart_item_id":{"$first":"$_id"},
                            "products":{"$push":"$product"},
                            "shop":{"$first":"$shop"}
                        }
                    },
                    {
                        "$lookup": {
                        "from": "system_users",
                        "as": "business_owner",
                        "localField": "shop.business_owner_id",
                        "foreignField": "_id"
                        }
                    },
                    {
                        "$unwind":"$business_owner"
                    },
                    {
                        "$lookup": {
                        "from": "provinces",
                        "as": "business_owner.province",
                        "localField": "business_owner.province",
                        "foreignField": "_id"
                        }
                    },
                    {
                        "$unwind":{
                        "path":"$business_owner.province",
                        "preserveNullAndEmptyArrays":true
                        }
                    },
                    {
                        "$addFields":{
                        "business_owner.district": {"$filter": {
                            "input": '$business_owner.province.district',
                        "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$business_owner.district']}
                        }},
                        }
                
                    },
                    {
                        "$unwind":{
                        "path":"$business_owner.district",
                        "preserveNullAndEmptyArrays":true
                        }
                    },
                    {
                        "$addFields":{
                        "business_owner.commune": {"$filter": {
                            "input": '$business_owner.district.commune',
                        "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$business_owner.commune']}
                        }},
                        }
                
                    },
                    {
                        "$unwind":{
                        "path":"$business_owner.commune",
                        "preserveNullAndEmptyArrays":true
                        }
                    },
                    
                    {
                        "$project":{
                            "products":1,
                            "shop.district":"$shop.district.district_name",
                            "shop.commune":"$shop.commune.commune_name",
                            "shop.address":1,
                            "shop.is_pickup_point":1,
                            "shop.province":"$shop.province.province",
                            "business_owner.postal_code":1,
                            "business_owner.fname":1,
                            "business_owner.lname":1,
                            "business_owner.province":"$business_owner.province.province",
                            "business_owner.district":"$business_owner.district.district_name",
                            "business_owner.commune":"$business_owner.commune.commune_name",
                            "business_owner.postal_code":"$business_owner.commune.postal_code",
                            "business_owner.fname":1,
                            "business_owner.lname":1,
                            "business_owner.address":1
                        }
                    },
                    

                
        
                ]
    
                try {

                    const SelfPickupDetails = await cart.aggregate(selfPickupFilter);

                        if(SelfPickupDetails){

                            basicFilter = [
                            {
                                "$match": { 
                                    user_id:mongoose.Types.ObjectId(req.user_id)
                                }
                            },
                            { "$lookup": {
                                "from": "billingdetails",
                                "localField": "billing_details",
                                "foreignField": "_id",
                                "as": "billing_details"
                            }},
                            {
                                "$unwind":{
                                    "path":"$billing_details",
                                    "preserveNullAndEmptyArrays":true
                                }
                            },
                            {
                                "$addFields":{
                                    "self_collection":SelfPickupDetails
                                }
                            },
                            {
                                "$project":{
                                "delivery_type":1,
                                "billing_details.full_name":1,
                                "billing_details.email":1,
                                "billing_details.phone_number":1,
                                "billing_details.user_id":1,
                                "self_collection":1,
                                "createdAt":1,
                                "updatedAt":1
                                }
                            }
                            
                            ]
        
                        }
        
                        else{
                            
                            result.noSelfPickupDetails = true;
                            return true;
                        } 
    
    
                
                } catch (error) {

                    console.log("Error at self pickup details : ",error);
                    result.databaseError = true;
                    return result;
                    
                }
    
           

            }


            const shippingData = await shipping.aggregate(basicFilter);


            if (shippingData.length) {
                result.shipping = shippingData;
                return result;

            }
            else {

                result.noShippingDetailsFound = true;
                return result;

            }

        }
        catch (err) {
            console.log(err);
            result.databaseError = true;
            return result;

        }

    }
    else {
        result.unAuthorised = true;
        return result;
    }

}

///FOR EXPERIENCE SHIPPING

exports.addExperienceShipping = async (req) => {

    let result = {}

    if (req.user_type === "end_user") {

        try {

            const shippingData = { ...req.body };
            shippingData.user_id = req.user_id;

            //Check if hotel id is provided or not     
            if (shippingData.hotel_id) {

                //fetch address and location from hotel id
                const hotelData = await hotel.findById({ _id: shippingData.hotel_id });

                if (hotelData) {
                    shippingData.location = hotelData.location;
                    shippingData.address = hotelData.address;

                }

            }

            result.shipping = await new experienceShipping(shippingData).save();

            return result

        }
        catch (err) {
            console.log(err);
            result.databaseError = true;
            return result;
        }

    }
    else {

        result.unAuthorised = true;
        return result;
    }

}

//Get all products from wishlist
exports.getExperienceShipping = async (req) => {

    let result = {};

    if (req.user_type === "end_user") {

        try {

            const shippingData = await experienceShipping.find({ user_id: req.user_id });

            if (shippingData.length) {
                result.shipping = shippingData;
                return result;

            }
            else {

            }

        }
        catch (err) {
            result.databaseError = true;
            return result;

        }

    }
    else {
        result.unAuthorised = true;
        return result;
    }

}


//Address related api 

exports.getSelfPickupDetails = async (req) =>{

    const {user_id,user_type} = req;

    let result = {};


    if(user_type === "end_user"){


        try {

            const basicFilter = [

                {
                    "$match":{
                        user_id: mongoose.Types.ObjectId(user_id)
                    }
                   
                   
                },
                {
                    "$lookup": {
                      "from": "shops",
                      "as": "shop",
                      "localField": "shop",
                      "foreignField": "_id"
                    }
                },
                {
                    "$unwind":"$shop"
                },
                {
                    "$lookup": {
                      "from": "provinces",
                      "as": "shop.province",
                      "localField": "shop.province",
                      "foreignField": "_id"
                    }
                },
                {
                    "$unwind":{
                      "path":"$shop.province",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                  {
                    "$addFields":{
                      "shop.district": {"$filter": {
                        "input": '$shop.province.district',
                       "as": 'var',
                        "cond": {"$eq": ['$$var._id', '$shop.district']}
                    }},
                    }
            
                  },
                  {
                    "$unwind":{
                      "path":"$shop.district",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                  {
                    "$addFields":{
                      "shop.commune": {"$filter": {
                        "input": '$shop.district.commune',
                       "as": 'var',
                        "cond": {"$eq": ['$$var._id', '$shop.commune']}
                    }},
                    }
            
                  },
                  {
                    "$unwind":{
                      "path":"$shop.commune",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                {
                    "$lookup": {
                      "from": "products",
                      "as": "product",
                      "localField": "product",
                      "foreignField": "_id"
                    }
                },
                {"$unwind":"$product"},
                {
                    "$project":{
                        "product.variants": {"$filter": {
                            "input": '$product.variants',
                           "as": 'var',
                            "cond": {"$eq": ['$$var._id', '$variant']}
                        }},
                        "product._id":1,
                        "product.product_images":1,
                        "product.quantity":"$quantity",
                        "product.product_name":1,
                        "product.category":1,
                        "product.sub_category":1,
                        "product.createdAt":1,
                        "product.updatedAt":1,
                        "shop.shop_name":1,
                        "shop.district":1,
                        "shop.commune":1,
                        "shop.address":1,
                        "shop.postal_code":1,
                        "shop.province":1,
                        "shop.business_owner_id":1,
                        "shop.is_pickup_point":1,
                        "shop._id":1
                    }
                },
                {
                    "$unwind":"$product.variants"
                },
                {
                    "$group":{
                        "_id":"$shop._id",
                        "cart_item_id":{"$first":"$_id"},
                        "products":{"$push":"$product"},
                        "shop":{"$first":"$shop"}
                    }
                },
                {
                    "$lookup": {
                      "from": "system_users",
                      "as": "business_owner",
                      "localField": "shop.business_owner_id",
                      "foreignField": "_id"
                    }
                },
                {
                    "$unwind":"$business_owner"
                },
                {
                    "$lookup": {
                      "from": "provinces",
                      "as": "business_owner.province",
                      "localField": "business_owner.province",
                      "foreignField": "_id"
                    }
                },
                {
                    "$unwind":{
                      "path":"$business_owner.province",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                  {
                    "$addFields":{
                      "business_owner.district": {"$filter": {
                        "input": '$business_owner.province.district',
                       "as": 'var',
                        "cond": {"$eq": ['$$var._id', '$business_owner.district']}
                    }},
                    }
            
                  },
                  {
                    "$unwind":{
                      "path":"$business_owner.district",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                  {
                    "$addFields":{
                      "business_owner.commune": {"$filter": {
                        "input": '$business_owner.district.commune',
                       "as": 'var',
                        "cond": {"$eq": ['$$var._id', '$business_owner.commune']}
                    }},
                    }
            
                  },
                  {
                    "$unwind":{
                      "path":"$business_owner.commune",
                      "preserveNullAndEmptyArrays":true
                    }
                  },
                
                {
                    "$project":{
                        "products":1,
                        "shop.province._id":1,
                        "shop.province.province_name":"$shop.province.province",
                        "shop.province.country":1,
                        "shop.district._id":1,
                        "shop.district.district_name":1,
                        "shop.commune":1,
                        "shop.address":1,
                        "shop.is_pickup_point":1,
                        "business_owner.postal_code":1,
                        "business_owner.fname":1,
                        "business_owner.lname":1,
                        "business_owner.province._id":1,
                        "business_owner.province.province_name":"$business_owner.province.province",
                        "business_owner.province.country":1,
                        "business_owner.district._id":1,
                        "business_owner.district.district_name":1,
                        "business_owner.commune":1,
                        "business_owner.postal_code":1,
                        "business_owner.fname":1,
                        "business_owner.lname":1,
                        "business_owner.address":1
                    }
                },
                

               
    
            ]
    
            const cartData = await cart.aggregate(basicFilter);
    
            if(cartData){
    
                result.data = cartData;
                return result;
            }
    
            else{
                
                result.dataNotFound = true;
                return true;
            }
            
        } catch (error) {

            console.log(error);
            result.databaseError = true;
            return true;
            
        }









    }
    else{
        result.restricted = true;
        return result;
    }



}

