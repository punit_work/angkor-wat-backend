const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const ShippingController = require('./shipping-controller');
const ShippingHelper = require('./shipping-helper');

//middleware
const authCheck = require('../../middleware/check-auth');

// Path - /v1/shipping/
// All Routes for shipping

//Add shipping details
router.post(
  "/add-shipping-details",
  authCheck,
  [
    body('delivery_type').notEmpty().withMessage('Delivery type is required'),
    body('hotel_id').optional().notEmpty().isMongoId().withMessage("Hotel id is required"),
    body('billing_details').optional().isMongoId().notEmpty().withMessage("billing_deatails as [id] is required")

  ],
  async (req, res) => {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    if(!['hotel','self-collection','user-address'].includes(req.body.delivery_type)){
      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please select delivery_type from [ 'hotel','self-collection','user-address ]");
    }
    if(req.body.delivery_type === 'hotel' && !req.body.hotel_id){
      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "You need to provide hotel_id.");
    }
    if((req.body.delivery_type === 'billing_details') && (!req.body.billing_details)){
      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "You need to provide billing_details");
    }

    const response = await ShippingController.addShippingDetails(req);
    if (response.unAuthorised) {
      return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }

    if (response.databaseError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.shipping) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHIPPING_DETAILS_ADDED_SUCCESSFULLY, response.shipping)
  }
);

//Get shipping details
router.get("/get-shipping-details",
  authCheck,
  async (req, res) => {

    const response = await ShippingController.getShippingDetails(req);

    if(response.noShippingDetailsFound){
      return Response.error(res, ResponseCode.NOT_FOUND,"Shipping details not found");
    }
    if (response.databaseError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.shipping) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHIPPING_DATA_RETRIVED_SUCCESSFULLY, response.shipping)

  }

)

///FOR EXPERIENCE SHIPPING

router.post(
  "/add-experience-shipping-details",
  authCheck,
  [
    body('service_time').notEmpty().withMessage('Time is required'),
    body('hotel_id').if(body('hotel_id').exists()).notEmpty().withMessage("Hotel id is required"),
    body('address').if(body('address').exists()).notEmpty().withMessage("Address is required"),
    body('location').if(body('location').exists()).notEmpty().withMessage("Location is required")

  ],
  async (req, res) => {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    const response = await ShippingController.addExperienceShipping(req);
    if (response.unAuthorised) {
      return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }

    if (response.databaseError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.shipping) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHIPPING_DETAILS_ADDED_SUCCESSFULLY, response.shipping)
  }
);

//Get shipping details
router.get("/get-experience-shipping-details",
  authCheck,
  async (req, res) => {

    const response = await ShippingController.getExperienceShipping(req);

    if (response.databaseError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.shipping) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHIPPING_DATA_RETRIVED_SUCCESSFULLY, response.shipping)

  }

)


//Get self pickup address
router.get("/get-self-pickup-details",
  authCheck,
  async (req, res) => {

    const response = await ShippingController.getSelfPickupDetails(req);

    if (response.databaseError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } 
    if(response.restricted){
      return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    }
    if(response.noDataFound){
      return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.ERROR_NOT_FOUND);
    }
    else if (response.data) return Response.success(res, ResponseCode.SUCCESS, "Pickup details retrived successfully", response.data)

  }

)


module.exports = router;