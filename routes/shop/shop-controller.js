// models
const systemUser = require("../../model/SystemUsers");
const category = require("../../model/Category");
const shop = require("../../model/Shop");
const Province = require("../../model/Province");
const Shop = require("../../model/Shop");
const Ratings = require("../../model/Ratings");
const { JsonWebTokenError } = require("jsonwebtoken");
const fs = require('fs');
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const { log } = require("console");
const { addTranslatedData } = require("../../global/util");

exports.getShopById = async req => {
  let shop_data,
    result = {};
  try {
    if (["admin", "end_user"].includes(req.user_type))
      shop_data = await shop.findById(req.params.shop_id);
    else if (req.user_type === "business_owner")
      shop_data = await shop.findOne({
        business_owner_id: req.user_id,
        _id: req.params.shop_id
      });

    if (shop_data) {
      const rating = await Ratings.findOne({ shop_id: shop_data._id }).select(
        "-_id no_of_ratings average_rating"
      );

      let shop = JSON.parse(JSON.stringify(shop_data));;

      if (rating) shop.shop_rating = rating;

      result.shop = shop;
      return result;
    }

    result.noShopFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getAllShops = async req => {
  let shops = [],
    result = {};

  try {
    //if end user or admin request for shop
    if (["admin", "end_user"].includes(req.user_type)) {


      const basicFilter = [{
        "$match":{
            vendor_type:req.params.vendor_type
        }
      },
      {
        "$lookup":{
          "from":"ratings",
          "as":"rating",
          "localField":"_id",
          "foreignField":"shop_id"
        }

      },
      {
        "$unwind":{
          "path":"$rating",
          "preserveNullAndEmptyArrays":true
        }
      },
      {
        "$project":{
          "shop_images":1,
          "tags":1,
          // "shop_name":1,
          "translations":1,
          "email":1,
          "fname":1,
          "lname":1,
          "website":1,
          "shop_logo":1,
          // "shop_description":1,
          "category":1,
          "is_featured":1,
          "createdAt":1,
          "updatedAt":1,
          "shop.shop_name":1,
          "shop.province":1,
          "loaction":1,
          "payment_methods":1,
          "address":1,
          "province":1,
          "city":1,
          "vendor_type":1,
          "pincode":1,
          "rating.average_rating":1
        }
      }
    
    ]
      shops = await shop.aggregate([...basicFilter]);
    } else if (["business_owner"].includes(req.user_type)) {
      shops = await shop.find({ business_owner_id: req.user_id });
    } else if (req.user_type === 'vendor') {
      shops = await shop.find({_id: req.user_id});
    }
  } catch (error) {
    result.databaseError = true;
    return result;
  }

  result.shops = shops;
  return result;
};

exports.createShop = async req => {
  let shops,
    result = {};
  if (req.user_type === "super_admin") {
    const shopData = { ...req.body };

    // checking optional params
    if (shopData.promocode) shopData.promocode = JSON.parse(shopData.promocode);
    if (shopData.tax) shopData.tax = JSON.parse(shopData.tax);
    // if (shopData.shop_description) shopData.shop_description = JSON.parse(shopData.shop_description);

    const translations = {
      en:{},
      kh:{}
    };

    let hasError;

     await addTranslatedData({
       data:JSON.parse(shopData.en),
       lang:'en',
       requiredFields:['shop_name','shop_description','address'],
       translationObject:translations
      }).catch(err => {
       hasError = true;
       result['validationError'] = true;
       result['message'] = `${err}`;
     });

     if(hasError){
      return result;
    }

     await addTranslatedData({
       data:JSON.parse(shopData.kh),
       lang:'kh',
       requiredFields:['shop_name','shop_description','address'],
       translationObject:translations
      }).catch(err => {
      hasError = true;
      result['validationError'] = true;
      result['message'] = `${err}`;
    });

    if(hasError){
      return result;
    }

    shopData.translations = translations;

    shopData.shop_images = req.files.shop_images.map(({ path }) =>
      path.replace(/\\/g, "/")
    );

    shopData.shop_logo = req.files.shop_logo[0].path.replace(/\\/g, '/');
    
    try {
      // verify retailer id
      const isValidRetailer = await systemUser.findOne({
        _id: req.user_id
      });
      if (!isValidRetailer) {
        throw new Error("retailer not found");
      }

      if (await systemUser.findOne({email: shopData.email})) {

        result.shopAlreadyExistWithProvidedEmail = true;
        result.message = 'Shop already created with this email, please choose another email.';
        return result;
      }

      

      const hashed_password = await bcrypt.hash(shopData.password, 12);
      shopData.password = hashed_password;

      const newUser = await new systemUser({
        email:shopData.email,
        password:hashed_password,
        fname:shopData.fname,
        lname:shopData.lname,
        parent_admin_id:req.user_id,
        role:'commerce_user',
        commerce_type: shopData.vendor_type === 'product_seller' ? 'shop' : 'service'
      }).save();

      if(newUser){
        shopData.owner_id = newUser._id
      }
      shopData.business_owner_id = req.user_id;

      delete shopData.email;
      delete shopData.fname;
      delete shopData.lname;
      delete shopData.password;
      delete shopData.en;
      delete shopData.kh;



      // verify category id
      const categoryDetails = await category.findById({_id: shopData.category});

      if(!categoryDetails){
        throw new Error('category not found');
      }
      else{

        if(shopData.vendor_type === 'service_provider'){
          if(!categoryDetails.category_for === 'Service'){
            throw new Error('This category is not belong to vendor type');
          }

        }
        if(shopData.vendor_type === 'product_seller'){
          if(!categoryDetails.category_for === 'Product'){
            throw new Error('This category is not belong to vendor type');
          }
        }
        }

      // create shop
      shops = await shop.create({
        ...shopData,
        location: {
          type: "Point",
          coordinates: [req.body.longitude, req.body.latitude]
        }
      });
    } catch (error) {

      console.log(error);
      
      result.databaseError = true;
      return result;
    }

    result.shops = shops;
    return result;
  } else {
    result.notAuthorized = true;
    return result;
  }
};

exports.updateShop = async req => {
  let shops,
    result = {};

  if (req.user_type === "business_owner" || req.user_type === "vendor" || req.user_type === 'super_admin') {
    let shopData = { ...req.body };

    try {
      // verify shop owner
      const ValidShopData = await shop.findById({
        _id: req.params.shop_id,
        business_owner_id: req.user_id
      });


      if(req.body.shop_description){
        shopData.shop_description = JSON.parse(req.body.shop_description);
      }

      if (shopData.email) {
          
        const existingShopOwner = await shop.findOne({email: shopData.email});

        if (existingShopOwner && existingShopOwner._id.toString() !== req.params.shop_id) {

          result.shopAlreadyExistWithProvidedEmail = true;
          result.message = 'Shop already created with this email, please choose another email.';
          return result;
        } 
      }

      if (shopData.old_password && shopData.new_password) {

        if (ValidShopData.password) {
            
          const confirm_old_password = await bcrypt.compare(shopData.old_password, ValidShopData.password);

          if (confirm_old_password) {

            const hashed_password = await bcrypt.hash(shopData.new_password, 12);
            shopData.password = hashed_password;
          
          } else {

            result.invalidPassword = true;
            result.message = 'Old Password is not correct.';
            return result;
          }
        }

        else {

          const hashed_password = await bcrypt.hash(shopData.new_password, 12);
          shopData.password = hashed_password;
        }
      }

      if (!ValidShopData) {
        result.storeNotFound = true;
        return result;
      }

      // verify category id
      if (shopData.category) {
        const isValidCategory = await category.findById({
          _id: shopData.category
        });
        if (!isValidCategory) {
          result.categoryNotFound = true;
          return result;
        }
      }

      // PROMOCODE OR TAX

      // update, add, remove promocode

      if (ValidShopData.promocode) {
        // assign array to shopData.promocode
        shopData.promocode = ValidShopData.promocode;

        // add promocode

        if (shopData.add_promocode) {
          shopData.add_promocode = JSON.parse(shopData.add_promocode);

          if (shopData.add_promocode.length > 0) {
            shopData.promocode = [
              ...ValidShopData.promocode,
              ...shopData.add_promocode
            ];
          }
        }

        // update promocode

        if (shopData.update_promocode) {

          shopData.update_promocode = JSON.parse(shopData.update_promocode);

          const {
            id,
            code_name,
            min_subtotal,
            offer_price
          } = shopData.update_promocode;

          const index = shopData.promocode.findIndex(
            data => data._id.toString() == id
          );

          code_name != null
            ? (shopData.promocode[index].code_name = code_name)
            : "";
          min_subtotal != null
            ? (shopData.promocode[index].min_subtotal = min_subtotal)
            : "";
          offer_price != null
            ? (shopData.promocode[index].offer_price = offer_price)
            : "";
        }

        //remove promocode

        if (shopData.remove_promocode) {
          shopData.remove_promocode = JSON.parse(shopData.remove_promocode);

          shopData.remove_promocode.forEach(num => {
            let index = shopData.promocode.findIndex(i => i.id === num);
            if (index >= 0) {
              shopData.promocode.splice(index, 1);
            }
          });
        }

        //remove all promocode
        if (shopData.remove_all_promocode) shopData.promocode = [];

      }

      // if there's no promocode in database add new
      else if (shopData.add_promocode)
        shopData.promocode = Array.isArray(shopData.add_promocode)
          ? [...shopData.add_promocode]
          : [shopData.add_promocode];
      else {
      }

      // for tax

      if (ValidShopData.tax) {
        shopData.tax = ValidShopData.tax;

        if (shopData.add_tax) {

          shopData.add_tax = JSON.parse(shopData.add_tax);

          if (shopData.add_tax.length > 0) {
            shopData.tax = [...ValidShopData.tax, ...shopData.add_tax];
          }

        }

        // update tax
        if (shopData.update_tax) {

          shopData.update_tax = JSON.parse(shopData.update_tax);

          const { id, amount, min_subtotal } = shopData.update_tax;

          const index = shopData.tax.findIndex(
            data => data._id.toString() == id
          );

          amount != null ? (shopData.tax[index].amount = amount) : "";
          min_subtotal != null
            ? (shopData.tax[index].min_subtotal = min_subtotal)
            : "";
        }

        //remove tax multiple / single
        // const ans = data2.filter( num => !data.includes(num) );

        // OLD func
        // if(shopData.remove_tax) shopData.tax = shopData.tax.filter( data => data._id.toString() !== shopData.remove_tax.id );
        // if(shopData.remove_tax) shopData.tax = shopData.tax.filter( data => !shopData.remove_tax.includes( data._id.toString() ) );

        if (shopData.remove_tax) {
          shopData.remove_tax = JSON.parse(shopData.remove_tax);
          shopData.remove_tax.forEach(num => {
            let index = shopData.tax.findIndex(i => i.id === num);
            if (index >= 0) {
              shopData.tax.splice(index, 1);
            }
          });
        }

        //remove all tax
        if (shopData.remove_all_tax) shopData.tax = [];

      }
      // if there's no tax in database add new
      else if (shopData.add_tax)
        shopData.tax = Array.isArray(shopData.add_tax)
          ? [...shopData.add_tax]
          : [shopData.add_tax];
      else {
      }

      // mapping image files
      if (req.files && req.files.shop_images && req.files.shop_images.length > 0) {
        shopData.shop_images = [
          ...req.files.shop_images.map(({ path }) => path.replace(/\\/g, "/")),
          ...ValidShopData.shop_images
        ];
      }

      if (req.files && req.files.shop_logo) {
        shopData.shop_logo = req.files.shop_logo[0].path.replace(/\\/g, '/');
      }

      if (shopData.latitude && shopData.longitude) {
        shopData.location = {
          type: "Point",
          coordinates: [shopData.longitude, shopData.latitude]
        };
      }

      if (shopData.remove_shop_images && JSON.parse(shopData.remove_shop_images).length > 0) {

        shopData.shop_images = ValidShopData.shop_images.filter(img => {
          
          if (JSON.parse(shopData.remove_shop_images).includes(img)) fs.unlink(img, err => console.log(err));
          else return img;

        })
      }

      const translations = {
        en:{},
        kh:{}
      }

      let hasError;

     await addTranslatedData({
       data:JSON.parse(shopData.en),
       lang:'en',
       requiredFields:['shop_name','shop_description','address'],
       translationObject:translations
      }).catch(err => {
       hasError = true;
       result['validationError'] = true;
       result['message'] = `${err}`;
     });

     if(hasError){
      return result;
    }

     await addTranslatedData({
       data:JSON.parse(shopData.kh),
       lang:'kh',
       requiredFields:['shop_name','shop_description','address'],
       translationObject:translations
      }).catch(err => {
      hasError = true;
      result['validationError'] = true;
      result['message'] = `${err}`;
    });

    if(hasError){
      return result;
    }


    delete shopData.en;
    delete shopData.kh;

    shopData.translations = translations





      // update shop
      shops = await shop.findOneAndUpdate(
        { _id: req.params.shop_id },
        { ...shopData },
        {
          new: true
        }
      );

    } catch (error) {

      console.log(error);
      result.databaseError = true;
      return result;
    }

    result.shops = shops;
    return result;
  } else {
    result.notAuthorized = true;
    return result;
  }
};

exports.getShopsByProvince = async (user_type, province_id) => {
  let result = {};

  try {
    if (user_type === "end_user") {
      const province_details = await Province.findById(province_id);

      if (province_details) {
        const shops = await shop.find({
          province: {
            $regex: province_details.province,
            $options: "i"
          }
        });

        if (shops.length) {
          result.shops = shops;
          return result;
        }

        result.noShopFound = true;
        return result;
      }

      result.noShopFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.addRatingForShop = async (
  user_type,
  user_id,
  { rating_number, shop_id }
) => {
  let result = {};

  try {
    if (user_type === "end_user") {
      const shop = await Shop.findById(shop_id);

      if (shop) {
        const rating = await Ratings.findOne({ shop_id, product_id: null });

        if (rating) {
          if (
            rating.rated_by.find(users => users.user_id.toString() === user_id)
          ) {
            result.alreadyRated = true;
            return result;
          }

          const average_rating =
            (rating.rated_by.reduce((acc, curr) => acc + curr.rating, 0) +
              rating_number) /
            (rating.no_of_ratings + 1);
          rating.no_of_ratings = rating.no_of_ratings + 1;
          rating.average_rating = average_rating.toFixed(1);
          rating.rated_by.push({ user_id, rating: rating_number });
          await rating.save();

          result.ratingAdded = true;
          return result;
        }

        const ratingObject = {
          shop_id,
          no_of_ratings: 1,
          average_rating: rating_number,
          rated_by: [{ user_id, rating: rating_number }]
        };

        await new Ratings(ratingObject).save();

        result.ratingAdded = true;
        return result;
      }

      result.noShopFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getPopularStores = async user_type => {
  let result = {};
  let shopList = [];

  try {
    if (user_type === "end_user") {
     
      shopList = await Shop.aggregate([
        {
          "$lookup":{
            "from":"ratings",
            "as":"rating",
            "localField": "_id",
            "foreignField": "shop_id"
          }
        },
        {
          "$unwind":"$rating"
        },
        {
          "$lookup":{
            "from":"products",
            "as":"product",
            "localField":"_id",
            "foreignField":"shop_id"
          }

        },
        {
          "$sort":{
            "rating.average_rating":-1
          }
        },
        {
          "$project":{
            "location":1,
            "shop_images":1,
            "tags":1,
            "payment_methods":1,
            "is_featured":1,
            "shop_name":1,
            "address":1,
            "province":1,
            "business_owner_id":1,
            "promocode":1,
            "tax":1,
            "createdAt":1,
            "updatedAt":1,
            "rating.average_rating":1,
            "product.product_name":1
            
          }
        }
        
      ])

      if(shopList.length){
        result.popularStores = shopList;
        return result;
      }


      result.noShopFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.markStoreAsFeatured = async (user_type, shop_ids) => {
  let result = {};

  try {
    if (user_type === "admin") {
      for (let id of shop_ids) {
        try {
          const shop = await Shop.findById(id);

          if (shop) {
            shop.is_featured = true;
            await shop.save();
          }
        } catch (err) {
          continue;
        }
      }

      result.shopsMarkedAsFeatured = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.unmarkStoreFromFeatured = async (user_type, shop_ids) => {
  let result = {};

  try {
    if (user_type === "admin") {
      for (let id of shop_ids) {
        try {
          const shop = await Shop.findById(id);

          if (shop) {
            shop.is_featured = false;
            await shop.save();
          }
        } catch (err) {
          continue;
        }
      }

      result.shopsUnMarkedFromFeatured = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getFeaturedStores = async user_type => {
  let result = {};

  try {
    if (user_type === "end_user") {
      const shops = await Shop.find({ is_featured: true }).limit(5);

      if (shops.length) {
        result.featuredShops = shops;
        return result;
      }

      result.noShopFound = true;
      return result;
    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.getFilteredShops = async (req) =>{

  const {user_type,user_id} = req;

  if(user_type === 'end_user'){

      const {filterBy,sortBy,multipleFilter,category_id,sub_category_id,province,title} = req.body;

      let basicFilter = [];

      let shopList = [];
      let result = {};

      const filterOptions = {

          "category":{
              "$match":{category:mongoose.Types.ObjectId(category_id)}
          },
          "province":{
              "$match":{province:mongoose.Types.ObjectId(province)}
          },
          "category-province":{
            "$match":{category:mongoose.Types.ObjectId(category_id),province:mongoose.Types.ObjectId(province)}
          },
          "title":{
            "$match":{shop_name: {"$regex":title,"$options":"i"} }
          }
      }

      const sortOptions = {
          "most-popular":{
              "$sort":{"rating.average_rating":-1}
          },
          "new-arrivals":{
              "$sort":{"createdAt":-1}
          },
          "price-high-to-low":{
            "$sort":{
              "variants.0.price":-1
            }
          },
          "price-low-to-high":{
            "$sort":{
              "variants.0.price":1
            }
          },
          "alphabetically":{
            "$sort":{
              "shop_title":1
            }

          }

      }

      if(multipleFilter){

        const filters = multipleFilter.split(',');

        if(filters.includes('category') && filters.includes('province')){

          if(sortBy){

              let preserveNullAndEmptyArrays = true;

              if(['most-popular'].includes(sortBy)){
                  preserveNullAndEmptyArrays = false;
              }
      
              basicFilter = [
                filterOptions["category-province"],
              {
                "$lookup":{
                  "from":"ratings",
                  "as":"rating",
                  "localField":"_id",
                  "foreignField":"shop_id"
                }
      
              },
              {
                "$unwind":{
                  "path":"$rating",
                  "preserveNullAndEmptyArrays":preserveNullAndEmptyArrays
                }
              },
              {
                "$project":{
                  "shop_name":1,
                  "shop_logo":1,
                  "category":1,
                  "createdAt":1,
                  "province":1,
                  "rating":1,
                  "shop_title": { "$toLower": "$shop_name" }
                }
              },
              sortOptions[sortBy]
          
            
            ]
    
      
            try {
                shopList = await Shop.aggregate(basicFilter);
            } 
            catch (error) {
      
                console.log("Error at Multiple filter with sort : ",error);
                result.databaseError = true;
                return result;
      
            }



          }
          else{

              basicFilter = [
                filterOptions["category-province"],
              {
                "$lookup":{
                  "from":"ratings",
                  "as":"rating",
                  "localField":"_id",
                  "foreignField":"shop_id"
                }

              },
              {
                "$unwind":{
                  "path":"$rating",
                  "preserveNullAndEmptyArrays":true
                }
              },
              {
                "$project":{
                  "shop_name":1,
                  "shop_logo":1,
                  "category":1,
                  "createdAt":1,
                  "province":1,
                  "rating.average_rating":1,
                  "shop_title": { "$toLower": "$shop_name" }
                }
              }
          
            
            ]

            try {
                shopList = await Shop.aggregate(basicFilter);
            } 
            catch (error) {

                console.log("Error at multiple filter : ",error);
                result.databaseError = true;
                return result;

            }


          }

        } 

      }
      else if(filterBy && sortBy){

        let preserveNullAndEmptyArrays = true;

        if(['most-popular'].includes(sortBy)){
            preserveNullAndEmptyArrays = false;
        }

        basicFilter = [
          filterOptions[filterBy],
        {
          "$lookup":{
            "from":"ratings",
            "as":"rating",
            "localField":"_id",
            "foreignField":"shop_id"
          }

        },
        {
          "$unwind":{
            "path":"$rating",
            "preserveNullAndEmptyArrays":preserveNullAndEmptyArrays
          }
        },
        {
          "$project":{
            "shop_name":1,
            "shop_logo":1,
            "category":1,
            "createdAt":1,
            "province":1,
            "rating.average_rating":1,
            "shop_title": { "$toLower": "$shop_name" }
          }
        },
        sortOptions[sortBy]
    
      
      ]


      try {
          shopList = await Shop.aggregate(basicFilter);
      } 
      catch (error) {

          console.log("Error at filter and sort : ",error);
          result.databaseError = true;
          return result;

      }

      }
      else if(filterBy){

          basicFilter = [
              filterOptions[filterBy],
            {
              "$lookup":{
                "from":"ratings",
                "as":"rating",
                "localField":"_id",
                "foreignField":"shop_id"
              }

            },
            {
              "$unwind":{
                "path":"$rating",
                "preserveNullAndEmptyArrays":true
              }
            },
            {
              "$project":{
                "shop_name":1,
                "shop_logo":1,
                "category":1,
                "createdAt":1,
                "province":1,
                "rating.average_rating":1,
                "shop_title": { "$toLower": "$shop_name" }
              }
            }
        
          
          ]

          try {
              shopList = await Shop.aggregate(basicFilter);
          } 
          catch (error) {

              console.log("Error at filterBy : ",error);
              result.databaseError = true;
              return result;

          }

      }
      else if(sortBy){

          let preserveNullAndEmptyArrays = true;

          if(['most-popular'].includes(sortBy)){
              preserveNullAndEmptyArrays = false;
          }

          basicFilter = [
            {
              "$match":{}
            },
            {
              "$lookup":{
                "from":"ratings",
                "as":"rating",
                "localField":"_id",
                "foreignField":"shop_id"
              }

            },
            {
              "$unwind":{
                "path":"$rating",
                "preserveNullAndEmptyArrays":preserveNullAndEmptyArrays
              }
            },
            {
              "$project":{
                "shop_name":1,
                "shop_logo":1,
                "category":1,
                "createdAt":1,
                "province":1,
                "rating.average_rating":1,
                "shop_title": { "$toLower": "$shop_name" }
              }
            },
              sortOptions[sortBy]
          ]

          try {
            shopList = await Shop.aggregate(basicFilter);
          } 
          catch (error) {

              console.log("Error at sortBy : ",error);
              result.databaseError = true;
              return result;

          }


      }

      if(shopList.length){
          result.list = shopList;
          return result
      }
      else{
          result.noShopsFound = true;
          return result
      }

  }
  else{
      result.unauthorizedAccess = true;
      return result
  }



}

// updated by system user check sys user folder
exports.markShopAsPickupPoint = async (req) => {

  let result = {};

  const {user_id,user_type} = req;
  let {shop_ids} = req.body;

  


  try {
    shop_ids = shop_ids.map(id => mongoose.Types.ObjectId(id))
    if (["admin", "business_owner"].includes(user_type)) {


      if(user_type === 'business_owner'){

        const isValidShopOwner = systemUser.findOne({_id:user_id});

        if(isValidShopOwner){

            const shop = await Shop.updateMany({
              _id:{$in:shop_ids}
            },{is_pickup_point:true});

           if(shop.nModified > 0){
             result.updatedShop = shop.nModified;
             return result;
           }
           else{
             result.nothingUpdated = true;
             return result;
           }


        }
        else{
          result.notValidOwner = true;
          return result;
        }

      }

    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
};

exports.unmarkShopAsPickupPoint = async (req) => {

  let result = {};

  const {user_id,user_type} = req;
  let {shop_ids} = req.body;

  


  try {
    shop_ids = shop_ids.map(id => mongoose.Types.ObjectId(id))
    if (["admin", "business_owner"].includes(user_type)) {


      if(user_type === 'business_owner'){

        const isValidShopOwner = systemUser.findOne({_id:user_id});

        if(isValidShopOwner){

            const shop = await Shop.updateMany({
              _id:{$in:shop_ids}
            },{is_pickup_point:false});

           if(shop.nModified > 0){
             result.updatedShop = shop.nModified;
             return result;
           }
           else{
             result.nothingUpdated = true;
             return result;
           }


        }
        else{
          result.notValidOwner = true;
          return result;
        }

      }

    }

    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
  
};


