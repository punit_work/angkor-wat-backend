const ShopController = require("./shop-controller");
const express = require("express");
const router = express.Router();
const { body, param, validationResult, query } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");
const uploadShopImage = require("../../middleware/upload-shop-image");
const authCheck = require("../../middleware/check-auth");

// Path - /v1/shop/get-shop/:shop_id
// Route for getting single shop for end_users or belonging to a owner
router.get('/get-shop/:shop_id',

  authCheck,

  [
    param('shop_id').isMongoId().withMessage('Parameter [shop_id] is missing or invalid')
  ],

  async (req, res) => {

    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    const response = await ShopController.getShopById(req);

    if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

    else if (response.noShopFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SHOPS_AVAILABLE);

    else if (response.shop) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RETRIVED_SHOP_DATA_SUCCESSFULLY, response.shop);

  }
)

// Path - /v1/shop/get-shops
// Route for getting all the shops for end_users or belonging to a owner 
router.get("/get-shops/:vendor_type",

  authCheck,

  [
    param('vendor_type').isString().trim().notEmpty().withMessage('Parameter [vendor_type] is missing or invalid')
  ],


  async (req, res) => {
   
    const error = validationResult(req);

    if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    if(!['service_provider','product_seller'].includes(req.params.vendor_type)){
      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide valid vendor type from : ['service_provider','product_seller'] ")
    }
    const response = await ShopController.getAllShops(req);

    if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

    else if (response.noShopFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SHOPS_AVAILABLE);

    else if (response.shops) {
      const successMessage = response.shops.length > 0 ? ResponseMessage.RETRIVED_SHOP_DATA_SUCCESSFULLY : ResponseMessage.NO_SHOPS_AVAILABLE
      return Response.success(res, ResponseCode.SUCCESS, successMessage, response.shops);
    }
  }
);

// Path - /v1/shop/create-shop
// Route for creating new shop for business_owner
router.post("/create-shop",

  authCheck,

  uploadShopImage.fields([{ name: 'shop_images'}, {name: 'shop_logo'}]),

  [
    body('email').trim().normalizeEmail().isEmail().withMessage('Parameter [email] is missing or invalid.'),
    body('fname').trim().isLength({ min: 1 }).withMessage('Parameter [fname] is missing or invalid.'),
    body('vendor_type').trim().isLength({ min: 1 }).withMessage('Parameter [vendor_type] is missing or invalid.'),
    body('phone_number').trim().isLength({ min: 8 }).withMessage('Parameter [phone_number] is missing or invalid.'),
    body('lname').trim().isLength({ min: 1 }).withMessage('Parameter [lname] is missing or invalid.'),
    body('password').trim().isLength({ min: 8 }).withMessage('Parameter [password] is missing or invalid.'),
    // body('shop_name').notEmpty().withMessage('Shop name is required'),
    body('category').optional().isMongoId().withMessage('Category is required'),
    body('en').isJSON().withMessage('Parameter [en] is missing or invalid.'),
    body('kh').isJSON().withMessage('Parameter [kh] is missing or invalid.'),
    // body('address').notEmpty().withMessage('Address is required'),
    body('province').notEmpty().withMessage('Province is required'),
    body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
    body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
    body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
    body('city').optional().notEmpty().withMessage('City is required'),
    body('pincode').optional().notEmpty().withMessage('Pincode is required'),
    body('promocode').optional().notEmpty().withMessage('Promocode details is required'),
    body('tax').optional().notEmpty().withMessage('Tax details is required'),
    body('latitude').trim().isLength({ min: 1 }).withMessage('Parameter [latitude] is missing or invalid'),
    body('longitude').trim().isLength({ min: 1 }).withMessage('Parameter [longitude] is missing or invalid'),
    body('tags').optional().isArray({ min: 1 }).withMessage('Parameter [tags] cannot be empty'),
    body('payment_methods').optional().isArray({ min: 1 }).withMessage('Parameter [payment_methods] cannot be empty')
  ],

  async (req, res) => {

    const error = validationResult(req);

    if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }


    const response = await ShopController.createShop(req);

    if (response.notAuthorized) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);

    if (response.shopAlreadyExistWithProvidedEmail) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);
    if (response.databaseError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    }
    if (response.validationError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, response.message);
    }

    else if (response.shops) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHOP_CREATED_SUCCESSFULLY, response.shops)
  }
);

// Path - /v1/shop/update-shop/:shop_id
// Route for updating shop for the given business_owner_id
router.post("/update-shop/:shop_id",

  authCheck,

  uploadShopImage.fields([{ name: 'shop_images'}, {name: 'shop_logo'}]),

  [
    param('shop_id').isMongoId().withMessage('Parameter [shop_id] is missing or invalid'),
    body('email').optional().trim().normalizeEmail().isEmail().withMessage('Parameter [email] is missing or invalid.'),
    body('fname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [fname] is missing or invalid.'),
    body('lname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [lname] is missing or invalid.'),
    body('shop_name').optional().notEmpty().withMessage('Shop name is required'),
    body('category').optional().notEmpty().withMessage('Category is required'),
    body('address').optional().notEmpty().withMessage('Address is required'),
    body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
    body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
    body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
    body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
    body('city').optional().notEmpty().withMessage('City is required'),
    body('pincode').optional().notEmpty().withMessage('Pincode is required'),
    body('promocode').optional().isArray().notEmpty().withMessage('Promocode details is required'),
    body('tax').optional().isArray().notEmpty().withMessage('Tax details is required'),
    body('latitude').optional().trim().isLength({ min: 1 }).withMessage('Parameter [latitude] is missing or invalid'),
    body('longitude').optional().trim().isLength({ min: 1 }).withMessage('Parameter [longitude] is missing or invalid'),
    body('tags').optional().isArray({ min: 1 }).withMessage('Parameter [tags] cannot be empty'),
    body('payment_methods').optional().isArray({ min: 1 }).withMessage('Parameter [payment_methods] cannot be empty')
  ],

  async (req, res) => {
    console.log(req.user_type);
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    if (req.body.remove_shop_images && !Array.isArray(JSON.parse(req.body.remove_shop_images))) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, 'Parameter [remove_shop_images] is invalid');
    }

    const response = await ShopController.updateShop(req);

    if (response.notAuthorized) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
    // if(response.invalidPromocodeDetails) return Response.error(res,ResponseCode.UNPROCESSABLE_ENTITY,ResponseMessage.INVALID_PROMOCODE_DETAILS);
    // if(response.invalidTaxDetails) return Response.error(res,ResponseCode.UNPROCESSABLE_ENTITY,ResponseMessage.INVALID_TAX_DETAILS);

    if (response.shopAlreadyExistWithProvidedEmail) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

    else if (response.invalidPassword) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

    if (response.databaseError) {
      return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    }

    else if (response.storeNotFound) {
      return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SHOPS_AVAILABLE);
    }

    else if (response.categoryNotFound) {
      return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_CATEGORY_FOUND);
    }

    else if (response.shops) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHOP_UPDATED_SUCCESSFULLY, response.shops)
  }
);

// Path - /v1/shop/get-shops-by-province/:province_id
// Route for getting shops by province
router.get('/get-shops-by-province/:province_id',

  authCheck,

  [
    param('province_id').isMongoId().withMessage('Parameter [province_id] is missing or invalid'),
  ],

  async (req, res) => {

    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    const response = await ShopController.getShopsByProvince(req.user_type, req.params.province_id);

    if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

    else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

    else if (response.noShopFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SHOPS_AVAILABLE);

    else if (response.shops) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RETRIVED_SHOP_DATA_SUCCESSFULLY, response.shops);

  }
)

// Path - /v1/shop/add-rating
// Route for adding ratings for shop
router.post('/add-rating',

  authCheck,

  [
    body('shop_id').isMongoId().withMessage('Parameter [shop_id] is missing or invalid'),
    body('rating_number').isFloat({ min: 1, max: 5 }).withMessage('Parameter [rating_number] must be between 1 to 5')
  ],

  async (req, res) => {

    // Checking if req.body has any error
    const error = validationResult(req);

    if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    const response = await ShopController.addRatingForShop(req.user_type, req.user_id, req.body);

    if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

    else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

    else if (response.noShopFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SHOPS_AVAILABLE);

    else if (response.alreadyRated) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.RATING_ALREADY_ADDED);

    else if (response.ratingAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RATING_ADDED);
  }
);

// Path - /v1/shop/get-popular-stores
// Route for fething popular stores
router.get('/get-popular-stores',

  authCheck,

  async (req, res) => {

    const response = await ShopController.getPopularStores(req.user_type);

    if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

    else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

    else if (response.noShopFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SHOPS_AVAILABLE);

    else if (response.popularStores) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RETRIVED_SHOP_DATA_SUCCESSFULLY, response.popularStores);

  });

// Path - /v1/shop/get-featured-stores
// Route for fetching featured stores
router.get('/get-featured-stores',

  authCheck,

  async (req, res) => {

    const response = await ShopController.getFeaturedStores(req.user_type);

    if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);

    else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than end users');

    else if (response.noShopFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SHOPS_AVAILABLE);

    else if (response.featuredShops) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RETRIVED_SHOP_DATA_SUCCESSFULLY, response.featuredShops);

  });


  router.post('/get-filtered-shops',
  authCheck,
  [
    body('filterBy').optional().trim().isLength({ min: 1 }).withMessage("Please give valid filter option"),
    body('sortBy').optional().trim().isLength({ min: 1 }).withMessage("Please give valid sort option"),
    body('category_id').optional().isMongoId().withMessage('Parameter category is invalid'),
    body('ratings').optional().isFloat({min:1,max:5}).withMessage('Ratings must be number between 1 - 5 & number'),
    body('title').optional().not().isEmpty().withMessage('Title is required'),
    body('province').optional().isMongoId().withMessage('Parameter province is invalid'),
    body('multipleFilter').optional().trim().isLength({ min: 1 }).withMessage('Parameter multipleFilter is required'),
  ],
  async(req,res)=>{

    const errors = validationResult(req);
    // check if user has sent category field to filter
    if (!errors.isEmpty()) {

      return Response.error(res, ResponseCode.BAD_REQUEST, errors.array().map((error) => ({
        field: error.param,
        errorMessage: error.msg
      })));
    }

    

    if(!(req.body.filterBy || req.body.sortBy)) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, 'Please provide sort or filter options');

  
    // FILTER 

    if(req.body.filterBy){

      if(!['category',
      'province',
      'title',
      'multipleFilter'
    ].includes(req.body.filterBy)) return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide valid filter options');
      
      const filterBy = req.body.filterBy;

      if(filterBy === 'category'){

        if(!req.body.category_id) return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide category id');

      }

      if(filterBy === 'province'){
        if(!req.body.province)return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide province');
      }

      if(filterBy === 'title'){
        if(!req.body.title)return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide title');
      }

      
      if(filterBy === 'multipleFilter'){
        if(!req.body.multipleFilter) return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide multiple filters');

        const filters = req.body.multipleFilter.split(',');
       
        if(!filters.some(value => ['category','province'].includes(value))) return Response.error(res, ResponseCode.BAD_REQUEST, "Please provide valid filter options for multiple filters from this list [ 'category','province'] ");

        if(filters.includes('category')){

          if(!req.body.category_id) return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide category id');
  
        }

        if(filters.includes('province')){
          if(!req.body.province)return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide province');
        }
          
    
      }

    }

   

    // SORT

    if(req.body.sortBy)
    {

      if(!['most-popular','new-arrivals','alphabetically'].includes(req.body.sortBy)) return Response.error(res, ResponseCode.BAD_REQUEST, 'Please provide valid sort options from [ price-high-to-low ,price-low-to-high ,popularity ,new-arrivals ,alphabetically]');

    }
   

  // get all the products according to the filter & sort applied
  const response = await ShopController.getFilteredShops(req);

  // check if invalid user is trying to access 
  if (response.invalidUser) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.INVALID_USER_TYPE)

  // check if min price is a valid numeric or not
  else if (response.minPriceIsNaN) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_MIN_PRICE)

  // check if max price is a valid numeric or not
  else if (response.maxPriceIsNaN) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_MAX_PRICE)

  // check if category is found or not
  else if (response.categoryError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.NO_CATEGORY_FOUND)

  // check if database Error
  else if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE)

  // check if any products are found using applied category
  else if (response.noShopsFound) return Response.error(res, ResponseCode.NOT_FOUND,"Shops not found")

  
   // check if category is found or not
   else if (response.inValidFilterSelection) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide valid filter selection")

  // send productList in response
  else if (response.list) return Response.success(res, ResponseCode.SUCCESS, "Shops fetched successfully", response.list);

  })

module.exports = router;
