const SystemUsers = require("../../model/SystemUsers");
const SystemUserHelper = require("./system-user-helper");
const bcrypt = require("bcryptjs");
const Session = require("../../model/Session");

// External
const Shop = require("../../model/Shop");
const ArticleController = require("../article/article-controller");

// Function for registration of system users (admin/business_owner)
async function register(req, data, file) {
  let system_user,
    result = {};
  const { user_id, user_type } = req;

  try {
    system_user = await SystemUsers.findOne({ email: data.email });

    if (system_user) {
      result.userAlreadyExist = true;
      return result;
    }

    const hashed_password = await bcrypt.hash(data.password, 12);

    system_user = {
      fname:data.fname,
      lname:data.lname,
      email:data.email,
      password:hashed_password,
    }
    if(data.address){
      system_user.address = data.address;
    }
    if(data.province){
      system_user.province = data.province;
    }
    if(data.district){
      system_user.district = data.district;
    }
    if(data.commune){
      system_user.commune = data.commune;
    }
    if(data.admin_type){
      system_user.admin_type = data.admin_type;
    }
    if(data.discover_type){
      system_user.discover_type = data.discover_type;
    }
    if(data.commerce_type){
      system_user.commerce_type = data.commerce_type;
    }

    if(data.parent_admin_id){
      system_user.parent_admin_id = data.parent_admin_id;
    } else {
      system_user.created_by = user_id;
    }

    if (data.user_type === 'business_owner') {
      system_user.id_proof = file.path.replace(/\\/g, "/");
      system_user.id_proof_name = file.originalname;
      system_user.role = "business_owner";
    } else if(data.admin_type === 'discover' || data.admin_type === 'commerce'){
      system_user.role = "admin";
    } else if(data.discover_type === 'author' || data.admin_type === 'editor'){
      system_user.role = "discover_user";
    } else if(data.commerce_type === 'shop' || data.commerce_type === 'service'){
      system_user.role = "commerce_user";
    } else {
      delete system_user.id_proof;
      delete system_user.id_proof_name;
      system_user.parent_admin_id = user_id;
    }

    system_user = await new SystemUsers(system_user).save();

    if (system_user) {
      let system_user_type = "";
      system_user_type = system_user.role;
      if(['admin'].includes(system_user.role)){
        system_user_type = system_user.admin_type;
      }
      if(['discover_user'].includes(system_user.role)){
        system_user_type = system_user.discover_type;
      }
      if(['commerce_user'].includes(system_user.role)){
        system_user_type = system_user.commerce_type;
      }
  
      const tokenResponse = await SystemUserHelper.generateJwtToken(
        system_user,
        system_user_type
      );
  
      if (tokenResponse.tokenNotGenerated) {
        result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
        return result;
      }
  
      const sessionDetails = {
        system_user_id: system_user._id,
        session_id: tokenResponse.session_id,
        ip_address: await SystemUserHelper.getClientIP(req),
        user_agent: req.headers["user-agent"],
        user_type: system_user_type,
      };
  
      await new Session(sessionDetails).save();
      result.token = tokenResponse.token;
      result.role = system_user.role;
      return result;
    }
    
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function registerSuperUser(req, data, file) {
  let system_user,
    result = {};
  const { user_id, user_type } = req;

  try {
    system_user = await SystemUsers.findOne({ email: data.email });

    if (system_user) {
      result.userAlreadyExist = true;
      return result;
    }

    const hashed_password = await bcrypt.hash(data.password, 12);

    system_user = { ...data, password: hashed_password };

    if (file) {
      system_user.id_proof = file.path.replace(/\\/g, "/");
      system_user.id_proof_name = file.originalname;
      system_user.role = "business_owner";
    } else {
      system_user.role = "super_admin";
    }

    system_user = await new SystemUsers(system_user).save();
  } catch (err) {
    result.databaseError = true;
    return result;
  }

  if (system_user) {
    let system_user_type = "";

    // set user type for business_owner or super_admin
    if (system_user.role) {
      system_user_type = system_user.role;
    } else {
      if (system_user.admin_type) {
        system_user_type = system_user.admin_type;
      }
      if (system_user.discover_type) {
        system_user_type = system_user.discover_type;
      }
    }

    const tokenResponse = await SystemUserHelper.generateJwtToken(
      system_user,
      system_user_type
    );

    if (tokenResponse.tokenNotGenerated) {
      result.tokenNotGenerated = tokenResponse.tokenNotGenerated;
      return result;
    }

    const sessionDetails = {
      system_user_id: system_user._id,
      session_id: tokenResponse.session_id,
      ip_address: await SystemUserHelper.getClientIP(req),
      user_agent: req.headers["user-agent"],
      user_type: system_user_type,
    };

    await new Session(sessionDetails).save();
    result.token = tokenResponse.token;
    return result;
  }
}

// Function to check if user session is active or not
async function isUserSessionActive({ user_type, user_id, session_id }) {
  let userSession;

  if (
    [
      "super_admin",
      "admin",
      "business_owner",
      "service_provider",
      "vendor",
      "discover",
      "commerce",
      "author",
      "editor",
    ].includes(user_type)
  )
    userSession = await Session.findOne({
      system_user_id: user_id,
      session_id,
    });
  else userSession = await Session.findOne({ user_id, session_id });

  if (!userSession) return false;
  else if (!userSession.is_active) return false;
  else if (userSession.is_active) return true;
  else return false;
}

// Function to login system user (admin/business_owner)
async function login(req, email, password, type) {
  let system_user,
    result = {};

  try {
    if (type && type.trim().toLowerCase() === "vendor")
      system_user = await Shop.findOne({ email });
    else system_user = await SystemUsers.findOne({ email });
  } catch (err) {
    result.databaseError = true;
    return result;
  }

  if (system_user && system_user.status === "block") {
    result.unAuthorised = true;
    return result;
  }
  if (system_user && (await bcrypt.compare(password, system_user.password))) {
    let tokenResponse;
    let system_user_type = "";

    if (type && type.trim().toLowerCase() === "vendor")
      tokenResponse = await SystemUserHelper.generateJwtToken(
        system_user,
        "vendor"
      );
    else {
      // set user type for business_owner or super_admin
      // if (system_user.role) {
      //   system_user_type = system_user.role;
      // } else {
      //   if (system_user.admin_type) {
      //     system_user_type = system_user.admin_type;
      //   }
      //   if (system_user.discover_type) {
      //     system_user_type = system_user.discover_type;
      //   }
      // }
      system_user_type = system_user.role;
      if(['admin'].includes(system_user.role)){
        system_user_type = system_user.admin_type;
      }
      if(['discover_user'].includes(system_user.role)){
        system_user_type = system_user.discover_type;
      }
      if(['commerce_user'].includes(system_user.role)){
        system_user_type = system_user.commerce_type;
      }

      tokenResponse = await SystemUserHelper.generateJwtToken(
        system_user,
        system_user_type
      );
    }

    if (tokenResponse.token) {
      let sessionDetails;

      if (type && type.trim().toLowerCase() === "vendor") {
        sessionDetails = {
          user_type: "vendor",
          system_user_id: system_user._id,
          session_id: tokenResponse.session_id,
          ip_address: await SystemUserHelper.getClientIP(req),
          user_agent: req.headers["user-agent"],
        };
      } else {
        sessionDetails = {
          user_type: system_user_type,
          system_user_id: system_user._id,
          session_id: tokenResponse.session_id,
          ip_address: await SystemUserHelper.getClientIP(req),
          user_agent: req.headers["user-agent"],
        };
      }

      await new Session(sessionDetails).save();

      let data;

      if (type && type.trim().toLowerCase() === "vendor") {
        data = {
          token: tokenResponse.token,
          role: "vendor",
        };
      } else {
        data = {
          token: tokenResponse.token,
          role: system_user_type,
        };
      }

      result.data = data;
    } else result.tokenNotGenerated = true;

    return result;
  } else {
    result.invalidCredentials = true;
    return result;
  }
}

// Function for logging out system user
async function logout(user_id, session_id) {
  let result = {};

  try {
    if (user_id) {
      const deleteSession = await Session.deleteMany({
        system_user_id: user_id,
      });

      if (deleteSession.deletedCount > 0) {
        result.userLoggedOut = true;
        return result;
      } else {
        result.noSessionFound = true;
        return result;
      }
    }
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function addServiceProvider(req, data) {
  let system_user,
    result = {};

  if (req.user_type == "business_owner") {
    try {
      system_user = await SystemUsers.findOne({ email: data.email });

      if (system_user) {
        result.userAlreadyExist = true;
        result.message = "User already exists with same or different role.";
        return result;
      }

      const hashed_password = await bcrypt.hash(data.password, 12);

      system_user = { ...data, password: hashed_password };
      system_user.role = "service_provider";
      (system_user.business_owner_id = req.user_id),
        (system_user = await new SystemUsers(system_user).save());

      result.serviceProviderAdded = true;
      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorizedAccess = true;
    return result;
  }
}

async function updateServiceProvider(req, data) {
  let system_data,
    result = {};

  if (req.user_type == "business_owner") {
    try {
      let system_user = await SystemUsers.findOne({
        _id: req.params.service_provider_id,
      });

      if (!system_user) {
        result.noUserFound = true;
        return result;
      }

      system_data = data;

      const existingServiceProvider = await SystemUsers.findOne({
        email: data.email,
      });

      if (
        existingServiceProvider &&
        existingServiceProvider._id.toString() !==
          req.params.service_provider_id
      ) {
        result.userAlreadyExist = true;
        result.message = "User already exist with same or different role.";
        return result;
      }

      if (system_data.old_password && system_data.new_password) {
        if (system_user.password) {
          const confirm_old_password = await bcrypt.compare(
            system_data.old_password,
            system_user.password
          );

          if (confirm_old_password) {
            const hashed_password = await bcrypt.hash(data.new_password, 12);
            system_data.password = hashed_password;
          } else {
            result.invalidPassword = true;
            result.message = "Old Password is not correct.";
            return result;
          }
        } else {
          const hashed_password = await bcrypt.hash(data.new_password, 12);
          system_data.password = hashed_password;
        }
      }

      await SystemUsers.updateOne(
        { _id: req.params.service_provider_id },
        system_data,
        { new: true }
      );

      result.serviceProviderUpdated = true;
      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorizedAccess = true;
    return result;
  }
}

/// Update Author
async function updateAuthor(req, data) {
  let system_data,
    result = {};

  if (req.user_type == "discover" || req.user_type == "author") {
    try {
      let system_user = await SystemUsers.findOne({ _id: req.body.author_id });

      if (!system_user) {
        result.noUserFound = true;
        return result;
      }

      system_data = data;

      await SystemUsers.updateOne({ _id: req.body.author_id }, system_data, {
        new: true,
      });

      result.authorUpdated = true;
      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorizedAccess = true;
    return result;
  }
}

/// Update Editor
async function updateEditor(req, data) {
  let system_data,
    result = {};

  if (req.user_type == "discover" || req.user_type == "editor") {
    try {
      //Todo: To keep check for the parent Id.

      let system_user = await SystemUsers.findOne({ _id: req.body.editor_id });

      if (!system_user) {
        result.noUserFound = true;
        return result;
      }

      system_data = data;

      await SystemUsers.updateOne({ _id: req.body.editor_id }, system_data, {
        new: true,
      });

      result.editorUpdated = true;
      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorizedAccess = true;
    return result;
  }
}

// UPDATE SYSTEM USER PROFILE
async function updateSystemUserProfile(req) {
  const { user_type, user_id } = req;

  const userData = req.body;

  let result = {};

  try {
    if (userData.email) {
      // check if email is already in use or not;
      const validEmail = await SystemUsers.findOne(
        { email: userData.email },
        { email: 1 }
      );

      if (!validEmail) {
        result.dataError = true;
        result.message = "This email is already exists";
        return result;
      }
    }

    const updateStatus = await SystemUsers.updateOne(
      { _id: user_id },
      userData,
      { new: true }
    );

    if (updateStatus) {
      result.success = true;
      result.message = "User details Updated successfully";
      return result;
    } else {
      result.dataError = true;
      result.message = "Update failed";
      return result;
    }
  } catch (err) {
    console.log(err);
    result.databaseError = true;
    return result;
  }
}

async function getServiceProviders(user_id) {
  let result = {};

  try {
    const service_providers = await SystemUsers.find({
      business_owner_id: user_id,
    }).select("-password");

    if (service_providers.length) {
      result.service_providers = service_providers;
      return result;
    } else {
      result.noServiceProvidersFound = true;
      result.message = "No service provider found";
      return result;
    }
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function getAuthors(req) {
  let result = {};

  const { user_id, user_type } = req;

  if (["discover", "super_admin", "editor"].includes(user_type)) {
    try {
      const authors = await SystemUsers.find({
        discover_type: "author",
      }).select("-password");

      if (authors.length) {
        result.authors = authors;
      } else {
        result.noAuthorsFound = true;
        result.message = "No authors found";
      }

      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

async function getEditors(req) {
  let result = {};

  const { user_id, user_type } = req;

  if (["discover", "super_admin"].includes(user_type)) {
    try {
      const editors = await SystemUsers.find({
        discover_type: "editor",
      }).select("-password");

      if (editors.length) {
        result.editors = editors;
      } else {
        result.noEditorsFound = true;
        result.message = "No editors found";
      }

      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

async function removeAuthor(req) {
  let result = {};

  const { user_id, user_type } = req;
  const { author_id } = req.params;

  if (["discover", "super_admin"].includes(user_type)) {
    try {
      // find if author is exist or not
      const authorStatus = await SystemUsers.findOne({
        _id: author_id,
        discover_type: "author",
      });

      if (!authorStatus) {
        result.authorNotFound = true;
        return result;
      }

      // Update all articles's author and assgin new [who is current user_id]
      // ArticleController.updateAuthor(old_author,new_author)

      const old_author = author_id;
      const new_author = user_id;

      const authorUpdateStatus = await ArticleController.updateAuthor(
        old_author,
        new_author
      );
      let updateStatus = 0;

      if (authorUpdateStatus) {
        updateStatus = authorUpdateStatus.nModified;
      }

      const deleteStatus = await SystemUsers.deleteOne({
        _id: author_id,
        discover_type: "author",
      });

      if (deleteStatus.deletedCount > 0) {
        result.success = true;
        result.message = `Successfully deleted author ${authorStatus.fname}, ${updateStatus} articles affected.`;
      } else {
        result.failure = true;
        result.message = "Failure at deleting this author, please try again";
      }

      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

async function removeEditor(req) {
  let result = {};

  const { user_id, user_type } = req;
  const { editor_id } = req.params;

  if (["discover", "super_admin"].includes(user_type)) {
    try {
      // find if editor is exist or not
      const editorStatus = await SystemUsers.findOne({
        _id: editor_id,
        discover_type: "editor",
      });

      if (!editorStatus) {
        result.editorNotFound = true;
        return result;
      }

      const deleteStatus = await SystemUsers.deleteOne({
        _id: editor_id,
        discover_type: "editor",
      });

      if (deleteStatus.deletedCount > 0) {
        result.success = true;
        result.message = `Successfully deleted editor ${editorStatus.fname}.`;
      } else {
        result.failure = true;
        result.message = "Failure at deleting this editor, please try again";
      }

      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

async function getDashboard(req) {
  const { user_type, user_id } = req;
  const { data_for } = req.query;

  let result = {};

  if (
    [
      "super_admin",
      "discover",
      "commerce",
      "author",
      "editor",
      "business_owner",
    ].includes(user_type)
  ) {
    try {
      console.log(user_type, data_for);

      if (user_type === "super_admin") {
        if (data_for === "discover") {
          const discoverDashboardData =
            await ArticleController.getSuperAdminDashboardData(req);
          return discoverDashboardData;
        } else if (data_for === "commerce") {
        }
      } else if (user_type === "discover") {
        const discoverAdminDashboardData =
          await ArticleController.getDiscoverAdminDashboardData(req);
        return discoverAdminDashboardData;
      } else if (user_type === "author") {
        const discoverAuthorDashboardData =
          await ArticleController.getDiscoverAuthorDashboardData(req);
        return discoverAuthorDashboardData;
      } else if (user_type === "editor") {
        const discoverEditorDashboardData =
          await ArticleController.getDiscoverEditorDashboardData(req);
        return discoverEditorDashboardData;
      }
    } catch (err) {
      console.log(err);
      result.databaseError = true;
      return true;
    }
  } else {
    let result = {};
    result.unAuthorised = true;

    return result;
  }
}

async function getUserDetails(req) {
  const { user_type, user_id } = req;

  let result = {};

  console.log(user_type);
  if (
    [
      "super_admin",
      "discover",
      "commerce",
      "author",
      "editor",
      "business_owner",
    ].includes(user_type)
  ) {
    try {
      query = {};
      if (["super_admin", "business_owner",'admin'].includes(user_type)) {
        query = { _id: user_id, role: user_type };
      } else if (["discover", "commerce"].includes(user_type)) {
        query = { _id: user_id, admin_type: user_type };
      } else if (["author", "editor"].includes(user_type)) {
        query = { _id: user_id, discover_type: user_type };
      } else {
        result.unAuthorised = true;
        return result;
      }

      const userData = await SystemUsers.findOne(query).select("-password");

      if (userData) {
        result.success = true;
        result.userDetails = userData;
        return result;
      } else {
        result.dataNotFound = true;
        result.message = "User Details not found";
        return result;
      }
    } catch (err) {
      console.log(err);

      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

//Get admins details
async function getAdmins(req) {
  let result = {};

  const { user_id, user_type } = req;

  if (["super_admin"].includes(user_type)) {
    try {
      const admins = await SystemUsers.find({
        admin_type: { $exists: true },
      }).select("-password");

      if (admins.length) {
        result.admins = admins;
      } else {
        result.noAdminsFound = true;
        result.message = "No admins found";
      }

      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

//Remove admins
async function removeAdmin(req) {
  let result = {};

  const { user_id, user_type } = req;
  const { admin_id } = req.params;

  if (["super_admin"].includes(user_type)) {
    try {
      // find if admin is exist or not
      const adminStatus = await SystemUsers.findOne({
        _id: admin_id,
        admin_type: { $exists: true },
      });

      if (!adminStatus) {
        result.adminNotFound = true;
        return result;
      }

      const deleteStatus = await SystemUsers.deleteOne({
        _id: admin_id,
        admin_type: { $exists: true },
      });

      if (deleteStatus.deletedCount > 0) {
        result.success = true;
        result.message = `Successfully deleted admin ${adminStatus.fname}.`;
      } else {
        result.failure = true;
        result.message = "Failure at deleting this admin, please try again";
      }

      return result;
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

// Active / Block [ admin , author , editor ]
async function setSystemUserStatus(req) {
  const { user_id, user_type } = req;
  const { system_user_id, status } = req.body;

  let result = {};

  if (["super_admin"].includes(user_type)) {
    try {
      if (["active", "block"].includes(status) && system_user_id) {
        const updateSystemUserStatus = await SystemUsers.updateOne(
          { _id: system_user_id },
          { status: status }
        );

        if (updateSystemUserStatus) {
          const deleteSession = await Session.deleteMany({
            system_user_id: system_user_id,
          });

          console.log(deleteSession);

          result.systemUserStatusUpdated = true;
          return result;
        } else {
          result.nothingUpdated = true;
          return result;
        }
      } else {
        result.nothingUpdated = true;
        return result;
      }
    } catch (err) {
      result.databaseError = true;
      return result;
    }
  } else {
    result.unAuthorised = true;
    return result;
  }
}

module.exports = {
  register,
  registerSuperUser,
  isUserSessionActive,
  logout,
  login,
  addServiceProvider,
  updateServiceProvider,
  getServiceProviders,
  updateAuthor,
  updateEditor,
  getAuthors,
  getEditors,
  getAdmins,
  removeAdmin,
  removeAuthor,
  setSystemUserStatus,
  removeEditor,
  getDashboard,
  getUserDetails,
  updateSystemUserProfile,
};