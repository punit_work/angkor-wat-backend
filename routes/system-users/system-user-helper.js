const jwt = require('jsonwebtoken');
const {v4: uuidv4} = require('uuid');

// Function for generating jwt token for secure authentication
async function generateJwtToken(user, user_type) {

    let token, result = {}, session_id;

    try {

        // Generating a unique session_id
        session_id = uuidv4();

        // Signing jwt payload along with secret key and options config
        token = jwt.sign({user_type, user_id: user._id, session_id,status:user.status}, process.env.JWT_SECRET_KEY);
    
    } catch (err) {

        result.tokenNotGenerated = true;
        return result;
    }

    if (token) {

        result.token = token;
        result.session_id = session_id;
        return result;
    }
}

// Function for getting client IP
async function getClientIP(requestObject) {

    with(requestObject)
    return (headers['x-forwarded-for'] || '').split(',')[0] || connection.remoteAddress
}

module.exports = {
    generateJwtToken,
    getClientIP
}