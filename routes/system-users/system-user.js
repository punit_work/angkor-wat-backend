const SystemUserController = require('./system-user-controller');
const express = require('express');
const router = express.Router();
const { body, validationResult, query, param } = require('express-validator');
const Response = require('../../global/response');
const ResponseCode = require('../../global/code');
const ResponseMessage = require('../../global/message');
const uploadDocument = require('../../middleware/upload-document');
const uploadCategoryImage = require('../../middleware/upload-category-image');
const authCheck = require('../../middleware/check-auth');
const { doFileExist } = require('../../global/fileCheck');
const ShopController = require('../shop/shop-controller');
const CategoryController = require('../category/category-controller');
const ArticleController = require('../article/article-controller');
const ServiceController = require('../services/services-controller');
const ServiceHelper = require('../services/services-helper');
const ProvinceController = require('../provinces/province-controller');
const PaymentController = require('../payment/payment-controller');
const TempleController = require('../temple/temple-controller');
const TagController = require('../tag/tag-controller');

// Path - /v1/system-user/register
// Route for registration of system user
router.post('/register',

    authCheck,
    // Multer middleware for storing user documents 
    uploadDocument.single('id_proof'),

    [
        body('email').trim().normalizeEmail().isEmail().withMessage('Parameter [email] is missing or invalid.'),
        body('fname').trim().isLength({ min: 1 }).withMessage('Parameter [fname] is missing or invalid.'),
        body('lname').trim().isLength({ min: 1 }).withMessage('Parameter [lname] is missing or invalid.'),
        body('password').trim().isLength({ min: 8 }).withMessage('Parameter [password] is missing or invalid.'),
        body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
        body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
        body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
        body("admin_type").optional().notEmpty().withMessage("Please provide valid admin_type"),
        body("discover_type").optional().notEmpty().withMessage("Please provide valid discover_type"),
        body("commerce_type").optional().notEmpty().withMessage("Please provide valid commerce_type"),
        body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
        body('address').optional().trim().isLength({ min: 5 }).withMessage('Please provide valid address')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.register(req, req.body, req.file);

        // Sending error response if database error
        if (response.databaseError) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        // Sending error response if user already exist
        else if (response.userAlreadyExist) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.USER_ALREADY_EXIST);
        }

        // Sending error response if jwt token is not generated
        else if (response.tokenNotGenerated) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN + ' - while generating token.');
        }

        // Sending jwt token in response if generated
        else if (response.token) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_REGISTERED_SUCCESSFULLY, response);
    }
)

// // Path - /v1/system-user/register-super-user
// // Route for registration of system user
// router.post('/register-super-user',

//     // authCheck,
//     // Multer middleware for storing user documents 
//     uploadDocument.single('id_proof'),

//     [
//         body('email').trim().normalizeEmail().isEmail().withMessage('Parameter [email] is missing or invalid.'),
//         body('fname').trim().isLength({ min: 1 }).withMessage('Parameter [fname] is missing or invalid.'),
//         body('lname').trim().isLength({ min: 1 }).withMessage('Parameter [lname] is missing or invalid.'),
//         body('password').trim().isLength({ min: 8 }).withMessage('Parameter [password] is missing or invalid.'),
//         body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
//         body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
//         body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
//         body("admin_type").optional().notEmpty().withMessage("Please provide valid admin_type"),
//         body("discover_type").optional().notEmpty().withMessage("Please provide valid discover_type"),
//         body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
//         body('address').optional().trim().isLength({ min: 5 }).withMessage('Please provide valid address')
//     ],

//     async (req, res) => {

//         // Checking if req.body has any error
//         const error = validationResult(req);

//         if (!error.isEmpty()) {

//             doFileExist(req.file);
//             return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
//                 field: error.param,
//                 errorMessage: error.msg
//             })));
//         }

//         const response = await SystemUserController.registerSuperUser(req, req.body, req.file);

//         // Sending error response if database error
//         if (response.databaseError) {

//             doFileExist(req.file);
//             return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
//         }

//         // Sending error response if user already exist
//         else if (response.userAlreadyExist) {

//             doFileExist(req.file);
//             return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.USER_ALREADY_EXIST);
//         }

//         // Sending error response if jwt token is not generated
//         else if (response.tokenNotGenerated) {

//             doFileExist(req.file);
//             return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN + ' - while generating token.');
//         }

//         // Sending jwt token in response if generated
//         else if (response.token) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_REGISTERED_SUCCESSFULLY, response.token);
//     }
// )

router.post('/add-service-provider',

    authCheck,

    [
        body('email').trim().normalizeEmail().isEmail().withMessage('Parameter [email] is missing or invalid.'),
        body('fname').trim().isLength({ min: 1 }).withMessage('Parameter [fname] is missing or invalid.'),
        body('lname').trim().isLength({ min: 1 }).withMessage('Parameter [lname] is missing or invalid.'),
        body('password').trim().isLength({ min: 8 }).withMessage('Parameter [password] is missing or invalid.'),
        body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
        body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
        body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
        body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
        body('address').optional().trim().isLength({ min: 5 }).withMessage('Please provide valid address')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.addServiceProvider(req, req.body);

        // Sending error response if database error
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        // Sending error response if user already exist
        else if (response.userAlreadyExist) {
            return Response.error(res, ResponseCode.BAD_REQUEST, response.message);
        }

        else if (response.serviceProviderAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SYSTEM_PROVIDER_ADDED_SUCCESSFULLY);
    }
)

router.post('/update-service-provider/:service_provider_id',

    authCheck,

    [
        param('service_provider_id').notEmpty().isMongoId().withMessage('Parameter [service_provider_id] is missing or invalid'),
        body('email').optional().trim().normalizeEmail().isEmail().withMessage('Parameter [email] is missing or invalid.'),
        body('fname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [fname] is invalid.'),
        body('lname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [lname] is invalid.'),
        body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
        body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
        body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
        body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
        body('address').optional().trim().isLength({ min: 5 }).withMessage('Please provide valid address'),

    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.updateServiceProvider(req, req.body);

        // Sending error response if database error
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.invalidPassword) {

            return Response.error(res, ResponseCode.BAD_REQUEST, response.message);
        }

        // Sending error response if user already exist
        else if (response.noUserFound) {
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND);
        }

        // Sending error response if user already exist
        else if (response.userAlreadyExist) {
            return Response.error(res, ResponseCode.BAD_REQUEST, response.message);
        }

        else if (response.serviceProviderUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SYSTEM_PROVIDER_UPDATED_SUCCESSFULLY);
    }
)


/// Route to update author
router.post('/update-author',

    authCheck,

    [
        body('author_id').notEmpty().isMongoId().withMessage('Parameter [author_id] missing or invalid'),
        body('fname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [fname] is invalid.'),
        body('lname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [lname] is invalid.'),
        body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
        body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
        body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
        body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
        body('address').optional().trim().isLength({ min: 5 }).withMessage('Please provide valid address'),
        body("age").optional().notEmpty().isNumeric().withMessage("Please provide valid age"),
        body("gender").optional().notEmpty().isString().withMessage("Please provide valid gender"),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (req.body.gender) {
            if (!['male', 'female', 'others'].includes(req.body.gender)) {
                return Response.error(
                    res,
                    ResponseCode.UNPROCESSABLE_ENTITY,
                    "Please provide valid gender."
                );

            }
        }

        const response = await SystemUserController.updateAuthor(req, req.body);

        // Sending error response if database error
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        // Sending error response if user already exist
        else if (response.noUserFound) {
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND);
        }

        else if (response.unAuthorizedAccess) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.UNAUTHORIZED_ACCESS);

        else if (response.authorUpdated) return Response.success(res, ResponseCode.SUCCESS, "Author updated success.");
    }
)

/// Route to update editor
router.post('/update-editor',

    authCheck,

    [
        body('editor_id').notEmpty().isMongoId().withMessage('Parameter [editor_id] missing or invalid'),
        body('fname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [fname] is invalid.'),
        body('lname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [lname] is invalid.'),
        body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
        body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
        body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
        body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
        body('address').optional().trim().isLength({ min: 5 }).withMessage('Please provide valid address'),
        body("age").optional().notEmpty().isNumeric().withMessage("Please provide valid age"),
        body("gender").optional().notEmpty().isString().withMessage("Please provide valid gender"),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (req.body.gender) {
            if (!['male', 'female', 'others'].includes(req.body.gender)) {
                return Response.error(
                    res,
                    ResponseCode.UNPROCESSABLE_ENTITY,
                    "Please provide valid gender."
                );

            }
        }
        const response = await SystemUserController.updateEditor(req, req.body);

        // Sending error response if database error
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.unAuthorizedAccess) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.UNAUTHORIZED_ACCESS);

        // Sending error response if user already exist
        else if (response.noUserFound) {
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NO_USER_FOUND);
        }

        else if (response.editorUpdated) return Response.success(res, ResponseCode.SUCCESS, "Editor updated success.");
    }
)

router.get('/get-service-providers',

    authCheck,

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.getServiceProviders(req.user_id);

        // Sending error response if database error
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.noServiceProvidersFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }

        else if (response.service_providers) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SYSTEM_PROVIDER_FETCHED_SUCCESSFULLY, response.service_providers);
    }
)

// Path - /v1/system-user/login
// Route for system user login
router.post('/login',

    // Handling validations for request body
    [
        body('email').trim().isEmail().normalizeEmail().withMessage('Paramater [email] is missing or invalid'),
        body('password').trim().isLength({ min: 1 }).withMessage('Paramater [password] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const { email, password, type } = req.body;

        const response = await SystemUserController.login(req, email, password, type);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        if(response.unAuthorised) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED + " user is currently blocked by admin");
        // Sending error response if invalid credentials
        else if (response.invalidCredentials) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.INVALID_CREDENTIALS);

        // Sending error response if token not generated
        else if (response.tokenNotGenerated) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN + ' - while generating token.');

        // Sending token if system user logged in successfully
        else if (response.data) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_IN_SUCCESSFULLY, response.data);
    }
)

// Path - /v1/system-user/logout
// Route for system user logout
router.get('/logout',

    // Middleware to check if user is authenticated
    authCheck,

    async (req, res) => {

        const response = await SystemUserController.logout(req.user_id, req.session_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no session found
        else if (response.noSessionFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_SESSION_FOUND);

        // Sending success response if user logged out 
        else if (response.userLoggedOut) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.USER_LOGGED_OUT_SUCCESSFULLY);
    }
)

// Path - /v1/system-user/get-shops
// Route for fetching shops of owner
router.get('/get-shops', query("ownerId").not().isEmpty().withMessage("Please provide owner id"), async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return Response.error(
            res,
            ResponseCode.BAD_REQUEST,
            errors.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg,
            }))
        );
    }

    const response = await ShopController.getAllShops(req);
    if (response.databaseError) {
        return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
    } else if (response.shops) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.RETRIVED_STORES_DATA_SUCCESSFULLY, response.shops)
})

// Path - /v1/system-user/add-new-category
// Route for adding new category by system users
router.post('/add-new-category',

    // Middleware to check if user is authenticated
    authCheck,

    uploadCategoryImage.single('category_image'),

    [
        body('category_for').trim().isLength({ min: 1 }).withMessage('Parameter [category_for] is missing or invalid'),
        body('category_order').optional().isNumeric().withMessage('Category Order should be valid number'),
    ],

    async (req, res) => {

        if (!req.file) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.FILE_NOT_FOUND)

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (!['shop', 'product', 'tour package', 'service', 'article'].includes(req.body.category_for.toLowerCase())) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.PARAMETER_CATEGORY_FOR_IS_INVALID)
        }

        
        if (req.body.sub_category && !Array.isArray(JSON.parse(req.body.sub_category))) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.BAD_REQUEST, 'Parameter [sub_category] is missing or invalid')
        }

        const response = await CategoryController.addNewCategory(req.user_type, req.body, req.file);

        // Sending error response if database error
        if (response.databaseError) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) {

            doFileExist(req.file);
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');
        }
        else if (response.categoryAlreadyExists){

            doFileExist(req.file);
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, 'This category already exists !');

        }

        // Sending success response if category added
        else if (response.categoryAdded) {

            return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.CATEGORY_ADDED);
        }
    }
)

// Path - /v1/system-user/update-category
// Route for update category by system users
router.post('/update-category',

    // Middleware to check if user is authenticated
    authCheck,

    uploadCategoryImage.single('category_image'),

    [
        body('category_id').isMongoId().withMessage('Parameter [category_id] is missing or invalid'),
        body('category_for').optional().trim().isLength({ min: 1 }).withMessage('Parameter [category_for] is missing or invalid'),
        // body('category_name').optional().trim().isLength({ min: 3 }).withMessage('Category name should be minimum 3 characters long'),
        body('category_order').optional().isNumeric().withMessage('Category Order should be valid number'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (!req.file && !req.body.category_for  && !req.body.update_sub_category && !req.body.add_sub_category && !req.body.remove_sub_category && !req.body.update_sub_sub_category && !req.body.add_sub_sub_category && !req.body.remove_sub_sub_category) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, ResponseMessage.NOTHING_TO_UPDATE);
        }

        if (req.body.category_for && !['shop', 'product', 'tour package', 'service', 'article'].includes(req.body.category_for.toLowerCase())) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.PARAMETER_CATEGORY_FOR_IS_INVALID)
        }

        if (req.body.update_sub_category && !Array.isArray(JSON.parse(req.body.update_sub_category))) {

            console.log(JSON.parse(req.body.sub_category));
            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.BAD_REQUEST, 'Parameter [update_sub_category] is missing or invalid')
        }

        if (req.body.add_sub_category && !Array.isArray(JSON.parse(req.body.add_sub_category))) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.BAD_REQUEST, 'Parameter [add_sub_category] is missing or invalid')
        }

        if (req.body.remove_sub_category && !Array.isArray(JSON.parse(req.body.remove_sub_category))) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.BAD_REQUEST, 'Parameter [remove_sub_category] is missing or invalid')
        }

        const response = await CategoryController.updateCategory(req.user_type, req.body, req.file);

        // Sending error response if database error
        if (response.databaseError) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }


        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');
        }

        // Sending error response if no category found
        else if (response.noCategoryFound) {

            req.file && doFileExist(req.file);
            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_CATEGORY_FOUND);

        }

        // Sending success response if category updated
        else if (response.categoryUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.CATEGORY_UPDATED);
    }
)

// Path - /v1/system-user/change-category-order
// Route for update category order by system users
router.post('/change-category-order',authCheck,[
  body('category_id').isMongoId().withMessage('Parameter [category_id] is missing or invalid'),
  body('category_order').isNumeric().withMessage('Parameter [category_order] is missing or invalid'),
],async (req, res) => {

  // Checking if req.body has any error
  const error = validationResult(req);

  if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg
      })));
  }

  const response = await CategoryController.changeCategoryOrder(req.user_type,req.body);

  // Sending error response if database error
  if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

  // Sending error response if no business owner/admin found
  else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

  // Sending success response if category order is updated
  else if (response.orderUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.CATEGORY_ORDER_UPDATED);
})

// Path - /v1/system-user/remove-category/:category_id
// Route for removing category by system users
router.delete('/remove-category/:category_id',

    // Middleware to check if user is authenticated
    authCheck,

    [
        param('category_id').isMongoId().withMessage('Parameter [category_id] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await CategoryController.removeCategory(req.user_type, req.params.category_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending error response if no category found
        else if (response.noCategoryFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_CATEGORY_FOUND);

        else if (response.usedInArticle) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.CATEGORY_USED_IN_ARTICLE);

        // Sending success response if category removed
        else if (response.categoryRemoved) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.CATEGORY_REMOVED);
    }
)

// Path - /v1/system-user/add-province
// Route for adding new province by system users
router.post('/add-province',

    // Middleware to check if user is authenticated
    authCheck,

    [
        body('en').notEmpty().withMessage('Parameter [en] is missing or invalid'),
        body('kh').notEmpty().withMessage('Parameter [kh] is missing or invalid'),
        body('latitude').optional().trim().isLength({ min: 1 }).withMessage('Parameter [latitude] is missing or invalid'),
        body('longitude').optional().trim().isLength({ min: 1 }).withMessage('Parameter [longitude] is missing or invalid'),
        body('district').optional().isArray().withMessage('Please provide valid district array'),
        body('province_order').optional().isNumeric().withMessage('Parameter [province_order] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        // if (req.body.latitude && req.body.longitude) {

        //     const isLatLngInvalid = ServiceHelper.isLatLngValid(req.body.latitude, req.body.longitude);

        //     // Sending error response if lat-lng is invalid
        //     if (isLatLngInvalid) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_LAT_LNG);
        // }
        const response = await ProvinceController.addNewProvince(req.user_type, req.body);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        
        // Sending error response if validation error
        if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending success response if province added
        else if (response.provinceAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_ADDED);
    }
)

// Path - /v1/system-user/update-province
// Route for updating province by system users
router.post('/update-province',

    // Middleware to check if user is authenticated
    authCheck,

    [
        body('province_id').isMongoId().withMessage('Parameter [province_id] is missing or invalid'),
        body('en').notEmpty().withMessage('Parameter [en] is missing or invalid'),
        body('kh').notEmpty().withMessage('Parameter [kh] is missing or invalid'),
        body('latitude').notEmpty().withMessage('Parameter [latitude] is missing or invalid'),
        body('longitude').optional().trim().isLength({ min: 1 }).withMessage('Parameter [longitude] is missing or invalid'),
        body('district').optional().isArray().withMessage('Please provide valid district array'),
        body('province_order').optional().isNumeric().withMessage('Parameter [province_order] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (!req.body.province && !req.body.latitude && !req.body.longitude && !req.body.district) {

            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.NOTHING_TO_UPDATE);
        }

        // if (req.body.latitude && req.body.longitude) {

        //     const isLatLngValid = ServiceHelper.isLatLngValid(req.body.latitude, req.body.longitude);
        //     console.log(isLatLngValid);
        //     // Sending error response if lat-lng is invalid
        //     if (!isLatLngValid) return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.INVALID_LAT_LNG);
        // }

        const response = await ProvinceController.updateProvince(req.user_type, req.body);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending success response if province not found
        else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

        // Sending success response if province updated
        else if (response.provinceUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_UPDATED);
    }
)

// Path - /v1/system-user/delete-province/:province_id
// Route for deleting province by system users
router.delete('/delete-province/:province_id',

    // Middleware to check if user is authenticated
    authCheck,

    [
        param('province_id').isMongoId().withMessage('Parameter [province_id] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ProvinceController.deleteProvince(req.user_type, req.params.province_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending success response if province not found
        else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

        // Sending success response if province removed
        else if (response.provinceRemoved) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_REMOVED);
    }
)

// Path - /v1/system-user/add-district
// Route for adding district to a province
router.post('/add-district', authCheck, [
  body('province_id').isMongoId().withMessage('Parameter [province_id] is not a valid id'),
  body('en').notEmpty().withMessage('Parameter [en] is missing or invalid'),
  body('kh').notEmpty().withMessage('Parameter [kh] is missing or invalid'),
] ,async (req,res) => {
  // Checking if req.body has any error
  const error = validationResult(req);

  if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg
      })));
  }

  const response = await ProvinceController.addDistrict(req.user_type, req.body);

  // Sending error response if database error
  if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

  // Sending error response if no business owner/admin found
  else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

  // Sending success response if province not found
  else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

  else if (response.districtAlreadyExists) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.DISTRICT_ALREADY_EXISTS);

  // Sending success response if province updated
  else if (response.districtAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.DISTRICT_ADDED);
})

// Path - /v1/system-user/update-district
// Route for updating district to a province
router.post('/update-district', authCheck, [
  body('province_id').isMongoId().withMessage('Parameter [province_id] is not a valid id'),
  body('en').notEmpty().withMessage('Parameter [en] is missing or invalid'),
  body('kh').notEmpty().withMessage('Parameter [kh] is missing or invalid'),
] ,async (req,res) => {
  // Checking if req.body has any error
  const error = validationResult(req);

  if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg
      })));
  }

  const response = await ProvinceController.updateDistrict(req.user_type, req.body);

  // Sending error response if database error
  if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

  // Sending error response if no business owner/admin found
  else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

   // Sending error response if validation error
   if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);


  // Sending success response if province not found
  else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

  else if (response.districtAlreadyExists) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.DISTRICT_ALREADT_EXISTS);

  // Sending success response if province updated
  else if (response.provinceUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_UPDATED);
})

// Path - /v1/system-user/add-commune
// Route for adding commune to a district
router.post('/add-commune', authCheck, [
  body('province_id').isMongoId().withMessage('Parameter [province_id] is not a valid id'),
  body('district_id').isMongoId().withMessage('Parameter [district_id] is not a valid id'),
] ,async (req,res) => {
  // Checking if req.body has any error
  const error = validationResult(req);

  if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg
      })));
  }

  const response = await ProvinceController.addCommune(req.user_type, req.body);

  // Sending error response if database error
  if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

  // Sending error response if no business owner/admin found
  else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

  // Sending success response if province not found
  else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

  else if (response.noDistrictFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_DISTRICT_FOUND);

  // Sending success response if province updated
  else if (response.communeAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.COMMUNE_ADDED);
})

// Path - /v1/system-user/update-commune
// Route for adding commune to a district
router.post('/update-commune', authCheck, [
  body('province_id').isMongoId().withMessage('Parameter [province_id] is not a valid id'),
  body('district_id').isMongoId().withMessage('Parameter [district_id] is not a valid id'),
] ,async (req,res) => {
  // Checking if req.body has any error
  const error = validationResult(req);

  if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg
      })));
  }

  const response = await ProvinceController.updateCommune(req.user_type, req.body);

  // Sending error response if database error
  if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

  // Sending error response if no business owner/admin found
  else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

  // Sending success response if province not found
  else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

  else if (response.noDistrictFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_DISTRICT_FOUND);

  // Sending success response if province updated
  else if (response.communeUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.COMMUNE_UPDATED);
})

// Path - /v1/system-user/delete-commune
// Route for delete commune of a district
router.post('/delete-commune', authCheck, [
  body('province_id').isMongoId().withMessage('Parameter [province_id] is not a valid id'),
  body('district_id').isMongoId().withMessage('Parameter [district_id] is not a valid id'),
  body('commune_id').isMongoId().withMessage('Parameter [commune_id] is not a valid id'),
] ,async (req,res) => {
  // Checking if req.body has any error
  const error = validationResult(req);

  if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg
      })));
  }

  const response = await ProvinceController.deleteCommune(req.user_type, req.body);

  // Sending error response if database error
  if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

  // Sending error response if no business owner/admin found
  else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

  // Sending success response if province not found
  else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

  else if (response.noDistrictFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_DISTRICT_FOUND);

  // Sending success response if province updated
  else if (response.communeDeleted) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.COMMUNE_DELETED);
})

// Path - /v1/system-user/delete-district
// Route for delete district of a province
router.post('/delete-district', authCheck, [
  body('province_id').isMongoId().withMessage('Parameter [province_id] is not a valid id'),
  body('district_id').isMongoId().withMessage('Parameter [district_id] is not a valid id'),
] ,async (req,res) => {
  // Checking if req.body has any error
  const error = validationResult(req);

  if (!error.isEmpty()) {

      return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
          field: error.param,
          errorMessage: error.msg
      })));
  }

  const response = await ProvinceController.deleteDistrict(req.user_type, req.body);

  // Sending error response if database error
  if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

  // Sending error response if no business owner/admin found
  else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

  // Sending success response if province not found
  else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

  else if (response.noDistrictFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_DISTRICT_FOUND);

  // Sending success response if province updated
  else if (response.districtDeleted) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.COMMUNE_DELETED);
})


// Path - /v1/system-user/change-province-order
// Route for changing province order by system users
router.post('/change-province-order',

    // Middleware to check if user is authenticated
    authCheck,

    [
        body('province_id').isMongoId().withMessage('Parameter [province_id] is missing or invalid'),
        body('province_order').isInt().withMessage('Parameter [province_order] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ProvinceController.changeProvinceOrder(req.user_type, req.body.province_id, req.body.province_order);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending success response if province not found
        else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

        // Sending success response if province removed
        else if (response.province) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_UPDATED);
    }
)

// Path - /v1/system-user/change-district-order
// Route for changing district order by system users
router.post('/change-district-order',

    // Middleware to check if user is authenticated
    authCheck,
    [
        body('province_id').isMongoId().withMessage('Parameter [province_id] is missing or invalid'),
        body('district_id').isMongoId().withMessage('Parameter [district_id] is missing or invalid'),
        body('district_order').isInt().withMessage('Parameter [district_order] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ProvinceController.changeDistrictOrder(req.user_type, req.body);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner/admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending success response if province not found
        else if (response.noProvinceFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_PROVINCE_FOUND);

        // Sending success response if province removed
        else if (response.province) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.PROVINCE_UPDATED);
    }
)

// Path - /v1/system-user/mark-stores-as-featured
// Route for marking stores as featured by system users
router.post('/mark-stores-as-featured',

    authCheck,

    [
        body('shop_ids').isArray({ min: 1 }).withMessage('Parameter [shop_ids] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ShopController.markStoreAsFeatured(req.user_type, req.body.shop_ids);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin');

        // Sending success response if shops marked as featured
        else if (response.shopsMarkedAsFeatured) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHOPS_MARKED_AS_FEATURED);
    }
)

// Path - /v1/system-user/remove-stores-from-featured
// Route for unmarking stores from featured by system users
router.post('/remove-stores-from-featured',

    authCheck,

    [
        body('shop_ids').isArray({ min: 1 }).withMessage('Parameter [shop_ids] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ShopController.unmarkStoreFromFeatured(req.user_type, req.body.shop_ids);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin');

        // Sending success response if shops unmarked from featured
        else if (response.shopsUnMarkedFromFeatured) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SHOPS_UNMARKED_FROM_FEATURED);
    }
)


router.post('/mark-article-as-featured',
    authCheck,

    [
        body('article_ids').isArray({ min: 1 }).withMessage('Parameter [article_ids] is empty or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ArticleController.markArticleAsFeatured(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin');

        // Limit reached
        else if(response.limitReached && response.limit) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, `Please send less than ${response.limit} articles`);

        else if(response.limitReached) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, `Limit reached for featured articles please remove some articles from featured to add new`);
    

        // Sending success response if shops unmarked from featured
        else if (response.articlesMarkedAsFeatured) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.ARTICLE_MARKED_AS_FEATURES);



    }
)


router.post('/remove-articles-from-featured',

    authCheck,

    [
        body('article_ids').isArray({ min: 1 }).withMessage('Parameter [article_ids] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ArticleController.unmarkArticleFromFeatured(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin');

        // Sending success response if shops unmarked from featured
        else if (response.articlesUnMarkedFromFeatured) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.ARTICLE_UNMARKED_FROM_FEATURES);
    }
)

router.post('/mark-services-as-featured',
    authCheck,

    [
        body('service_ids').isArray({ min: 1 }).withMessage('Parameter [service_ids] is empty or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }
        console.log(req.article_ids);
        const response = await ServiceController.markServiceAsFeatured(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin');

        // Sending success response if shops unmarked from featured
        else if (response.servicesMarkedAsFeatured) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_MARKED_AS_FEATURES);



    }
)


router.post('/remove-services-from-featured',

    authCheck,

    [
        body('service_ids').isArray({ min: 1 }).withMessage('Parameter [service_ids] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ServiceController.unmarkServicesFromFeatured(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no admin found
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin');

        // Sending success response if shops unmarked from featured
        else if (response.servicesUnMarkedFromFeatured) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.SERVICES_UNMARKED_FROM_FEATURES);
    }
)

router.post('/add-payment-method',

    authCheck,

    [
        body('payment_method_name').isLength({ min: 1 }).withMessage('Parameter [payment_method_name] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (req.user_type !== 'admin') return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED);

        const response = await PaymentController.addPaymentMethod(req.body.payment_method_name);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if payment method already exist
        else if (response.paymentMethodAlreadyExist) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

        // Sending success response if payment method added
        else if (response.paymentMethod) return Response.success(res, ResponseCode.SUCCESS, response.message, response.paymentMethod);
    }
)

router.post('/update-payment-method',

    authCheck,

    [
        body('payment_method_id').isMongoId().withMessage('Parameter [payment_method_id] is missing or invalid'),
        body('payment_method_name').isLength({ min: 1 }).withMessage('Parameter [payment_method_name] is missing or invalid'),

    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (req.user_type !== 'admin') return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED);

        const response = await PaymentController.updatePaymentMethod(req.body.payment_method_name, req.body.payment_method_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if payment method already exist
        else if (response.paymentMethodAlreadyExist) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

        // Sending error response if no payment method found
        else if (response.noPaymentMethodFound) return Response.error(res, ResponseCode.NOT_FOUND, response.message);

        // Sending success response if payment method updated
        else if (response.paymentMethod) return Response.success(res, ResponseCode.SUCCESS, response.message, response.paymentMethod);
    }
)

router.delete('/delete-payment-method/:payment_method_id',

    authCheck,

    [
        param('payment_method_id').isMongoId().withMessage('Parameter [payment_method_id] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (req.user_type !== 'admin') return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED);

        const response = await PaymentController.deletePaymentMethod(req.params.payment_method_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no payment method found
        else if (response.noPaymentMethodFound) return Response.error(res, ResponseCode.NOT_FOUND, response.message);

        // Sending success response if payment method deleted
        else if (response.paymentMethodDeleted) return Response.success(res, ResponseCode.SUCCESS, response.message);
    }
)


router.post('/mark-shop-as-pickup-point',
    authCheck,

    [
        body('shop_ids').isArray({ min: 1 }).withMessage('Parameter [service_ids] is empty or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ShopController.markShopAsPickupPoint(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin and business owner');

        else if (response.notValidOwner) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, "Sorry your are not valid owner for this action.");

        // Sending success response if shops unmarked from featured
        else if (response.updatedShop) return Response.success(res, ResponseCode.SUCCESS, response.updatedShop + " Shops marked as pickup location");



    }
)


router.post('/unmark-shop-from-pickup-point',

    authCheck,

    [
        body('shop_ids').isArray({ min: 1 }).withMessage('Parameter [service_ids] is missing or invalid'),
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await ShopController.unmarkShopAsPickupPoint(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin and business owner');

        else if (response.notValidOwner) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, "Sorry your are not valid owner for this action.");

        // Sending success response if shops unmarked from featured
        else if (response.updatedShop) return Response.success(res, ResponseCode.SUCCESS, response.updatedShop + " Shops unmarked as pickup location");

    }
)

// access [ super_admin , discover , editor ]
router.get('/get-authors',

    authCheck,

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.getAuthors(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.UNAUTHORIZED_ACCESS);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }


        else if (response.noAuthorsFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }
        else if (response.authors) return Response.success(res, ResponseCode.SUCCESS, response.message, response.authors);
    }
)

// access [ super_admin , discover ]
router.get('/get-editors',

    authCheck,

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.getEditors(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.UNAUTHORIZED_ACCESS);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }


        else if (response.noEditorsFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }
        else if (response.editors) return Response.success(res, ResponseCode.SUCCESS, response.message, response.editors);
    }
)

// access [ super_admin ]
router.get('/get-admins',

    authCheck,

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.getAdmins(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }


        else if (response.noAdminsFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }
        else if (response.admins) return Response.success(res, ResponseCode.SUCCESS, response.message, response.admins);
    }
)

// access [ super_admin , discover ]
router.delete('/remove-author/:author_id',

    authCheck,

    [
        param('author_id').isMongoId().withMessage('Parameter [author_id] is missing or invalid')
    ],
    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (!req.params.author_id) {
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide [author_id] param ");
        }

        const response = await SystemUserController.removeAuthor(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.authorNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, "Author not found");
        }
        else if (response.failure) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, response.message);
        }
        else if (response.success) return Response.success(res, ResponseCode.SUCCESS, response.message);
    }
)

// access [ super_admin , discover ]
router.delete('/remove-editor/:editor_id',

    authCheck,

    [
        param('editor_id').isMongoId().withMessage('Parameter [editor_id] is missing or invalid')
    ],
    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (!req.params.editor_id) {
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide [editor_id] param ");
        }

        const response = await SystemUserController.removeEditor(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.editorNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, "Editor not found");
        }
        else if (response.failure) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, response.message);
        }
        else if (response.success) return Response.success(res, ResponseCode.SUCCESS, response.message);
    }
)


/// access['super_admin', 'discover', 'commerce', 'author', 'editor', 'business_owner']
router.get('/get-dashboard-data',

    authCheck,
    [
        query('data_for').optional().isString().withMessage("data_for param is required !")
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await SystemUserController.getDashboard(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.UNAUTHORIZED_ACCESS);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.dataNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }
        else if (response.success) return Response.success(res, ResponseCode.SUCCESS, response.message, response.dashboardData);
    }
)


// User data
router.get('/get-user-details',

    authCheck,

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }
        
        const response = await SystemUserController.getUserDetails(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.UNAUTHORIZED_ACCESS);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.dataNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }
        else if (response.success) return Response.success(res, ResponseCode.SUCCESS, response.message, response.userDetails);
    }
)

// Update system user profile
router.post('/update-system-user-profile',

    authCheck,

    [
        body('email').optional().trim().normalizeEmail().isEmail().withMessage('Parameter [email] is missing or invalid.'),
        body('fname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [fname] is invalid.'),
        body('lname').optional().trim().isLength({ min: 1 }).withMessage('Parameter [lname] is invalid.'),
        body("province").optional().notEmpty().isMongoId().withMessage("Please provide valid province id"),
        body("district").optional().notEmpty().isMongoId().withMessage("Please provide valid district id"),
        body("commune").optional().notEmpty().isMongoId().withMessage("Please provide valid commune id"),
        body("postal_code").optional().notEmpty().isNumeric().withMessage("Please provide valid postal code"),
        body('address').optional().trim().isLength({ min: 5 }).withMessage('Please provide valid address'),
        body("age").optional().notEmpty().isNumeric().withMessage("Please provide valid age"),
        body("gender").optional().notEmpty().isString().withMessage("Please provide valid gender")
    ],


    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (Object.keys(req.body).length === 0) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Nothing for update !");
            
        }

        if(req.body.age && req.body.age < 18){

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide valid age , age must be greater than 18");

        }
        if(req.body.gender && !['female','male','others'].includes(req.body.gender)){

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide valid gender, valid oprions ['female','male','others']");

        }
        if(!['super_admin','discover', 'commerce','author','editor','business_owner'].includes(req.user_type)){

            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, "You are not authorised for this action");

        }

        const response = await SystemUserController.updateSystemUserProfile(req);

        if(response.dataError){

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

        }

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, response.message);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.dataNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, response.message);
        }
        else if (response.success) return Response.success(res, ResponseCode.SUCCESS, response.message, response.message);
    }
)

// access [ super_admin ]
router.delete('/remove-admin/:admin_id',

    authCheck,

    [
        param('admin_id').isMongoId().withMessage('Parameter [admin_id] is missing or invalid')
    ],

    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        if (!req.params.admin_id) {
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, "Please provide [admin_id] param ");
        }

        const response = await SystemUserController.removeAdmin(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        // Sending error response if database error
        else if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        }

        else if (response.adminNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, "Admin not found");
        }
        else if (response.failure) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, response.message);
        }
        else if (response.success) return Response.success(res, ResponseCode.SUCCESS, response.message);
    }
)

// Active / Block user
router.post('/set-system-user-status', 

    authCheck,

    [
        body('system_user_id').isMongoId(),
        body('status').notEmpty().withMessage("Status is required and valid values are : ['active' , 'block' ]")
    ],

    async(req,res)=>{

         // Checking if req.body has any error
         const error = validationResult(req);

         if (!error.isEmpty()) {
 
             return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                 field: error.param,
                 errorMessage: error.msg
             })));
         }


         const {status,system_user_id} = req.body;

         if(!['active','block'].includes(status)){

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY,"Please select valid status from ['block,'active']")

         } 
 

         const response = await SystemUserController.setSystemUserStatus(req);

         if(response.unAuthorised){

            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS,ResponseMessage.ERROR_UNAUTHORIZED);

         }
         if(response.databaseError){

            return Response.error(res, ResponseCode.DATABASE_ERROR,ResponseMessage.ERROR_DATABASE);

         }

         if(response.systemUserStatusUpdated){

            return Response.success(res, ResponseCode.SUCCESS,"Successfully updated status");

         }

         if(response.nothingToUpdate){

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY,"Nothing updated");

         }

    }

)

// TEMPLE

// CREATE TEMPLE
// Path : v1/system-user/add-temple
router.post('/add-temple',

    // Middleware to check if user is authenticated
    authCheck,
    [ 
      body('en').notEmpty().withMessage('Parameter [en] is required'),
      body('kh').notEmpty().withMessage('Parameter [kh] is required'),
      body('province_id').trim().isMongoId().withMessage('Parameter [province_id] is not a valid'),
      body('temple_order').optional().isNumeric().withMessage('Parameter [temple_order] is should be a valid integer')
    ],
    async (req, res) => {
      const error = validationResult(req);

      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }

        const response = await TempleController.addNewTemple(req.user_type,req.body);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        
        // Sending error response if validation error
        if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending error response if user enters start date as past date
        else if (response.templeAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TEMPLE_ADDED, response.templeAdded);        
    }
)

//  UPDATE TEMPLE
// v1/system-user/update-temple/:templeId
router.post('/update-temple/:temple_id',

    // Middleware to check if user is authenticated
    authCheck,
    [ 
      param('temple_id').trim().isMongoId().withMessage('Parameter [temple_id] is required'),
      body('en').optional().notEmpty().withMessage('Parameter [en] is required'),
      body('kh').optional().notEmpty().withMessage('Parameter [kh] is required'),
      body('temple_order').optional().isInt().withMessage('Parameter [temple_order] is required'),
      body('province_id').optional().trim().isMongoId().withMessage('Parameter [province_id] is not a valid')
    ],
    async (req, res) => {
      const error = validationResult(req);

      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }
        const response = await TempleController.updateTemple(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if validation error
        if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);
        
        if (response.noTempleFound) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.NO_TEMPLE_FOUND);

        // Sending error response if user enters start date as past date
        else if (response.templeUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TEMPLE_UPDATED, response.templeUpdated);
    }
)

// DELETE TEMPLE
// v1/system-user/delete-temple/:templeId
router.delete('/delete-temple/:temple_id',

    // Middleware to check if user is authenticated
    authCheck,
    [ param('temple_id').trim().isMongoId().withMessage('Parameter [temple_id] is required') ],
    async (req, res) => {
      const error = validationResult(req);
        
      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }
        const response = await TempleController.deleteTemple(req.user_type,req.params.temple_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if user enters start date as past date
        else if (response.templeRemoved) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TEMPLE_REMOVED, response.templeRemoved);
    }
)

// CHANGE TEMPLE ORDER
// /v1/system-user/change-temple-order
router.post('/change-temple-order',
  authCheck,
  [
    body('temple_id').isMongoId().withMessage('Parameter [temple_id] is invalid'),
    body('temple_order').isInt().withMessage('Parameter [temple_order] is not a valid number')
  ],
  async (req,res) => {
    const { temple_id,temple_order } = req.body;
    const error = validationResult(req);
        
      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }
        const response = await TempleController.changeTempleOrder(req.user_type,temple_id,temple_order);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // if temple is not found by the given id
        else if (response.noTempleFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_TEMPLE_FOUND);
       
        // if access is restricted
        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED);

        // Sending error response if user enters start date as past date
        else if (response.temple) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TEMPLE_UPDATED, response.temple);
  }
)

// TAG

// CREATE TAG
// Path : v1/system-user/add-tag
router.post('/add-tag',

    // Middleware to check if user is authenticated
    authCheck,
    [ 
      body('en').notEmpty().withMessage('Parameter [en] is required'),
      body('kh').notEmpty().withMessage('Parameter [kh] is required'),
    ],
    async (req, res) => {
      const error = validationResult(req);

      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }

        const response = await TagController.addNewTag(req.user_type,req.body);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);
        
        // Sending error response if validation error
        if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);

        else if (response.accessRestricted) return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ACCESS_RETRICTED + ' for other than admin/business owners');

        // Sending error response if user enters start date as past date
        else if (response.tagAdded) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TAG_ADDED, response.tagAdded);
    }
)

//  UPDATE TAG
// v1/system-user/update-tag/:tag_id
router.post('/update-tag/:tag_id',

    // Middleware to check if user is authenticated
    authCheck,
    [ 
      param('tag_id').trim().isMongoId().withMessage('Parameter [tag_id] is required'),
      body('en').optional().notEmpty().withMessage('Parameter [en] is required'),
      body('kh').optional().notEmpty().withMessage('Parameter [kh] is required'),
    ],
    async (req, res) => {
      const error = validationResult(req);

      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }
        const response = await TagController.updateTag(req);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if validation error
        if (response.validationError) return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, response.message);
        
        if (response.noTagFound) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.NO_TAG_FOUND);

        // Sending error response if user enters start date as past date
        else if (response.tagUpdated) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TAG_UPDATED, response.tagUpdated);
    }
)

// DELETE TAG
// v1/system-user/delete-tag/:tag_id
router.delete('/delete-tag/:tag_id',

    // Middleware to check if user is authenticated
    authCheck,
    [ param('tag_id').trim().isMongoId().withMessage('Parameter [tag_id] is required') ],
    async (req, res) => {
      const error = validationResult(req);
        
      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }
        const response = await TagController.deleteTag(req.user_type,req.params.tag_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if user enters start date as past date
        else if (response.tagRemoved) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TAG_REMOVED, response.tagRemoved);
    }
)

module.exports = router;