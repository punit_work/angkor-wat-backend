const Tag = require("../../model/Tag");

// Function for adding new province
async function addNewTag(user_type, { en,kh }) {
  let result = {};
  try {
    if (["discover", "super_admin"].includes(user_type)) {
      console.log(typeof en);
      console.log(kh);
      const translations = { 
        en:{
          tag:''
        },
        kh:{
          tag:''
        }
      }

      const addTranslatedData = async (data,lang) => {
        if(data.tag && data.tag !== ''){
          const tagExists = await Tag.findOne({[`translations.${lang}.tag`]:data.tag});
          
          if(tagExists){
            result.validationError = true
            result.message = `Tag name for ${lang} language already exists`
            return result
          } else {
            translations[lang].tag = data.tag;
          }
        } else {
          result.validationError = true
          result.message = `Tag name for ${lang} language is a required field`
          return result
        }
      }

      let hasError;
       hasError = await addTranslatedData(en,'en')
       hasError = await addTranslatedData(kh,'kh')

      if(hasError){
        return hasError;
      }

      await Tag.create({ translations });
      result.tagAdded = true;
      return result;
    }
    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for updating tag
async function updateTag(req) {
  const { en,kh, tag } = req.body;
  let result = {};
  const updateData = {};
  const translations = {
    en:{
      tag:''
    },
    kh:{
      tag:''
    }
  }
  
  const updateTranslatedData = async (data,lang) => {
    if(data.tag && data.tag !== ''){
      const tagExists = await Tag.findOne({[`translations.${lang}.tag`]:data.tag});
      
      if(tagExists && (tagExists._id && tagExists._id.toString() !== req.params.tag_id)){
        result.validationError = true
        result.message = `Tag name for ${lang} language already exists`
        return result
      } else {
        translations[lang].tag = data.tag;
      }
    } else {
      result.validationError = true
      result.message = `Tag name for ${lang} language is a required field`
      return result
    }
  }

  let hasError;
   hasError = await updateTranslatedData(en,'en')
   hasError = await updateTranslatedData(kh,'kh')

  if(hasError){
    return hasError;
  }

    updateData.translations = translations;

  try {
    const updated_tag = await Tag.findByIdAndUpdate(
      req.params.tag_id,
      { $set: updateData },
      { new: true }
    );

    if (updated_tag) {
      result.tagUpdated = true;
      return result;
    }

    result.noTagFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for deleting Tag
// v1/temples/:temple_id
async function deleteTag(user_type, tag_id) {
  let result = {};
  try {
    let tag = null;
    await Tag.findByIdAndRemove(tag_id, function (err, deletedTag) {
      if (deletedTag) {
        tag = deletedTag;
      }
    });
    if (tag) {
      result.tagRemoved = true;
      return result;
    } else {
      result.noTagFound = true;
      return result;
    }

    // result.accessRestricted = true;
    // return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for fetching list of province
async function getAllTags() {
  let result = {};
  try {
    const tag = await Tag.find({}).sort({ 'translations.en.tag': 1 });

    if (tag.length) {
      result.tag = tag;
      return result;
    }

    result.noTagFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}


module.exports = {
  addNewTag,
  getAllTags,
  deleteTag,
  updateTag,
};
