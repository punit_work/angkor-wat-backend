const express = require('express')
const router = express.Router();
const authCheck = require('../../middleware/check-auth');
const Response = require('../../global/response');
const ResponseMessage = require('../../global/message');
const ResponseCode = require('../../global/code');
const TagController = require('./tag-controller');
const {body, param, validationResult} = require('express-validator');

// FETCH ALL TEMPLES
// Path - /v1/temples
router.get('/',

    // Middleware to check if user is authenticated
    authCheck,

    async (req, res) => {

        const response = await TagController.getAllTags();

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner found
        else if (response.noTempleFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_TEMPLE_FOUND);

        // Sending error response if user enters start date as past date
        else if (response.tag) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TAG_FETCHED, response.tag);        
    }
)


module.exports = router;