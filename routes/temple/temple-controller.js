const Temple = require("../../model/Temple");

// Function for adding new province
async function addNewTemple(user_type, { en,kh , temple_order, province_id }) {
  let result = {};
  try {
    if (["discover", "super_admin"].includes(user_type)) {
      console.log(typeof en);
      console.log(kh);
      const translations = { 
        en:{
          temple_name:''
        },
        kh:{
          temple_name:''
        }
      }

      const addTranslatedData = async (data,lang) => {
        if(data.temple_name && data.temple_name !== ''){
          const templeExists = await Temple.findOne({[`translations.${lang}.temple_name`]:data.temple_name});
          
          if(templeExists){
            result.validationError = true
            result.message = `Temple name for ${lang} language already exists`
            return result
          } else {
            translations[lang].temple_name = data.temple_name;
          }
        } else {
          result.validationError = true
          result.message = `Temple name for ${lang} language is a required field`
          return result
        }
      }

      let hasError;
       hasError = await addTranslatedData(en,'en')
       hasError = await addTranslatedData(kh,'kh')

      if(hasError){
        return hasError;
      }

      await Temple.create({
        translations,
        temple_order: temple_order ? temple_order : 0,
        province_id,
      });
      result.templeAdded = true;
      return result;
    }
    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for updating temple
async function updateTemple(req) {
  const { en,kh, temple_order, province_id } = req.body;
  let result = {};
  const updateData = {};
  const translations = {
    en:{
      temple_name:''
    },
    kh:{
      temple_name:''
    }
  }
  
  const updateTranslatedData = async (data,lang) => {
    if(data.temple_name && data.temple_name !== ''){
      const templeExists = await Temple.findOne({[`translations.${lang}.temple_name`]:data.temple_name});
      
      if(templeExists && (templeExists._id && templeExists._id.toString() !== req.params.temple_id)){
        result.validationError = true
        result.message = `Temple name for ${lang} language already exists`
        return result
      } else {
        translations[lang].temple_name = data.temple_name;
      }
    } else {
      result.validationError = true
      result.message = `Temple name for ${lang} language is a required field`
      return result
    }
  }

  let hasError;
   hasError = await updateTranslatedData(en,'en')
   hasError = await updateTranslatedData(kh,'kh')

  if(hasError){
    return hasError;
  }

    updateData.translations = translations;
  if (temple_order) {
    updateData.temple_order = temple_order;
  }
  if (province_id && province_id !== "") {
    updateData.province_id = province_id;
  }

  try {
    const updated_temple = await Temple.findByIdAndUpdate(
      req.params.temple_id,
      { $set: updateData },
      { new: true }
    );

    if (updated_temple) {
      result.templeUpdated = true;
      return result;
    }

    result.noTempleFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for deleting Temple
// v1/temples/:temple_id
async function deleteTemple(user_type, temple_id) {
  let result = {};
  try {
    let temple = null;
    await Temple.findByIdAndRemove(temple_id, function (err, deletedTemple) {
      if (deletedTemple) {
        temple = deletedTemple;
      }
    });
    if (temple) {
      result.templeRemoved = true;
      return result;
    } else {
      result.noTempleFound = true;
      return result;
    }

    // result.accessRestricted = true;
    // return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

// Function for fetching list of province
async function getAllTemples() {
  let result = {};
  try {
    const temple = await Temple.find({}).sort({ temple_order: 1 });

    if (temple.length) {
      result.temple = temple;
      return result;
    }

    result.noTempleFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function getTempleByProvinceId(province_id) {
  let result = {};
  try {
    const temple = await Temple.find({ province_id }).sort({ temple_order: 1 });

    if (temple.length) {
      result.temple = temple;
      return result;
    }

    result.noTempleFound = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

async function changeTempleOrder(user_type, temple_id, temple_order) {
  let result = {};

  try {
    if(['admin','business_user','super_admin'].includes(user_type)){
      const temple = await Temple.findByIdAndUpdate(
        temple_id,
        {
          $set: { temple_order },
        },
        { new: true }
      );
  
      if (temple) {
        result.temple = temple;
        return result;
      }
  
      result.noTempleFound = true;
      return result;
    }
    result.accessRestricted = true;
    return result;
  } catch (err) {
    result.databaseError = true;
    return result;
  }
}

module.exports = {
  addNewTemple,
  getAllTemples,
  deleteTemple,
  updateTemple,
  getTempleByProvinceId,
  changeTempleOrder
};
