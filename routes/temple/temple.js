const express = require('express')
const router = express.Router();
const authCheck = require('../../middleware/check-auth');
const Response = require('../../global/response');
const ResponseMessage = require('../../global/message');
const ResponseCode = require('../../global/code');
const TempleController = require('./temple-controller');
const {body, param, validationResult} = require('express-validator');

// FETCH ALL TEMPLES
// Path - /v1/temples
router.get('/',

    // Middleware to check if user is authenticated
    authCheck,

    async (req, res) => {

        const response = await TempleController.getAllTemples();

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if no business owner found
        else if (response.noTempleFound) return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.NO_TEMPLE_FOUND);

        // Sending error response if user enters start date as past date
        else if (response.temple) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TEMPLE_FETCHED, response.temple);        
    }
)


// GET TEMPLE BY PROVINCE ID
// v1/temple/getTempleByProvince/:province_id
router.get('/get-temples-by-province/:province_id',

    // Middleware to check if user is authenticated
    authCheck,
    [ param('province_id').trim().isMongoId().withMessage('Parameter [province_id] is required') ],
    async (req, res) => {
      const error = validationResult(req);
        
      if (!error.isEmpty()) {
        return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
            field: error.param,
            errorMessage: error.msg
        })));
      }
        const response = await TempleController.getTempleByProvinceId(req.params.province_id);

        // Sending error response if database error
        if (response.databaseError) return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_UNKNOWN);

        // Sending error response if user enters start date as past date
        else if (response.temple) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.TEMPLE_FETCHED, response.temple);
    }
)




module.exports = router;