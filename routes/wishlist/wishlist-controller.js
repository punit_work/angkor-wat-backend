//Models
const product = require('../../model/Product');
const service = require('../../model/Service');
const wishlist = require('../../model/Wishlist');
const mongoose = require('mongoose');

// action model data => add - Wishlist - Product

//Add products to wishlist 
exports.addWishlistProduct = async(req) => {

    let result={}

    if(req.user_type === "end_user" ){

    try{
        
            //Check if product is already in wishlist or not    
            const isProductExist = await wishlist.findOne({user_id:req.user_id,product:req.body.product_id,product_variant:req.body.product_variant});

            if(isProductExist){
                result.productAlreadyInWishlist = true;
                return result;
            }

            const wishlistData = {user_id:req.user_id,product:req.body.product_id,product_variant:req.body.product_variant}
            result.wishlist = await new wishlist(wishlistData).save();
            
            return result;
        }
        catch(err){
            result.databaseError = true;
            return result;
        }

    }
    else{

        result.unAuthorised = true;
        return result;
    }

}

//Get all products from wishlist
exports.getAllWishlistProducts = async(req) => {

    let result = {};

    if(req.user_type === "end_user"){

        try{

            // const wishlistData = await wishlist.find({user_id:req.user_id, product : { $exists: true, $ne: null }}).populate("product");

            const basicFilter = [
                {
                    "$match":{
                        user_id:mongoose.Types.ObjectId(req.user_id),
                        product:{"$exists":true,"$ne":null}
                    }
                },
                {
                    "$lookup":{
                      "from":"products",
                      "as":"product",
                      "localField":"product",
                      "foreignField":"_id"
                    }
              
                },
                {
                    "$unwind":"$product"
                },
                {
                    "$addFields":{
                      "product.variant": {"$filter": {
                        "input": '$product.variants',
                       "as": 'var',
                        "cond": {"$eq": ['$$var._id','$product_variant']}}
                      }
                    }
                },
                {
                    "$lookup":{
                        "from":"shops",
                        "as":"shop",
                        "localField":"product.shop_id",
                        "foreignField":"_id"
                    }
                
                },
                {
                    "$unwind":"$shop"
                },
                {
                    "$lookup":{
                        "from":"ratings",
                        "as":"rating",
                        "localField":"product._id",
                        "foreignField":"product_id"
                    }
                 
                },
                {
                    "$unwind":"$rating"
                },
                {   "$unwind":"$product.variant" },
                {
                    "$project":{
                        "createdAt":1,
                        "product._id":1,
                        "product.product_name":1,
                        "product.product_images":1,
                        "product.variant":1,
                        "shop.shop_name":1,
                        "shop._id":1,
                        "rating.average_rating":1
                    }
                }
            ]

            const wishlistData = await wishlist.aggregate(basicFilter);
            
            if(wishlistData.length){
                result.wishlist = wishlistData;
                return result;

            }
            else{
                result.wishlistIsEmpty = true;
                return result;
            }

        }
        catch(err){
            console.log(err);
            result.databaseError = true;
            return result;

        }

    }
    else{
        result.unAuthorised = true;
        return result;
    }

}

//Remove products from wishlist
exports.removeWishlistProduct = async(req) => {

    let result = {};

    if(req.user_type === "end_user"){

        //remove product from wishlist

            try{

                const wishlistData = await wishlist.findOneAndRemove({_id:req.params.id,user_id:req.user_id});

                if(wishlistData){
                    result.wishlist = wishlistData;
                    return result;
                }
                else{
                    result.productNotFound = true;
                    return result;
                }
        }
        catch(err){
            console.log(err)
            result.databaseError = true;
            return result;

        }

    }
    else{
        result.unAuthorised = true;
        return result;
    }

}

//Remove multiple wishlist products
exports.removeMultipleWishlistProduct = async(req) => {

    let result = {};

    if(req.user_type === "end_user"){

        //remove product from wishlist
        const productIds = req.body.productIds.map((data)=>mongoose.Types.ObjectId(data));

            try{

                const wishlistData = await wishlist.remove({_id:{ $in: productIds },user_id:req.user_id});

                if(wishlistData.deletedCount>0){
                    result.wishlist = wishlistData.deletedCount;
                    return result;
                }
                else{
                    result.productNotFound = true;
                    return result;
                }
        }
        catch(err){
            console.log(err)
            result.databaseError = true;
            return result;

        }

    }
    else{
        result.unAuthorised = true;
        return result;
    }

}

//Empty all products from wishlist
exports.emptyWishlist = async(req) =>{
    
    let result = {}

    if(req.user_type === "end_user"){

        //check if there's products in wishlist or not

            try{

                const wishlistProduct = await wishlist.find({user_id:req.user_id});

                if(wishlistProduct.length){
                result.wishlist = await wishlist.deleteMany({user_id:req.user_id, product : { $exists: true, $ne: null }});
                return result

                }
                else{
                    result.productNotFound = true;
                    return result;
                }
        }
        catch(err){
           
            result.databaseError = true;
            return result;

        }


    }
    else{

        result.unAuthorised = true;
        return result;
        
    }
     


}



// EXPERIENCE ----------------------------------
//Add service to wishlist 
exports.addWishlistService = async(req) => {

    let result={}

    if(req.user_type === "end_user" ){

    try{

            //Check if service exists or not
            const serviceExists = await service.findOne({_id:req.body.service_id})

            if(!serviceExists){
                result.serviceNotExists = true;
                return result;
            }


        

            //Check if service is already in wishlist or not    
            const ServiceAlreadyInWishlist = await wishlist.findOne({user_id:req.user_id,service:req.body.service_id});

            if(ServiceAlreadyInWishlist){
                result.serviceAlreadyInWishlist = true;
                return result;
            }

            const wishlistData = {user_id:req.user_id,service:req.body.service_id}
            result.wishlist = await new wishlist(wishlistData).save();
            
            return result;
        }
        catch(err){
            result.databaseError = true;
            return result;
        }

    }
    else{

        result.unAuthorised = true;
        return result;
    }

}

//Get all services from wishlist
exports.getAllWishlistServices = async(req) => {

    let result = {};

    if(req.user_type === "end_user"){

        try{

            const basicFilter = [
                {
                    "$match":{
                        user_id:mongoose.Types.ObjectId(req.user_id),
                        service:{"$exists":true,"$ne":null}
                    }
                },
                {
                    "$lookup":{
                      "from":"services",
                      "as":"service",
                      "localField":"service",
                      "foreignField":"_id"
                    }
              
                },
                {
                    "$unwind":"$service"
                },
                {
                    "$lookup":{
                        "from":"ratings",
                        "as":"rating",
                        "localField":"service._id",
                        "foreignField":"service_id"
                    }
                 
                },
                {
                    "$unwind":{
                      "path":"$rating",
                      "preserveNullAndEmptyArrays":true
                    }
                },
                {
                    "$addFields":{
                        "service_description":{"$first":"$service.service_details"}
                    }
                },
                {
                    "$project":{
                        "createdAt":1,
                        "service._id":1,
                        "service.service_image":{"$first":"$service.service_images"},
                        "service.service_title":1,
                        "service.service_duration":"$service_description.duration",
                        "service.package":{"$first":"$service.package"},
                        "rating.average_rating":1

                    }
                }
            ]

            const wishlistData = await wishlist.aggregate(basicFilter);
            
            if(wishlistData.length){
                result.wishlist = wishlistData;
                return result;

            }
            else{
                result.wishlistIsEmpty = true;
                return result;
            }

        }
        catch(err){
            console.log(err)
            result.databaseError = true;
            return result;

        }

    }
    else{
        result.unAuthorised = true;
        return result;
    }

}

//Remove service from wishlist
exports.removeWishlistService = async(req) => {

    let result = {};

    if(req.user_type === "end_user"){

        //remove service from wishlist

            try{

                const wishlistData = await wishlist.findOneAndRemove({service:req.params.service_id,user_id:req.user_id});

                if(wishlistData){
                    result.wishlist = wishlistData;
                    return result;
                }
                else{
                    result.serviceNotFound = true;
                    return result;
                }
        }
        catch(err){
            
            result.databaseError = true;
            return result;

        }

    }
    else{
        result.unAuthorised = true;
        return result;
    }

}

//Remove multiple wishlist service
exports.removeMultipleWishlistService = async(req) => {

    let result = {};

    if(req.user_type === "end_user"){

        //remove product from wishlist
        const serviceIds = req.body.serviceIds.map((data)=>mongoose.Types.ObjectId(data));

            try{

                const wishlistData = await wishlist.remove({_id:{ $in: serviceIds },user_id:req.user_id});

                if(wishlistData.deletedCount>0){
                    result.wishlist = wishlistData.deletedCount;
                    return result;
                }
                else{
                    result.serviceNotFound = true;
                    return result;
                }
        }
        catch(err){
            console.log(err)
            result.databaseError = true;
            return result;

        }

    }
    else{
        result.unAuthorised = true;
        return result;
    }

}

//Empty all service from wishlist
exports.emptyWishlistService = async(req) =>{
    
    let result = {}

    if(req.user_type === "end_user"){

        //check if there's products in wishlist or not

            try{

                const wishlistProduct = await wishlist.find({user_id:req.user_id, service : { $exists: true, $ne: null }});

                if(wishlistProduct.length){
                result.wishlist = await wishlist.deleteMany({user_id:req.user_id});
                return result

                }
                else{
                    result.serviceNotFound = true;
                    return result;
                }
        }
        catch(err){
           
            result.databaseError = true;
            return result;

        }


    }
    else{

        result.unAuthorised = true;
        return result;
        
    }
     


}

