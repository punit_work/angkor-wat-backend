const express = require("express");
const router = express.Router();
const { body, validationResult, query, param } = require("express-validator");
const Response = require("../../global/response");
const ResponseCode = require("../../global/code");
const ResponseMessage = require("../../global/message");

//helper and controller
const WishlistController = require('./wishlist-controller');
const WishlistHelper = require('./wishlist-helper');

//middleware
const authCheck = require('../../middleware/check-auth');

// path - /v1/wishlist
//  All Routes for wishlist

//Add products to wishlist
router.post(
    "/add-wishlist-product",
    authCheck,
    [
        body('product_id').notEmpty().withMessage('Product is required'),
        body('product_variant').notEmpty().withMessage('Product variant is required')
    ],
    async (req, res) => {
        const error = validationResult(req);
        if (!error.isEmpty()) {
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await WishlistController.addWishlistProduct(req);

        if (response.unAuthorised) {

            return Response.error(res, ResponseCode.SUCCESS, ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if (response.productAlreadyInWishlist) {
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.PRODUCT_ALREADY_IN_WISHLIST);
        }

        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.WISHLIST_PRODUCT_ADDED_SUCCESSFULLY, response.wishlist)
    }
);

//Get all products from wishlist
router.get("/get-wishlist-product",
    authCheck,
    async (req, res) => {

        const response = await WishlistController.getAllWishlistProducts(req);

        if (response.unAuthorised) {

            return Response.error(res, ResponseCode.SUCCESS, ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if (response.wishlistIsEmpty) {
            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_PRODUCT_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.WISHLIST_PRODUCT_RETRIVED, response.wishlist)

    }

)

//Remove products from wishlist
router.delete("/remove-wishlist-product/:id",
    authCheck,
    [
        param('id').isMongoId().withMessage('Parameter [id] is missing or invalid')
    ]
    ,
    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await WishlistController.removeWishlistProduct(req);


        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if (response.productNotFound) {

            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_PRODUCT_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.WISHLIST_PRODUCT_REMOVED_SUCCESSFULLY)

    }

)

//Remove products from wishlist
router.post("/remove-multiple-wishlist-product",
    authCheck,
    [
        body('productIds').isArray().isLength({ min: 1}).withMessage('Plase give atleast one product id in array')
    ]
    ,
    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await WishlistController.removeMultipleWishlistProduct(req);


        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if (response.productNotFound) {

            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_PRODUCT_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS,response.wishlist+" "+ResponseMessage.WISHLIST_PRODUCT_REMOVED_SUCCESSFULLY)

    }

)

//Empty all products from wishlist
router.delete("/empty-wishlist",
    authCheck,
    async (req, res) => {

        const response = await WishlistController.emptyWishlist(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.SUCCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if (response.productNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_PRODUCT_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.EMPTY_WISHLIST)

    }

)

//Add service to wishlist
router.post(
    "/add-wishlist-service",
    authCheck,
    [
        body('service_id').notEmpty().withMessage('service_id is required')
    ],
    async (req, res) => {
        const error = validationResult(req);
        if (!error.isEmpty()) {
            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await WishlistController.addWishlistService(req);

        if (response.unAuthorised) {

            return Response.error(res, ResponseCode.SUCCESS, ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if (response.serviceAlreadyInWishlist) {
            return Response.error(res, ResponseCode.BAD_REQUEST, ResponseMessage.SERVICE_ALREADY_IN_WISHLIST);
        }

        if (response.serviceNotExists) {
            return Response.error(res, ResponseCode.NOT_FOUND, "Sorry this service is no longer exists");
        }


        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.WISHLIST_SERVICE_ADDED_SUCCESSFULLY, response.wishlist)
    }
);

//Get all services from wishlist
router.get("/get-wishlist-service",
    authCheck,
    async (req, res) => {

        const response = await WishlistController.getAllWishlistServices(req);

        if (response.unAuthorised) {

            return Response.error(res, ResponseCode.SUCCESS, ResponseMessage.ERROR_UNAUTHORIZED);

        }

        if (response.wishlistIsEmpty) {
            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_SERVICE_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.WISHLIST_SERVICE_RETRIVED, response.wishlist)

    }

)

//Remove services from wishlist
router.delete("/remove-wishlist-service/:service_id",
    authCheck,
    [
        param('service_id').isMongoId().withMessage('Parameter [service_id] is missing or invalid')
    ]
    ,
    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }


        const response = await WishlistController.removeWishlistService(req);


        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.SUCCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if (response.serviceNotFound) {

            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_SERVICE_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.WISHLIST_SERVICE_REMOVED_SUCCESSFULLY)

    }

)


//Remove products from wishlist
router.post("/remove-multiple-wishlist-service",
    authCheck,
    [
        body('serviceIds').isArray().isLength({ min: 1}).withMessage('Plase give atleast one service id in array')
    ]
    ,
    async (req, res) => {

        // Checking if req.body has any error
        const error = validationResult(req);

        if (!error.isEmpty()) {

            return Response.error(res, ResponseCode.UNPROCESSABLE_ENTITY, error.array().map((error) => ({
                field: error.param,
                errorMessage: error.msg
            })));
        }

        const response = await WishlistController.removeMultipleWishlistService(req);


        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.UNAUTHORIZED_ACCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if (response.serviceNotFound) {

            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_SERVICE_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS,response.wishlist+" "+ResponseMessage.WISHLIST_SERVICE_REMOVED_SUCCESSFULLY)

    }

)


//Empty all services from wishlist
router.delete("/empty-wishlist-services",
    authCheck,
    async (req, res) => {

        const response = await WishlistController.emptyWishlistService(req);

        if (response.unAuthorised) {
            return Response.error(res, ResponseCode.SUCCESS, ResponseMessage.ERROR_UNAUTHORIZED);
        }
        if (response.serviceNotFound) {
            return Response.error(res, ResponseCode.NOT_FOUND, ResponseMessage.WISHLIST_SERVICE_NOT_FOUND);

        }
        if (response.databaseError) {
            return Response.error(res, ResponseCode.DATABASE_ERROR, ResponseMessage.ERROR_DATABASE);
        } else if (response.wishlist) return Response.success(res, ResponseCode.SUCCESS, ResponseMessage.EMPTY_WISHLIST_SERVICE)

    }

)


module.exports = router;